import { ChartOptions } from "./charts.interface";
import { EChartOption } from "echarts";

export const cardChat = {
  responsive: true,
  tooltips: {
    enabled: false,
  },
  legend: {
    display: false,
  },
  scales: {
    yAxes: [
      {
        gridLines: {
          display: false,
          drawBorder: false,
        },
        ticks: {
          beginAtZero: true,
          display: false,
        },
      },
    ],
    xAxes: [
      {
        gridLines: {
          drawBorder: false,
          display: false,
        },
        ticks: {
          display: false,
        },
      },
    ],
  },
  title: {
    display: false,
  },
};
export const cardChartData = [
  {
    label: "New Patients",
    data: [50, 61, 80, 50, 72, 52, 60, 41, 30, 45, 70, 40, 93, 63, 50, 62],
    borderWidth: 4,
    pointStyle: "circle",
    pointRadius: 4,
    borderColor: "rgba(103,119,239,.7)",
    pointBackgroundColor: "rgba(103,119,239,.2)",
    backgroundColor: "rgba(103,119,239,.2)",
    pointBorderColor: "transparent",
  },
];
export const cardChartData1 = [
  {
    label: "New Patients",
    data: [50, 61, 80, 50, 40, 93, 63, 50, 62, 72, 52, 60, 41, 30, 45, 70],
    borderWidth: 4,
    pointStyle: "circle",
    pointRadius: 4,
    borderColor: "rgba(253,126,20,.7)",
    pointBackgroundColor: "rgba(253,126,20,.2)",
    backgroundColor: "rgba(253,126,20,.2)",
    pointBorderColor: "transparent",
  },
];
export const cardChartData2 = [
  {
    label: "New Patients",
    data: [52, 60, 41, 30, 45, 70, 50, 61, 80, 50, 72, 40, 93, 63, 50, 62],
    borderWidth: 4,
    pointStyle: "circle",
    pointRadius: 4,
    borderColor: "rgba(40,167,69,.7)",
    pointBackgroundColor: "rgba(40,167,69,.2)",
    backgroundColor: "rgba(40,167,69,.2)",
    pointBorderColor: "transparent",
  },
];
export const cardChartData3 = [
  {
    label: "New Patients",
    data: [30, 45, 70, 40, 93, 63, 50, 62, 50, 61, 80, 50, 72, 52, 60, 41],
    borderWidth: 4,
    pointStyle: "circle",
    pointRadius: 4,
    borderColor: "rgba(0,123,255,.7)",
    pointBackgroundColor: "rgba(0,123,255,.2)",
    backgroundColor: "rgba(0,123,255,.2)",
    pointBorderColor: "transparent",
  },
];
export const cardChartLabels = [
  "16-07-2018",
  "17-07-2018",
  "18-07-2018",
  "19-07-2018",
  "20-07-2018",
  "21-07-2018",
  "22-07-2018",
  "23-07-2018",
  "24-07-2018",
  "25-07-2018",
  "26-07-2018",
  "27-07-2018",
  "28-07-2018",
  "29-07-2018",
  "30-07-2018",
  "31-07-2018",
];
export const barChartApex: Partial<ChartOptions> = {
  series: [
    {
      name: "Mujer",
      data: [0, 0, 0, 0, 0, 0, 0, 0],
    },
    {
      name: "Hombre",
      data: [0, 0, 0, 0, 0, 0, 0, 0],
    },
  ],
  chart: {
    type: "bar",
    height: 350,
    foreColor: "#9aa0ac",
    toolbar: {
      show: false,
    },
  },
  plotOptions: {
    bar: {
      horizontal: false,
      columnWidth: "55%",
      borderRadius: 5,
    },
  },
  dataLabels: {
    enabled: false,
  },
  stroke: {
    show: true,
    width: 2,
    colors: ["transparent"],
  },
  xaxis: {
    categories: [
      "0-5 Años",
      "5-18 Años",
      "18-30 Años",
      "30-40 Años",
      "40-50 Años",
      "50-60 Años",
      "60-70 Años",
      "70-Más Años",
    ],
    labels: {
      style: {
        colors: "#9aa0ac",
      },
    },
  },
  yaxis: {
    seriesName: "Percentage",
    labels: {
      formatter: function (value) {
        return value + "%";
      },
    },
  },
  fill: {
    opacity: 1,
  },
  tooltip: {
    theme: "dark",
    marker: {
      show: true,
    },
    x: {
      show: true,
    },
    y: {
      formatter: function (val) {
        return val + "%";
      },
    },
  },
  colors: ["#00FF00", "#000"],
};
export const bubbleChartOptions: any = {
  responsive: true,
  legend: {
    display: true,
    labels: {
      fontColor: "#9aa0ac",
    },
  },
  scales: {
    xAxes: [
      {
        ticks: {
          min: 0,
          max: 12,
          fontFamily: "Poppins",
          fontColor: "#9aa0ac", // Font Color
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          min: 0,
          max: 100,
          fontFamily: "Poppins",
          fontColor: "#9aa0ac", // Font Color
        },
      },
    ],
  },
};
export const bubbleChartType = "bubble";
export const bubbleChartLegend = true;
export const bubbleChartData: Array<any> = [
  {
    data: [
      { x: 1, y: 45, r: 22.22 },
      { x: 2, y: 42, r: 33.0 },
      { x: 3, y: 60, r: 15.22 },
      { x: 4, y: 75, r: 21.5 },
      { x: 5, y: 30, r: 35.67 },
      { x: 6, y: 25, r: 25.67 },
      { x: 7, y: 10, r: 12.7 },
      { x: 8, y: 34, r: 12.7 },
      { x: 9, y: 54, r: 12.7 },
      { x: 10, y: 34, r: 12.7 },
      { x: 11, y: 23, r: 12.7 },
      { x: 12, y: 33, r: 12.7 },
    ],
    label: "(mes, cantidad, porcentaje) ",
  },
];
export const donut_chart: EChartOption = {
  tooltip: {
    trigger: "item",
    formatter: "{a} <br/>{b} : {c} ({d}%)",
  },
  legend: {
    data: ["Atendidas", "No atendidas", "Canceladas"],
    textStyle: {
      color: "#9aa0ac",
      padding: [5, 10],
    },
  },
  toolbox: {
    show: !0,
    feature: {
      magicType: {
        show: !0,
        type: ["pie", "funnel"],
        option: {
          funnel: {
            x: "25%",
            width: "50%",
            funnelAlign: "center",
            max: 1548,
          },
        },
      },
      restore: {
        show: !0,
        title: "Restore",
      },
      saveAsImage: {
        show: !0,
        title: "Save Image",
      },
    },
  },
  series: [
    {
      name: "Estatus de las citas",
      type: "pie",
      radius: ["35%", "55%"],
      itemStyle: {
        normal: {
          label: {
            show: !0,
          },
          labelLine: {
            show: !0,
          },
        },
        emphasis: {
          label: {
            show: !0,
            position: "center",
          },
        },
      },
      data: [
        {
          value: 335,
          name: "Atendidas",
        },
        {
          value: 310,
          name: "No atendidas",
        },
        {
          value: 234,
          name: "Canceladas",
        },
      ],
    },
  ],
  color: ["#72BE81", "#DFC126", "#DE725C"],
};

export const areaChartOptions: any = {
  chart: {
    height: 360,
    type: "area",
    foreColor: "#9aa0ac",
  },
  dataLabels: {
    enabled: false,
  },
  stroke: {
    curve: "smooth",
  },
  series: [
    {
      name: "Periodico",
      data: [109,34],
    },
    {
      name: "Redes Sociales",
      data: [52,23],
    },
    {
      name: "Motor de Busqueda",
      data: [63,47],
    },
    {
      name: "Recomendacion",
      data: [61,57],
    },
    {
      name: "Otro",
      data: [36,67],
    },
  ],

  xaxis: {
    type: "date",
    categories: [
      "Año pasado",
      "Año actual",
    ],
    labels: {
      style: {
        colors: "#9aa0ac",
      },
    },
  },
  yaxis: {
    labels: {
      style: {
        color: "#9aa0ac",
      },
    },
  },
  tooltip: {
    x: {
      format: "dd/MM/yy HH:mm",
    },
  },
};

export const radarOptions: any = {
  chart: {
    height: 350,
    type: "bar",
    stacked: true,
    toolbar: {
      show: true,
    },
    zoom: {
      enabled: true,
    },
    foreColor: "#9aa0ac",
  },
  responsive: [
    {
      breakpoint: 480,
      options: {
        legend: {
          position: "bottom",
          offsetX: -5,
          offsetY: 0,
        },
      },
    },
  ],
  plotOptions: {
    bar: {
      horizontal: false,
      // endingShape: 'rounded',
      columnWidth: "180%",
    },
  },
  series: [
    {
      name: "PRODUCT A",
      data: [44, 55, 41, 67, 22, 43],
    },
    {
      name: "PRODUCT B",
      data: [13, 23, 20, 8, 13, 27],
    },
    {
      name: "PRODUCT C",
      data: [11, 17, 15, 15, 21, 14],
    },
    {
      name: "PRODUCT D",
      data: [21, 7, 25, 13, 22, 8],
    },
  ],
  xaxis: {
    type: "datetime",
    categories: [
      "01/01/2011 GMT",
      "01/02/2011 GMT",
      "01/03/2011 GMT",
      "01/04/2011 GMT",
      "01/05/2011 GMT",
      "01/06/2011 GMT",
    ],
    labels: {
      style: {
        colors: "#9aa0ac",
      },
    },
  },
  yaxis: {
    labels: {
      style: {
        color: "#9aa0ac",
      },
    },
  },
  legend: {
    position: "bottom",
    offsetY: 0,
  },
  fill: {
    opacity: 1,
  },
};

export const colorScheme: any = {
  domain: ["#9370DB", "#87CEFA", "#FA8072", "#FF7F50", "#90EE90", "#9370DB"],
};

export const colorScheme2: any = {
  domain: ["#9370DB", "#87CEFA", "#FA8072", "#FF7F50", "#90EE90"],
};

export const single: any = [
  {
    name: "Ene-Feb",
    value: 1,
  },
  {
    name: "Mar-Abr",
    value: 1,
  },
  {
    name: "May-Jun",
    value: 1,
  },
  {
    name: "Jul-Ago",
    value: 1,
  },
  {
    name: "Sep-Oct",
    value: 1,
  },
  {
    name: "Nov-Dic",
    value: 1,
  },
];

export const single2: any = [
  {
    name: "Periodico",
    value: 1,
  },
  {
    name: "Redes Sociales",
    value: 1,
  },
  {
    name: "Motor de Busqueda",
    value: 1,
  },
  {
    name: "Recomendacion",
    value: 1,
  },
  {
    name: "Otro",
    value: 1,
  },
];
