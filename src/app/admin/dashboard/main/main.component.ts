import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { ChartOptions } from "./charts.interface";
import { EChartOption } from "echarts";
import {
  single,
  single2,
  cardChat,
  colorScheme,
  donut_chart,
  radarOptions,
  barChartApex,
  colorScheme2,
  cardChartData,
  cardChartData3,
  cardChartData2,
  cardChartData1,
  bubbleChartData,
  bubbleChartType,
  cardChartLabels,
  areaChartOptions,
  bubbleChartLegend,
  bubbleChartOptions,
} from "./chart.config";
import { HttpService } from "../../../core/service/http.service";
import { GraficasService } from "../../../core/service/graficas.service";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: [],
})
export class MainComponent implements OnInit {
  @ViewChild("modalpago", { static: true }) modalpago: TemplateRef<any>;
  mensaje;
  visitas = [];
  doctores = [];
  pacientes = [];
  guageThick = 8;
  gaugeSize = 170;
  gaugeValue = 28.3;
  gananciasMes = "0";
  guageType1 = "full";
  guageType2 = "semi";
  guageType3 = "arch";
  pacientesNuevos = [];
  public single = single;
  public single2 = single2;
  public cardChart = cardChat;
  anio = new Date().getFullYear();
  public colorScheme = colorScheme;
  public colorScheme2 = colorScheme2;
  public cardChartData = cardChartData;
  public cardChartData1 = cardChartData1;
  public cardChartData2 = cardChartData2;
  public cardChartData3 = cardChartData3;
  public cardChartLabel = cardChartLabels;
  public bubbleChartData = bubbleChartData;
  public bubbleChartType = bubbleChartType;
  public bubbleChartLegend = bubbleChartLegend;
  public radarChartOptions: any = radarOptions;
  public bubbleChartOptions = bubbleChartOptions;
  public donut_chart: EChartOption = donut_chart;
  public areaChartOptions: any = areaChartOptions;
  public edadesOptions: Partial<ChartOptions> = barChartApex;

  constructor(
    private http: HttpService,
    private modalService: NgbModal,
    private graficasSvc: GraficasService
  ) {}

  ngOnInit() {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.getNuevosPacientes(idclinica);
    this.getTotalPacientes(idclinica);
    this.getTotalDoctores(idclinica);
    this.getIngresosMes(idclinica);
    this.getCitasAnio(idclinica);
    // this.getPayment(id);
  }

  getTotalPacientes(idClinica) {
    this.http.post("pacientes.php", { caso: 0, idClinica }).subscribe({
      next: (pacientes: any) => {
        this.pacientes = pacientes;
        this.llenarGraficas();
      },
      error: (err) => console.error(err),
    });
  }

  getTotalDoctores(idC) {
    this.http.post("usuarios.php", { caso: 18, idC }).subscribe({
      next: (doctores: any) => {
        this.doctores = doctores;
      },
      error: (err) => console.error(err),
    });
  }

  getNuevosPacientes(idClinica) {
    const fecha = `${new Date().getFullYear()}-01-01`;
    const fecha2 = `${new Date().getFullYear()}-12-30`;
    this.http
      .post("graficas.php", { caso: 0, idClinica, fecha, fecha2 })
      .subscribe({
        next: (pacientes: any) => {
          this.pacientesNuevos = pacientes;
        },
        error: (err) => console.error(err),
      });
  }

  getIngresosMes(idClinica) {
    const fecha = this.formatDate(new Date(), true);
    this.http.post("graficas.php", { caso: 1, idClinica, fecha }).subscribe({
      next: (ingresos: any) => {
        if (ingresos !== null) {
          this.gananciasMes = ingresos[0].Total;
        }
      },
      error: (err) => console.error(err),
    });
  }

  getCitasAnio(idClinica: any) {
    const fecha = `${new Date().getFullYear()}-01-01`;
    const fecha2 = `${new Date().getFullYear()}-12-30`;
    this.http
      .post("graficas.php", { caso: 2, idClinica, fecha, fecha2 })
      .subscribe({
        next: (citas: any) => {
          const { atendidas, noAtendidas, canceladas } =
            this.graficasSvc.citasByEstatus(citas);
          this.donut_chart.series[0].data[0].value = atendidas;
          this.donut_chart.series[0].data[1].value = noAtendidas;
          this.donut_chart.series[0].data[2].value = canceladas;
        },
        error: (err) => console.error(err),
      });
  }

  llenarGraficas() {
    const { mujeresArr, hombresArr } = this.graficasSvc.patientByAge(
      this.pacientes
    );
    this.edadesOptions.series = [
      { name: "Mujer", data: mujeresArr },
      { name: "Hombre", data: hombresArr },
    ];
    const { montsArr } = this.graficasSvc.patientsByCreatedAt(this.pacientes);
    this.single = montsArr;
    const { EncontrasteArr } = this.graficasSvc.patientsByEncontraste(
      this.pacientes
    );
    this.single2 = EncontrasteArr;
  }

  getPayment(idUser) {
    this.http
      .post("usuario.php", { caso: 2, idUser })
      .subscribe(([{ statusSubscripcion }]: any) => {
        if (statusSubscripcion === "pendiente") {
          this.openModal("pendiente", this.modalpago);
        } else if (statusSubscripcion === "no pagado") {
          this.openModal("no pagado", this.modalpago);
        } else if (statusSubscripcion === "suscripcion cancelada") {
          this.openModal("suscripcion cancelada", this.modalpago);
        } else if (statusSubscripcion === "cobro expirado") {
          this.openModal("cobro expirado", this.modalpago);
        }
      });
  }

  openModal(mensaje, content) {
    this.mensaje = mensaje;
    let ngbModalOptions: NgbModalOptions = {
      centered: true,
      ariaLabelledBy: "modal-basic-title",
    };
    this.modalService.open(content, ngbModalOptions);
  }

  formatDate(fecha: Date, menosMes = false): string {
    const day = fecha.getDate();
    let month = fecha.getMonth() + 1;
    const year = fecha.getFullYear();
    if (menosMes === true) {
      month = month - 1;
    }
    if (month < 10) {
      return `${year}-0${month}-${day}`;
    } else {
      return `${year}-${month}-${day}`;
    }
  }
}
