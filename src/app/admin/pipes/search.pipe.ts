import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultUsuario = [];
    for ( const user of value){
      if (user.Correo.toLowerCase().indexOf(arg.toLowerCase()) > - 1) {
        resultUsuario.push(user);
      }
    }
    return resultUsuario;
  }

}
