import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AlertService } from "../../core/service/alert.service";
import { HttpService } from "../../core/service/http.service";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { AuthService } from "../../core/service/auth.service";

@Component({
  selector: "app-perfil",
  templateUrl: "./perfil.component.html",
  styles: [],
})
export class PerfilComponent implements OnInit {
  @ViewChild("placesRef")
  placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  user: any = [];
  userForm: FormGroup;
  passwordForm: FormGroup;
  facturarForm: FormGroup;
  hide = false;
  hide2 = false;
  isPass = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
  img = new FormData();
  nombreImg = "";
  historialFacturas: any = [];
  datosUser;

  constructor(
    private http: HttpService,
    private fb: FormBuilder,
    private sweet: AlertService,
    private authSvc: AuthService
  ) {}

  ngOnInit(): void {
    this.getUSerInfo();
    this.passwordForm = this.fb.group({
      password: ["", [Validators.required, Validators.pattern(this.isPass)]],
      cpassword: ["", [Validators.required, Validators.minLength(8)]],
    });
    this.userForm = this.fb.group({
      nombre: ["", Validators.required],
      apellido: ["", Validators.required],
      Direccion: ["", Validators.required],
      Correo: ["", Validators.required],
      img: [""],
    });
    this.facturarForm = this.fb.group({
      uid: [""],
      nombre: ["", [Validators.required]],
      apellido: ["", [Validators.required]],
      email: [
        "",
        [
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ],
      ],
      email2: [""],
      email3: [""],
      telefono: [
        "",
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
        ],
      ],
      razons: ["", [Validators.required]],
      rfc: [
        "",
        [
          Validators.required,
          Validators.minLength(12),
          Validators.maxLength(13),
        ],
      ],
      calle: ["", [Validators.required]],
      numero_exterior: ["", [Validators.required]],
      numero_interior: ["", [Validators.required]],
      codpos: ["", [Validators.required]],
      colonia: ["", [Validators.required]],
      estado: ["", [Validators.required]],
      ciudad: ["", [Validators.required]],
    });
    this.datosUser = JSON.parse(localStorage.getItem("datosusuarioclinica"));
  }

  public handleAddressChange2(address: Address): void {
    let numero_exterior = "";
    let calle = "";
    let colonia = "";
    let ciudad = "";
    let estado = "";
    let codpos = "";
    // let pais = "";
    for (let i = 0; i < address.address_components.length; i++) {
      const element = address.address_components[i].types[0];
      if (element === "street_number") {
        numero_exterior = address.address_components[i].long_name;
      }
      if (element === "route") {
        calle = address.address_components[i].long_name;
      }
      if (element === "sublocality_level_1") {
        colonia = address.address_components[i].long_name;
      }
      if (element === "locality") {
        ciudad = address.address_components[i].long_name;
      }
      if (element === "administrative_area_level_1") {
        estado = address.address_components[i].long_name;
      }
      if (element === "postal_code") {
        codpos = address.address_components[i].long_name;
      }
      // if (element === "country") {
      //   pais = address.address_components[i].long_name;
      // }
    }
    this.facturarForm.patchValue({
      calle,
      numero_exterior,
      codpos,
      colonia,
      estado,
      ciudad,
    });
    this.facturarForm.controls["calle"].disable();
    this.facturarForm.controls["numero_exterior"].disable();
    this.facturarForm.controls["codpos"].disable();
    this.facturarForm.controls["colonia"].disable();
    this.facturarForm.controls["estado"].disable();
    this.facturarForm.controls["ciudad"].disable();
  }

  public handleAddressChange(address: Address) {
    this.userForm.patchValue({
      Direccion: address.formatted_address,
    });
  }

  getUSerInfo() {
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.http.post("usuario.php", { caso: 2, idUser: id }).subscribe({
      next: (user: any) => {
        if (user !== null) {
          this.user = user[0];
          this.authSvc.imgUser$.emit(this.user.Imagen);
          this.authSvc.nameUser$.emit(
            `${this.user.NombreUsuario} ${this.user.Apellidos}`
          );
          const newData = {
            ...this.datosUser,
            firstName: user[0].NombreUsuario,
            lastName: user[0].Apellidos,
            img: user[0].Imagen,
          };
          localStorage.setItem("datosusuarioclinica", JSON.stringify(newData));
          this.userForm.patchValue({
            nombre: this.user.NombreUsuario,
            apellido: this.user.Apellidos,
            Direccion: this.user.Direccion,
            Correo: this.user.Correo,
          });
          this.userForm.disable();
          this.facturarForm.patchValue({
            uid: user[0].uid,
            nombre: user[0].NombreUsuario,
            apellido: user[0].Apellidos,
            email: user[0].Correo,
            telefono: user[0].Celular,
            razons: user[0].razons,
            rfc: user[0].RFC,
            calle: user[0].calle,
            numero_exterior: user[0].numero_exterior,
            numero_interior: user[0].numero_interior,
            codpos: user[0].codpos,
            colonia: user[0].colonia,
            estado: user[0].estado,
            ciudad: user[0].ciudad,
          });
        }
      },
      error: (e) => console.log(e),
    });
  }

  clienteFactura(cliente) {
    if (!this.facturarForm.valid) {
      this.sweet.alert("", "Debe llenar todos los campos", "error");
      return;
    }
    if (cliente.uid !== "") {
      this.updateClienteFactura(cliente);
    } else {
      this.http.post("crearCliente.php", cliente).subscribe((res: any) => {
        if (res.status === "success") {
          this.clienteFacturaBD(res.Data);
        } else {
          this.sweet.alert(
            "",
            "Ocurrió un error intentelo de nuevo 1",
            "error"
          );
        }
      });
    }
  }

  updateClienteFactura(cliente) {
    this.http.post("actualizarCliente.php", cliente).subscribe((res: any) => {
      if (res.status === "success") {
        this.clienteFacturaBD(res.Data);
      } else {
        this.sweet.alert("", "Ocurrió un error intentelo de nuevo 2", "error");
      }
    });
  }

  clienteFacturaBD(user) {
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.http
      .post("usuario.php", { caso: 8, user, idUser: id })
      .subscribe((res: any) => {
        if (res === "Se modifico") {
          this.sweet.alert(
            "",
            "Se registrarón los datos a facturar",
            "success"
          );
          this.getUSerInfo();
        } else {
          this.sweet.alert(
            "",
            "Ocurrió un error intentelo de nuevo 3",
            "error"
          );
        }
      });
  }

  generarFactura(cliente) {
    if (!this.facturarForm.valid) {
      this.sweet.alert("", "Debe llenar todos los campos", "error");
      return;
    }
    this.http.post("crearCFDI.php", cliente).subscribe((res: any) => {
      if (res.response === "success") {
        this.saveFacturaBD(res);
      } else {
        this.sweet.alert("", "Ocurrió un error intentelo de nuevo", "error");
      }
    });
  }

  saveFacturaBD(datosPaciente) {
    const idPaciente = JSON.parse(localStorage.getItem("perfilusuario"));
    this.http
      .post("usuario.php", { caso: 9, datosPaciente, idPaciente })
      .subscribe((res: any) => {
        if (res === "Se modifico") {
          this.sweet.alert("", "Se registró el folio de su factura", "success");
        } else {
          this.sweet.alert("", "Ocurrió un error intentelo de nuevo", "error");
        }
      });
  }

  getHistorialFacturas() {
    const idPaciente = JSON.parse(localStorage.getItem("perfilusuario"));
    this.http
      .post("usuario.php", { caso: 10, idPaciente })
      .subscribe((res: any) => {
        if (res !== null && res !== null && res !== "") {
          this.historialFacturas = res;
        }
      });
  }

  downloadFactura(factura: any) {
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    let raw = JSON.stringify({
      idFactura: factura.idFactura,
      fecha_update: factura.fecha_update,
    });
    let requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
    };
    fetch("https://projectsbyanimatiomx.com/phps/odonto/downloadPDF.php", requestOptions)
      .then((response: any) => response.blob())
      .then((myBlob) => {
        let objectURL = URL.createObjectURL(myBlob);
        window.open(objectURL);
      })
      .catch((error) => console.log("error", error));
  }

  updatePassword(user) {
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    if (user.password === user.cpassword) {
      this.http
        .post("usuario.php", { caso: 3, idUser: id, user })
        .subscribe((res: any) => {
          if (res === "Se actualizo") {
            this.sweet.alert(
              "",
              "Se cambio su contraseña correctamente",
              "success"
            );
          } else {
            this.sweet.alert(
              "",
              "Ocurrio un error intentelo de nuevo",
              "error"
            );
          }
        });
    } else {
      this.sweet.alert("", "Las contraseñas deben coincidir", "error");
    }
  }

  cancelSuscription() {
    if (this.user.metodoPago === "stripe") {
      const res = this.user.idsubscript.replace(/['"]+/g, "");
      this.http
        .post("stripe/cancelarSuscripcion.php", {
          idSubs: `${res}`,
        })
        .subscribe((res: any) => {
          if (res.cancel_at_period_end === true) {
            this.saveDataCancel(res);
          } else {
            this.sweet.alert(
              "Error",
              "Ocurrió un error inesperado, intentelo nuevamente, si persiste el error cominiquese con el admin. del sistema",
              "error"
            );
          }
        });
    } else if (this.user.metodoPago === "paypal") {
      this.http.post("paypal/getAccesToken.php", {}).subscribe((res: any) => {
        this.paypalCancelSuscription(this.user.idsubscript, res.access_token);
      });
    }
  }

  paypalCancelSuscription(idSubs, token) {
    this.http
      .post("paypal/cancelSubscription.php", { idSubs, token })
      .subscribe((res: any) => {
        console.log(res);
      });
  }

  saveDataCancel(objCancel) {
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.http
      .post("usuario.php", { caso: 4, objCancel, idUser: id })
      .subscribe((res: any) => {
        if (res === "Se actualizo") {
          this.sweet.alert(
            "Cancelada",
            "Se canceló la suscripción correctamente, se le permitirá el acceso hasta finalizar el periodo actual",
            "success"
          );
          this.ngOnInit();
        } else {
          this.sweet.alert(
            "Error",
            "Ocurrió un error inesperado, intentelo nuevamente, si persiste el error cominiquese con el admin. del sistema",
            "error"
          );
        }
      });
  }

  changeView() {
    this.hide = this.hide ? false : true;
  }
  changeView2() {
    this.hide2 = this.hide2 ? false : true;
  }

  onInputClick(event) {
    event.target.value = "";
  }

  cargarImagen(img) {
    this.nombreImg = `${new Date().getTime()}${this.clearText(img.name)}`;
    this.img.append("imagenPropia", img, this.nombreImg);
  }

  clearText = (str) => {
    const texto = str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    const textofinal = texto.replace(/[^a-zA-Z0-9]/g, "");
    return textofinal;
  };

  activarFormulario() {
    this.userForm.enable();
  }

  preUpdate(user: any) {
    if (this.nombreImg === "") {
      this.sweet.subiendo("su información");
      this.actualizarUsuario(user);
    } else {
      user.img = this.nombreImg;
      this.subirImg(user);
    }
  }

  actualizarUsuario(user: any) {
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.http
      .post("usuario.php", { caso: 5, user, idUser: id })
      .subscribe((res: any) => {
        if (res === "Se actualizo") {
          this.sweet.alertClose();
          this.sweet.alert(
            "",
            "Se actualizó su información correctamente",
            "success"
          );
          this.getUSerInfo();
        } else {
          this.sweet.alertClose();
          this.sweet.alert(
            "Error!",
            "Ocurrio un error, intentelo nuevamente",
            "error"
          );
        }
      });
  }

  subirImg(user: any) {
    this.sweet.subiendo("su información");
    this.http.postFile("uploadfoto.php", this.img).subscribe((res: any) => {
      if (res.msj === "Imagen subida") {
        this.actualizarUsuario(user);
      } else {
        this.sweet.alertClose();
        this.sweet.alert(
          "Error!",
          "Ocurrio un error, intentelo nuevamente",
          "error"
        );
      }
    });
  }

  firstLetterUppercase(id) {
    if (id) {
      let texto = (<HTMLInputElement>document.getElementById(id)).value;
      if (texto !== "") {
        let textofinal = texto[0].toUpperCase() + texto.slice(1);
        (<HTMLInputElement>document.getElementById(id)).value = textofinal;
      }
    }
  }

  firstWordUpercase(id) {
    let texto = (<HTMLInputElement>document.getElementById(id)).value;
    let cadena = texto.toLowerCase().split(" ");
    for (let i = 0; i < cadena.length; i++) {
      cadena[i] = cadena[i].charAt(0).toUpperCase() + cadena[i].substring(1);
    }
    texto = cadena.join(" ");
    (<HTMLInputElement>document.getElementById(id)).value = texto;
  }
}
