import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AdminRoutingModule } from "./admin-routing.module";
import { MatTabsModule } from "@angular/material/tabs";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialFileInputModule } from "ngx-material-file-input";
import { MatPasswordStrengthModule } from "@angular-material-extensions/password-strength";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

import { PerfilComponent } from "./perfil/perfil.component";

@NgModule({
  declarations: [PerfilComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatPasswordStrengthModule.forRoot(),
    MatSlideToggleModule,
    MaterialFileInputModule,
    GooglePlaceModule,
  ],
})
export class AdminModule {}
