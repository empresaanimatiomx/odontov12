import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  TemplateRef,
} from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { AuthService } from "../../../core/service/auth.service";
import { Router } from "@angular/router";
import Swal from "sweetalert2";
import * as xlsx from "xlsx";
import { AlertService } from "src/app/core/service/alert.service";
@Component({
  selector: "app-listado-pacientes",
  templateUrl: "./listado-pacientes.component.html",
  styleUrls: [],
})
export class ListadoPacientesComponent implements OnInit {
  @ViewChild("epltable", { static: false }) epltable: ElementRef;
  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  @ViewChild("roleTemplate", { static: true }) roleTemplate: TemplateRef<any>;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  rows = [];
  imagenclinicaPrint = "";
  data: any = [];
  filteredData: any = [];
  selectedOption: string;
  columns = [
    { name: "Nombres" },
    { name: "Apellidos" },
    { name: "Celular" },
    { name: "Genero" },
    { name: "Doctor" },
  ];
  idClinica: number;

  constructor(
    private sweet: AlertService,
    private httpClient: HttpClient,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.imagenclinicaPrint =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
    this.idClinica = this.authService.currentUserValue.idclinica;
  }
  ngOnInit() {
    if (this.idClinica > 0) {
      this.obtenerlistapacientes();
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraclinica());
    }
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet = xlsx.utils.table_to_sheet(
      this.epltable.nativeElement
    );
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    xlsx.writeFile(wb, "ListadoPacientes.xlsx");
  }

  iraclinica() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  obtenerlistapacientes(refresh?) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 0, idClinica: this.idClinica };
    const URL: any = this.baseURL + "pacientes.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        this.data = respuesta;
        this.filteredData = respuesta;
        refresh && this.sweet.toast("los pacientes");
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }

  cambiarestatuspaciente(row) {
    var mensaje;
    var mensaje1;
    var mensaje2;
    var color;
    var status;
    var idpaciente = row.idPacientes;
    var estatus = row.Activo;
    if (estatus === "1") {
      mensaje = "¿Estas seguro de deshabilitar al paciente?";
      mensaje1 = "Paciente deshabilitado correctamente";
      mensaje2 = "DESHABILITAR PACIENTE";
      color = "#f44336";
      status = "0";
    } else {
      mensaje = "¿Estas seguro de habilitar al paciente?";
      mensaje1 = "Paciente habilitado correctamente";
      mensaje2 = "HABILITAR PACIENTE";
      color = "#28a745";
      status = "1";
    }
    Swal.fire({
      title: mensaje,
      showCancelButton: true,
      confirmButtonText: mensaje2,
      cancelButtonText: "Cancelar",
      confirmButtonColor: color,
      cancelButtonColor: "#007bff",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        this.cambiarestatus(Number(idpaciente), status).subscribe(
          (respuesta) => {
            const res = respuesta;
            if (res.toString() === "Se modifico") {
              this.obtenerlistapacientes();
              this.swalalertmensaje("success", mensaje1);
            } else {
              this.swalalertmensaje(
                "error",
                "Ocurrio un error, intente de nuevo por favor"
              );
            }
          }
        );
      }
    });
  }

  cambiarestatus(idP, estatus) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 11, idUsuario: idP, estatuspaciente: estatus };
    const URL: any = this.baseURL + "pacientes.php";
    return this.httpClient.post(URL, JSON.stringify(options), headers);
  }

  keyPress(e: any) {
    const key = e.keycode || e.which;
    const teclado = String.fromCharCode(key).toLowerCase();
    const letras = " abcdefghijklmnñopqrstuvwxyzáéíóú";
    if (letras.indexOf(teclado) == -1) {
      return false;
    }
  }

  iravista(idpaciente, apartadodepagina) {
    localStorage.setItem("perfilusuario", JSON.stringify(idpaciente));
    localStorage.setItem("tabs", JSON.stringify(apartadodepagina));
    console.log(apartadodepagina);
    this.router.navigate(["/admin/paciente/detalle-paciente"]);
  }

  filterDatatable(event) {
    const val = event.target.value.toLowerCase();
    const colsAmt = this.columns.length;
    const keys = Object.keys(this.filteredData[0]);
    this.data = this.filteredData.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, "", {
      duration: 2000,
      verticalPosition: "bottom",
      horizontalPosition: "right",
      panelClass: ["bg-red"],
    });
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, "", {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  swalalertmensaje(icon, mensaje) {
    Swal.fire({
      position: "center",
      icon: icon,
      title: mensaje,
      showConfirmButton: true,
      allowOutsideClick: false,
      confirmButtonText: "Aceptar",
    });
  }
}
