/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { AlertService } from "src/app/core/service/alert.service";
import { AuthService } from "../../../core/service/auth.service";
import { HttpService } from "../../../core/service/http.service";

@Component({
  selector: "app-listado-citas",
  templateUrl: "./listado-citas.component.html",
  styles: [],
})
export class ListadoCitasComponent implements OnInit {
  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";

  porConfirmar: any = [];
  confirmadas: any = [];
  horaCancelada: any = [];
  finalizada: any = [];
  porConfirmarFilter: any = [];
  confirmadasFilter: any = [];
  horaCanceladaFilter: any = [];
  finalizadaFilter: any = [];

  columns = [
    { name: "Nombres" },
    { name: "Apellidos" },
    { name: "Celular" },
    { name: "Genero" },
    { name: "Doctor" },
  ];
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  rows = [];
  selectedOption: string;
  idClinica;
  idCita;
  infocita;
  NuevaCitaForm: FormGroup;
  pacientes: any = [];
  doctores: any = [];
  servicios: any = [];
  motivosconsulta: any = [];
  historialcita: any = [];
  cita;

  constructor(
    private sweet: AlertService,
    private http: HttpService,
    private authService: AuthService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.NuevaCitaForm = this.fb.group({
      paciente: ["", [Validators.required]],
      doctor: ["", [Validators.required]],
      fecha: ["", [Validators.required]],
      horario: ["", [Validators.required]],
      motivo: ["", [Validators.required]],
      servicio: ["", [Validators.required]],
      estatus: ["", [Validators.required]],
      observaciones: [""],
    });
    this.NuevaCitaForm.get("paciente").disable();
    this.NuevaCitaForm.get("doctor").disable();
    this.NuevaCitaForm.get("servicio").disable();
    this.NuevaCitaForm.get("motivo").disable();
  }

  ngOnInit(): void {
    this.getAllCitas();
    this.GetPacientes(this.idClinica);
    this.GetDoctores(this.idClinica);
    this.GetServicios(this.idClinica);
    this.obtenermotivos(this.idClinica);
  }

  getAllCitas(refresh?) {
    this.http
      .post("citas.php", { caso: 20, idC: this.idClinica })
      .subscribe((respuesta: any) => {
        if (respuesta != null) {
          for (let i = 0; i < respuesta.length; i++) {
            const status = respuesta[i].EstatusCita;
            if (status === "Por confirmar") {
              this.porConfirmarFilter.push(respuesta[i]);
            }
            if (status === "Confirmado") {
              this.confirmadasFilter.push(respuesta[i]);
            }
            if (status === "Hora Cancelada") {
              this.horaCanceladaFilter.push(respuesta[i]);
            }
            if (status === "Finalizada") {
              this.finalizadaFilter.push(respuesta[i]);
            }
          }
          this.porConfirmar = this.porConfirmarFilter;
          this.confirmadas = this.confirmadasFilter;
          this.horaCancelada = this.horaCanceladaFilter;
          this.finalizada = this.finalizadaFilter;
          refresh && this.sweet.toast("las citas");
        }
      });
  }

  //obtiene el listado de pacientes
  GetPacientes(idclinica) {
    const options: any = { caso: 10, idC: idclinica };
    this.http.post(`citas.php`, options).subscribe((respuesta) => {
      this.pacientes = respuesta;
    });
  }

  //obtiene el listado de doctores
  GetDoctores(idclinica) {
    const options: any = { caso: 5, idClinica: idclinica };
    this.http.post(`pacientes.php`, options).subscribe((respuesta) => {
      this.doctores = respuesta;
    });
  }

  //obtiene el listado de servicios
  GetServicios(idclinica) {
    const options: any = { caso: 2, idC: idclinica };
    this.http.post(`citas.php`, options).subscribe((respuesta) => {
      this.servicios = respuesta;
    });
  }

  // metodo para obtener los convenios
  obtenermotivos(idclinica) {
    const options: any = { caso: 11, idC: idclinica };
    this.http.post(`citas.php`, options).subscribe((respuesta: any) => {
      this.motivosconsulta = respuesta;
    });
  }

  modalHistory(content, cita: any) {
    this.idCita = cita.idCitas;
    this.cita = cita;
    const options: any = { caso: 19, idc: this.idCita };
    this.http.post(`citas.php`, options).subscribe((respuesta) => {
      this.historialcita = respuesta;
      let ngbModalOptions: NgbModalOptions = {
        backdrop: "static",
        keyboard: false,
        centered: true,
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
      };
      this.modalService.open(content, ngbModalOptions);
    });
  }

  modalDetalle(cita: any, content) {
    this.idCita = cita.idCitas;
    const options: any = { caso: 13, idc: this.idCita };
    this.http.post(`citas.php`, options).subscribe((respuesta) => {
      this.infocita = respuesta;
      let ngbModalOptions: NgbModalOptions = {
        backdrop: "static",
        keyboard: false,
        centered: true,
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
      };
      this.modalService.open(content, ngbModalOptions);
      this.NuevaCitaForm.setValue({
        paciente: this.infocita[0].idPacientes,
        doctor: this.infocita[0].idDoctores,
        fecha: this.infocita[0].Fecha,
        horario: this.infocita[0].Horario,
        motivo: this.infocita[0].Motivos,
        servicio: this.infocita[0].idServicios,
        estatus: this.infocita[0].EstatusCita,
        observaciones: this.infocita[0].Observaciones,
      });
    });
  }

  filterDatatable(event) {
    const val = event.target.value.toLowerCase();
    const colsAmt = this.columns.length;
    const keys = Object.keys(this.porConfirmarFilter[0]);
    this.porConfirmar = this.porConfirmarFilter.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }
}
