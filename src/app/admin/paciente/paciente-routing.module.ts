import { Page404Component } from './../../authentication/page404/page404.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NuevoPacienteComponent } from './nuevo-paciente/nuevo-paciente.component';
import { ListadoPacientesComponent } from './listado-pacientes/listado-pacientes.component';
import { PerfilPacienteComponent } from './perfil-paciente/perfil-paciente.component';
import { DetallePacienteComponent } from './perfil-paciente/detalle-paciente/detalle-paciente.component';
import { ListadoCitasComponent } from './listado-citas/listado-citas.component';

const routes: Routes = [
  {
    path: 'nuevo-paciente',
    component: NuevoPacienteComponent,
  },
  {
    path: 'listado-pacinte',
    component: ListadoPacientesComponent,
  },
  {
    path: 'listado-citas',
    component: ListadoCitasComponent,
  },
  {
    path: 'listado-perfil-paciente',
    component: PerfilPacienteComponent,
  },
  {
    path: 'detalle-paciente',
    component: DetallePacienteComponent,
  },

  { path: '**', component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PacienteRoutingModule {}