import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MatMenuModule } from "@angular/material/menu";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSortModule } from "@angular/material/sort";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MaterialFileInputModule } from "ngx-material-file-input";
import { MatTabsModule } from "@angular/material/tabs";
import { MatRadioModule } from "@angular/material/radio";
import { NgImageFullscreenViewModule } from "ng-image-fullscreen-view";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatStepperModule } from "@angular/material/stepper";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

import { NuevoPacienteComponent } from "./nuevo-paciente/nuevo-paciente.component";
import { ListadoPacientesComponent } from "./listado-pacientes/listado-pacientes.component";
import { PerfilPacienteComponent } from "./perfil-paciente/perfil-paciente.component";
import { PacienteRoutingModule } from "./paciente-routing.module";
import { DetallePacienteComponent } from "./perfil-paciente/detalle-paciente/detalle-paciente.component";
import { RecetasComponent } from "./perfil-paciente/recetas/recetas.component";
import { EvolucionesComponent } from "./perfil-paciente/evoluciones/evoluciones.component";
import { DatosPersonalesComponent } from "./perfil-paciente/datos-personales/datos-personales.component";
import { HistorialClinicoComponent } from "./perfil-paciente/historial-clinico/historial-clinico.component";
import { ExamenOdontoComponent } from "./perfil-paciente/examen-odonto/examen-odonto.component";
import { PlanTratamientoComponent } from "./perfil-paciente/plan-tratamiento/plan-tratamiento.component";
import { PresupuestoComponent } from "./perfil-paciente/presupuesto/presupuesto.component";
import { DocumentosComponent } from "./perfil-paciente/documentos/documentos.component";
import { DiagnosticoComponent } from "./perfil-paciente/diagnostico/diagnostico.component";
import { FotosComponent } from "./perfil-paciente/fotos/fotos.component";
import { NuevaRecetaComponent } from "./perfil-paciente/documentos/nueva-receta/nueva-receta.component";
import { ListadoRecetasComponent } from "./perfil-paciente/documentos/listado-recetas/listado-recetas.component";
import { RadiologiaComponent } from "./perfil-paciente/documentos/radiologia/radiologia.component";
import { DocumentacionComponent } from "./perfil-paciente/documentacion/documentacion.component";
import { CapitalizeFirstDirective } from "src/app/shared/capitalize-first.directive";
import { ListadoCitasComponent } from './listado-citas/listado-citas.component';
import { FacturacionComponent } from './perfil-paciente/facturacion/facturacion.component';
import { FacturasComponent } from './perfil-paciente/facturas/facturas.component';
import { NgApexchartsModule } from "ng-apexcharts";
import { LaboratorioComponent } from './perfil-paciente/documentos/laboratorio/laboratorio.component';
import { AutocompleteLibModule } from "angular-ng-autocomplete";


@NgModule({
  declarations: [
    NuevoPacienteComponent,
    ListadoPacientesComponent,
    PerfilPacienteComponent,
    DetallePacienteComponent,
    RecetasComponent,
    EvolucionesComponent,
    DatosPersonalesComponent,
    HistorialClinicoComponent,
    ExamenOdontoComponent,
    PlanTratamientoComponent,
    PresupuestoComponent,
    DocumentosComponent,
    DiagnosticoComponent,
    FotosComponent,
    NuevaRecetaComponent,
    ListadoRecetasComponent,
    RadiologiaComponent,
    DocumentacionComponent,
    CapitalizeFirstDirective,
    ListadoCitasComponent,
    FacturacionComponent,
    FacturasComponent,
    LaboratorioComponent,
  ],
  imports: [
    CommonModule,
    PacienteRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatSortModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatSelectModule,
    MatMenuModule,
    MatCheckboxModule,
    MaterialFileInputModule,
    MatProgressSpinnerModule,
    NgxDatatableModule,
    MatTabsModule,
    MatRadioModule,
    NgImageFullscreenViewModule,
    PerfectScrollbarModule,
    MatExpansionModule,
    MatStepperModule,
    NgxChartsModule,
    GooglePlaceModule,
    MatSlideToggleModule,
    NgApexchartsModule,
    AutocompleteLibModule,
  ],
  providers: [],
})
export class PacienteModule {}
