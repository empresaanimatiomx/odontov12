import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { AlertService } from "../../../core/service/alert.service";
import { AuthService } from "../../../core/service/auth.service";
import { HttpService } from "../../../core/service/http.service";

@Component({
  selector: "app-nuevo-paciente",
  templateUrl: "./nuevo-paciente.component.html",
  styleUrls: [],
})
export class NuevoPacienteComponent implements OnInit {
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  idClinica: number;
  patientForm: FormGroup;
  sucursales: any = [];
  conveniosclinica: any = [];
  docs: any = [];
  pacientes: any = [];
  keyword = "name";
  data = [];
  isEmail = /\S+@\S+\.\S+/;
  nombreImg = "";
  img = new FormData();

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router,
    private http: HttpService,
    private sweet: AlertService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.patientForm = this.fb.group({
      nombre: ["", Validators.required],
      apellido: ["", Validators.required],
      rfc: ["", Validators.minLength(12)],
      convenio: [""],
      notaconvenio: ["", Validators.maxLength(250)],
      direccion: [""],
      celular: ["", Validators.minLength(10)],
      correo: ["", [Validators.required, Validators.pattern(this.isEmail)]],
      fechanac: [""],
      edad: ["0"],
      gruposanguineo: [""],
      genero: [""],
      estadocivil: [""],
      sucursal: ["", Validators.required],
      ocupacion: [""],
      encontraste: ["", Validators.required],
      observaciones: ["", Validators.maxLength(80)],
      doctor: [""],
      familiarregistrado: [""],
      parentescodefamiliar: [""],
      nombreemergencia: [""],
      telefonoemergencia: [""],
      parentescoemergencia: [""],
      responsabledepago: [""],
      imagenpaciente: [""],
      Tipo_usuario: ["Paciente"],
      sms: [false],
      email: [false],
    });
  }

  ngOnInit(): void {
    this.getProfeciones();
    if (this.idClinica > 0) {
      this.doctores();
      this.obtenerconvenios();
      this.listapacientes();
      this.getSucursales();
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraclinica());
    }
  }

  getSucursales() {
    this.http
      .post("categorias.php", { caso: 0, idclinica: this.idClinica })
      .subscribe((respuesta: any) => {
        this.sucursales = respuesta;
      });
  }

  getProfeciones() {
    this.http.getActividades().subscribe({
      next: (actividades: any) => {
        this.data = actividades;
      },
      error: (err) => console.log(err),
    });
  }

  selectEvent(item) {
    this.patientForm.patchValue({
      ocupacion: item.name,
    });
  }

  handleAddressChange(address: Address) {
    this.patientForm.patchValue({
      direccion: address.formatted_address,
    });
  }

  iraclinica() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  cargarImagen(event: any) {
    if (event.target.files.length) {
      const file = event.target.files[0];
      this.nombreImg = `${new Date().getTime()}${file.name}`;
      this.img.append("imagenPropia", file, this.nombreImg);
    }
  }

  preSave(form: any) {
    const telefono = form.celular;
    if (telefono === "") {
      this.onSubmit(form);
      return;
    }
    this.http.post("pacientes.php", { caso: 15, telefono }).subscribe({
      next: (pacientes: any) => {
        if (pacientes !== null) {
          if (pacientes[0].Celular !== "") {
            this.sweet.alert(
              "",
              "Ya existe un paciente con este numero telefonico por favor introduzca uno distinto",
              "error"
            );
          } else {
            this.onSubmit(form);
          }
          return;
        } else {
          this.onSubmit(form);
        }
      },
      error: (e) => console.error(e),
    });
  }

  onSubmit(form: any) {
    if (this.nombreImg !== "") {
      form.imagenpaciente = this.nombreImg;
      this.agregarpacienteimg(form);
    } else {
      form.imagenpaciente = "user_default.png";
      this.agregarpaciente(form);
    }
  }

  irAPerfil(idpaciente) {
    localStorage.setItem("perfilusuario", JSON.stringify(idpaciente));
    this.router.navigate(["/admin/paciente/detalle-paciente"]);
    localStorage.setItem("tabs", JSON.stringify(0));
  }

  agregarpacienteimg(form: FormGroup) {
    this.sweet.subiendo("la imagen");
    this.http
      .postFile("uploadfoto.php", this.img)
      .subscribe((response: any) => {
        if (response !== null) {
          if (response.msj === "Imagen subida") {
            this.sweet.alert("", "Se subio la imagen correctamente", "success");
            this.agregarpaciente(form);
          } else {
            this.sweet.alert(
              "",
              "La imagen no pudo subirse, intente de nuevo por favor",
              "error"
            );
          }
        } else {
          this.sweet.alert(
            "",
            "La imagen no pudo subirse, intente de nuevo por favor",
            "error"
          );
        }
      });
  }

  agregarpaciente(nuevoPaciente) {
    const options: any = {
      caso: 1,
      nuevoPaciente,
      idClinica: this.idClinica,
    };
    this.http.post("pacientes.php", options).subscribe({
      next: (res: any) => {
        if (res !== null) {
          if (res?.msj === "Se inserto") {
            this.nombreImg = "";
            this.img = new FormData();
            this.sweet.alertClose();
            this.sweet.alert("", "Paciente agregado correctamente", "success");
            this.irAPerfil(res.id);
          } else {
            this.sweet.alert(
              "",
              "Ocurrio un error, intente de nuevo por favor",
              "error"
            );
          }
        }
      },
      error: (err) => console.error(err),
    });
  }

  obtenerconvenios() {
    this.http
      .post("convenios.php", { caso: 8, idclinica: this.idClinica })
      .subscribe((respuesta) => {
        this.conveniosclinica = respuesta;
      });
  }

  doctores() {
    this.http
      .post("pacientes.php", { caso: 5, idClinica: this.idClinica })
      .subscribe((respuesta) => {
        this.docs = respuesta;
      });
  }

  listapacientes() {
    this.http
      .post("citas.php", { caso: 10, idC: this.idClinica })
      .subscribe((respuesta) => {
        this.pacientes = respuesta;
      });
  }

  calcularedad(e: any) {
    const currentYear = new Date().getFullYear();
    const holdYear = new Date(e.value).getFullYear();
    this.patientForm.patchValue({
      edad: currentYear - holdYear,
      fechanac: this.formatDate(new Date(e.value)),
    });
    this.patientForm.get("edad")!.disable();
  }

  firstLetterUpperCase(id) {
    const texto = (<HTMLInputElement>document.getElementById(id)).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      (<HTMLInputElement>document.getElementById(id)).value = textofinal;
    }
  }

  toUpperCaseAll(id) {
    let texto = (<HTMLInputElement>document.getElementById(id)).value;
    (<HTMLInputElement>document.getElementById(id)).value = texto.toUpperCase();
  }

  firstWordUpercase(id) {
    let texto = (<HTMLInputElement>document.getElementById(id)).value;
    let cadena = texto.toLowerCase().split(" ");
    for (let i = 0; i < cadena.length; i++) {
      cadena[i] = cadena[i].charAt(0).toUpperCase() + cadena[i].substring(1);
    }
    texto = cadena.join(" ");
    (<HTMLInputElement>document.getElementById(id)).value = texto;
  }

  selectFam(e) {
    if (e.value == "Sin Familiar Registrado") {
      this.patientForm.patchValue({
        parentescodefamiliar: "",
        nombreemergencia: "",
        telefonoemergencia: "",
        parentescoemergencia: "",
        responsabledepago: "",
      });
      this.patientForm.get("parentescodefamiliar").disable();
      this.patientForm.get("nombreemergencia").disable();
      this.patientForm.get("telefonoemergencia").disable();
      this.patientForm.get("parentescoemergencia").disable();
      this.patientForm.get("responsabledepago").disable();
    } else {
      this.patientForm.get("parentescodefamiliar").enable();
      this.patientForm.get("nombreemergencia").enable();
      this.patientForm.get("telefonoemergencia").enable();
      this.patientForm.get("parentescoemergencia").enable();
      this.patientForm.get("responsabledepago").enable();
      this.getFamReg(e.value);
    }
  }

  getFamReg(idPaciente: string) {
    this.http.post("pacientes.php", { caso: 16, idPaciente }).subscribe({
      next: ([res]: any) => {
        this.patientForm.patchValue({
          parentescodefamiliar: res.Parentesco,
          nombreemergencia: res.Nombre_Emergencia,
          telefonoemergencia: res.Celular,
          parentescoemergencia: res.Parentesco_emergencia,
          responsabledepago: res.Responsable_Pago,
        });
      },
      error: (err: any) => console.log(err),
    });
  }

  checkMail(e) {
    this.patientForm.patchValue({
      email: e.checked,
    });
  }

  checkSms(e) {
    this.patientForm.patchValue({
      sms: e.checked,
    });
  }

  formatDate(fecha: Date): string {
    const day = fecha.getDate();
    const month = fecha.getMonth() + 1;
    const year = fecha.getFullYear();
    if (month < 10) {
      return `${year}-0${month}-${day}`;
    } else {
      return `${year}-${month}-${day}`;
    }
  }
}
