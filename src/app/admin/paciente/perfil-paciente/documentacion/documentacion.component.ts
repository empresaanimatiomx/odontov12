import { Component, OnInit} from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import Swal from 'sweetalert2';
import { SubirArchivoService } from "src/app/admin/administrador/documentos/documento/subir-archivo.service";
import { AuthService } from "src/app/core/service/auth.service";
import { Clipboard } from "@angular/cdk/clipboard";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-documentacion",
  templateUrl: "./documentacion.component.html",
  styles: [],
})
export class DocumentacionComponent implements OnInit {
  arreglodocumentos: any = [];

  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  public respuestaImagenEnviada;
  Archivoseleccionado: null;
  nombreArchivo: any;
  Dataarchivo: File = null;
  archivo = "";
  archivoFinal: any;
  idClinica: number;
  idUsuario: number;
  ifNumero = false;
  numero = "";
  datos = "";

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router,
    private enviandoImagen: SubirArchivoService,
    private authService: AuthService,
    private clipboard: Clipboard,
    private modalService: NgbModal
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.idUsuario = this.authService.currentUserValue.id;
  }
  ngOnInit() {
    if (this.idClinica > 0) {
      this.obtenerdocumentos();
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
  }

  /** obtener documentos */
  obtenerdocumentos(): void {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 7, idc: this.idClinica };
    const URL: any = this.baseURL + "documentos.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        this.arreglodocumentos = respuesta;
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  // metdo para obtener la imagen de input file
  Subirdocumento(event, files: FileList, fileInput: any) {
    console.log("Entro");
    const Numero1 = Math.floor(Math.random() * 10001).toString();
    const Numero2 = Math.floor(Math.random() * 1001).toString();
    // Nombre del archivo
    this.Archivoseleccionado = event.target.files[0];
    this.nombreArchivo = event.target.files[0].name;
    this.archivo = Numero1 + Numero2 + this.nombreArchivo.replace(/ /g, "");
    this.archivoFinal = files;
    this.Dataarchivo = <File>fileInput.target.files[0];
    this.cargandodocumento();
  }
  // metodo para subir el documento y guardar el resultado en base de datos
  cargandodocumento() {
    this.mensajesubiendoinf();
    if (this.archivo === "") {
      this.swalalertmensaje("error", "Debe seleccionar un archivo");
    } else {
      this.enviandoImagen
        .subirarchivo(this.archivoFinal[0], this.archivo)
        .subscribe((response) => {
          console.log(response);
          this.respuestaImagenEnviada = response;
          if (this.respuestaImagenEnviada.msj === "Archivo subido") {
            const headers: any = new HttpHeaders({
              "Content-Type": "application/json",
            });
            const options: any = {
              caso: 6,
              named: this.archivo,
              idc: this.idClinica,
              idUsuario: this.idUsuario,
            };
            const URL: any = this.baseURL + "documentos.php";
            this.httpClient
              .post(URL, JSON.stringify(options), headers)
              .subscribe((res) => {
                if (res.toString() === "Se inserto") {
                  this.obtenerdocumentos();
                  this.swalalertmensaje(
                    "success",
                    "Archivo subido correctamente"
                  );
                } else {
                  this.swalalertmensaje(
                    "error",
                    "Ocurrio un error, intente de nuevo por favor"
                  );
                }
              });
          } else if (
            this.respuestaImagenEnviada.msj === "No se recibio ningun archivo"
          ) {
            this.swalalertmensaje(
              "error",
              "No es un formato valido, intente de nuevo por favor"
            );
          } else {
            this.swalalertmensaje(
              "error",
              "Ocurrio un error, intente de nuevo por favor"
            );
          }
        });
    }
  }

  eliminardocumento(nombre, id) {
    console.log(nombre + "---" + id);
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 5, named: nombre, idd: id };
    const URL: any = this.baseURL + "documentos.php";
    this.httpClient
      .post(URL, JSON.stringify(options), headers)
      .subscribe((res) => {
        if (res.toString() === "Se elimino") {
          this.obtenerdocumentos();
          this.swalalertmensaje("success", "Archivo eliminado correctamente");
        } else {
          this.swalalertmensaje(
            "error",
            "Ocurrio un error, intente de nuevo por favor"
          );
        }
      });
  }

  saberquetipodearchivoes(nombre) {
    if (nombre.includes(".doc") || nombre.includes(".docx")) {
      return "word";
    } else if (
      nombre.includes(".xml") ||
      nombre.includes(".xls") ||
      nombre.includes(".xlsx")
    ) {
      return "excel";
    } else if (nombre.includes(".pptx")) {
      return "powerpoint";
    } else if (nombre.includes(".pdf")) {
      return "pdf";
    } else {
      return "archivo";
    }
  }

  mensajesubiendoinf() {
    Swal.fire({
      title: "Subiendo archivo ",
      html: "Espere un momento por favor",
      allowOutsideClick: false,
      showCancelButton: false,
      showConfirmButton: false,
    });
  }

  swalalertmensaje(icon, mensaje) {
    Swal.fire({
      position: "center",
      icon: icon,
      title: mensaje,
      showConfirmButton: true,
      allowOutsideClick: false,
      confirmButtonText: "Aceptar",
    });
  }

  onToggle(e) {
    this.ifNumero = e.checked;
  }

  compartir(modal, info?) {
    this.datos = `https://projectsbyanimatiomx.com/phps/odonto/fotospaciente/${info.Nombre}`;
    this.clipboard.copy(this.datos);
    this.modalService.open(modal, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
    });
  }
}