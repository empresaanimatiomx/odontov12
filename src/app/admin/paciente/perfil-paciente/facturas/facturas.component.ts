/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
import { Component, OnInit } from "@angular/core";
import { HttpService } from "src/app/core/service/http.service";

@Component({
  selector: "app-facturas",
  templateUrl: "./facturas.component.html",
  styleUrls: [],
})
export class FacturasComponent implements OnInit {
  historialFacturas: any = [];
  paciente;
  constructor(
    private postSvc: HttpService
  ) {}

  ngOnInit(): void {
    this.getHistorialFacturas();
    this.getDatosPaciente();
  }

  getDatosPaciente() {
    const idPaciente = JSON.parse(localStorage.getItem("perfilusuario"));
    this.postSvc
      .post("datos-personales.php", {
        caso: 3,
        idPaciente,
      })
      .subscribe({
        next: (paciente: any) => {
          if (paciente !== null) {
            this.paciente = paciente[0];
          }
        },
        error: (e) => console.log(e),
      });
  }

  getHistorialFacturas() {
    const idPaciente = JSON.parse(localStorage.getItem("perfilusuario"));
    this.postSvc
      .post("datos-personales.php", { caso: 6, idPaciente })
      .subscribe((res: any) => {
        if (res !== null && res !== null && res !== "") {
          this.historialFacturas = res;
        }
      });
  }

  downloadFactura(factura: any) {
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    let raw = JSON.stringify({
      idFactura: factura.idFactura,
      fecha_update: factura.fecha_update,
    });
    let requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
    };
    fetch(
      "https://projectsbyanimatiomx.com/phps/odonto/downloadPDF.php",
      requestOptions
    )
      .then((response: any) => response.blob())
      .then((myBlob) => {
        let objectURL = URL.createObjectURL(myBlob);
        window.open(objectURL);
      })
      .catch((error) => console.log("error", error));
  }
}
