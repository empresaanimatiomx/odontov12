import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TratamientoService {
//intercambio de datos
dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
baseURL = 'https://projectsbyanimatiomx.com/phps/odonto/';
dialogData: any;
tratamientos: any = [];
constructor(private httpClient: HttpClient) {}
// poner los valores de la consulta en el dataChange
get data(): any[] {
  return this.dataChange.value;
}
getDialogData() {
  return this.dialogData;
}
/** CRUD METHODS */
obtenerTratamiento(idP): void {
  const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
  const options: any = { 'caso': 1,'idPaciente':idP};
  const URL: any = this.baseURL + 'tratamiento.php';
  this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
    respuesta => {
      this.tratamientos = respuesta;
      if (this.tratamientos !== null){
        this.dataChange.next(this.tratamientos);
        console.log(respuesta);
        console.log(this.dataChange);
      }
    },
    (error: HttpErrorResponse) => {
      console.log(error.name + ' ' + error.message);
    }
    );
}

// DEMO ONLY, you can find working methods below
agregartratamiento(fecha: Date,obs: String,sumato: String,totalconinpu: String,totalapagar: String,arrayser: any,textoplanes: String,textosubtotal: String,idpaciiente: Number){

  let fechafinal = moment(fecha).format('YYYY/MM/DD');
  const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
  const options: any = { 'caso': 2,'fecha':fechafinal,'observaciones':obs,'SumaTotal':sumato,'Totalconimpuesto':totalconinpu,
  'Totalapagar':totalapagar,'arrayservicio':arrayser,'arrayplanestexto':textoplanes,'arraysubtotaltexto':textosubtotal,'idPaciente':idpaciiente};
  const URL: any = this.baseURL + 'tratamiento.php';
  return this.httpClient.post(URL, JSON.stringify(options), headers);
}

actualizartratamiento(obs: String,sumato: String,totalconinpu: String,totalapagar: String,arrayser: any,textoplanes: String,textosubtotal: String,idplantratamiento: Number,arraysereliminar: any){
  const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
  const options: any = { 'caso': 5,'observaciones':obs,'SumaTotal':sumato,'Totalconimpuesto':totalconinpu,
  'Totalapagar':totalapagar,'arrayservicio':arrayser,'arrayplanestexto':textoplanes,
  'arraysubtotaltexto':textosubtotal,'idplantratamiento':idplantratamiento,'arrayservicioe':arraysereliminar};
  const URL: any = this.baseURL + 'tratamiento.php';
  return  this.httpClient.post(URL, JSON.stringify(options), headers);
}
eliminartratamiento(id: number): void {
  console.log(id);
  const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
  const options: any = { 'caso': 4,'idplantratamiento':id  };
  const URL: any = this.baseURL + 'tratamiento.php';
  this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
    respuesta => {
    });
}
}
