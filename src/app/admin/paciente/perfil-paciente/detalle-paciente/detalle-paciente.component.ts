import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import moment from "moment";
moment.locale("es");
import { Chart } from "chart.js";
const headers: any = new HttpHeaders({ "Content-Type": "application/json" });
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../../../../core/service/auth.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-detalle-paciente",
  templateUrl: "./detalle-paciente.component.html",
  styleUrls: [],
})
export class DetallePacienteComponent implements OnInit {
  @ViewChild("nuevacita", { static: false }) nuevacita: ElementRef;
  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  tabs: number;
  idpaciente: number;
  infopaciente: any = [];
  public areaChartOptions: any;
  arreglodeyear: any = [];
  yearselect = "";
  citaspaciente: any = [];
  citaspacientetablaproximas: any = [];
  citaspacientetablaultimas: any = [];
  citaspacientetablafinalizadas: any = [];
  colorScheme = {
    domain: ["#9370DB", "#87CEFA", "#FA8072", "#FF7F50", "#90EE90", "#9370DB"],
  };
  public single = [
    {
      name: "China",
      value: 2243772,
    },
    {
      name: "USA",
      value: 1826000,
    },
    {
      name: "India",
      value: 1173657,
    },
    {
      name: "Japan",
      value: 857363,
    },
    {
      name: "Germany",
      value: 496750,
    },
    {
      name: "France",
      value: 204617,
    },
  ];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  showYAxisLabel = true;
  legendPosition = "right";
  hbarxAxisLabel = "Sales";
  hbaryAxisLabel = "Country";
  chart: any;
  idCita = "";
  valor = "";
  valorboolean = false;
  infocita: any = [];
  nombrepaciente = "";
  nombredoctor = "";
  nombremotivo = "";
  nombreservicio = "";
  nombrepaciente1 = "";
  nombredoctor1 = "";
  nombremotivo1 = "";
  nombreservicio1 = "";
  horariocita = "";
  motivocita = "";
  fechacita = "";
  serviciocita = "";
  estatuscita = "";
  observacionescita = "";
  motivootro = false;
  NuevaCitaForm: FormGroup;
  pacientes: any = [];
  doctores: any = [];
  idClinica: number;
  servicios: any = [];
  convenios: any = [];
  motivosconsulta: any = [];
  filterDoc: any = [];
  sms;

  constructor(
    private httpClient: HttpClient,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private authService: AuthService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.tabs = Number(JSON.parse(localStorage.getItem("tabs")));
    this.idpaciente = Number(JSON.parse(localStorage.getItem("perfilusuario")));
    this.arreglodeyear = [];
    for (var i = 2022; i <= 2050; i++) {
      this.arreglodeyear.push(i);
    }
    this.NuevaCitaForm = this.fb.group({
      paciente: ["", [Validators.required]],
      doctor: ["", [Validators.required]],
      fecha: ["", [Validators.required]],
      horario: ["", [Validators.required]],
      motivo: ["", [Validators.required]],
      motivoo: [""],
      servicio: ["", [Validators.required]],
      estatus: ["", [Validators.required]],
      observaciones: [""],
    });
  }

  ngOnInit() {
    this.informacionpaciente();
    this.datos();
    this.GetPacientes(this.idClinica);
    this.GetDoctores(this.idClinica);
    this.GetServicios(this.idClinica);
    this.obtenermotivos(this.idClinica);
  }

  //obtener informacion del paciente
  informacionpaciente() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 13, idPaciente: this.idpaciente };
    const URL: any = this.baseURL + "pacientes.php";
    this.httpClient
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        this.infopaciente = respuesta;
      });
  }

  //obtenereltabsactual
  vistaactual(tabs) {
    localStorage.setItem("tabs", JSON.stringify(tabs));
  }

  grafica() {
    this.chart = new Chart("canvas", {
      type: "horizontalBar",
      data: {
        labels: [
          "ENE",
          "FEB",
          "MAR",
          "ABR",
          "MAY",
          "JUN",
          "JUL",
          "AGO",
          "SEP",
          "OCT",
          "NOV",
          "DIC",
        ],
        datasets: [
          {
            label: "",
            data: this.citaspaciente,
            backgroundColor: [
              "rgba(111, 172, 83, 1)",
              "rgba(247, 224, 13, 1)",
              "rgba(222, 145, 47, 1)",
              "rgba(55, 167, 156, 1)",
              "rgba(255, 191, 166, 1)",
              "rgba(102, 168, 222, 1)",
              "rgba(203, 34, 40, 1)",
              "rgba(63, 63, 191, 1)",
              "rgba(221, 115, 207, .67)",
              "rgba(159, 69, 211, 1)",
              "rgba(51, 233, 167, 1)",
              "rgba(222, 16, 27, 0.38)",
            ],
            borderColor: [
              "rgba(111, 172, 83, 1)",
              "rgba(247, 224, 13, 1)",
              "rgba(222, 145, 47, 1)",
              "rgba(55, 167, 156, 1)",
              "rgba(255, 191, 166, 1)",
              "rgba(102, 168, 222, 1)",
              "rgba(203, 34, 40, 1)",
              "rgba(63, 63, 191, 1)",
              "rgba(221, 115, 207, .67)",
              "rgba(159, 69, 211, 1)",
              "rgba(51, 233, 167, 1)",
              "rgba(222, 16, 27, 0.38)",
            ],
            borderWidth: 1,
          },
        ],
      },
      options: {
        title: {
          display: false,
        },
        legend: {
          display: false,
        },
        scales: {
          yAxes: [{}],
          xAxes: [
            {
              gridLines: {
                zeroLineWidth: 0,
                zeroLineColor: "rgba(166, 197, 212, 1)",
              },
            },
          ],
        },
      },
    });
  }

  private graficacita() {
    this.areaChartOptions = {
      series: [
        {
          name: "Citas",
          data: this.citaspaciente,
        },
      ],
      chart: {
        type: "area",
        height: 350,
        zoom: {
          enabled: false,
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: "smooth",
      },
      xaxis: {
        categories: [
          "Enero",
          "Febrero",
          "Marzo",
          "Abril",
          "Mayo",
          "Junio",
          "Julio",
          "Agosto",
          "Septiembre",
          "Octubre",
          "Noviembre",
          "Diciembre",
        ],
      },
      yaxis: {
        opposite: false,
      },
      legend: {
        horizontalAlign: "right",
      },
      colors: ["#3f51b5"],
    };
  }

  datos() {
    if (this.yearselect) {
      this.yearselect = this.yearselect;
    } else {
      this.yearselect = String(new Date().getFullYear());
    }
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = {
      caso: 0,
      id_usuario: this.idpaciente,
      year: this.yearselect,
    };
    const URL: any = this.baseURL + "graficas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        if (respuesta) {
          this.citaspaciente = respuesta;
          //this.graficacita();
          this.grafica();
          this.obtenercitasproximas();
          this.obtenercitasultimas();
          this.obtenercitasfinalizadas();
        } else {
          this.citaspaciente = [];
          //this.graficacita();
          this.grafica();
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }
  obtenercitasproximas() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 1, id_usuario: this.idpaciente };
    const URL: any = this.baseURL + "graficas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        if (respuesta.toString() !== "Vacio") {
          this.citaspacientetablaproximas = respuesta;
        } else {
          this.citaspacientetablaproximas = [];
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }
  obtenercitasultimas() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 2, id_usuario: this.idpaciente };
    const URL: any = this.baseURL + "graficas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        if (respuesta.toString() !== "Vacio") {
          this.citaspacientetablaultimas = respuesta;
        } else {
          this.citaspacientetablaultimas = [];
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }
  obtenercitasfinalizadas() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 3, id_usuario: this.idpaciente };
    const URL: any = this.baseURL + "graficas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe({
      next: (respuesta) => {
        if (respuesta.toString() !== "Vacio") {
          this.citaspacientetablafinalizadas = respuesta;
        } else {
          this.citaspacientetablafinalizadas = [];
        }
      },
      error: (err) => console.log(err),
    });
  }

  detallecita(event, valor) {
    this.idCita = event.idCitas;
    this.valor = valor;
    // if (valor === '3') {
    //   this.NuevaCitaForm.controls['estatus'].setValue('Finalizada');
    // }
    const options: any = { caso: 13, idc: this.idCita };
    this.httpClient
      .post(this.baseURL + "citas.php", JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.infocita = respuesta;
        this.fechacita = this.infocita[0].Fecha;
        this.horariocita = this.infocita[0].Horario;
        this.motivocita = this.infocita[0].Motivos;
        this.serviciocita = this.infocita[0].idServicios;
        this.estatuscita = this.infocita[0].EstatusCita;
        this.observacionescita = this.infocita[0].ObservacionesCita;

        let ngbModalOptions: NgbModalOptions = {
          backdrop: "static",
          keyboard: false,
          centered: true,
          ariaLabelledBy: "modal-basic-title",
        };
        this.modalService.open(this.nuevacita, ngbModalOptions);
        const fecha = moment(this.infocita[0].Fecha).format("MM/DD/YYYY");
        this.nombredoctor =
          this.infocita[0].NombreUsuario + " " + this.infocita[0].Apellidos;
        this.nombrepaciente =
          this.infocita[0].Nombre + " " + this.infocita[0].Apellido;
        this.nombremotivo1 = this.infocita[0].NombreMotivo;
        this.nombreservicio1 = this.infocita[0].NombreServicio;
        this.nombremotivo = this.infocita[0].NombreMotivo;
        this.nombreservicio = this.infocita[0].NombreServicio;
        if (this.infocita[0].idClinica !== "") {
          this.motivootro = false;
          this.NuevaCitaForm.setValue({
            paciente: this.infocita[0].idPacientes,
            doctor: this.infocita[0].idDoctores,
            fecha: new Date(fecha),
            horario: this.infocita[0].Horario,
            motivo: this.infocita[0].Motivos,
            motivoo: "",
            servicio: this.infocita[0].idServicios,
            estatus: this.infocita[0].EstatusCita,
            observaciones: this.infocita[0].ObservacionesCita,
          });
          this.NuevaCitaForm.disable();
        } else {
          this.motivootro = true;
          this.NuevaCitaForm.setValue({
            paciente: this.infocita[0].idPacientes,
            doctor: this.infocita[0].idDoctores,
            fecha: new Date(fecha),
            horario: this.infocita[0].Horario,
            motivo: "Otro motivo de consulta de cita",
            motivoo: this.infocita[0].NombreMotivo,
            servicio: this.infocita[0].idServicios,
            estatus: this.infocita[0].EstatusCita,
            observaciones: this.infocita[0].ObservacionesCita,
          });
          this.NuevaCitaForm.disable();
        }
      });
  }

  cerrarmodal(form: FormGroup) {
    form.reset();
    this.fechacita = "";
    this.horariocita = "";
    this.motivocita = "";
    this.serviciocita = "";
    this.estatuscita = "";
    this.observacionescita = "";
    this.modalService.dismissAll();
  }

  obtenernombrepaciente(option: any) {
    this.nombrepaciente = option.source.triggerValue;
  }

  obtenernombredoctor(option: any) {
    this.nombredoctor = option.source.triggerValue;
  }

  obtenernombremotivo(option: any) {
    this.nombremotivo = option.source.triggerValue;
  }

  obtenernombreservicio(option: any) {
    this.nombreservicio = option.source.triggerValue;
  }

  //obtiene el listado de pacientes
  GetPacientes(idclinica) {
    const options: any = { caso: 10, idC: idclinica };
    const URL: any = this.baseURL + "citas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        if (respuesta) {
          this.pacientes = respuesta;
        } else {
          this.pacientes = [];
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }

  //obtiene el listado de doctores

  GetDoctores(idclinica) {
    const options: any = { caso: 5, idClinica: idclinica };
    this.httpClient
      .post(`${this.baseURL}pacientes.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.doctores = respuesta;
      });
  }

  //obtiene el listado de servicios
  GetServicios(idclinica) {
    const options: any = { caso: 0, idClinica: idclinica };
    this.httpClient
      .post(`${this.baseURL}servicios.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.servicios = respuesta;
      });
  }

  // metodo para obtener los doctores de la clinica
  obtenerDocs(idclinica) {
    const options: any = { caso: 0.1, idC: idclinica };
    const URL: any = this.baseURL + "citas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        if (respuesta) {
          this.filterDoc = respuesta;
        } else {
          this.filterDoc = [];
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }

  // metodo para obtener los convenios
  obtenermotivos(idclinica) {
    const options: any = { caso: 11, idC: idclinica };
    const URL: any = this.baseURL + "citas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        if (respuesta) {
          this.motivosconsulta = respuesta;
        } else {
          this.motivosconsulta = [];
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }

  // metodo para obtener los convenios
  obtenerconvenios(idclinica) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 8, idclinica: idclinica };
    const URL: any = this.baseURL + "convenios.php";
    this.httpClient
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.convenios = respuesta;
      });
  }

  eliminarcita(formCita: FormGroup) {
    Swal.fire({
      title: "¿Esta seguro de eliminar esta cita?",
      showCancelButton: true,
      confirmButtonText: `Eliminar cita`,
      cancelButtonText: "Cancelar",
      confirmButtonColor: "#f44336",
      cancelButtonColor: "#007bff",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        const headers: any = new HttpHeaders({
          "Content-Type": "application/json",
        });
        const options: any = { caso: 21, idc: this.infocita[0].idCitas };
        const URL: any = this.baseURL + "citas.php";
        this.httpClient
          .post(URL, JSON.stringify(options), headers)
          .subscribe((respuesta) => {
            const res = respuesta;
            if (res.toString() === "Se elimino") {
              this.datos();
              this.fechacita = "";
              this.horariocita = "";
              this.motivocita = "";
              this.serviciocita = "";
              this.estatuscita = "";
              this.observacionescita = "";
              formCita.reset();
              this.modalService.dismissAll();
              this.swalalertmensaje("success", "Cita eliminada correctamente");
            } else {
              this.swalalertmensaje(
                "error",
                "Ocurrio un error, intente de nuevo por favor"
              );
            }
          });
      }
    });
  }

  guardarNuevaCita(formCita: FormGroup) {
    if (formCita.value.motivo === "Otro motivo de consulta de cita") {
      const options: any = {
        caso: 12,
        Nombredemotivo: formCita.value.motivoo,
        Mostrarenagenda: "SI",
      };
      this.httpClient
        .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
        .subscribe((respuesta: any) => {
          if (respuesta != null) {
            formCita.value.motivo = respuesta;
            let fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
            let horario = formCita.value.horario;
            let motivo = formCita.value.motivo;
            let servicio = formCita.value.servicio;
            let estatus = formCita.value.estatus;
            let observaciones = formCita.value.observaciones;

            let arraycita = [];
            let mensajemodificacion = "";
            let motivomodificacion = "";

            if (
              this.fechacita === fecha &&
              this.horariocita === horario &&
              this.motivocita === motivo &&
              this.serviciocita === servicio &&
              this.estatuscita === estatus &&
              this.observacionescita === observaciones
            ) {
              this.fechacita = "";
              this.horariocita = "";
              this.motivocita = "";
              this.serviciocita = "";
              this.estatuscita = "";
              this.observacionescita = "";
              this.modalService.dismissAll();
              Swal.fire("No hubo cambios por modificar", "", "success");
            } else {
              if (this.fechacita !== fecha) {
                arraycita.push("fecha");
                mensajemodificacion =
                  "La fecha de la cita se ha cambia de " +
                  this.fechacita +
                  " a " +
                  fecha;
                motivomodificacion = "Modificación de fecha";
              }
              if (this.horariocita !== horario) {
                arraycita.push("horario");
                mensajemodificacion =
                  mensajemodificacion +
                  "\nLa hora de la cita se ha cambia de " +
                  this.horariocita +
                  " a " +
                  horario;
                motivomodificacion =
                  motivomodificacion + "\nModificación de Hora";
              }
              if (this.motivocita !== motivo) {
                arraycita.push("motivo");
                mensajemodificacion =
                  mensajemodificacion +
                  "\nEl motivo de la cita se ha cambia de " +
                  this.nombremotivo1 +
                  " a " +
                  this.nombremotivo;
                motivomodificacion =
                  motivomodificacion + "\nModificación de motivo";
              }
              if (this.serviciocita !== servicio) {
                arraycita.push("servicio");
                mensajemodificacion =
                  mensajemodificacion +
                  "\nEl servicio de la cita se ha cambia de " +
                  this.nombreservicio +
                  " a " +
                  this.nombreservicio1;
                motivomodificacion =
                  motivomodificacion + "\nModificación de servicio";
              }
              if (this.estatuscita !== estatus) {
                arraycita.push("estatus");
                mensajemodificacion =
                  mensajemodificacion +
                  "\nEl estatus de la cita se ha cambia de " +
                  this.estatuscita +
                  " a " +
                  estatus;
                motivomodificacion =
                  motivomodificacion + "\nModificación de estatus";
              }
              if (this.observacionescita !== observaciones) {
                arraycita.push("observaciones");
                mensajemodificacion =
                  mensajemodificacion +
                  "\nSe modifico la observación de la cita de " +
                  this.observacionescita +
                  " a " +
                  observaciones;
                motivomodificacion =
                  motivomodificacion + "\nModificación de observaciones";
              }

              if (this.sms || estatus === "Por confirmar") {
                const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
                const fecha1 = moment(formCita.value.fecha).format(
                  "MMMM Do YYYY"
                );
                const hora = formCita.value.horario;
                const observaciones = formCita.value.observaciones;
                const options: any = {
                  caso: 8,
                  nuevacita: formCita.value,
                  idc: this.infocita[0].idCitas,
                  fecha: fecha,
                };
                this.httpClient
                  .post(
                    `${this.baseURL}citas.php`,
                    JSON.stringify(options),
                    headers
                  )
                  .subscribe((respuesta: any) => {
                    if (respuesta == "Se modifico") {
                      const options: any = {
                        caso: 18,
                        id_usuario: this.authService.currentUserValue.id,
                        motivo: motivomodificacion,
                        detalle: mensajemodificacion,
                        fecha: moment().format("YYYY-MM-DD"),
                        hora: moment().format("HH:MM"),
                        idc: this.infocita[0].idCitas,
                      };
                      this.httpClient
                        .post(
                          `${this.baseURL}citas.php`,
                          JSON.stringify(options),
                          headers
                        )
                        .subscribe((respuesta: any) => {
                          if (respuesta == "Se inserto") {
                            const telefono = "+522441049843";
                            let link;
                            const estatusc = formCita.value.estatus;
                            let mensaje = "";
                            if (estatusc === "Por confirmar") {
                              link = this.infocita[0].idCitas;
                              if (observaciones) {
                                mensaje =
                                  "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                                  this.nombredoctor +
                                  "--salto--Paciente: " +
                                  this.nombrepaciente +
                                  "--salto--Fecha y Hora: " +
                                  fecha1 +
                                  " " +
                                  hora +
                                  "--salto--Motivo: " +
                                  this.nombremotivo +
                                  "--salto--Servicio: " +
                                  this.nombreservicio +
                                  "--salto--Observaciones: " +
                                  observaciones +
                                  "--salto--Para confirmar o cancelar la cita da clic en el siguiente link: --salto--";
                              } else {
                                mensaje =
                                  "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                                  this.nombredoctor +
                                  "--salto--Paciente: " +
                                  this.nombrepaciente +
                                  "--salto--Fecha y Hora: " +
                                  fecha1 +
                                  " " +
                                  hora +
                                  "--salto--Motivo: " +
                                  this.nombremotivo +
                                  "--salto--Servicio: " +
                                  this.nombreservicio +
                                  "--salto--Para confirmar o cancelar la cita da clic en el siguiente link: --salto--";
                              }
                            } else {
                              link = 0;
                              if (observaciones) {
                                mensaje =
                                  "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                                  this.nombredoctor +
                                  "--salto--Paciente: " +
                                  this.nombrepaciente +
                                  "--salto--Fecha y Hora: " +
                                  fecha1 +
                                  " " +
                                  hora +
                                  "--salto--Motivo: " +
                                  this.nombremotivo +
                                  "--salto--Servicio: " +
                                  this.nombreservicio +
                                  "--salto--Observaciones: " +
                                  observaciones;
                              } else {
                                mensaje =
                                  "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                                  this.nombredoctor +
                                  "--salto--Paciente: " +
                                  this.nombrepaciente +
                                  "--salto--Fecha y Hora: " +
                                  fecha1 +
                                  " " +
                                  hora +
                                  "--salto--Motivo: " +
                                  this.nombremotivo +
                                  "--salto--Servicio: " +
                                  this.nombreservicio;
                              }
                            }
                            this.httpClient
                              .get(
                                "https://smsyemail.herokuapp.com/sms/" +
                                  telefono +
                                  "/" +
                                  mensaje +
                                  "/" +
                                  link +
                                  "/" +
                                  0
                              )
                              .subscribe(
                                (data) => {
                                  console.log(JSON.stringify(data));
                                },
                                (error) => {
                                  console.log("err");
                                  console.log(JSON.stringify(error));
                                }
                              );
                            this.fechacita = "";
                            this.horariocita = "";
                            this.motivocita = "";
                            this.serviciocita = "";
                            this.estatuscita = "";
                            this.observacionescita = "";
                            formCita.reset();
                            this.sms = false;
                            this.modalService.dismissAll();
                            Swal.fire(
                              "Se actualizo la cita correctamente",
                              "",
                              "success"
                            );
                            this.ngOnInit();
                          }
                        });
                    } else {
                      Swal.fire(
                        "Ocurrio un error intentelo de nuevo",
                        "",
                        "error"
                      );
                    }
                  });
              } else {
                const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
                const options: any = {
                  caso: 8,
                  nuevacita: formCita.value,
                  idc: this.infocita[0].idCitas,
                  fecha: fecha,
                };
                this.httpClient
                  .post(
                    `${this.baseURL}citas.php`,
                    JSON.stringify(options),
                    headers
                  )
                  .subscribe((respuesta: any) => {
                    if (respuesta == "Se modifico") {
                      const options: any = {
                        caso: 18,
                        id_usuario: this.authService.currentUserValue.id,
                        motivo: motivomodificacion,
                        detalle: mensajemodificacion,
                        fecha: moment().format("YYYY-MM-DD"),
                        hora: moment().format("HH:MM"),
                        idc: this.infocita[0].idCitas,
                      };
                      this.httpClient
                        .post(
                          `${this.baseURL}citas.php`,
                          JSON.stringify(options),
                          headers
                        )
                        .subscribe((respuesta: any) => {
                          if (respuesta == "Se inserto") {
                            this.fechacita = "";
                            this.horariocita = "";
                            this.motivocita = "";
                            this.serviciocita = "";
                            this.estatuscita = "";
                            this.observacionescita = "";
                            formCita.reset();
                            this.modalService.dismissAll();
                            Swal.fire(
                              "Se actualizo la cita correctamente",
                              "",
                              "success"
                            );
                          }
                        });
                    } else {
                      Swal.fire(
                        "Ocurrio un error intentelo de nuevo",
                        "",
                        "error"
                      );
                    }
                    this.modalService.dismissAll();
                    this.ngOnInit();
                  });
              }
            }
          }
        });
    } else {
      let fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
      let horario = formCita.value.horario;
      let motivo = formCita.value.motivo;
      let servicio = formCita.value.servicio;
      let estatus = formCita.value.estatus;
      let observaciones = formCita.value.observaciones;

      let arraycita = [];
      let mensajemodificacion = "";
      let motivomodificacion = "";

      if (
        this.fechacita === fecha &&
        this.horariocita === horario &&
        this.motivocita === motivo &&
        this.serviciocita === servicio &&
        this.estatuscita === estatus &&
        this.observacionescita === observaciones
      ) {
        this.fechacita = "";
        this.horariocita = "";
        this.motivocita = "";
        this.serviciocita = "";
        this.estatuscita = "";
        this.observacionescita = "";
        this.modalService.dismissAll();
        Swal.fire("No hubo cambios por modificar", "", "success");
      } else {
        if (this.fechacita !== fecha) {
          arraycita.push("fecha");
          mensajemodificacion =
            "La fecha de la cita se ha cambia de " +
            this.fechacita +
            " a " +
            fecha;
          motivomodificacion = "Modificación de fecha";
        }
        if (this.horariocita !== horario) {
          arraycita.push("horario");
          mensajemodificacion =
            mensajemodificacion +
            "\nLa hora de la cita se ha cambia de " +
            this.horariocita +
            " a " +
            horario;
          motivomodificacion = motivomodificacion + "\nModificación de Hora";
        }
        if (this.motivocita !== motivo) {
          arraycita.push("motivo");
          mensajemodificacion =
            mensajemodificacion +
            "\nEl motivo de la cita se ha cambia de " +
            this.nombremotivo1 +
            " a " +
            this.nombremotivo;
          motivomodificacion = motivomodificacion + "\nModificación de motivo";
        }
        if (this.serviciocita !== servicio) {
          arraycita.push("servicio");
          mensajemodificacion =
            mensajemodificacion +
            "\nEl servicio de la cita se ha cambia de " +
            this.nombreservicio +
            " a " +
            this.nombreservicio1;
          motivomodificacion =
            motivomodificacion + "\nModificación de servicio";
        }
        if (this.estatuscita !== estatus) {
          arraycita.push("estatus");
          mensajemodificacion =
            mensajemodificacion +
            "\nEl estatus de la cita se ha cambia de " +
            this.estatuscita +
            " a " +
            estatus;
          motivomodificacion = motivomodificacion + "\nModificación de estatus";
        }
        if (this.observacionescita !== observaciones) {
          arraycita.push("observaciones");
          mensajemodificacion =
            mensajemodificacion +
            "\nSe modifico la observación de la cita de " +
            this.observacionescita +
            " a " +
            observaciones;
          motivomodificacion =
            motivomodificacion + "\nModificación de observaciones";
        }

        if (this.sms || estatus === "Por confirmar") {
          const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
          const fecha1 = moment(formCita.value.fecha).format("MMMM Do YYYY");
          const hora = formCita.value.horario;
          const observaciones = formCita.value.observaciones;
          const options: any = {
            caso: 8,
            nuevacita: formCita.value,
            idc: this.infocita[0].idCitas,
            fecha: fecha,
          };
          this.httpClient
            .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
            .subscribe((respuesta: any) => {
              if (respuesta == "Se modifico") {
                const options: any = {
                  caso: 18,
                  id_usuario: this.authService.currentUserValue.id,
                  motivo: motivomodificacion,
                  detalle: mensajemodificacion,
                  fecha: moment().format("YYYY-MM-DD"),
                  hora: moment().format("HH:MM"),
                  idc: this.infocita[0].idCitas,
                };
                this.httpClient
                  .post(
                    `${this.baseURL}citas.php`,
                    JSON.stringify(options),
                    headers
                  )
                  .subscribe((respuesta: any) => {
                    if (respuesta == "Se inserto") {
                      const telefono = "+522441049843";
                      let link;
                      const estatusc = formCita.value.estatus;
                      let mensaje = "";
                      if (estatusc === "Por confirmar") {
                        link = this.infocita[0].idCitas;
                        if (observaciones) {
                          mensaje =
                            "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                            this.nombredoctor +
                            "--salto--Paciente: " +
                            this.nombrepaciente +
                            "--salto--Fecha y Hora: " +
                            fecha1 +
                            " " +
                            hora +
                            "--salto--Motivo: " +
                            this.nombremotivo +
                            "--salto--Servicio: " +
                            this.nombreservicio +
                            "--salto--Observaciones: " +
                            observaciones +
                            "--salto--Para confirmar o cancelar la cita da clic en el siguiente link: --salto--";
                        } else {
                          mensaje =
                            "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                            this.nombredoctor +
                            "--salto--Paciente: " +
                            this.nombrepaciente +
                            "--salto--Fecha y Hora: " +
                            fecha1 +
                            " " +
                            hora +
                            "--salto--Motivo: " +
                            this.nombremotivo +
                            "--salto--Servicio: " +
                            this.nombreservicio +
                            "--salto--Para confirmar o cancelar la cita da clic en el siguiente link: --salto--";
                        }
                      } else {
                        link = 0;
                        if (observaciones) {
                          mensaje =
                            "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                            this.nombredoctor +
                            "--salto--Paciente: " +
                            this.nombrepaciente +
                            "--salto--Fecha y Hora: " +
                            fecha1 +
                            " " +
                            hora +
                            "--salto--Motivo: " +
                            this.nombremotivo +
                            "--salto--Servicio: " +
                            this.nombreservicio +
                            "--salto--Observaciones: " +
                            observaciones;
                        } else {
                          mensaje =
                            "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                            this.nombredoctor +
                            "--salto--Paciente: " +
                            this.nombrepaciente +
                            "--salto--Fecha y Hora: " +
                            fecha1 +
                            " " +
                            hora +
                            "--salto--Motivo: " +
                            this.nombremotivo +
                            "--salto--Servicio: " +
                            this.nombreservicio;
                        }
                      }
                      this.httpClient
                        .get(
                          "https://smsyemail.herokuapp.com/sms/" +
                            telefono +
                            "/" +
                            mensaje +
                            "/" +
                            link +
                            "/" +
                            0
                        )
                        .subscribe(
                          (data) => {
                            console.log(JSON.stringify(data));
                          },
                          (error) => {
                            console.log("err");
                            console.log(JSON.stringify(error));
                          }
                        );
                      this.fechacita = "";
                      this.horariocita = "";
                      this.motivocita = "";
                      this.serviciocita = "";
                      this.estatuscita = "";
                      this.observacionescita = "";
                      this.sms = false;
                      formCita.reset();
                      this.modalService.dismissAll();
                      Swal.fire(
                        "Se actualizo la cita correctamente",
                        "",
                        "success"
                      );
                      this.ngOnInit();
                    }
                  });
              } else {
                Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
              }
            });
        } else {
          const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
          const options: any = {
            caso: 8,
            nuevacita: formCita.value,
            idc: this.infocita[0].idCitas,
            fecha: fecha,
          };
          this.httpClient
            .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
            .subscribe((respuesta: any) => {
              if (respuesta == "Se modifico") {
                const options: any = {
                  caso: 18,
                  id_usuario: this.authService.currentUserValue.id,
                  motivo: motivomodificacion,
                  detalle: mensajemodificacion,
                  fecha: moment().format("YYYY-MM-DD"),
                  hora: moment().format("HH:MM"),
                  idc: this.infocita[0].idCitas,
                };
                this.httpClient
                  .post(
                    `${this.baseURL}citas.php`,
                    JSON.stringify(options),
                    headers
                  )
                  .subscribe((respuesta: any) => {
                    if (respuesta == "Se inserto") {
                      this.fechacita = "";
                      this.horariocita = "";
                      this.motivocita = "";
                      this.serviciocita = "";
                      this.estatuscita = "";
                      this.observacionescita = "";
                      formCita.reset();
                      this.modalService.dismissAll();
                      Swal.fire(
                        "Se actualizo la cita correctamente",
                        "",
                        "success"
                      );
                    }
                  });
              } else {
                Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
              }
              this.modalService.dismissAll();
              this.ngOnInit();
            });
        }
      }
    }
  }

  swalalertmensaje(icon, mensaje) {
    Swal.fire({
      position: "center",
      icon: icon,
      title: mensaje,
      showConfirmButton: true,
      allowOutsideClick: false,
      confirmButtonText: "Aceptar",
    });
  }

  onToggle(event) {
    this.sms = event.checked;
  }
}
