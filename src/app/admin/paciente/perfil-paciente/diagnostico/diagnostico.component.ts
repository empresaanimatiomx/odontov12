import { Component, ElementRef, OnInit, ViewChild, TemplateRef} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, FormControl, Validators, } from "@angular/forms";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { AuthService } from "src/app/core/service/auth.service";
import Swal from "sweetalert2";

const headers: any = new HttpHeaders({"Content-Type":"application/json"});

@Component({
  selector: 'app-diagnostico',
  templateUrl: './diagnostico.component.html',
  styles: [ ]
})
export class DiagnosticoComponent implements OnInit {
 private baseURL = 'https://projectsbyanimatiomx.com/phps/odonto/';
  @ViewChild("roleTemplate", { static: true }) roleTemplate: TemplateRef<any>;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  rows = [];
  selectedRowData: selectRowInterface;
  newUserImg = "assets/images/user_default.png";
  data: any = [];
  filteredData: any = [];
  editForm: FormGroup;
  register: FormGroup;
  selectedOption: string;
  columns = [
    { name: "Fecha y Hora" },
    { name: "Clasificacion" },
    { name: "Observaciones" },
    { name: "Acciones" },
  ];
  idClinica;
  idPaciente;
  clasificaciones: any = [];
  constructor(private authService: AuthService,private fb: FormBuilder,private modalService: NgbModal,
    private httpClient: HttpClient) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    const info = localStorage.getItem('perfilusuario');
    this.idPaciente = info.replace('"','').replace('"','');
    this.editForm = this.fb.group({
      idDiagnostico: new FormControl(),
      Fecha: new FormControl(),
      ClasificacionArea: new FormControl(),
      Clasificacion: new FormControl(),
      Observaciones: new FormControl(),
    });
  }
  ngOnInit() {
    this.getDiagnostico((data) => {
      this.data = data;
      this.filteredData = data;
    });
    this.register = this.fb.group({
      idDiagnostico: [""],
      Fecha: [""],
      ClasificacionArea: ["", [Validators.required]],
      Clasificacion: ["", [Validators.required]],
      Observaciones: [""],
    });
    this.getClasificaciones();
  }

  getDiagnostico(cb) {
    const options: any = {"caso":0,"idPaciente":this.idPaciente};
    this.httpClient.post(`${this.baseURL}diagnostico.php`, JSON.stringify(options), headers)
    .subscribe((data:any)=> {
      this.data=data;
      this.filteredData=data;
      cb(data);
      });
  }
  getClasificaciones() {
    const options: any = { caso: 1};
    this.httpClient.post(`${this.baseURL}diagnostico.php`, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.clasificaciones = respuesta;
      });
  }

  modalEditarDiagnostico(row, rowIndex, content) {
    this.modalService.open(content, {ariaLabelledBy:"modal-basic-title",centered:true,size:"lg"});
    this.editForm.setValue({
      idDiagnostico: row.idDiagnostico,
      Fecha: row.fecha,
      Clasificacion: row.idClasificacion,
      ClasificacionArea: row.pertenece,
      Observaciones: row.observaciones,
    });
    this.selectedRowData = row;
  }
  actualizarDiagnostico(form: FormGroup) {
    const options: any = { caso: 3, 'diagnostico':form.value};
    this.httpClient.post(`${this.baseURL}diagnostico.php`, JSON.stringify(options), headers).subscribe(
      (respuesta:any) => {
        if (respuesta==='Se modifico') {
          Swal.fire("Se modifico el diagnostico correctamente", "", "success");
          this.modalService.dismissAll();
          this.ngOnInit();
        }else{
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }

  modalNuevoDiagnostico(content) {
    this.modalService.open(content, {ariaLabelledBy:"modal-basic-title",centered:true,size:"lg"});
  }
  agregarDiagnostico(form: FormGroup) {
    const options: any = { caso: 2, 'diagnostico':form.value,'idPaciente':this.idPaciente};
    this.httpClient.post(`${this.baseURL}diagnostico.php`, JSON.stringify(options), headers).subscribe(
      (respuesta:any) => {
        if (respuesta==='Se inserto') {
          Swal.fire("Se agrego el diagnostico correctamente", "", "success");
          this.modalService.dismissAll();
          this.ngOnInit();
        }else{
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }

  eliminarDiagnostico(id) {
    const options: any = { caso: 4, 'idDiagnosticos':id};
    this.httpClient.post(`${this.baseURL}diagnostico.php`, JSON.stringify(options), headers).subscribe(
      (respuesta:any) => {
        if (respuesta==='Se elimino') {
          Swal.fire("Se elimino el diagnostico correctamente", "", "success");
          this.modalService.dismissAll();
          this.ngOnInit();
        }else{
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }

  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  refreshTable(){
    this.ngOnInit();
  }

}
export interface selectRowInterface {
  idDiagnostico: number;
  Fecha: String;
  Clasificacion: String;
  Observaciones: String;
}