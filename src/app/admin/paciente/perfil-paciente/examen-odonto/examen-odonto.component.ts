import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import { MatSnackBar, MatSnackBarConfig } from "@angular/material/snack-bar";
import { AlertService } from "src/app/core/service/alert.service";

@Component({
  selector: "app-examen-odonto",
  templateUrl: "./examen-odonto.component.html",
  styles: [],
})
export class ExamenOdontoComponent {
  dientes: any = [];
  mandibulas: any = [];
  especialidades: any = [];
  data = [];
  coronas: any = [];
  coronas_puente: any = [];
  puente_impante: any = [];
  puente_implante_monofasicos: any = [];
  puentes: any = [];
  carillas: any = [];
  implante_de_Corona: any = [];
  corona_en_implante_monofasico: any = [];
  puente_de_implentes: any = [];
  puente_con_implantes_monofasicos: any = [];
  implantes_con_sobredentadura: any = [];
  puente_con_12_unidades_con_6_implante: any = [];
  puenbte_con_12_unidades_con_8_implantes_monofasicos: any = [];
  dentadura_con_4_minimplantes: any = [];
  dentaduras_parciales_con_puente: any = [];
  puente_completo: any = [];
  dentadura_postiza_completa: any = [];
  sobredentadura_sobre_barras: any = [];

  mandibula = [];
  mandibula1 = [2, 3, 4, 5];
  mandibula2 = [6, 7, 8, 9, 10, 11];
  mandibula3 = [12, 13, 14, 15];
  mandibula4 = [18, 19, 20, 21];
  mandibula5 = [22, 23, 24, 25, 26, 27];
  mandibula6 = [28, 29, 30, 31];

  urlImg = "assets/images/mandibula/mandibula.png";
  especialidad = "";

  arregloespecialidad: any = [];
  arreglopuente: any = [];
  dentaduras: any = [];

  setAutoHide: boolean = true;
  autoHide: number = 4000;
  action: boolean = true;

  constructor(
    private sweet: AlertService,
    private http: HttpClient,
    public snackBar: MatSnackBar
  ) {
    this.http.get("assets/data/odontograma.json").subscribe((data: any) => {
      this.dientes = data.dientes;
      this.mandibulas = data.mandibulas;
      this.especialidades = data.especialidades;
      this.coronas = data.coronas;
      this.coronas_puente = data.coronas_puente;
      this.puente_impante = data.puente_implante;
      this.puente_implante_monofasicos = data.puente_implante_monofasicos;
      this.puentes = data.puentes;
      this.carillas = data.carillas;
      this.implante_de_Corona = data.implante_de_Corona;
      this.corona_en_implante_monofasico = data.corona_en_implante_monofasico;
      this.puente_de_implentes = data.puentes;
      this.puente_con_implantes_monofasicos =
        data.puente_con_implantes_monofasicos;
      this.implantes_con_sobredentadura = data.implantes_con_sobredentadura;
      this.puente_con_12_unidades_con_6_implante =
        data.puente_con_12_unidades_con_6_implante;
      this.puenbte_con_12_unidades_con_8_implantes_monofasicos =
        data.puenbte_con_12_unidades_con_8_implantes_monofasicos;
      this.dentadura_con_4_minimplantes = data.dentadura_con_4_minimplantes;
      this.dentaduras_parciales_con_puente =
        data.dentaduras_parciales_con_puente;
      this.puente_completo = data.puente_completo;
      this.dentadura_postiza_completa = data.dentadura_postiza_completa;
      this.sobredentadura_sobre_barras = data.sobredentadura_sobre_barras;
    });
  }

  cambiarMandibula(mandibula) {
    this.urlImg = mandibula.urlimg;
    switch (mandibula.class) {
      case "mandibula1":
        this.mandibula = this.mandibula1;
        break;
      case "mandibula2":
        this.mandibula = this.mandibula2;
        break;
      case "mandibula3":
        this.mandibula = this.mandibula3;
        break;
      case "mandibula4":
        this.mandibula = this.mandibula4;
        break;
      case "mandibula5":
        this.mandibula = this.mandibula5;
        break;
      case "mandibula6":
        this.mandibula = this.mandibula6;
        break;
      default:
        this.mandibula = [];
        break;
    }
  }

  selectEsp(esp) {
    if (this.urlImg === "assets/images/mandibula/mandibula.png") {
      this.sweet.alert(
        "",
        "Debe seleccionar una sección de la boca primero",
        "error"
      );
    }
    this.especialidad = esp.nombre;
    if (this.especialidad === "Puente") {
      console.log(this.mandibula);
      var arraypuente = [];
      for (let index = 0; index < this.mandibula.length; index++) {
        this.dientes.map((diente) => {
          if (diente.id === this.mandibula[index]) {
            this.puentes.map((puente) => {
              if (puente.id === this.mandibula[index]) {
                const comparacion = this.arregloespecialidad.filter(
                  (buscador) => buscador.id === this.mandibula[index]
                );
                if (comparacion.length > 0) {
                  arraypuente.push(comparacion);
                }
              }
            });
          }
        });
      }
      if (this.mandibula.length === arraypuente.length) {
        this.mensajetoast("Todos los dientes han sido tratados");
      } else {
        if (arraypuente.length === 1) {
          if (arraypuente[0][0].id === 6) {
            var arrardientes = [7, 8, 9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 7) {
            var arrardientes = [8, 9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 8) {
            var arrardientes = [9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 9) {
            var arrardientes = [6, 7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 10) {
            var arrardientes = [6, 7, 8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 11) {
            var arrardientes = [6, 7, 8, 9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 8 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 2) {
            var arrardientes = [3, 4, 5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 3 || corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 3) {
            var arrardientes = [4, 5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 4 || corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 4) {
            var arrardientes = [2, 3];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 2 || corona.id === 3) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 5) {
            var arrardientes = [2, 3, 4];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 2 || corona.id === 4) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 12) {
            var arrardientes = [13, 14, 15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 13 || corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 13) {
            var arrardientes = [14, 15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 14 || corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 14) {
            var arrardientes = [12, 13];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 12 || corona.id === 13) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 15) {
            var arrardientes = [12, 13, 14];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 12 || corona.id === 14) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 22) {
            var arrardientes = [23, 24, 25, 26, 27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (
                    corona.id === 23 ||
                    corona.id === 25 ||
                    corona.id === 27
                  ) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 23) {
            var arrardientes = [24, 25, 26, 27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 24 || corona.id === 27) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 24) {
            var arrardientes = [25, 26, 27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 25 || corona.id === 27) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 25) {
            var arrardientes = [22, 23, 24];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22 || corona.id === 24) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 26) {
            var arrardientes = [22, 23, 24, 25];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22 || corona.id === 25) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 27) {
            var arrardientes = [22, 23, 24, 25, 26];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (
                    corona.id === 22 ||
                    corona.id === 24 ||
                    corona.id === 26
                  ) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 28) {
            var arrardientes = [29, 30, 31];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 29 || corona.id === 31) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 29) {
            var arrardientes = [30, 31];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 30 || corona.id === 31) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 30) {
            var arrardientes = [28, 29];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 28 || corona.id === 29) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 31) {
            var arrardientes = [28, 29, 30];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 28 || corona.id === 30) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 18) {
            var arrardientes = [19, 20, 21];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 19 || corona.id === 21) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 19) {
            var arrardientes = [20, 21];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 20 || corona.id === 21) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 20) {
            var arrardientes = [18, 19];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 18 || corona.id === 19) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 21) {
            var arrardientes = [18, 19, 20];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 18 || corona.id === 20) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else if (arraypuente.length === 2) {
          if (arraypuente[0][0].id === 6 && arraypuente[1][0].id === 7) {
            var arrardientes = [8, 9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 6 && arraypuente[1][0].id === 8) {
            var arrardientes = [9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 6 && arraypuente[1][0].id === 9) {
            var arrardientes = [7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 10
          ) {
            var arrardientes = [7, 8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [7, 8, 9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 7 && arraypuente[1][0].id === 8) {
            var arrardientes = [9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 7 && arraypuente[1][0].id === 9) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 10
          ) {
            var arrardientes = [8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [8, 9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 8 && arraypuente[1][0].id === 9) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 10
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 9 &&
            arraypuente[1][0].id === 10
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 9 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 10 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [6, 7, 8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 2 && arraypuente[1][0].id === 3) {
            var arrardientes = [4, 5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 4 || corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 2 && arraypuente[1][0].id === 4) {
            var arrardientes = [5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 2 && arraypuente[1][0].id === 5) {
            var arrardientes = [3, 4];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 3 || corona.id === 4) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 3 && arraypuente[1][0].id === 4) {
            var arrardientes = [5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 3 && arraypuente[1][0].id === 5) {
            var arrardientes = [4];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 4) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 4 && arraypuente[1][0].id === 5) {
            var arrardientes = [2, 3];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 2 || corona.id === 3) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 13
          ) {
            var arrardientes = [14, 15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 14 || corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 14
          ) {
            var arrardientes = [15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 15
          ) {
            var arrardientes = [13, 14];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 13 || corona.id === 14) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 13 &&
            arraypuente[1][0].id === 14
          ) {
            var arrardientes = [15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 13 &&
            arraypuente[1][0].id === 15
          ) {
            var arrardientes = [14];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 14) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 14 &&
            arraypuente[1][0].id === 15
          ) {
            var arrardientes = [12, 13];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 12 || corona.id === 13) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23
          ) {
            var arrardientes = [24, 25, 26, 27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 24 || corona.id === 27) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 24
          ) {
            var arrardientes = [25, 26, 27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 25 || corona.id === 27) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 25
          ) {
            var arrardientes = [23, 24];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 23 || corona.id === 24) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 26
          ) {
            var arrardientes = [23, 24, 25];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 23 || corona.id === 25) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 27
          ) {
            var arrardientes = [23, 24, 25, 26];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 23 || corona.id === 26) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 24
          ) {
            var arrardientes = [25, 26, 27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 25 || corona.id === 27) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 25
          ) {
            var arrardientes = [26, 27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 26 || corona.id === 27) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 26
          ) {
            var arrardientes = [24, 25];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 24 || corona.id === 25) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 27
          ) {
            var arrardientes = [24, 25, 26];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 24 || corona.id === 26) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 24 &&
            arraypuente[1][0].id === 25
          ) {
            var arrardientes = [22, 23];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22 || corona.id === 23) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 24 &&
            arraypuente[1][0].id === 26
          ) {
            var arrardientes = [22, 23];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22 || corona.id === 23) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 24 &&
            arraypuente[1][0].id === 27
          ) {
            var arrardientes = [22, 23];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22 || corona.id === 23) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 25 &&
            arraypuente[1][0].id === 26
          ) {
            var arrardientes = [22];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 25 &&
            arraypuente[1][0].id === 27
          ) {
            var arrardientes = [22];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 26 &&
            arraypuente[1][0].id === 27
          ) {
            var arrardientes = [22, 23, 24, 25];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22 || corona.id === 25) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 28 &&
            arraypuente[1][0].id === 29
          ) {
            var arrardientes = [30, 31];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 30 || corona.id === 31) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 28 &&
            arraypuente[1][0].id === 30
          ) {
            var arrardientes = [31];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 31) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 28 &&
            arraypuente[1][0].id === 31
          ) {
            var arrardientes = [29, 30];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 29 || corona.id === 30) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 29 &&
            arraypuente[1][0].id === 30
          ) {
            var arrardientes = [31];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 31) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 29 &&
            arraypuente[1][0].id === 31
          ) {
            var arrardientes = [30];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 30) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 30 &&
            arraypuente[1][0].id === 31
          ) {
            var arrardientes = [28, 29];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 28 || corona.id === 29) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 18 &&
            arraypuente[1][0].id === 19
          ) {
            var arrardientes = [20, 21];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 20 || corona.id === 21) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 18 &&
            arraypuente[1][0].id === 20
          ) {
            var arrardientes = [21];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 21) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 18 &&
            arraypuente[1][0].id === 21
          ) {
            var arrardientes = [19, 20];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 19 || corona.id === 20) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 19 &&
            arraypuente[1][0].id === 20
          ) {
            var arrardientes = [21];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 21) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 19 &&
            arraypuente[1][0].id === 21
          ) {
            var arrardientes = [20];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 20) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 20 &&
            arraypuente[1][0].id === 21
          ) {
            var arrardientes = [18, 19];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 18 || corona.id === 19) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else if (arraypuente.length === 3) {
          if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8
          ) {
            var arrardientes = [9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 9
          ) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [8, 9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9
          ) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 10 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [7, 8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9
          ) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 10 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 10 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 9 &&
            arraypuente[1][0].id === 10 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6, 7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 2 &&
            arraypuente[1][0].id === 3 &&
            arraypuente[2][0].id === 4
          ) {
            var arrardientes = [5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 2 &&
            arraypuente[1][0].id === 3 &&
            arraypuente[2][0].id === 5
          ) {
            var arrardientes = [4];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 4) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 2 &&
            arraypuente[1][0].id === 4 &&
            arraypuente[2][0].id === 5
          ) {
            var arrardientes = [3];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 3) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 3 &&
            arraypuente[1][0].id === 4 &&
            arraypuente[2][0].id === 5
          ) {
            var arrardientes = [2];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 2) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 13 &&
            arraypuente[2][0].id === 14
          ) {
            var arrardientes = [15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 13 &&
            arraypuente[2][0].id === 15
          ) {
            var arrardientes = [14];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 14) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 14 &&
            arraypuente[2][0].id === 15
          ) {
            var arrardientes = [13];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 13) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 13 &&
            arraypuente[1][0].id === 14 &&
            arraypuente[2][0].id === 15
          ) {
            var arrardientes = [12];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 12) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 24
          ) {
            var arrardientes = [25, 26, 27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 25 || corona.id === 27) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 25
          ) {
            var arrardientes = [26, 27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 26 || corona.id === 27) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 26
          ) {
            var arrardientes = [24, 25];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 24 || corona.id === 25) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 27
          ) {
            var arrardientes = [24, 25, 26];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 24 || corona.id === 26) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 25
          ) {
            var arrardientes = [26, 27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 26 || corona.id === 27) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 26
          ) {
            var arrardientes = [23];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 23) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 27
          ) {
            var arrardientes = [25, 26];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 25 || corona.id === 26) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 25 &&
            arraypuente[2][0].id === 26
          ) {
            var arrardientes = [23, 24];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 23 || corona.id === 24) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 25 &&
            arraypuente[2][0].id === 27
          ) {
            var arrardientes = [23, 24];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 23 || corona.id === 24) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 26 &&
            arraypuente[2][0].id === 27
          ) {
            var arrardientes = [23, 24, 25];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 23 || corona.id === 25) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 25
          ) {
            var arrardientes = [26, 27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 26 || corona.id === 27) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 26
          ) {
            var arrardientes = [22];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 27
          ) {
            var arrardientes = [22];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 25 &&
            arraypuente[2][0].id === 26
          ) {
            var arrardientes = [22];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 25 &&
            arraypuente[2][0].id === 27
          ) {
            var arrardientes = [22];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 26 &&
            arraypuente[2][0].id === 27
          ) {
            var arrardientes = [24, 25];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 24 || corona.id === 25) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 24 &&
            arraypuente[1][0].id === 25 &&
            arraypuente[2][0].id === 26
          ) {
            var arrardientes = [22, 23];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22 || corona.id === 23) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 24 &&
            arraypuente[1][0].id === 25 &&
            arraypuente[2][0].id === 27
          ) {
            var arrardientes = [22, 23];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22 || corona.id === 23) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 24 &&
            arraypuente[1][0].id === 26 &&
            arraypuente[2][0].id === 27
          ) {
            var arrardientes = [22, 23];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22 || corona.id === 23) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 25 &&
            arraypuente[1][0].id === 26 &&
            arraypuente[2][0].id === 27
          ) {
            var arrardientes = [22, 23, 24];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22 || corona.id === 24) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 28 &&
            arraypuente[1][0].id === 29 &&
            arraypuente[28][0].id === 30
          ) {
            var arrardientes = [31];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 31) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 28 &&
            arraypuente[1][0].id === 29 &&
            arraypuente[28][0].id === 31
          ) {
            var arrardientes = [30];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 30) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 28 &&
            arraypuente[1][0].id === 30 &&
            arraypuente[28][0].id === 31
          ) {
            var arrardientes = [29];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 29) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 29 &&
            arraypuente[1][0].id === 30 &&
            arraypuente[28][0].id === 31
          ) {
            var arrardientes = [28];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 28) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 18 &&
            arraypuente[1][0].id === 19 &&
            arraypuente[2][0].id === 20
          ) {
            var arrardientes = [21];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 21) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 18 &&
            arraypuente[1][0].id === 19 &&
            arraypuente[2][0].id === 21
          ) {
            var arrardientes = [20];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 20) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 18 &&
            arraypuente[1][0].id === 20 &&
            arraypuente[2][0].id === 21
          ) {
            var arrardientes = [19];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 19) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 19 &&
            arraypuente[1][0].id === 20 &&
            arraypuente[2][0].id === 21
          ) {
            var arrardientes = [18];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 18) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else if (arraypuente.length === 4) {
          if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 9
          ) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 10
          ) {
            var arrardientes = [9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10
          ) {
            var arrardientes = [8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 24 &&
            arraypuente[3][0].id === 25
          ) {
            var arrardientes = [26, 27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 26 || corona.id === 27) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 24 &&
            arraypuente[3][0].id === 26
          ) {
            var arrardientes = [25];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 25) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 24 &&
            arraypuente[3][0].id === 27
          ) {
            var arrardientes = [25, 26];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 25 || corona.id === 26) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 25 &&
            arraypuente[3][0].id === 26
          ) {
            var arrardientes = [24];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 24) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 25 &&
            arraypuente[3][0].id === 27
          ) {
            var arrardientes = [24];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 24) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 26 &&
            arraypuente[3][0].id === 27
          ) {
            var arrardientes = [24, 25];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 24 || corona.id === 25) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 25 &&
            arraypuente[3][0].id === 26
          ) {
            var arrardientes = [23];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 23) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 25 &&
            arraypuente[3][0].id === 27
          ) {
            var arrardientes = [23];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 23) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 26 &&
            arraypuente[3][0].id === 27
          ) {
            var arrardientes = [23];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 23) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 25 &&
            arraypuente[2][0].id === 26 &&
            arraypuente[3][0].id === 27
          ) {
            var arrardientes = [23, 24];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 23 || corona.id === 24) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 25 &&
            arraypuente[3][0].id === 26
          ) {
            var arrardientes = [22];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 25 &&
            arraypuente[3][0].id === 27
          ) {
            var arrardientes = [22];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 26 &&
            arraypuente[3][0].id === 27
          ) {
            var arrardientes = [22];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 25 &&
            arraypuente[2][0].id === 26 &&
            arraypuente[3][0].id === 27
          ) {
            var arrardientes = [22];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 24 &&
            arraypuente[1][0].id === 25 &&
            arraypuente[2][0].id === 26 &&
            arraypuente[3][0].id === 27
          ) {
            var arrardientes = [22, 23];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22 || corona.id === 23) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else if (arraypuente.length === 5) {
          if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 9 &&
            arraypuente[4][0].id === 10
          ) {
            var arrardientes = [11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 9 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 10 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 24 &&
            arraypuente[3][0].id === 25 &&
            arraypuente[4][0].id === 26
          ) {
            var arrardientes = [27];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 27) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 24 &&
            arraypuente[3][0].id === 25 &&
            arraypuente[4][0].id === 27
          ) {
            var arrardientes = [26];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 26) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 24 &&
            arraypuente[3][0].id === 26 &&
            arraypuente[4][0].id === 27
          ) {
            var arrardientes = [25];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 25) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 23 &&
            arraypuente[2][0].id === 25 &&
            arraypuente[3][0].id === 26 &&
            arraypuente[4][0].id === 27
          ) {
            var arrardientes = [24];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 24) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 22 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 25 &&
            arraypuente[3][0].id === 26 &&
            arraypuente[4][0].id === 27
          ) {
            var arrardientes = [23];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 23) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 23 &&
            arraypuente[1][0].id === 24 &&
            arraypuente[2][0].id === 25 &&
            arraypuente[3][0].id === 26 &&
            arraypuente[4][0].id === 27
          ) {
            var arrardientes = [22];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.coronas.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 22) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente";
                  } else {
                    corona.nombre = "puente";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else {
          for (let index = 0; index < this.mandibula.length; index++) {
            this.dientes.map((diente) => {
              if (diente.id === this.mandibula[index]) {
                this.puentes.map((puente) => {
                  if (puente.id === this.mandibula[index]) {
                    puente.class = `puente${this.mandibula[index]} d-block`;
                    diente.class = `diente${this.mandibula[index]} d-block`;
                  }
                });
              }
            });
            if (this.mandibula.includes(2)) {
              this.coronas_puente.map((corona) => {
                if (corona.id === this.mandibula[index]) {
                  if (corona.id === 2 || corona.id === 5) {
                    corona.class = `corona${this.mandibula[index]} d-block`;
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            } else if (this.mandibula.includes(11)) {
              this.coronas_puente.map((corona) => {
                if (corona.id === this.mandibula[index]) {
                  if (
                    corona.id === 6 ||
                    corona.id === 11 ||
                    corona.id === 8 ||
                    corona.id === 9
                  ) {
                    corona.class = `corona${this.mandibula[index]} d-block`;
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            } else if (this.mandibula.includes(15)) {
              this.coronas_puente.map((corona) => {
                if (corona.id === this.mandibula[index]) {
                  if (corona.id === 12 || corona.id === 15) {
                    corona.class = `corona${this.mandibula[index]} d-block`;
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            } else if (this.mandibula.includes(31)) {
              this.coronas_puente.map((corona) => {
                if (corona.id === this.mandibula[index]) {
                  if (corona.id === 28 || corona.id === 31) {
                    corona.class = `corona${this.mandibula[index]} d-block`;
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            } else if (this.mandibula.includes(27)) {
              this.coronas_puente.map((corona) => {
                if (corona.id === this.mandibula[index]) {
                  if (
                    corona.id === 22 ||
                    corona.id === 27 ||
                    corona.id === 24 ||
                    corona.id === 25
                  ) {
                    corona.class = `corona${this.mandibula[index]} d-block`;
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            } else if (this.mandibula.includes(21)) {
              this.coronas_puente.map((corona) => {
                if (corona.id === this.mandibula[index]) {
                  if (corona.id === 18 || corona.id === 21) {
                    corona.class = `corona${this.mandibula[index]} d-block`;
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            } else {
              console.log("falso");
            }
          }
        }
      }
      console.log(this.arregloespecialidad);
    } else if (this.especialidad === "Puente de implentes") {
      console.log(this.mandibula);
      var arraypuente = [];
      for (let index = 0; index < this.mandibula.length; index++) {
        this.dientes.map((diente) => {
          if (diente.id === this.mandibula[index]) {
            this.puentes.map((puente) => {
              if (puente.id === this.mandibula[index]) {
                const comparacion = this.arregloespecialidad.filter(
                  (buscador) => buscador.id === this.mandibula[index]
                );
                if (comparacion.length > 0) {
                  arraypuente.push(comparacion);
                }
              }
            });
          }
        });
      }
      if (this.mandibula.length === arraypuente.length) {
        this.mensajetoast("Todos los dientes han sido tratados");
      } else {
        if (arraypuente.length === 1) {
          if (arraypuente[0][0].id === 6) {
            var arrardientes = [7, 8, 9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((implante) => {
                if (implante.id === arrardientes[index]) {
                  if (
                    implante.id === 7 ||
                    implante.id === 9 ||
                    implante.id === 11
                  ) {
                    implante.class = `corona${arrardientes[index]} d-block`;
                    implante.nombre = "puente de implantes";
                  } else {
                    implante.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(implante);
                }
              });
            }
          } else if (arraypuente[0][0].id === 7) {
            var arrardientes = [8, 9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((implante) => {
                if (implante.id === arrardientes[index]) {
                  if (implante.id === 8 || implante.id === 11) {
                    implante.class = `corona${arrardientes[index]} d-block`;
                    implante.nombre = "puente de implantes";
                  } else {
                    implante.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(implante);
                }
              });
            }
          } else if (arraypuente[0][0].id === 8) {
            var arrardientes = [9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 9) {
            var arrardientes = [6, 7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 10) {
            var arrardientes = [6, 7, 8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 11) {
            var arrardientes = [6, 7, 8, 9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 8 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 2) {
            var arrardientes = [3, 4, 5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 3 || corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 3) {
            var arrardientes = [4, 5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 4 || corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 4) {
            var arrardientes = [2, 3];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 2 || corona.id === 3) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 5) {
            var arrardientes = [2, 3, 4];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 2 || corona.id === 4) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 12) {
            var arrardientes = [13, 14, 15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 13 || corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 13) {
            var arrardientes = [14, 15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 14 || corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 14) {
            var arrardientes = [12, 13];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 12 || corona.id === 13) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 15) {
            var arrardientes = [12, 13, 14];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 12 || corona.id === 14) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else if (arraypuente.length === 2) {
          if (arraypuente[0][0].id === 6 && arraypuente[1][0].id === 7) {
            var arrardientes = [8, 9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 6 && arraypuente[1][0].id === 8) {
            var arrardientes = [9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 6 && arraypuente[1][0].id === 9) {
            var arrardientes = [7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 10
          ) {
            var arrardientes = [7, 8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [7, 8, 9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 7 && arraypuente[1][0].id === 8) {
            var arrardientes = [9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 7 && arraypuente[1][0].id === 9) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 10
          ) {
            var arrardientes = [8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [8, 9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 8 && arraypuente[1][0].id === 9) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 10
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 9 &&
            arraypuente[1][0].id === 10
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 9 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 10 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [6, 7, 8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 2 && arraypuente[1][0].id === 3) {
            var arrardientes = [4, 5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 4 || corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 2 && arraypuente[1][0].id === 4) {
            var arrardientes = [5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 2 && arraypuente[1][0].id === 5) {
            var arrardientes = [3, 4];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 3 || corona.id === 4) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 3 && arraypuente[1][0].id === 4) {
            var arrardientes = [5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 3 && arraypuente[1][0].id === 5) {
            var arrardientes = [4];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 4) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 4 && arraypuente[1][0].id === 5) {
            var arrardientes = [2, 3];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 2 || corona.id === 3) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 13
          ) {
            var arrardientes = [14, 15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 14 || corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 14
          ) {
            var arrardientes = [15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 15
          ) {
            var arrardientes = [13, 14];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 13 || corona.id === 14) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 13 &&
            arraypuente[1][0].id === 14
          ) {
            var arrardientes = [15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 13 &&
            arraypuente[1][0].id === 15
          ) {
            var arrardientes = [14];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 14) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 14 &&
            arraypuente[1][0].id === 15
          ) {
            var arrardientes = [12, 13];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 12 || corona.id === 13) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else if (arraypuente.length === 3) {
          if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8
          ) {
            var arrardientes = [9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 9
          ) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [8, 9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9
          ) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 10 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [7, 8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9
          ) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 10 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 10 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 9 &&
            arraypuente[1][0].id === 10 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6, 7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 2 &&
            arraypuente[1][0].id === 3 &&
            arraypuente[2][0].id === 4
          ) {
            var arrardientes = [5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 2 &&
            arraypuente[1][0].id === 3 &&
            arraypuente[2][0].id === 5
          ) {
            var arrardientes = [4];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 4) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 2 &&
            arraypuente[1][0].id === 4 &&
            arraypuente[2][0].id === 5
          ) {
            var arrardientes = [3];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 3) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 3 &&
            arraypuente[1][0].id === 4 &&
            arraypuente[2][0].id === 5
          ) {
            var arrardientes = [2];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 2) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 13 &&
            arraypuente[2][0].id === 14
          ) {
            var arrardientes = [15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 13 &&
            arraypuente[2][0].id === 15
          ) {
            var arrardientes = [14];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 14) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 14 &&
            arraypuente[2][0].id === 15
          ) {
            var arrardientes = [13];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 13) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 13 &&
            arraypuente[1][0].id === 14 &&
            arraypuente[2][0].id === 15
          ) {
            var arrardientes = [12];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 12) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else if (arraypuente.length === 4) {
          if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 9
          ) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 10
          ) {
            var arrardientes = [9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10
          ) {
            var arrardientes = [8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else if (arraypuente.length === 5) {
          if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 9 &&
            arraypuente[4][0].id === 10
          ) {
            var arrardientes = [11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 9 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 10 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_impante.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente de implantes";
                  } else {
                    corona.nombre = "puente de implantes";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else {
          for (let index = 0; index < this.mandibula.length; index++) {
            this.dientes.map((diente) => {
              if (diente.id === this.mandibula[index]) {
                this.puentes.map((puente) => {
                  if (puente.id === this.mandibula[index]) {
                    puente.class = `puente${this.mandibula[index]} d-block`;
                    diente.class = `diente${this.mandibula[index]} d-block`;
                  }
                });
              }
            });
            if (this.mandibula.includes(2)) {
              this.puente_impante.map((implante) => {
                if (implante.id === this.mandibula[index]) {
                  if (implante.id === 2 || implante.id === 5) {
                    implante.class = `corona${this.mandibula[index]} d-block`;
                  }
                  this.arregloespecialidad.push(implante);
                }
              });
            } else if (this.mandibula.includes(11)) {
              this.puente_impante.map((implante) => {
                if (implante.id === this.mandibula[index]) {
                  if (
                    implante.id === 6 ||
                    implante.id === 11 ||
                    implante.id === 8 ||
                    implante.id === 9
                  ) {
                    implante.class = `corona${this.mandibula[index]} d-block`;
                  }
                  this.arregloespecialidad.push(implante);
                }
              });
            } else if (this.mandibula.includes(15)) {
              this.puente_impante.map((implante) => {
                if (implante.id === this.mandibula[index]) {
                  if (implante.id === 12 || implante.id === 15) {
                    implante.class = `corona${this.mandibula[index]} d-block`;
                  }
                  this.arregloespecialidad.push(implante);
                }
              });
            } else if (this.mandibula.includes(31)) {
            } else if (this.mandibula.includes(27)) {
            } else if (this.mandibula.includes(21)) {
            } else {
              console.log("falso");
            }
          }
        }
      }
      console.log(this.arregloespecialidad);
    } else if (this.especialidad === "Puente con implantes monofasicos") {
      console.log(this.mandibula);
      var arraypuente = [];
      for (let index = 0; index < this.mandibula.length; index++) {
        this.dientes.map((diente) => {
          if (diente.id === this.mandibula[index]) {
            this.puentes.map((puente) => {
              if (puente.id === this.mandibula[index]) {
                const comparacion = this.arregloespecialidad.filter(
                  (buscador) => buscador.id === this.mandibula[index]
                );
                if (comparacion.length > 0) {
                  arraypuente.push(comparacion);
                }
              }
            });
          }
        });
      }
      if (this.mandibula.length === arraypuente.length) {
        this.mensajetoast("Todos los dientes han sido tratados");
      } else {
        if (arraypuente.length === 1) {
          if (arraypuente[0][0].id === 6) {
            var arrardientes = [7, 8, 9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((implante) => {
                if (implante.id === arrardientes[index]) {
                  if (
                    implante.id === 7 ||
                    implante.id === 9 ||
                    implante.id === 11
                  ) {
                    implante.class = `corona${arrardientes[index]} d-block`;
                    implante.nombre = "puente con implantes monofasicos";
                  } else {
                    implante.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(implante);
                }
              });
            }
          } else if (arraypuente[0][0].id === 7) {
            var arrardientes = [8, 9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((implante) => {
                if (implante.id === arrardientes[index]) {
                  if (implante.id === 8 || implante.id === 11) {
                    implante.class = `corona${arrardientes[index]} d-block`;
                    implante.nombre = "puente con implantes monofasicos";
                  } else {
                    implante.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(implante);
                }
              });
            }
          } else if (arraypuente[0][0].id === 8) {
            var arrardientes = [9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 9) {
            var arrardientes = [6, 7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 10) {
            var arrardientes = [6, 7, 8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 11) {
            var arrardientes = [6, 7, 8, 9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 8 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 2) {
            var arrardientes = [3, 4, 5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 3 || corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 3) {
            var arrardientes = [4, 5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 4 || corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 4) {
            var arrardientes = [2, 3];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 2 || corona.id === 3) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 5) {
            var arrardientes = [2, 3, 4];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 2 || corona.id === 4) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 12) {
            var arrardientes = [13, 14, 15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 13 || corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 13) {
            var arrardientes = [14, 15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 14 || corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 14) {
            var arrardientes = [12, 13];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 12 || corona.id === 13) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 15) {
            var arrardientes = [12, 13, 14];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 12 || corona.id === 14) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else if (arraypuente.length === 2) {
          if (arraypuente[0][0].id === 6 && arraypuente[1][0].id === 7) {
            var arrardientes = [8, 9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 6 && arraypuente[1][0].id === 8) {
            var arrardientes = [9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 6 && arraypuente[1][0].id === 9) {
            var arrardientes = [7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 10
          ) {
            var arrardientes = [7, 8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [7, 8, 9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 7 && arraypuente[1][0].id === 8) {
            var arrardientes = [9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 7 && arraypuente[1][0].id === 9) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 10
          ) {
            var arrardientes = [8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [8, 9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 8 && arraypuente[1][0].id === 9) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 10
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 9 &&
            arraypuente[1][0].id === 10
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 9 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 10 &&
            arraypuente[1][0].id === 11
          ) {
            var arrardientes = [6, 7, 8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 2 && arraypuente[1][0].id === 3) {
            var arrardientes = [4, 5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 4 || corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 2 && arraypuente[1][0].id === 4) {
            var arrardientes = [5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 2 && arraypuente[1][0].id === 5) {
            var arrardientes = [3, 4];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 3 || corona.id === 4) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 3 && arraypuente[1][0].id === 4) {
            var arrardientes = [5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 3 && arraypuente[1][0].id === 5) {
            var arrardientes = [4];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 4) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (arraypuente[0][0].id === 4 && arraypuente[1][0].id === 5) {
            var arrardientes = [2, 3];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 2 || corona.id === 3) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 13
          ) {
            var arrardientes = [14, 15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 14 || corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 14
          ) {
            var arrardientes = [15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 15
          ) {
            var arrardientes = [13, 14];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 13 || corona.id === 14) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 13 &&
            arraypuente[1][0].id === 14
          ) {
            var arrardientes = [15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 13 &&
            arraypuente[1][0].id === 15
          ) {
            var arrardientes = [14];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 14) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 14 &&
            arraypuente[1][0].id === 15
          ) {
            var arrardientes = [12, 13];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 12 || corona.id === 13) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else if (arraypuente.length === 3) {
          if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8
          ) {
            var arrardientes = [9, 10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 9
          ) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [8, 9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9
          ) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 10 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [7, 8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9
          ) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 10 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 10 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 9 &&
            arraypuente[1][0].id === 10 &&
            arraypuente[2][0].id === 11
          ) {
            var arrardientes = [6, 7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 2 &&
            arraypuente[1][0].id === 3 &&
            arraypuente[2][0].id === 4
          ) {
            var arrardientes = [5];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 5) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 2 &&
            arraypuente[1][0].id === 3 &&
            arraypuente[2][0].id === 5
          ) {
            var arrardientes = [4];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 4) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 2 &&
            arraypuente[1][0].id === 4 &&
            arraypuente[2][0].id === 5
          ) {
            var arrardientes = [3];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 3) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 3 &&
            arraypuente[1][0].id === 4 &&
            arraypuente[2][0].id === 5
          ) {
            var arrardientes = [2];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 2) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 13 &&
            arraypuente[2][0].id === 14
          ) {
            var arrardientes = [15];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 15) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 13 &&
            arraypuente[2][0].id === 15
          ) {
            var arrardientes = [14];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 14) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 12 &&
            arraypuente[1][0].id === 14 &&
            arraypuente[2][0].id === 15
          ) {
            var arrardientes = [13];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 13) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 13 &&
            arraypuente[1][0].id === 14 &&
            arraypuente[2][0].id === 15
          ) {
            var arrardientes = [12];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 12) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else if (arraypuente.length === 4) {
          if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 9
          ) {
            var arrardientes = [10, 11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10 || corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 10
          ) {
            var arrardientes = [9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [9, 10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9 || corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10
          ) {
            var arrardientes = [8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [8, 9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8 || corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [7, 8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7 || corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 8 &&
            arraypuente[1][0].id === 9 &&
            arraypuente[2][0].id === 10 &&
            arraypuente[3][0].id === 11
          ) {
            var arrardientes = [6, 7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6 || corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else if (arraypuente.length === 5) {
          if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 9 &&
            arraypuente[4][0].id === 10
          ) {
            var arrardientes = [11];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 11) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 9 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [10];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 10) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 8 &&
            arraypuente[3][0].id === 10 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [9];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 9) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 7 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [8];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 8) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 6 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [7];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 7) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          } else if (
            arraypuente[0][0].id === 7 &&
            arraypuente[1][0].id === 8 &&
            arraypuente[2][0].id === 9 &&
            arraypuente[3][0].id === 10 &&
            arraypuente[4][0].id === 11
          ) {
            var arrardientes = [6];
            for (let index = 0; index < arrardientes.length; index++) {
              this.dientes.map((diente) => {
                if (diente.id === arrardientes[index]) {
                  this.puentes.map((puente) => {
                    if (puente.id === arrardientes[index]) {
                      puente.class = `puente${arrardientes[index]} d-block`;
                      diente.class = `diente${arrardientes[index]} d-block`;
                    }
                  });
                }
              });
              this.puente_implante_monofasicos.map((corona) => {
                if (corona.id === arrardientes[index]) {
                  if (corona.id === 6) {
                    corona.class = `corona${arrardientes[index]} d-block`;
                    corona.nombre = "puente con implantes monofasicos";
                  } else {
                    corona.nombre = "puente con implantes monofasicos";
                  }
                  this.arregloespecialidad.push(corona);
                }
              });
            }
          }
        } else {
          for (let index = 0; index < this.mandibula.length; index++) {
            this.dientes.map((diente) => {
              if (diente.id === this.mandibula[index]) {
                this.puentes.map((puente) => {
                  if (puente.id === this.mandibula[index]) {
                    puente.class = `puente${this.mandibula[index]} d-block`;
                    diente.class = `diente${this.mandibula[index]} d-block`;
                  }
                });
              }
            });
            if (this.mandibula.includes(2)) {
              this.puente_implante_monofasicos.map((implante) => {
                if (implante.id === this.mandibula[index]) {
                  if (implante.id === 2 || implante.id === 5) {
                    implante.class = `corona${this.mandibula[index]} d-block`;
                  }
                  this.arregloespecialidad.push(implante);
                }
              });
            } else if (this.mandibula.includes(11)) {
              this.puente_implante_monofasicos.map((implante) => {
                if (implante.id === this.mandibula[index]) {
                  if (
                    implante.id === 6 ||
                    implante.id === 11 ||
                    implante.id === 8 ||
                    implante.id === 9
                  ) {
                    implante.class = `corona${this.mandibula[index]} d-block`;
                  }
                  this.arregloespecialidad.push(implante);
                }
              });
            } else if (this.mandibula.includes(15)) {
              this.puente_implante_monofasicos.map((implante) => {
                if (implante.id === this.mandibula[index]) {
                  if (implante.id === 12 || implante.id === 15) {
                    implante.class = `corona${this.mandibula[index]} d-block`;
                  }
                  this.arregloespecialidad.push(implante);
                }
              });
            } else if (this.mandibula.includes(31)) {
            } else if (this.mandibula.includes(27)) {
            } else if (this.mandibula.includes(21)) {
            } else {
              console.log("falso");
            }
          }
        }
      }
      console.log(this.arregloespecialidad);
    } else if (this.especialidad === "4 implantes con sobredentadura") {
      if (
        this.mandibula.includes(2) ||
        this.mandibula.includes(11) ||
        this.mandibula.includes(15)
      ) {
        this.limpiarcuadrante();
        console.log("arriba");
        var arrardientes = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        for (let index = 0; index < arrardientes.length; index++) {
          this.dientes.map((diente) => {
            if (diente.id === arrardientes[index]) {
              this.puentes.map((puente) => {
                if (puente.id === arrardientes[index]) {
                  puente.class = `puente${arrardientes[index]} d-block`;
                  diente.class = `diente${arrardientes[index]} d-block`;
                }
              });
            }
          });
          this.puente_implante_monofasicos.map((corona) => {
            if (corona.id === arrardientes[index]) {
              if (
                corona.id === 5 ||
                corona.id === 7 ||
                corona.id === 10 ||
                corona.id === 12
              ) {
                corona.class = `corona${arrardientes[index]} d-block`;
                corona.nombre = "4 implantes con sobredentadura";
              } else {
                corona.nombre = "4 implantes con sobredentadura";
              }
              this.arregloespecialidad.push(corona);
            }
          });
        }
      } else {
        console.log("abajo");
        this.limpiarcuadrante();
        var arrardientes = [
          28, 29, 30, 31, 22, 23, 24, 25, 26, 27, 18, 19, 20, 21,
        ];
        for (let index = 0; index < arrardientes.length; index++) {
          this.dientes.map((diente) => {
            if (diente.id === arrardientes[index]) {
              this.puentes.map((puente) => {
                if (puente.id === arrardientes[index]) {
                  puente.class = `puente${arrardientes[index]} d-block`;
                  diente.class = `diente${arrardientes[index]} d-block`;
                }
              });
            }
          });
          this.puente_implante_monofasicos.map((corona) => {
            if (corona.id === arrardientes[index]) {
              if (
                corona.id === 28 ||
                corona.id === 23 ||
                corona.id === 26 ||
                corona.id === 21
              ) {
                corona.class = `corona${arrardientes[index]} d-block`;
                corona.nombre = "4 implantes con sobredentadura";
              } else {
                corona.nombre = "4 implantes con sobredentadura";
              }
              this.arregloespecialidad.push(corona);
            }
          });
        }
      }
      console.log(this.arregloespecialidad);
    } else if (this.especialidad === "Dentadura postiza completa") {
      this.dentadura_postiza_completa.map((dentadura) => {
        if (dentadura.id === 1) {
          if (
            this.mandibula === this.mandibula1 ||
            this.mandibula === this.mandibula2 ||
            this.mandibula === this.mandibula3
          ) {
            dentadura.class =
              dentadura.class == `dentadura1 d-block`
                ? `dentadura1 d-none`
                : "dentadura1 d-block";
          } else return;
        } else if (
          this.mandibula === this.mandibula4 ||
          this.mandibula === this.mandibula5 ||
          this.mandibula === this.mandibula6
        ) {
          dentadura.class =
            dentadura.class == `dentadura3 d-block`
              ? `dentadura3 d-none`
              : `dentadura3 d-block`;
        } else return;
      });
    } else if (this.especialidad === "Dentaduras parciales con puente") {
      this.dentaduras_parciales_con_puente.map((dentadura) => {
        if (dentadura.id === 1) {
          if (
            this.mandibula === this.mandibula1 ||
            this.mandibula === this.mandibula2 ||
            this.mandibula === this.mandibula3
          ) {
            dentadura.class =
              dentadura.class == `dentadura2 d-block`
                ? `dentadura2 d-none`
                : "dentadura2 d-block";
          } else return;
        } else if (
          this.mandibula === this.mandibula4 ||
          this.mandibula === this.mandibula5 ||
          this.mandibula === this.mandibula6
        ) {
          dentadura.class =
            dentadura.class == `dentadura6 d-block`
              ? `dentadura6 d-none`
              : `dentadura6 d-block`;
        } else return;
      });
    } else if (this.especialidad === "Sobredentadura sobre barras") {
      this.sobredentadura_sobre_barras.map((dentadura) => {
        if (dentadura.id === 1) {
          if (
            this.mandibula === this.mandibula1 ||
            this.mandibula === this.mandibula2 ||
            this.mandibula === this.mandibula3
          ) {
            dentadura.class =
              dentadura.class == `dentadura4 d-block`
                ? `dentadura4 d-none`
                : "dentadura4 d-block";
          } else return;
        } else if (
          this.mandibula === this.mandibula4 ||
          this.mandibula === this.mandibula5 ||
          this.mandibula === this.mandibula6
        ) {
          dentadura.class =
            dentadura.class == `dentadura5 d-block`
              ? `dentadura5 d-none`
              : `dentadura5 d-block`;
        } else return;
      });
    }
  }

  selectTooth(num) {
    this.dientes.map((diente) => {
      if (diente.id === num) {
        if (this.especialidad) {
          if (this.especialidad === "Corona") {
            this.coronas.map((corona) => {
              if (corona.id === num) {
                if (corona.class === `corona${num} d-block`) {
                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );
                  if (comparacion.length > 0) {
                    if (
                      comparacion[0].nombre !== "puente" &&
                      comparacion[0].nombre !== "puente de implantes" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !== "4 implantes con sobredentadura"
                    ) {
                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      if (index >= 0) {
                        diente.class = `diente${num} d-none`;
                        corona.class = `corona${num} d-none`;
                        this.arregloespecialidad.splice(index, 1);
                      }
                    } else {
                      this.mensajetoast("Todos los dientes han sido tratados");
                    }
                  }
                } else {
                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );
                  if (comparacion.length > 0) {
                    if (
                      comparacion[0].nombre !== "puente" &&
                      comparacion[0].nombre !== "puente de implantes" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !== "4 implantes con sobredentadura"
                    ) {
                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      if (index >= 0) {
                        corona.class = `corona${num} d-block`;
                        diente.class = `diente${num} d-block`;
                        corona.nombre = "corona";
                        this.arregloespecialidad.splice(index, 1);
                        this.arregloespecialidad.push(corona);
                      }
                    } else {
                      this.mensajetoast("Todos los dientes han sido tratados");
                    }
                  } else {
                    corona.class = `corona${num} d-block`;
                    diente.class = `diente${num} d-block`;
                    corona.nombre = "corona";
                    this.arregloespecialidad.push(corona);
                  }
                }
              }
            });
            console.log(this.arregloespecialidad);
          } else if (this.especialidad === "Puente") {
            this.coronas.map((corona) => {
              if (corona.id === num) {
                if (corona.class === `corona${num} d-block`) {
                  console.log("entro");

                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );
                  console.log(comparacion);

                  if (comparacion.length > 0) {
                    console.log("entro 0.1");
                    if (comparacion[0].nombre === "puente") {
                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      if (index >= 0) {
                        corona.class = `corona${num} d-none`;
                        diente.class = `diente${num} d-none`;
                        this.arregloespecialidad.splice(index, 1);
                        console.log(this.arregloespecialidad);
                      }
                    }
                  }
                } else {
                  console.log("entro1");
                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );
                  console.log(comparacion);
                  if (comparacion.length > 0) {
                    if (comparacion[0].nombre === "puente") {
                      console.log("entro4");

                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      console.log(index);

                      if (index >= 0) {
                        console.log("entro5");

                        corona.class = `corona${num} d-none`;
                        diente.class = `diente${num} d-none`;
                        const index1 = this.puentes.findIndex(
                          (el1) => el1.id === num
                        );
                        if (index1 >= 0) {
                          this.puentes[index1].class = `puente${index1} d-none`;
                        }
                        this.arregloespecialidad.splice(index, 1);
                        // this.arregloespecialidad.splice(index, 1);
                        // this.arregloespecialidad.push(corona);
                      }
                    } else {
                      console.log("entro6");
                    }
                  } else {
                    console.log("entro3");
                    // corona.class = `corona${num} d-block`;
                    // diente.class = `diente${num} d-block`;
                    // this.arregloespecialidad.push(corona);
                  }
                }
              }
            });
            // this.puentes.map((puente) => {
            //   if (puente.id === num) {
            //     if (puente.class === `puente${num} d-block`) {
            //       diente.class = `diente${num} d-none`;
            //       puente.class = `puente${num} d-none`;
            //       const comparacion = this.arreglopuente.filter(
            //         (buscador) => buscador.id === num
            //       );
            //       if (comparacion.length > 0) {
            //         const index = this.arreglopuente.findIndex(
            //           (el) => el.id === num
            //         );
            //         if (index >= 0) {
            //           this.arreglopuente.splice(index, 1);
            //         }
            //       }
            //     } else {
            //       puente.class = `puente${num} d-block`;
            //       diente.class = `diente${num} d-block`;
            //       const comparacion = this.arreglopuente.filter(
            //         (buscador) => buscador.id === num
            //       );
            //       if (comparacion.length > 0) {
            //         const index = this.arreglopuente.findIndex(
            //           (el) => el.id === num
            //         );
            //         if (index >= 0) {
            //           this.arreglopuente.splice(index, 1);
            //           this.arreglopuente.push(puente);
            //         }
            //       } else {
            //         this.arreglopuente.push(puente);
            //       }
            //     }
            //   }
            // });
            // console.log(this.arreglopuente);
          } else if (this.especialidad === "Puente de implentes") {
            this.coronas.map((corona) => {
              if (corona.id === num) {
                if (corona.class === `corona${num} d-block`) {
                  console.log("entro");

                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );
                  console.log(comparacion);

                  if (comparacion.length > 0) {
                    console.log("entro 0.1");
                    if (comparacion[0].nombre === "puente de implantes") {
                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      if (index >= 0) {
                        corona.class = `corona${num} d-none`;
                        diente.class = `diente${num} d-none`;
                        const index1 = this.puentes.findIndex(
                          (el1) => el1.id === num
                        );
                        if (index1 >= 0) {
                          this.puentes[index1].class = `puente${index1} d-none`;
                        }
                        this.arregloespecialidad.splice(index, 1);
                        console.log(this.arregloespecialidad);
                      }
                    }
                  }
                } else {
                  console.log("entro1");
                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );
                  console.log(comparacion);
                  if (comparacion.length > 0) {
                    if (comparacion[0].nombre === "puente de implantes") {
                      console.log("entro4");

                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      console.log(index);

                      if (index >= 0) {
                        console.log("entro5");
                        corona.class = `corona${num} d-none`;
                        diente.class = `diente${num} d-none`;
                        const index1 = this.puentes.findIndex(
                          (el1) => el1.id === num
                        );
                        if (index1 >= 0) {
                          this.puentes[index1].class = `puente${index1} d-none`;
                        }
                        this.arregloespecialidad.splice(index, 1);
                        console.log(this.arregloespecialidad);
                      }
                    } else {
                      console.log("entro6");
                    }
                  } else {
                    console.log("entro3");
                  }
                }
              }
            });
          } else if (this.especialidad === "Puente con implantes monofasicos") {
            this.coronas.map((corona) => {
              if (corona.id === num) {
                if (corona.class === `corona${num} d-block`) {
                  console.log("entro");

                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );
                  console.log(comparacion);

                  if (comparacion.length > 0) {
                    console.log("entro 0.1");
                    if (
                      comparacion[0].nombre ===
                      "puente con implantes monofasicos"
                    ) {
                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      if (index >= 0) {
                        corona.class = `corona${num} d-none`;
                        diente.class = `diente${num} d-none`;
                        const index1 = this.puentes.findIndex(
                          (el1) => el1.id === num
                        );
                        if (index1 >= 0) {
                          this.puentes[index1].class = `puente${index1} d-none`;
                        }
                        this.arregloespecialidad.splice(index, 1);
                        console.log(this.arregloespecialidad);
                      }
                    }
                  }
                } else {
                  console.log("entro1");
                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );
                  console.log(comparacion);
                  if (comparacion.length > 0) {
                    if (
                      comparacion[0].nombre ===
                      "puente con implantes monofasicos"
                    ) {
                      console.log("entro4");

                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      console.log(index);

                      if (index >= 0) {
                        console.log("entro5");
                        corona.class = `corona${num} d-none`;
                        diente.class = `diente${num} d-none`;
                        const index1 = this.puentes.findIndex(
                          (el1) => el1.id === num
                        );
                        if (index1 >= 0) {
                          this.puentes[index1].class = `puente${index1} d-none`;
                        }
                        this.arregloespecialidad.splice(index, 1);
                        console.log(this.arregloespecialidad);
                      }
                    } else {
                      console.log("entro6");
                    }
                  } else {
                    console.log("entro3");
                  }
                }
              }
            });
          } else if (this.especialidad === "Carilla") {
            this.carillas.map((carilla) => {
              if (carilla.id === num) {
                if (carilla.class === `corona${num} d-block`) {
                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );

                  if (comparacion.length > 0) {
                    if (
                      comparacion[0].nombre !== "puente" &&
                      comparacion[0].nombre !== "puente de implantes" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !== "4 implantes con sobredentadura"
                    ) {
                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      if (index >= 0) {
                        diente.class = `diente${num} d-none`;
                        carilla.class = `corona${num} d-none`;
                        this.arregloespecialidad.splice(index, 1);
                      }
                    } else {
                      this.mensajetoast("Todos los dientes han sido tratados");
                    }
                  }
                } else {
                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );
                  if (comparacion.length > 0) {
                    if (
                      comparacion[0].nombre !== "puente" &&
                      comparacion[0].nombre !== "puente de implantes" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !== "4 implantes con sobredentadura"
                    ) {
                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      if (index >= 0) {
                        carilla.class = `corona${num} d-block`;
                        diente.class = `diente${num} d-block`;
                        this.arregloespecialidad.splice(index, 1);
                        this.arregloespecialidad.push(carilla);
                      }
                    } else {
                      this.mensajetoast("Todos los dientes han sido tratados");
                    }
                  } else {
                    carilla.class = `corona${num} d-block`;
                    diente.class = `diente${num} d-block`;
                    this.arregloespecialidad.push(carilla);
                  }
                }
              }
            });
            console.log(this.arregloespecialidad);
          } else if (this.especialidad === "Implante de corona") {
            this.implante_de_Corona.map((implante) => {
              if (implante.id === num) {
                if (implante.class === `corona${num} d-block`) {
                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );

                  // diente.class = `diente${num} d-none`;
                  // implante.class = `corona${num} d-none`;
                  // const comparacion = this.arregloespecialidad.filter(
                  //   (buscador) => buscador.id === num
                  // );
                  if (comparacion.length > 0) {
                    if (
                      comparacion[0].nombre !== "puente" &&
                      comparacion[0].nombre !== "puente de implantes" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !== "4 implantes con sobredentadura"
                    ) {
                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      if (index >= 0) {
                        diente.class = `diente${num} d-none`;
                        implante.class = `corona${num} d-none`;
                        this.arregloespecialidad.splice(index, 1);
                      }
                    } else {
                      this.mensajetoast("Todos los dientes han sido tratados");
                    }
                  }
                } else {
                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );
                  if (comparacion.length > 0) {
                    if (
                      comparacion[0].nombre !== "puente" &&
                      comparacion[0].nombre !== "puente de implantes" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !== "4 implantes con sobredentadura"
                    ) {
                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      if (index >= 0) {
                        implante.class = `corona${num} d-block`;
                        diente.class = `diente${num} d-block`;
                        implante.nombre = "implante de corona";
                        this.arregloespecialidad.splice(index, 1);
                        this.arregloespecialidad.push(implante);
                      }
                    } else {
                      this.mensajetoast("Todos los dientes han sido tratados");
                    }
                  } else {
                    implante.class = `corona${num} d-block`;
                    diente.class = `diente${num} d-block`;
                    implante.nombre = "implante de corona";
                    this.arregloespecialidad.push(implante);
                  }
                }
              }
            });
            console.log(this.arregloespecialidad);
          } else if (this.especialidad === "Corona en implante monofasico") {
            this.corona_en_implante_monofasico.map((implante) => {
              if (implante.id === num) {
                if (implante.class === `corona${num} d-block`) {
                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );

                  if (comparacion.length > 0) {
                    if (
                      comparacion[0].nombre !== "puente" &&
                      comparacion[0].nombre !== "puente de implantes" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !== "4 implantes con sobredentadura"
                    ) {
                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      if (index >= 0) {
                        diente.class = `diente${num} d-none`;
                        implante.class = `corona${num} d-none`;
                        this.arregloespecialidad.splice(index, 1);
                      }
                    } else {
                      this.mensajetoast("Todos los dientes han sido tratados");
                    }
                  }

                  // diente.class = `diente${num} d-none`;
                  // implante.class = `corona${num} d-none`;
                  // const comparacion = this.arregloespecialidad.filter(
                  //   (buscador) => buscador.id === num
                  // );
                  // if (comparacion.length > 0) {
                  //   const index = this.arregloespecialidad.findIndex(
                  //     (el) => el.id === num
                  //   );
                  //   if (index >= 0) {
                  //     this.arregloespecialidad.splice(index, 1);
                  //   }
                  // }
                } else {
                  const comparacion = this.arregloespecialidad.filter(
                    (buscador) => buscador.id === num
                  );
                  if (comparacion.length > 0) {
                    if (
                      comparacion[0].nombre !== "puente" &&
                      comparacion[0].nombre !== "puente de implantes" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos" &&
                      comparacion[0].nombre !==
                        "puente con implantes monofasicos"
                    ) {
                      const index = this.arregloespecialidad.findIndex(
                        (el) => el.id === num
                      );
                      if (index >= 0) {
                        implante.class = `corona${num} d-block`;
                        diente.class = `diente${num} d-block`;
                        implante.nombre = "corona en implante monofasico";
                        this.arregloespecialidad.splice(index, 1);
                        this.arregloespecialidad.push(implante);
                      }
                    } else {
                      this.mensajetoast("Todos los dientes han sido tratados");
                    }
                  } else {
                    implante.class = `corona${num} d-block`;
                    diente.class = `diente${num} d-block`;
                    implante.nombre = "corona en implante monofasico";
                    this.arregloespecialidad.push(implante);
                  }
                }
              }
            });
            console.log(this.arregloespecialidad);
          }
        }
      }
    });
  }

  limpiarcuadrante() {
    let nombre = "";
    this.arregloespecialidad.map((especialidad) => {
      if (especialidad.id === this.mandibula[3]) {
        nombre = especialidad.nombre;
      }
    });
    if (nombre === "4 implantes con sobredentadura") {
      if (
        this.mandibula.includes(2) ||
        this.mandibula.includes(11) ||
        this.mandibula.includes(15)
      ) {
        var arrardientes = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        for (let i = 0; i < arrardientes.length; i++) {
          const index = this.arregloespecialidad.findIndex(
            (buscador) => buscador.id === arrardientes[i]
          );
          if (index >= 0) {
            this.dientes.map((diente) => {
              if (diente.id === arrardientes[i]) {
                this.arregloespecialidad.map((especialidad) => {
                  if (especialidad.id === arrardientes[i]) {
                    if (
                      especialidad.nombre === "4 implantes con sobredentadura"
                    ) {
                      diente.class = `diente${arrardientes[i]} d-none`;
                      this.limpiarcuatroimplanteconsobredentadura(
                        arrardientes[i]
                      );
                    }
                  }
                });
              }
            });
            this.arregloespecialidad.splice(index, 1);
          }
        }
      } else {
        var arrardientes = [
          28, 29, 30, 31, 22, 23, 24, 25, 26, 27, 18, 19, 20, 21,
        ];
        for (let i = 0; i < arrardientes.length; i++) {
          const index = this.arregloespecialidad.findIndex(
            (buscador) => buscador.id === arrardientes[i]
          );
          if (index >= 0) {
            this.dientes.map((diente) => {
              if (diente.id === arrardientes[i]) {
                this.arregloespecialidad.map((especialidad) => {
                  if (especialidad.id === arrardientes[i]) {
                    if (
                      especialidad.nombre === "4 implantes con sobredentadura"
                    ) {
                      diente.class = `diente${arrardientes[i]} d-none`;
                      this.limpiarcuatroimplanteconsobredentadura(
                        arrardientes[i]
                      );
                    }
                  }
                });
              }
            });
            this.arregloespecialidad.splice(index, 1);
          }
        }
      }
    } else {
      for (let i = 0; i < this.mandibula.length; i++) {
        const index = this.arregloespecialidad.findIndex(
          (buscador) => buscador.id === this.mandibula[i]
        );
        if (index >= 0) {
          this.dientes.map((diente) => {
            if (diente.id === this.mandibula[i]) {
              this.arregloespecialidad.map((especialidad) => {
                if (especialidad.id === this.mandibula[i]) {
                  if (especialidad.nombre === "carilla") {
                    diente.class = `diente${this.mandibula[i]} d-none`;
                    this.limpiarcariila(this.mandibula[i]);
                  }
                  if (especialidad.nombre === "corona") {
                    diente.class = `diente${this.mandibula[i]} d-none`;
                    this.limpiarcorona(this.mandibula[i]);
                  }
                  if (especialidad.nombre === "puente") {
                    diente.class = `diente${this.mandibula[i]} d-none`;
                    this.limpiarpuente(this.mandibula[i]);
                  }

                  if (especialidad.nombre === "implante de corona") {
                    diente.class = `diente${this.mandibula[i]} d-none`;
                    this.limpiarimplantedeCorona(this.mandibula[i]);
                  }

                  if (especialidad.nombre === "corona en implante monofasico") {
                    diente.class = `diente${this.mandibula[i]} d-none`;
                    this.limpiarcoronaenimplantemonofasico(this.mandibula[i]);
                  }

                  if (especialidad.nombre === "puente de implantes") {
                    diente.class = `diente${this.mandibula[i]} d-none`;
                    this.limpiarpuenteimpante(this.mandibula[i]);
                  }

                  if (
                    especialidad.nombre === "puente con implantes monofasicos"
                  ) {
                    diente.class = `diente${this.mandibula[i]} d-none`;
                    this.limpiarpuenteimplantemonofasicos(this.mandibula[i]);
                  }
                }
              });
            }
            // this.puentes.map((puente) => {
            //   this.coronas.map((corona) => {
            //     this.carillas.map((carilla) => {
            //       this.implante_de_Corona.map((implante) => {
            //         this.corona_en_implante_monofasico.map((implantecorona) => {
            //           if (diente.id === this.mandibula[i]) {
            //             diente.class = `diente${this.mandibula[i]} d-none`;
            //             puente.class = `puente${this.mandibula[i]} d-none`;
            //             corona.class = `corona${this.mandibula[i]} d-none`;
            //             carilla.class = `corona${this.mandibula[i]} d-none`;
            //             implante.class = `corona${this.mandibula[i]} d-none`;
            //             implantecorona.class = `corona${this.mandibula[i]} d-none`;
            //           }
            //         });
            //       });
            //     });
            //   });
            // });
          });
          this.arregloespecialidad.splice(index, 1);
        }
      }
    }
  }

  limpiarpuente(index) {
    this.puentes.map((puente) => {
      puente.class = `puente${index} d-none`;
    });
  }

  limpiarcorona(index) {
    this.coronas.map((corona) => {
      corona.class = `corona${index} d-none`;
    });
  }

  limpiarcariila(index) {
    this.carillas.map((carilla) => {
      carilla.class = `corona${index} d-none`;
    });
  }

  limpiarimplantedeCorona(index) {
    this.implante_de_Corona.map((implante) => {
      implante.class = `corona${index} d-none`;
    });
  }

  limpiarcoronaenimplantemonofasico(index) {
    this.corona_en_implante_monofasico.map((implantecorona) => {
      implantecorona.class = `corona${index} d-none`;
    });
  }

  limpiarpuenteimpante(index) {
    this.puente_impante.map((implante) => {
      this.puentes.map((puente) => {
        puente.class = `puente${index} d-none`;
        implante.class = `corona${index} d-none`;
      });
    });
  }

  limpiarpuenteimplantemonofasicos(index) {
    this.puente_implante_monofasicos.map((implante) => {
      this.puentes.map((puente) => {
        puente.class = `puente${index} d-none`;
        implante.class = `corona${index} d-none`;
      });
    });
  }

  limpiarcuatroimplanteconsobredentadura(index) {
    this.puente_implante_monofasicos.map((implante) => {
      this.puentes.map((puente) => {
        puente.class = `puente${index} d-none`;
        implante.class = `corona${index} d-none`;
      });
    });
  }

  mensajetoast(mensaje) {
    let config = new MatSnackBarConfig();
    config.verticalPosition = "top";
    config.horizontalPosition = "end";
    config.duration = this.setAutoHide ? this.autoHide : 0;
    this.snackBar.open(mensaje, this.action ? "Aceptar" : undefined, config);
  }
}
