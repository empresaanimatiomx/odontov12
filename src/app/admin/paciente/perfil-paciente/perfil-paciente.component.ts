import { Component, OnInit } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { MatSnackBar } from "@angular/material/snack-bar";
import { AuthService } from "../../../core/service/auth.service";
import { Router } from "@angular/router";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-perfil-paciente",
  templateUrl: "./perfil-paciente.component.html",
  styleUrls: [],
})
export class PerfilPacienteComponent implements OnInit {
  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  idClinica: number;
  data: any = [];
  filteredData: any = [];
  pacientes: any = [];
  pacientes1: any = [];
  public controldefiltro: FormControl = new FormControl();

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
  }

  ngOnInit() {
    if (this.idClinica > 0) {
      this.obtenerlistapacientes();
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraclinica());
    }
  }

  //metodo para ir a registrarclinica
  iraclinica() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  //metodo para traer el listado de pacientes
  obtenerlistapacientes() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 12, idClinica: this.idClinica };
    const URL: any = this.baseURL + "pacientes.php";
    this.httpClient
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        this.pacientes = respuesta;
        this.pacientes1 = respuesta;
      });
  }

  // metodo para pasar los valores de servios a un nuevo arreglo
  pasararreglodepacientes() {
    this.pacientes1 = this.pacientes;
  }

  // metodo de buscador
  filtrobuscar(event) {
    this.pasararreglodepacientes();
    const buscar = event.target.value;
    this.pacientes1 = this.pacientes.filter((paciente) => {
      return (
        paciente.Nombre.toLowerCase().indexOf(buscar.toLowerCase()) > -1 ||
        paciente.Apellido.toLowerCase().indexOf(buscar.toLowerCase()) > -1 ||
        paciente.Celular.toLowerCase().indexOf(buscar.toLowerCase()) > -1
      );
    });
  }

  //ir a detalle paciente
  iravista(idpaciente) {
    localStorage.setItem("perfilusuario", JSON.stringify(idpaciente));
    this.router.navigate(["/admin/paciente/detalle-paciente"]);
    localStorage.setItem("tabs", JSON.stringify(0));
  }
}
