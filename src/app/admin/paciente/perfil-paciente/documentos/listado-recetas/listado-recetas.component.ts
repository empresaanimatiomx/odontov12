import { Component, ElementRef, OnInit, ViewChild, TemplateRef} from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, FormControl, Validators, } from "@angular/forms";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { AuthService } from "src/app/core/service/auth.service";

const headers: any = new HttpHeaders({"Content-Type":"application/json"});

@Component({
  selector: 'app-listado-recetas',
  templateUrl: './listado-recetas.component.html',
  styles: [
  ]
})
export class ListadoRecetasComponent implements OnInit {
private baseURL = 'https://projectsbyanimatiomx.com/phps/odonto/';
  @ViewChild("roleTemplate", { static: true }) roleTemplate: TemplateRef<any>;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  rows = [];
  selectedRowData: selectRowInterface;
  newUserImg = "assets/images/user_default.png";
  data: any = [];
  filteredData: any = [];
  editForm: FormGroup;
  register: FormGroup;
  selectedOption: string;
  columns = [
    { name: "Fecha" },
    { name: "Descripcion" },
    { name: "Doctor" },
  ];
  idClinica;
  idPaciente;
  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private modalService: NgbModal,
    private httpClient: HttpClient
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    const info = localStorage.getItem('perfilusuario');
    this.idPaciente = info.replace('"','').replace('"','');
    this.editForm = this.fb.group({
      idRecetas: new FormControl(),
      Fecha: new FormControl(),
      Descripcion: new FormControl(),
    });
  }
  ngOnInit() {
    this.fetch((data) => {
      this.data = data;
      this.filteredData = data;
    });
    this.register = this.fb.group({
      idRecetas: [""],
      Fecha: ["",Validators.required],
      Descripcion: ["", [Validators.required]]
    });
  }

  fetch(cb) {
    const options: any = {"caso":0,"idPaciente":this.idPaciente};
    this.httpClient.post(`${this.baseURL}recetas.php`, JSON.stringify(options), headers)
    .subscribe(data=> {
      this.data=data;
      this.filteredData=data;
      cb(data);
      });
  }
  editRow(row, rowIndex, content) {
    this.modalService.open(content, {ariaLabelledBy:"modal-basic-title",centered:true});
    this.editForm.setValue({
      idRecetas: row.idRecetas,
      Fecha: row.fecha,
      Descripcion: row.descripcion,
    });
    this.selectedRowData = row;
  }
  addRow(content) {
    this.modalService.open(content, {ariaLabelledBy:"modal-basic-title",centered:true});
  }
  onEditSave(form: FormGroup) {

  }
  onAddRowSave(form: FormGroup) {

  }
  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
  openSnackBar(message: string) {
    this._snackBar.open(message, "", {
      duration: 2000,
      verticalPosition: "bottom",
      horizontalPosition: "right",
      panelClass: ["bg-red"],
    });
  }
  showNotification(colorName, text, placementFrom, placementAlign) {
    this._snackBar.open(text, "", {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
}
export interface selectRowInterface {
  idRecetas: number;
  Fecha: String;
  Descripcion: String;
}