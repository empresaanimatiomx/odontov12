import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { FormGroup, FormBuilder, FormControl, Validators, } from "@angular/forms";
import { AuthService } from 'src/app/core/service/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Swal from 'sweetalert2';
import { DatatableComponent } from "@swimlane/ngx-datatable";
const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });

@Component({
  selector: "app-nueva-receta",
  templateUrl: "./nueva-receta.component.html",
  styles: [],
})
export class NuevaRecetaComponent implements OnInit {
  private baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  data: any = [];
  filteredData: any = [];
  editForm: FormGroup;
  register: FormGroup;
  selectedRowData: selectRowInterface;
  idClinica;
  idPaciente;
  idDoctores;
  datosPaciente: any = [];
  doctores: any = [];
  sucursales: any = [];
  cedula;
  nombreDoctor;
  FechaHoy;
  plantillasDeReceta;
  rp: string = "";
  TituloPlantilla: string = "";
  columns = [{ name: "idRecetas" }, { name: "Fecha" }, { name: "Descripcion" }];
  nombre = "";
  correo = "";
  idUsuario;
  recetaplantilla = "";
  logoclinica = "";
  notadereceta = "";
  nombreClinica = "";

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    public httpClient: HttpClient
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.nombre =
      this.authService.currentUserValue.firstName +
      this.authService.currentUserValue.lastName;
    this.correo = this.authService.currentUserValue.username;
    this.idUsuario = this.authService.currentUserValue.id;
    this.logoclinica = this.authService.currentUserValue.Logo_clinica;
    this.nombreClinica = this.authService.currentUserValue.Nombre_clinica;

    const info = localStorage.getItem("perfilusuario");
    this.idPaciente = info.replace('"', "").replace('"', "");
    this.editForm = this.fb.group({
      idRecetas: new FormControl(),
      Fecha: new FormControl(),
      Descripcion: new FormControl(),
    });
  }

  ngOnInit(): void {
    this.fetch((data) => {
      this.data = data;
      this.filteredData = data;
    });
    this.FechaHoy = new Date();
    this.register = this.fb.group({
      idRecetas: [""],
      Fecha: [""],
      Descripcion: ["", [Validators.required]],
    });
    this.getDatosPaciente();
  }

  fetch(cb) {
    const options: any = { caso: 0, idPaciente: this.idPaciente };
    this.httpClient
      .post(`${this.baseURL}recetas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.data = respuesta;
        this.filteredData = respuesta;
        cb(respuesta);
      });
  }
  editRow(row, rowIndex, content) {
    this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
    });
    this.editForm.controls.Fecha.disable();
    this.editForm.setValue({
      idRecetas: row.idRecetas,
      Fecha: row.fecha,
      Descripcion: row.descripcion,
    });
    this.selectedRowData = row;
  }

  onEditSave(form: FormGroup) {
    const { idRecetas, Descripcion } = form.value;
    const options: any = { caso: 2, descri: Descripcion, idR: idRecetas };
    this.httpClient
      .post(`${this.baseURL}recetas.php`, JSON.stringify(options), headers)
      .subscribe((data: any) => {
        if (data == "Se modifico") {
          Swal.fire("Se modifico la receta correctamente", "", "success");
          this.modalService.dismissAll();
          this.ngOnInit();
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }

  modalPlantilla(modal, texto) {
    this.recetaplantilla = "";
    this.notadereceta = texto;
    this.GetPlantillaDeRecetas();
    setTimeout(() => {
      let ngbModalOptions: NgbModalOptions = {
        backdrop: "static",
        keyboard: false,
        centered: true,
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
      };
      this.modalService.open(modal, ngbModalOptions);
    }, 100);
  }

  getDatosPaciente() {
    const options: any = { caso: 3, idPaciente: this.idPaciente };
    this.httpClient
      .post(`${this.baseURL}recetas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.datosPaciente = respuesta[0];
      });
  }
  GetPlantillaDeRecetas() {
    const options: any = { caso: 5, idclinica: this.idClinica };
    this.httpClient
      .post(`${this.baseURL}recetas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.plantillasDeReceta = respuesta;
      });
  }

  guardarReceta() {
    const options: any = {
      caso: 6,
      descri: this.rp,
      idPaciente: this.idPaciente,
      idDoctor: this.nombre,
    };
    this.httpClient
      .post(`${this.baseURL}recetas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta == "Se inserto") {
          Swal.fire("Se agrego la receta correctamente", "", "success");
          this.idDoctores = "";
          this.idClinica = "";
          this.rp = "";
          this.cedula = "";
          this.ngOnInit();
        } else {
          Swal.fire("Ocurrio un error al guardar la receta", "", "error");
        }
      });
  }

  guardarDatosPlantilla(Titulo, Notas) {
    this.TituloPlantilla = "Plantilla: " + Titulo;
    this.recetaplantilla = Notas;
  }

  pasarvariablearp() {
    this.rp = this.recetaplantilla;
    this.modalService.dismissAll();
  }

  cambiarCedula(cedula, nombre, apellido, idDoctor) {
    this.idDoctores = idDoctor;
    this.cedula = cedula;
    this.nombreDoctor = nombre + " " + apellido;
  }

  FirstUppercaseLetter(idname) {
    const texto = (<HTMLInputElement>document.getElementById(idname)).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      (<HTMLInputElement>document.getElementById(idname)).value = textofinal;
    }
  }

  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        // check for a match
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
}

export interface selectRowInterface {
  idRecetas: number;
  Fecha: String;
  Descripcion: String;
}