import { Component, OnInit, ViewChild } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../../../../../core/service/auth.service";
import { AlertService } from "../../../../../core/service/alert.service";
import { HttpService } from "../../../../../core/service/http.service";

@Component({
  selector: "app-laboratorio",
  templateUrl: "./laboratorio.component.html",
  styles: [],
})
export class LaboratorioComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  ordenes = [];
  ordenesFilter = [];
  register: FormGroup;
  laboratorios = [];
  idClinica;
  doctores = [];
  sucursales = [];
  imagenclinicaPrint = "";

  constructor(
    private sweet: AlertService,
    private authService: AuthService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private http: HttpService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
  }

  ngOnInit() {
    this.register = this.fb.group({
      idOrdenLab: [""],
      NombreOrden: ["", [Validators.required]],
      idSucursal: ["", [Validators.required]],
      idLaboratorio: ["", [Validators.required]],
      idDoctor: ["", [Validators.required]],
    });
    this.getOrdenes();
    this.getSucursales();
    this.getDoctores();
    this.getLaboratorios();
    this.imagenclinicaPrint =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
  }

  getOrdenes(refresh?) {
    this.http
      .post("laboratorio.php", { caso: 7, idClinica: this.idClinica })
      .subscribe({
        next: (ordenes: any) => {
          this.ordenes = ordenes;
          this.ordenesFilter = ordenes;
          refresh && this.sweet.toast("las ordenes");
        },
        error: (err) => console.log(err),
      });
  }

  getLaboratorios() {
    this.http
      .post("laboratorio.php", { caso: 0, idClinica: this.idClinica })
      .subscribe({
        next: (laboratorios: any) => {
          this.laboratorios = laboratorios;
        },
        error: (err) => console.log(err),
      });
  }

  getDoctores() {
    this.http
      .post("pacientes.php", { caso: 5, idClinica: this.idClinica })
      .subscribe({
        next: (doctores: any) => {
          this.doctores = doctores;
        },
        error: (err) => console.log(err),
      });
  }

  getSucursales() {
    this.http
      .post("recetas.php", { caso: 4, idclinica: this.idClinica })
      .subscribe({
        next: (sucursales: any) => {
          this.sucursales = sucursales;
        },
        error: (err) => console.log(err),
      });
  }

  openModal(content, row?) {
    this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
    });
    if (row) {
      this.register.patchValue(row);
    }
  }

  closeModal(form: FormGroup) {
    this.modalService.dismissAll();
    form.reset();
  }

  modalPrevisulizar(modal) {
    setTimeout(() => {
      this.modalService.open(modal, {
        ariaLabelledBy: "modal-basic-title",
        centered: true,
        size: "lg",
      });
    }, 100);
  }

  onEditSave(form: FormGroup) {
    this.http
      .post("laboratorio.php", { caso: 5, ordenlab: form.value })
      .subscribe({
        next: (data: any) => {
          if (data == "Se modifico") {
            this.sweet.alert(
              "",
              "Se modifico la orden correctamente",
              "success"
            );
            form.reset();
            this.getOrdenes();
            this.modalService.dismissAll();
          } else {
            form.reset();
            this.sweet.alert(
              "",
              "Ocurrio un error intentelo de nuevo",
              "error"
            );
          }
        },
        error: (err) => console.log(err),
      });
  }

  guardarOrdenLaboratorio(form: FormGroup) {
    this.modalService.dismissAll();
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    const idPaciente = JSON.parse(localStorage.getItem("perfilusuario"));
    this.http
      .post("laboratorio.php", {
        caso: 3,
        ordenlab: form.value,
        idClinica: idclinica,
        idPaciente,
      })
      .subscribe({
        next: (data: any) => {
          if (data == "Se inserto") {
            this.sweet.alert("", "Se agrego la orden correctamente", "success");
            form.reset();
            this.getOrdenes();
          } else {
            form.reset();
            this.sweet.alert(
              "",
              "Ocurrio un error intentelo de nuevo",
              "error"
            );
          }
        },
        error: (err) => console.log(err),
      });
  }

  eliminarOrdenLab(idLaboratorio) {
    this.http.post("laboratorio.php", { caso: 6, idLaboratorio }).subscribe({
      next: (data: any) => {
        if (data == "Se elimino") {
          this.sweet.alert(
            "",
            "Se elimino el laboratorio correctamente",
            "success"
          );
          this.getOrdenes();
        } else {
          this.sweet.alert("", "Ocurrio un error intentelo de nuevo", "error");
        }
      },
      error: (err) => console.log(err),
    });
  }

  FirstUppercaseLetter(name) {
    const texto = this.register.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.register.controls[name].setValue(textofinal);
    }
  }

  filterDatatable(event) {
    if (event.target.value === "") {
      this.ordenes = this.ordenesFilter;
    }
    this.ordenes = this.http.filter(event, this.ordenesFilter);
    this.table.offset = 0;
  }
}
