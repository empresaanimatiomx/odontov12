import { Component, OnInit, ViewChild, ChangeDetectionStrategy, TemplateRef} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, FormControl, Validators, } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "src/app/core/service/auth.service";
import Swal from "sweetalert2";
import { AlertService } from "src/app/core/service/alert.service";

const headers: any = new HttpHeaders({
  "Content-Type": "application/json",
});

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "app-radiologia",
  templateUrl: "./radiologia.component.html",
  styles: [],
})
export class RadiologiaComponent implements OnInit {
  private baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  @ViewChild("addRecord", { static: true }) addRecord: TemplateRef<any>;
  rows = [];
  data: any = [];
  filteredData: any = [];
  ordenForm: FormGroup;
  columns = [
    { name: "Fecha" },
    { name: "NombreSucursal" },
    { name: "CentroRadiologico" },
    { name: "Observaciones" },
  ];
  imagenclinicaPrint = "";
  centros: any = [];
  sucursales: any = [];
  edit: boolean = false;

  constructor(
    private sweet: AlertService,
    private authService: AuthService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private httpClient: HttpClient
  ) {

  }
  ngOnInit() {
    this.getOrdenes();
    this.getCentro();
    this.obtenersucursales();
    this.imagenclinicaPrint =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
    this.ordenForm = this.fb.group({
      idOrden: [""],
      sucursal: ["", Validators.required],
      centroRadiologico: ["", Validators.required],
      rxRetroalveolar: [""],
      rxRetroalveolarTotal: [""],
      rxBiteWing: [""],
      rxOclusal: [""],
      rxPanoramica: [""],
      rxteleradiografiaLateral: [""],
      rxTeleradiografiaFrontal: [""],
      rxDigitaldeMano: [""],
      maxilarSuperior: [""],
      maxilarInferior: [""],
      bimaxilar: [""],
      craneoCompleto: [""],
      ATMBocaCerrada: [""],
      ATMBocaAbierta: [""],
      zona: [""],
      especialidad: [""],
      rickets: [""],
      roth: [""],
      jarabak: [""],
      frontal: [""],
      tejidosBlandos: [""],
      conversionCefalome: [""],
      VTO: [""],
      modelos: [""],
      fotografias: [""],
      eje: [""],
      holta: [""],
      video: [""],
      articulados: [""],
      formaEntrga: [""],
      observaciones: [""],
    });
  }

  obtenersucursales() {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    const options: any = { caso: 0, idClinica: idclinica };
    const URL: any = this.baseURL + "sucursal.php";
    this.httpClient
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        this.sucursales = respuesta;
      });
  }

  getCentro(modal?) {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    const options: any = { caso: 8, idclinica };
    this.httpClient
      .post(`${this.baseURL}recetas.php`, JSON.stringify(options), headers)
      .subscribe((data: any) => {
        this.centros = data;
        if (modal) {
          this.closeAndOpenModal(modal);
        }
      });
  }

  getOrdenes(refresh?) {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    const options: any = { caso: 0, idclinica };
    this.httpClient
      .post(`${this.baseURL}radiologia.php`, JSON.stringify(options), headers)
      .subscribe((data: any) => {
        this.data = data;
        this.filteredData = data;
        refresh && this.sweet.toast("las ordenes");
      });
  }

  editRow(row, content) {
    this.edit = true;
    this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
      size: "lg",
    });
    this.ordenForm.setValue(row);
  }

  addRow(content) {
    this.edit = false;
    this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
      size: "lg",
    });
  }

  selectLab(value, modal) {
    if (value === "si") {
      setTimeout(() => {
        this.modalService.open(modal, {
          ariaLabelledBy: "modal-basic-title",
          centered: true,
          size: "lg",
        });
      }, 100);
    }
  }

  modalPrevisulizar(modal) {
    setTimeout(() => {
      this.modalService.open(modal, {
        ariaLabelledBy: "modal-basic-title",
        centered: true,
        size: "lg",
      });
    }, 100);
  }

  preOrder(form: FormGroup) {
    if (form.value.idOrden !== "") {
      this.onEditSave(form);
    } else {
      this.saveOrden(form);
    }
  }

  onEditSave(form: FormGroup) {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    const options: any = { caso: 2, orden: form.value, idclinica };
    this.httpClient
      .post(`${this.baseURL}radiologia.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta === "Se modifico") {
          this.ngOnInit();
          Swal.fire("Se modifico la orden correctamente", "", "success");
          form.reset();
          this.modalService.dismissAll();
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }

  saveOrden(form: FormGroup) {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    const options: any = { caso: 1, orden: form.value, idclinica };
    this.httpClient
      .post(`${this.baseURL}radiologia.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta === "Se inserto") {
          this.ngOnInit();
          Swal.fire("Se agrego la orden correctamente", "", "success");
          form.reset();
          this.modalService.dismissAll();
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }

  closeAndOpenModal(modal) {
    this.modalService.dismissAll();
    setTimeout(() => {
      this.modalService.open(modal, {
        ariaLabelledBy: "modal-basic-title",
        centered: true,
        size: "lg",
      });
    }, 0);
  }

  filterDatatable(event) {
    const val = event.target.value.toLowerCase();
    const colsAmt = this.columns.length;
    const keys = Object.keys(this.filteredData[0]);
    this.data = this.filteredData.filter(function (item) {
      for (let i = 0; i < colsAmt; i++) {
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          return true;
        }
      }
    });
    this.table.offset = 0;
  }
}
