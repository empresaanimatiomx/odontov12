import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  Validators,
} from "@angular/forms";
import { AlertService } from "src/app/core/service/alert.service";
import { HttpService } from "../../../../core/service/http.service";

@Component({
  selector: "app-historial-clinico",
  templateUrl: "./historial-clinico.component.html",
  styles: [],
})
export class HistorialClinicoComponent implements OnInit {
  historialForm: FormGroup;
  motivosForm: FormGroup;
  motivosPaciente = [];
  historialPaciente;
  idPaciente;

  constructor(
    private http: HttpService,
    private fb: FormBuilder,
    private sweet: AlertService
  ) {
    this.idPaciente = JSON.parse(localStorage.getItem("perfilusuario"));
  }

  ngOnInit(): void {
    this.getHistorial(this.idPaciente);
    this.getMotivos(this.idPaciente);
    this.motivosForm = this.fb.group({
      motivo: ["", Validators.required],
      enfermedadActual: ["", Validators.required],
    });
    this.historialForm = this.fb.group({
      Enfermedades: [""],
      AntecedentesQuiru: [""],
      Alergias: [""],
      Medicamentos: [""],
      Habitos: [""],
      AntecedentesFamil: [""],
      Fuma: [""],
      Embarazo: [""],
      Alcohol: [""],
      Cepilladodiario: [""],
      Fumar: [""],
      Golosinas: [""],
      perniciosolabio: [""],
      perniciosolengua: [""],
      Higienebucal: [""],
      Artritisreumatoide: [""],
      Asma: [""],
      Autoinmune: [""],
      Cancer: [""],
      Cardiaco: [""],
      Diabetes: [""],
      ETS: [""],
      Gastritis: [""],
      Hepatitis: [""],
      Hipotiroidismo: [""],
      Sarampion: [""],
      Varicela: [""],
      Hospitalizaciones: [""],
      Operacionadenoides: [""],
      AcidoAcetilsalicilico: [""],
      Alimentos: [""],
      Aneteico: [""],
      Loratadina: [""],
      Metamizol: [""],
      Paracetamol: [""],
      Penicilina: [""],
      Sulfonamidas: [""],
      LineaMediaDentalSuperior: [""],
      LineaMediaDentalSupMM: [""],
      LineaMediaDentalInferior: [""],
      LineaMediaDentalInfMM: [""],
      ApiSuperior: [""],
      ApiInferior: [""],
      ApiObservacion: [""],
      CambiosEnElPisoDeLaBoca: [""],
      Mejillas: [""],
      Labios: [""],
      Paladar: [""],
      Frenillos: [""],
      Lengua: [""],
      CambioEnElEsmalteYLaDentina: [""],
      ObservacionesEstoma: [""],
      SuccionDigi: [""],
      DeglucionAti: [""],
      Onychophogia: [""],
      Bruxismo: [""],
      RespiracionOral: [""],
      Musculo: [""],
      MasticarEnfermedad: [""],
      ATM: [""],
      obsFuncional: [""],
      EnciasInflamadas: [""],
      Placa: [""],
      Calculo: [""],
      RecesionGingi: [""],
      PerdidaInsercion: [""],
      Movilidad: [""],
      ObservPeriodontal: [""],
      Frio: [""],
      Caliente: [""],
      PruebaElectr: [""],
      PercuHorizontal: [""],
      PercuVertica: [""],
      ObservacionesSensibi: [""],
    });
  }

  getHistorial(idPaciente) {
    this.http.post("historial.php", { caso: 1, idPaciente }).subscribe({
      next: (respuesta: any) => {
        if (respuesta !== null) {
          this.historialPaciente = respuesta;
          this.historialForm.patchValue(this.historialPaciente[0]);
          this.historialForm.disable();
        }
      },
      error: (err) => console.log(err),
    });
  }

  getMotivos(idPaciente) {
    this.http.post("historial.php", { caso: 3, idPaciente }).subscribe({
      next: (respuesta: any) => {
        if (respuesta !== null) {
          this.motivosPaciente = respuesta;
        }
      },
      error: (err) => console.log(err),
    });
  }

  toggleForm() {
    if (this.historialForm.enabled) {
      this.historialForm.disable();
    } else {
      this.historialForm.enable();
    }
  }

  saveHistorial(Historial) {
    const options = {
      caso: 0,
      Historial,
      idPaciente: this.idPaciente,
    };
    this.http.post("historial.php", options).subscribe({
      next: (respuesta: any) => {
        if (respuesta === "Se inserto") {
          this.sweet.alert(
            "",
            "Historial clinico guardado correctamente",
            "success"
          );
          this.getHistorial(this.idPaciente);
          this.historialForm.disable();
        }
      },
      error: (err) => console.log(err),
    });
  }

  saveMotivo(motivo, formDirective: FormGroupDirective) {
    const options = {
      caso: 4,
      motivo,
      idPaciente: this.idPaciente,
    };
    this.http.post("historial.php", options).subscribe({
      next: (respuesta: any) => {
        if (respuesta === "Se inserto") {
          this.sweet.alert(
            "",
            "Historial clinico guardado correctamente",
            "success"
          );
          this.getMotivos(this.idPaciente);
          formDirective.resetForm();
        }
      },
      error: (err) => console.log(err),
    });
  }

  deleteMotivo(idMotivo) {
    this.http.post("historial.php", { caso: 5, idMotivo }).subscribe({
      next: (respuesta: any) => {
        if (respuesta === "Se elimino") {
          this.sweet.alert(
            "",
            "Motivo eliminado correctamente",
            "success"
          );
          this.getMotivos(this.idPaciente);
        }
      },
      error: (err) => console.log(err),
    });
  }

  updateHistorial() {
    const options = {
      caso: 2,
      Historial: this.historialForm.value,
      idPaciente: this.idPaciente,
    };
    this.http.post("historial.php", options).subscribe({
      next: (respuesta: any) => {
        if (respuesta === "Se actualizo") {
          this.sweet.alert(
            "",
            "Historial clinico actualizado correctamente",
            "success"
          );
          this.getHistorial(this.idPaciente);
          this.historialForm.disable();
        }
      },
      error: (err) => console.log(err),
    });
  }

  onBlurMethod(name) {
    const texto = this.historialForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.historialForm.controls[name].setValue(textofinal);
    }
  }

  firstLetterUppercase(name) {
    const texto = this.motivosForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.motivosForm.controls[name].setValue(textofinal);
    }
  }
}
