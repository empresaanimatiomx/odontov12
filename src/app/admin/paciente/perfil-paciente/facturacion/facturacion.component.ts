/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AlertService } from "src/app/core/service/alert.service";
import { HttpService } from "src/app/core/service/http.service";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";

@Component({
  selector: "app-facturacion",
  templateUrl: "./facturacion.component.html",
  styles: [],
})
export class FacturacionComponent implements OnInit {
  @ViewChild("placesRef")
  placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  facturarForm: FormGroup;
  paciente;

  constructor(
    private fb: FormBuilder,
    private alertSvc: AlertService,
    private postSvc: HttpService
  ) {}

  ngOnInit(): void {
    this.facturarForm = this.fb.group({
      uid: [""],
      nombre: ["", [Validators.required]],
      apellido: ["", [Validators.required]],
      email: [
        "",
        [
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ],
      ],
      email2: [""],
      email3: [""],
      telefono: [
        "",
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
        ],
      ],
      razons: ["", [Validators.required]],
      rfc: [
        "",
        [
          Validators.required,
          Validators.minLength(12),
          Validators.maxLength(13),
        ],
      ],
      calle: ["", [Validators.required]],
      numero_exterior: ["", [Validators.required]],
      numero_interior: ["", [Validators.required]],
      codpos: ["", [Validators.required]],
      colonia: ["", [Validators.required]],
      estado: ["", [Validators.required]],
      ciudad: ["", [Validators.required]],
    });
    this.getDatosPaciente();
  }

  public handleAddressChange2(address: Address): void {
    this.facturarForm.getRawValue();
    let numero_exterior = "";
    let calle = "";
    let colonia = "";
    let ciudad = "";
    let estado = "";
    let codpos = "";
    // let pais = "";
    for (let i = 0; i < address.address_components.length; i++) {
      const element = address.address_components[i].types[0];
      if (element === "street_number") {
        numero_exterior = address.address_components[i].long_name;
      }
      if (element === "route") {
        calle = address.address_components[i].long_name;
      }
      if (element === "sublocality_level_1") {
        colonia = address.address_components[i].long_name;
      }
      if (element === "locality") {
        ciudad = address.address_components[i].long_name;
      }
      if (element === "administrative_area_level_1") {
        estado = address.address_components[i].long_name;
      }
      if (element === "postal_code") {
        codpos = address.address_components[i].long_name;
      }
      // if (element === "country") {
      //   pais = address.address_components[i].long_name;
      // }
    }
    this.facturarForm.patchValue({
      calle,
      numero_exterior,
      codpos,
      colonia,
      estado,
      ciudad,
    });
    this.facturarForm.controls["calle"].disable();
    this.facturarForm.controls["numero_exterior"].disable();
    this.facturarForm.controls["codpos"].disable();
    this.facturarForm.controls["colonia"].disable();
    this.facturarForm.controls["estado"].disable();
    this.facturarForm.controls["ciudad"].disable();
  }

  getDatosPaciente() {
    const idPaciente = JSON.parse(localStorage.getItem("perfilusuario"));
    this.postSvc
      .post("datos-personales.php", {
        caso: 3,
        idPaciente,
      })
      .subscribe({
        next: (paciente: any) => {
          if (paciente !== null) {
            this.paciente = paciente[0];
            this.facturarForm.patchValue({
              uid: paciente[0].uid,
              nombre: paciente[0].Nombre,
              apellido: paciente[0].Apellido,
              email: paciente[0].Correo,
              telefono: paciente[0].Celular,
              razons: paciente[0].razons,
              rfc: paciente[0].Rfc,
              calle: paciente[0].calle,
              numero_exterior: paciente[0].numero_exterior,
              numero_interior: paciente[0].numero_interior,
              codpos: paciente[0].codpos,
              colonia: paciente[0].colonia,
              estado: paciente[0].estado,
              ciudad: paciente[0].ciudad,
            });
          }
        },
        error: (e) => console.log(e),
      });
  }

  clienteFactura(cliente) {
    if (cliente.uid !== "") {
      this.updateClienteFactura(cliente);
    } else {
      this.postSvc.post("crearCliente.php", cliente).subscribe((res: any) => {
        if (res.status === "success") {
          this.clienteFacturaBD(res.Data);
        } else {
          this.alertSvc.alert(
            "",
            "Ocurrió un error intentelo de nuevo",
            "error"
          );
        }
      });
    }
  }

  updateClienteFactura(cliente) {
    this.postSvc
      .post("actualizarCliente.php", cliente)
      .subscribe((res: any) => {
        if (res.status === "success") {
          this.clienteFacturaBD(res.Data);
        } else {
          this.alertSvc.alert(
            "",
            "Ocurrió un error intentelo de nuevo",
            "error"
          );
        }
      });
  }

  clienteFacturaBD(datosPaciente) {
    // console.log(datosPaciente);
    // return;
    const idPaciente = JSON.parse(localStorage.getItem("perfilusuario"));
    this.postSvc
      .post("datos-personales.php", { caso: 4, datosPaciente, idPaciente })
      .subscribe((res: any) => {
        if (res === "Se modifico") {
          this.alertSvc.alert(
            "",
            "Se registrarón los datos a facturar",
            "success"
          );
          this.getDatosPaciente();
        } else {
          this.alertSvc.alert(
            "",
            "Ocurrió un error intentelo de nuevo",
            "error"
          );
        }
      });
  }

  generarFactura(cliente) {
    this.postSvc.post("crearCFDI.php", cliente).subscribe((res: any) => {
      if (res.response === "success") {
        this.saveFacturaBD(res);
      } else {
        this.alertSvc.alert(
          "",
          `Ocurrio un error ${res.message.messageDetail}`,
          "error"
        );
      }
    });
  }

  saveFacturaBD(datosPaciente) {
    const idPaciente = JSON.parse(localStorage.getItem("perfilusuario"));
    this.postSvc
      .post("datos-personales.php", { caso: 5, datosPaciente, idPaciente })
      .subscribe((res: any) => {
        if (res === "Se modifico") {
          this.alertSvc.alert(
            "",
            "Se registró el folio de su factura",
            "success"
          );
        } else {
          this.alertSvc.alert(
            "",
            "Ocurrió un error intentelo de nuevo",
            "error"
          );
        }
      });
  }

  firstLetterUppercase(id) {
    // let texto = (<HTMLInputElement>document.getElementById(id))?.value;
    // if (texto !== "") {
    //   let textofinal = texto[0]?.toUpperCase() + texto?.slice(1);
    //   (<HTMLInputElement>document.getElementById(id)).value = textofinal;
    // }
  }
}
