import { Component, OnInit} from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import Swal from 'sweetalert2';
import { SubirArchivoService } from "src/app/admin/administrador/documentos/documento/subir-archivo.service";
import { AuthService } from "src/app/core/service/auth.service";

@Component({
  selector: "app-fotos",
  templateUrl: "./fotos.component.html",
  styles: [],
})
export class FotosComponent implements OnInit {
  arregloimagenes: any = [];

  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  public respuestaImagenEnviada;
  Archivoseleccionado: null;
  nombreArchivo: any;
  Dataarchivo: File = null;
  archivo = "";
  archivoFinal: any;
  idClinica: number;
  idUsuario: number;
  currentIndex: any = -1;
  showFlag: any = false;

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar,
    private router: Router,
    private enviandoImagen: SubirArchivoService,
    private authService: AuthService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.idUsuario = this.authService.currentUserValue.id;
  }
  ngOnInit() {
    if (this.idClinica > 0) {
      this.obtenerfotos();
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
  }

  showLightbox(index) {
    this.currentIndex = index;
    this.showFlag = true;
  }

  closeEventHandler() {
    this.showFlag = false;
    this.currentIndex = -1;
  }

  /** obtener fotos */
  obtenerfotos(): void {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 4, idc: this.idClinica };
    const URL: any = this.baseURL + "documentos.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        this.arregloimagenes = respuesta;
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  // metdo para obtener la imagen de input file
  Subirimagen(event, files: FileList, fileInput: any) {
    console.log("Entro");
    const Numero1 = Math.floor(Math.random() * 10001).toString();
    const Numero2 = Math.floor(Math.random() * 1001).toString();
    // Nombre del archivo
    this.Archivoseleccionado = event.target.files[0];
    this.nombreArchivo = event.target.files[0].name;
    this.archivo = Numero1 + Numero2 + this.nombreArchivo.replace(/ /g, "");
    this.archivoFinal = files;
    this.Dataarchivo = <File>fileInput.target.files[0];
    this.cargandoImagen();
  }
  // metodo para subir la imagen y guardar el resultado en base de datos
  cargandoImagen() {
    this.mensajesubiendoinf();
    if (this.archivo === "") {
      this.swalalertmensaje("error", "Debe seleccionar un archivo");
    } else {
      this.enviandoImagen
        .subirfoto(this.archivoFinal[0], this.archivo)
        .subscribe((response) => {
          console.log(response);
          this.respuestaImagenEnviada = response;
          if (this.respuestaImagenEnviada.msj === "Imagen subida") {
            const headers: any = new HttpHeaders({
              "Content-Type": "application/json",
            });
            const options: any = {
              caso: 3,
              named: this.archivo,
              idc: this.idClinica,
              idUsuario: this.idUsuario,
            };
            const URL: any = this.baseURL + "documentos.php";
            this.httpClient
              .post(URL, JSON.stringify(options), headers)
              .subscribe((res) => {
                if (res.toString() === "Se inserto") {
                  this.obtenerfotos();
                  this.swalalertmensaje(
                    "success",
                    "Imagen subida correctamente"
                  );
                } else {
                  this.swalalertmensaje(
                    "error",
                    "Ocurrio un error, intente de nuevo por favor"
                  );
                }
              });
          } else if (
            this.respuestaImagenEnviada.msj === "Formato no soportado"
          ) {
            this.swalalertmensaje(
              "error",
              "No es un formato valido, intente de nuevo por favor"
            );
          } else {
            this.swalalertmensaje(
              "error",
              "Ocurrio un error, intente de nuevo por favor"
            );
          }
        });
    }
  }

  eliminarfoto(nombre, id) {
    console.log(nombre + "---" + id);
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 5, named: nombre, idd: id };
    const URL: any = this.baseURL + "documentos.php";
    this.httpClient
      .post(URL, JSON.stringify(options), headers)
      .subscribe((res) => {
        if (res.toString() === "Se elimino") {
          this.obtenerfotos();
          this.swalalertmensaje("success", "Imagen eliminada correctamente");
        } else {
          this.swalalertmensaje(
            "error",
            "Ocurrio un error, intente de nuevo por favor"
          );
        }
      });
  }

  mensajesubiendoinf() {
    Swal.fire({
      title: "Subiendo imagen ",
      html: "Espere un momento por favor",
      allowOutsideClick: false,
      showCancelButton: false,
      showConfirmButton: false,
    });
  }

  swalalertmensaje(icon, mensaje) {
    Swal.fire({
      position: "center",
      icon: icon,
      title: mensaje,
      showConfirmButton: true,
      allowOutsideClick: false,
      confirmButtonText: "Aceptar",
    });
  }
}
