import { Component, OnInit, ViewChild } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import {
  FormGroup,
  FormBuilder,
  Validators,
} from "@angular/forms";
import { AuthService } from "../../../../core/service/auth.service";
import { HttpService } from "../../../../core/service/http.service";
import { AlertService } from "../../../../core/service/alert.service";

@Component({
  selector: "app-presupuesto",
  templateUrl: "./presupuesto.component.html",
  styles: [],
})
export class PresupuestoComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  presupuestos = [];
  presupuestosFilter = [];
  servicios = [];
  register: FormGroup;
  selectedOption: string;
  idClinica;
  idPaciente;
  doctores = [];
  tratamientos = [];
  descuento = 0;
  Presupuestos = [];
  convenios = [];
  nombrePresupuesto;
  imagenclinicaPrint = "";
  dientes = [
    2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22,
    23, 24, 25, 26, 27, 28, 29, 30, 31,
  ];

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private sweet: AlertService,
    private modalService: NgbModal,
    private authService: AuthService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    const info = localStorage.getItem("perfilusuario");
    this.idPaciente = info.replace('"', "").replace('"', "");
  }

  ngOnInit() {
    this.imagenclinicaPrint =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
    this.register = this.fb.group({
      nombre: ["", [Validators.required]],
      doctor: ["", [Validators.required]],
      categoria: ["", [Validators.required]],
      convenio: [""],
      servicio: ["", [Validators.required]],
      diente: ["", [Validators.required]],
      Subtotal: ["", [Validators.required]],
      Descuento: ["0", [Validators.required]],
      Total: ["", [Validators.required]],
    });
    this.GetDoctores();
    this.GetConvenios();
    this.getPresupuestos();
  }

  GetConvenios() {
    this.http
      .post("convenios.php", { caso: 8, idclinica: this.idClinica })
      .subscribe({
        next: (convenios: any) => {
          this.convenios = convenios;
        },
        error: (err) => console.log(err),
      });
  }

  getPresupuestos(refresh?) {
    const options: any = { caso: 0, idPaciente: this.idPaciente };
    this.http.post("presupuestos.php", options).subscribe({
      next: (data: any) => {
        this.presupuestos = data;
        this.presupuestosFilter = data;
        refresh && this.sweet.toast("los presupuestos");
      },
      error: (err) => console.log(err),
    });
  }

  GetDoctores() {
    const options: any = { caso: 5, idClinica: this.idClinica };
    this.http.post("pacientes.php", options).subscribe({
      next: (doctores: any) => {
        this.doctores = doctores;
      },
      error: (err) => console.log(err),
    });
  }

  getTratamientosByConvenio(idConvenio) {
    console.log(idConvenio);
    this.http
      .post("convenios.php", { caso: 17, idConvenio })
      .subscribe({
        next: (tratamientos: any) => {
          if (tratamientos !== null) {
            this.tratamientos = tratamientos;
          }else{
            this.register.patchValue({
              convenio:'',
            });
            this.getTratamientos();
          }
        },
        error: (err) => console.log(err),
      });
  }

  getTratamientos() {
    this.http
      .post("AdminCategorias.php", { caso: 1, idclinica: this.idClinica })
      .subscribe({
        next: (tratamientos: any) => {
          this.tratamientos = tratamientos;
        },
        error: (err) => console.log(err),
      });
  }

  getServ(tratamiento) {
    this.http
      .post("presupuestos.php", {
        caso: 7,
        idCategoria: tratamiento.idCategorias,
      })
      .subscribe({
        next: (servicios: any) => {
          this.servicios = servicios;
        },
        error: (err) => console.log(err),
      });
  }

  suma(valor) {
    const precio = Number(valor);
    this.register.patchValue({
      Subtotal: precio,
      Total: precio,
    });
  }

  CambiarTotal() {
    const Subtotal: any = Number(
      this.register.controls["Subtotal"].value
    ).toFixed(2);
    const porcent: any = Number((Subtotal * this.descuento) / 100).toFixed(2);
    const resta: any = Number(Subtotal - porcent).toFixed(2);
    this.register.controls["Total"].setValue(resta);
  }

  addPresupuesto(content) {
    this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
      size: "lg",
    });
  }

  onAddRowSave() {
    const options: any = {
      caso: 1,
      presupuesto: this.Presupuestos,
      idPaciente: this.idPaciente,
      nombrePresup: this.nombrePresupuesto,
    };
    this.http.post("presupuestos.php", options).subscribe({
      next: (respuesta: any) => {
        if (respuesta == "Se inserto") {
          this.Presupuestos = [];
          this.sweet.alert(
            "",
            "Se guardo el presupuesto correctamente",
            "success"
          );
          this.ngOnInit();
          this.modalService.dismissAll();
        } else {
          this.sweet.alert("", "Ocurrio un error intentelo de nuevo", "error");
        }
      },
      error: (err) => console.log(err),
    });
  }

  eliminarPresup(idPresupuesto) {
    const options: any = { caso: 6, idPresupuesto };
    this.http.post("presupuestos.php", options).subscribe({
      next: (data: any) => {
        if (data == "Se elimino") {
          this.sweet.alert(
            "",
            "Se elimino el presupuesto correctamente",
            "success"
          );
          this.ngOnInit();
        } else {
          this.sweet.alert("", "Ocurrio un error intentelo de nuevo", "error");
        }
      },
      error: (err) => console.log(err),
    });
  }

  arregloPresupuesto(form: FormGroup) {
    this.Presupuestos.push(form.value);
    this.nombrePresupuesto = form.value.nombre;
    const doctor = form.value.doctor;
    this.register.reset();
    this.register.controls["nombre"].setValue(this.nombrePresupuesto);
    this.register.controls["doctor"].setValue(doctor);
    this.register.controls["Descuento"].setValue("0");
  }

  eliminarPresupArray(i) {
    this.Presupuestos.splice(i, 1);
  }

  filterDatatable(event) {
    this.table.offset = 0;
  }

  FirstUppercaseLetter(idname) {
    const texto = (<HTMLInputElement>document.getElementById(idname)).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      (<HTMLInputElement>document.getElementById(idname)).value = textofinal;
    }
  }
}
