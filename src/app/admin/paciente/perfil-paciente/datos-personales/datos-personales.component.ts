import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "src/app/core/service/auth.service";
import Swal from "sweetalert2";
import { SubirarchivoService } from "src/app/core/service/subirarchivo.service";
const headers: any = new HttpHeaders({ "Content-Type": "application/json" });
import moment from "moment";
import { DetallePacienteComponent } from "../detalle-paciente/detalle-paciente.component";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";

@Component({
  selector: "app-datos-personales",
  templateUrl: "./datos-personales.component.html",
  styles: [],
})
export class DatosPersonalesComponent implements OnInit {
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  private baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  patientForm: FormGroup;
  editar: boolean = false;
  data = [];
  sucursales: any = [];
  idPaciente;
  form: boolean;
  disabled = true;
  imagen: File = null;
  idClinica: number;
  docs: any = [];
  convenios: any = [];
  familiaRegistrado: any = [];
  nombreImagen: string;
  contador_palabras_nom: number;
  contador_palabras_apelli: number;
  contador_palabras_ocupa: number;
  contador_palabras_parentescofamiliarregis: number;
  contador_palabras_contactoemer: number;
  contador_palabras_parentescofamiliaremer: number;
  contador_palabras_responsa: number;
  edad = "";

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private http: HttpClient,
    private subirImagenService: SubirarchivoService,
    private perfilpaciente: DetallePacienteComponent
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    const info = localStorage.getItem("perfilusuario");
    this.idPaciente = info.replace('"', "").replace('"', "");
  }

  ngOnInit(): void {
    this.getSucursales();
    this.obtenerDoctores();
    this.obtenerConvenios();
    this.obtenerFamiliaRegistrado();
  }

  getSucursales() {
    const options: any = { caso: 0, idclinica: this.idClinica };
    const URL: any = this.baseURL + "categorias.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        this.sucursales = respuesta;
        this.obtenerDatosPaciente();
      });
  }

  public handleAddressChange(address: Address) {
    this.patientForm.patchValue({
      Direccion: address.formatted_address,
    });
  }
  activarform() {
    this.patientForm.enable();
    this.disabled = false;
  }

  desactivarform() {
    //aleta de mensaje
    Swal.fire({
      title: "Estas seguro que deseas cancelar?",
      text: "se perderán los cambios realizados",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        this.obtenerDatosPaciente();
        this.patientForm.disable();
        this.disabled = true;
      }
    });
  }

  obtenerDatosPaciente() {
    const options: any = { caso: 0, idPaciente: this.idPaciente };
    this.http
      .post(
        `${this.baseURL}datos-personales.php`,
        JSON.stringify(options),
        headers
      )
      .subscribe((data: any) => {
        this.data = data;
        if (data == null) {
          this.patientForm = this.fb.group({
            Nombre: ["", [Validators.required]],
            Apellido: ["", [Validators.required]],
            Rfc: ["", [Validators.maxLength(13)]],
            Convenio: [""],
            Direccion: [""],
            Celular: ["", [Validators.required]],
            Correo: ["", [Validators.required, Validators.email]],
            FechaNac: [""],
            Edad: ["", [Validators.required]],
            GrupoSanguineo: ["", [Validators.required]],
            Genero: ["", [Validators.required]],
            EstadoCivil: [""],
            sucursal: [""],
            Encontraste: [""],
            Ocupacion: [""],
            Observaciones: [""],
            Doctor: [""],
            NombreFami: ["", [Validators.required]],
            Parentesco: ["", [Validators.required]],
            Parentesco_emergencia: [""],
            Tel_Emergencia: ["", [Validators.required]],
            Responsable_Pago: [""],
            imagen: [""],
          });
          this.form = true;
        } else {
          this.patientForm = this.fb.group({
            Nombre: [this.data[0].Nombre, [Validators.required]],
            Apellido: [this.data[0].Apellido, [Validators.required]],
            Rfc: [this.data[0].Rfc],
            Convenio: [this.data[0].Convenio],
            Direccion: [this.data[0].Direccion],
            Celular: [this.data[0].Celular],
            Correo: [this.data[0].Correo, [Validators.email]],
            FechaNac: [this.data[0].FechaNac],
            Edad: [this.data[0].Edad],
            GrupoSanguineo: [this.data[0].GrupoSanguineo],
            Genero: [this.data[0].Genero],
            EstadoCivil: [this.data[0].EstadoCivil],
            sucursal: [this.data[0].sucursal],
            Encontraste: [this.data[0].Encontraste],
            Ocupacion: [this.data[0].Ocupacion],
            Observaciones: [this.data[0].Observaciones],
            Doctor: [this.data[0].Doctor],
            NombreFami: [this.data[0].NombreFami],
            Parentesco: [this.data[0].Parentesco],
            Parentesco_emergencia: [this.data[0].Parentesco_emergencia],
            Tel_Emergencia: [this.data[0].Tel_Emergencia],
            Responsable_Pago: [this.data[0].Responsable_Pago],
            imagen: [this.data[0].imagen],
          });
          this.form = true;
          this.disabled = true;
          this.patientForm.disable();
        }
      });
  }

  onSubmit() {}

  // metodo para obtener Doctores
  obtenerDoctores() {
    const options: any = { caso: 5, idClinica: this.idClinica };
    this.http
      .post(`${this.baseURL}pacientes.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.docs = respuesta;
      });
  }

  // metodo para obtener los convenios
  obtenerConvenios() {
    const options: any = { caso: 8, idclinica: this.idClinica };
    this.http
      .post(`${this.baseURL}convenios.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.convenios = respuesta;
      });
  }

  // metodo para obtener listado de usuarios
  obtenerFamiliaRegistrado() {
    const options: any = { caso: 0, idClinica: this.idClinica };
    this.http
      .post(`${this.baseURL}pacientes.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.familiaRegistrado = respuesta;
      });
  }

  actualizarPaciente() {
    if (
      this.patientForm.get("FechaNac").value !== "" ||
      this.patientForm.get("FechaNac").value !== null
    ) {
      var fecha = this.patientForm.get("FechaNac").value;
      this.patientForm.controls.FechaNac.setValue(
        moment(fecha).format("YYYY-MM-DD")
      );
    }
    if (this.imagen != null) {
      this.mensajesubiendoinf();
      this.subirImagenService
        .postFileImagen(this.imagen, this.nombreImagen)
        .subscribe((response: any) => {
          if (response.msj === "Imagen subida") {
            this.patientForm.controls["imagen"].setValue(this.nombreImagen);
            const options: any = {
              caso: 2,
              datosPaciente: this.patientForm.value,
              idPaciente: this.idPaciente,
            };
            this.http
              .post(
                `${this.baseURL}datos-personales.php`,
                JSON.stringify(options),
                headers
              )
              .subscribe((respuesta) => {
                const res = respuesta;
                if (res.toString() === "Se modifico") {
                  Swal.close();
                  this.swalalertmensaje(
                    "success",
                    "Paciente actualizado correctamente"
                  );
                  this.ngOnInit();
                  this.patientForm.disable();
                  this.disabled = true;
                  this.perfilpaciente.informacionpaciente();
                } else {
                  this.swalalertmensaje(
                    "error",
                    "Ocurrio un error, intente de nuevo por favor"
                  );
                }
              });
          } else {
            this.swalalertmensaje(
              "error",
              "La imagen no pudo subirse, intente de nuevo por favor"
            );
          }
        });
    } else {
      const options: any = {
        caso: 2,
        datosPaciente: this.patientForm.value,
        idPaciente: this.idPaciente,
      };
      this.http
        .post(
          `${this.baseURL}datos-personales.php`,
          JSON.stringify(options),
          headers
        )
        .subscribe((respuesta) => {
          const res = respuesta;
          if (res.toString() === "Se modifico") {
            this.swalalertmensaje(
              "success",
              "Paciente actualizado correctamente"
            );
            this.ngOnInit();
            this.patientForm.disable();
            this.disabled = true;
            this.perfilpaciente.informacionpaciente();
          } else {
            this.swalalertmensaje(
              "error",
              "Ocurrio un error, intente de nuevo por favor"
            );
          }
        });
    }
  }

  guardarImgenEnVariable(img: File) {
    this.imagen = img;
    this.nombreImagen = Date.now().toString() + this.imagen.name;
  }

  //aleta de mensaje
  swalalertmensaje(icon, mensaje) {
    Swal.fire({
      position: "center",
      icon: icon,
      title: mensaje,
      showConfirmButton: true,
      allowOutsideClick: false,
      confirmButtonText: "Aceptar",
    });
  }
  // mandar un dialogo donde se le indique al cliente que se esta procesando su informacion
  mensajesubiendoinf() {
    Swal.fire({
      title: "Subiendo información",
      html: "Espere un momento por favor",
      allowOutsideClick: false,
      showCancelButton: false,
      showConfirmButton: false,
    });
  }

  // Metodo para calcular edad
  calcularedad(e) {
    var fecha = (<HTMLInputElement>document.getElementById("fechanac")).value;
    var edad = String(moment().diff(fecha, "years", false));
    (<HTMLInputElement>document.getElementById("edadpa")).value = edad;
    this.edad = edad;
    this.patientForm.controls.Edad.setValue(edad);
  }

  //solo letras
  keyPress(e: any) {
    const key = e.keycode || e.which;
    const teclado = String.fromCharCode(key).toLowerCase();
    const letras = " abcdefghijklmnñopqrstuvwxyzáéíóú";
    if (letras.indexOf(teclado) == -1) {
      return false;
    }
  }

  FirstUppercaseLetter(idname) {
    const texto = (<HTMLInputElement>document.getElementById(idname)).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      (<HTMLInputElement>document.getElementById(idname)).value = textofinal;
    }
  }

  convertirletraamayuscula() {
    var texto = (<HTMLInputElement>document.getElementById("rfc")).value;
    (<HTMLInputElement>document.getElementById("rfc")).value =
      texto.toUpperCase();
  }
}
