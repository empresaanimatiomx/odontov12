import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { AlertService } from "../../../core/service/alert.service";
import { HttpService } from "../../../core/service/http.service";
import { AuthService } from "../../../core/service/auth.service";

@Component({
  selector: "app-sucursal",
  templateUrl: "./sucursal.component.html",
  styleUrls: [],
})
export class SucursalComponent implements OnInit {
  @ViewChild("epltable", { static: false }) epltable: ElementRef;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  img = new FormData();
  nombreImg = "";
  isEmail = /\S+@\S+\.\S+/;
  sucursales = [];
  sucursalesFilter = [];
  codigos = [];
  sucursalForm: FormGroup;
  idClinica;
  idSucursal;
  imagenclinicaPrint = "";
  nameImagen;

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router,
    private http: HttpService,
    private sweet: AlertService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.imagenclinicaPrint =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
    this.sucursalForm = this.fb.group({
      NombreSucursal: ["", Validators.required],
      Direccion: ["", Validators.required],
      codTelefono: [""],
      Telefono: ["", Validators.minLength(10)],
      codWhatsapp: [""],
      Whatsapp: ["", Validators.minLength(10)],
      Correo: ["", Validators.pattern(this.isEmail)],
      Nombrecorto: [""],
      Logo: [""],
    });
  }

  ngOnInit() {
    if (this.idClinica > 0) {
      this.getSucursales();
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.irAClinica());
    }
    this.getCodPaises();
  }

  getCodPaises() {
    this.http.getCodPaises().subscribe((v: any) => {
      this.codigos = v;
    });
  }

  public handleAddressChange(address: Address) {
    this.sucursalForm.patchValue({
      Direccion: address.formatted_address,
    });
  }

  irAClinica() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  cargarImagen(event) {
    if (event.target.files.length) {
      const file = event.target.files[0];
      this.nombreImg = `${new Date().getTime()}${file.name}`;
      this.img.append("imagenPropia", file, this.nombreImg);
    }
  }

  getSucursales(refresh?) {
    this.http
      .post("sucursal.php", { caso: 0, idClinica: this.idClinica })
      .subscribe((respuesta: any) => {
        this.sucursales = respuesta;
        this.sucursalesFilter = respuesta;
        refresh && this.snackBar.open(
          "Se actualizaron las sucursales",
          "Aceptar",
          { duration: 3000 }
        );
      });
  }

  cerrarmodal(form: FormGroup) {
    this.nombreImg = "";
    this.img = new FormData();
    form.reset();
    this.modalService.dismissAll();
  }

  editarSucursal(row, content) {
    this.nameImagen = row.Logo;
    this.idSucursal = row.idSucursal;
    let ngbModalOptions: NgbModalOptions = {
      centered: true,
      ariaLabelledBy: "modal-basic-title",
      size: "lg",
    };
    this.sucursalForm.patchValue(row);
    this.modalService.open(content, ngbModalOptions);
  }

  preUpdate(sucursal: any) {
    if (this.nombreImg === "") {
      this.sweet.subiendo("su información");
      this.actualizarSucursal(sucursal, this.idSucursal);
    } else {
      sucursal.Logo = this.nombreImg;
      this.subirImg(sucursal, this.idSucursal);
    }
  }

  subirImg(sucursal: any, idSucursal) {
    this.sweet.subiendo("su información");
    this.http.postFile("uploadfoto.php", this.img).subscribe({
      next: (res: any) => {
        if (res.msj === "Imagen subida") {
          this.actualizarSucursal(sucursal, idSucursal);
        } else {
          this.sweet.alert("", "No se subio la imagen", "error");
        }
      },
      error: (err) => {
        this.sweet.alert("", "No se subio la imagen", "error");
        console.log(err);
      },
    });
  }

  actualizarSucursal(sucursal: any, idSucursal) {
    const options: any = {
      caso: 3,
      idSucursal,
      suc: {
        telCompleto: `${sucursal.codTelefono}${sucursal.Telefono}`,
        wspCompleto: `${sucursal.codWhatsapp}${sucursal.Whatsapp}`,
        ...sucursal,
      },
    };
    this.http.post("sucursal.php", options).subscribe({
      next: (res: any) => {
        if (res === "Se modifico") {
          this.sweet.alert("", "Se actualizo la sucursal", "success");
          this.cerrarmodal(this.sucursalForm);
          this.getSucursales();
        } else {
          this.sweet.alert(
            "",
            "Ocurrio un error, intentelo nuevamente",
            "error"
          );
        }
      },
      error: (err) => console.log(err),
    });
  }

  firstLetterUpperCase(name) {
    const texto = this.sucursalForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.sucursalForm.controls[name].setValue(textofinal);
    }
  }

  convertirletraamayuscula() {
    const texto = (<HTMLInputElement>document.getElementById("rfc")).value;
    (<HTMLInputElement>document.getElementById("rfc")).value =
      texto.toUpperCase();
  }

  filterDatatable(event: any) {
    if (event.target.value === "") {
      this.sucursales = this.sucursalesFilter;
    }
    this.sucursales = this.http.filter(event, this.sucursalesFilter);
    this.table.offset = 0;
  }

  eliminarSucu(row) {
    this.sweet.alertConfirm("eliminar esta sucursal?").then((result) => {
      if (result.isConfirmed) {
        if (row.Matriz === "1") {
          this.sweet.alert(
            "",
            "La sucursal matriz no puede ser eliminada",
            "error"
          );
        }
        if (row.Matriz === "") {
          this.http
            .post("sucursal.php", { caso: 4, idSucursal: row.idSucursal })
            .subscribe((respuesta) => {
              const res = respuesta;
              if (res.toString() === "Se elimino") {
                this.getSucursales();
                this.sweet.alert(
                  "",
                  "Sucursal eliminada correctamente",
                  "success"
                );
              } else {
                this.sweet.alert(
                  "",
                  "Ocurrio un error, intente de nuevo por favor",
                  "error"
                );
              }
            });
        }
      }
    });
  }
}
