import { Page404Component } from './../../authentication/page404/page404.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClinicaComponent } from './clinica/clinica.component';
import { SucursalComponent } from './sucursal/sucursal.component';
import { NotasComponent } from './notas/notas.component';
import { SidebarComponent } from './../../layout/sidebar/sidebar.component';
import { NuevaSucursalComponent } from './nueva-sucursal/nueva-sucursal.component';

const routes: Routes = [
  {
    path: 'clinica',
    component: ClinicaComponent,
  },
  {
    path: 'sucursal',
    component: SucursalComponent,
  },
  {
    path: 'nueva-sucursal',
    component: NuevaSucursalComponent,
  },
  {
    path: 'nota',
    component: NotasComponent,
  },
  {
    path: 'menuIzq',
    component: SidebarComponent,
  },
  { path: '**', component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfiguracionRoutingModule {}