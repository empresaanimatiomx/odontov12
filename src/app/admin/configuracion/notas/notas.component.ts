import { HttpClient,HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/service/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-notas',
  templateUrl: './notas.component.html',
  styleUrls: []
})
export class NotasComponent implements OnInit{
  baseURL = 'https://projectsbyanimatiomx.com/phps/odonto/';
  roomForm: FormGroup;
  idClinica: number;
  Nota: any = [];
  valor = '';
  idNota = '';
  mensajes_validacion = {
    'nota': [
      { type: 'required', message: 'El campo es requerido' },
      { type: 'maxlength', message: 'El campo no debe ser mayor a 250 caracteres' },
    ],
  }

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router,
    ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.roomForm = this.fb.group({
        Note: ["", Validators.compose([
          Validators.required,
          Validators.maxLength(250)
        ])],
      });
  }

  ngOnInit(): void {
    if (this.idClinica > 0){
      this.verificarNota();
    }else{
      let snack = this.snackBar.open("Debes agregar una clinica","Agregar Clinica", {duration: 4000} );
      snack.onAction().subscribe( () => this.iraclinica());
    }
  }

  //metodo para ir a registrarclinica

  iraclinica(){
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  // Consulta si la clinica ya tiene nota
  verificarNota() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { caso: 0, idc: this.idClinica };
    const URL: any = this.baseURL + 'notas.php';
    this.httpClient.post(URL, JSON.stringify(options), headers,).subscribe(
      respuesta => {
        this.Nota = respuesta;
        if (this.Nota !== null){
          for (let i = 0; i < this.Nota.length; i++) {
            this.roomForm.setValue({
              Note: this.Nota[0].Nota,
            });
            this.idNota = this.Nota[0].idNotas;
          }
          this.valor = '1';
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      }
      );
  }

  onSubmit() {
    if (this.valor === '')
    {
      this.agregarNota(this.roomForm.getRawValue(), String(this.idClinica)).subscribe( respuesta =>{
        const res = respuesta;
        if (res.toString() === 'Se inserto'){
          this.swalalertmensaje('success','Nota agregada correctamente');
          this.verificarNota();
        }else {
          this.swalalertmensaje('error','Ocurrio un error, intente de nuevo por favor');
        }
      });;
    }
    if (this.valor === '1') {
      this.ModificarNota(this.roomForm.getRawValue(), Number(this.idNota)).subscribe( respuesta =>{
        const res = respuesta;
        if (res.toString() === 'Se modifico'){
          this.swalalertmensaje('success','Nota editada correctamente');
          this.verificarNota();
        }else {
          this.swalalertmensaje('error','Ocurrio un error, intente de nuevo por favor');
        }
      });
    }
  }

  // Agrega nota
  agregarNota(nota, Clinica: String) {
    let nom = nota.Note;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1, 'not':nom, 'idc': Clinica};
    const URL: any = this.baseURL + 'notas.php';
    return this.httpClient.post(URL, JSON.stringify(options), headers);
  }
      // Modifica receta
  ModificarNota(notaS, idNota: number) {
    let nota = notaS.Note;
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 2, 'not':nota, 'idn': idNota};
    const URL: any = this.baseURL + 'notas.php';
      return this.httpClient.post(URL, JSON.stringify(options), headers);
  }

  // metodo para mandar toast

  showNotification(colorName, text, placementFrom, placementAlign) {
    this.snackBar.open(text, '', {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }

  //aleta de mensaje
  swalalertmensaje(icon,mensaje){
    Swal.fire({
      position: 'center',
      icon: icon,
      title: mensaje,
      showConfirmButton: true,
      allowOutsideClick: false,
      confirmButtonText: 'Aceptar'
    })
  }

}
