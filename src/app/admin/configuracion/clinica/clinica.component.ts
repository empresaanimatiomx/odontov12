import { Component, ViewChild, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { AlertService } from "../../../core/service/alert.service";
import { HttpService } from "../../../core/service/http.service";
import { AuthService } from "../../../core/service/auth.service";
import { SubirarchivoService } from "../../../core/service/subirarchivo.service";

@Component({
  selector: "app-clinica",
  templateUrl: "./clinica.component.html",
  styleUrls: [],
})
export class ClinicaComponent implements OnInit {
  @ViewChild("placesRef")
  placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  Datosclinica: any = [];
  codigos: any = [];
  valor = "";
  idClinica = "";
  idUsuario = "";
  img: null;
  nombreImg = "";
  public respuestaImagenEnviada;
  public resultadoCarga;
  roomForm: FormGroup;
  mensajes_validacion = {
    nombreclinica: [
      { type: "required", message: "El nombre de usuario es requerido" },
    ],
    rfc: [
      { type: "required", message: "El RFC es requerido" },
      { type: "minlength", message: "El RFc debe de ser de 13 caracteres" },
    ],
    dirrecion: [{ type: "required", message: "La dirección es requerida" }],
    codigoTelefono: [
      { type: "required", message: "El telefono de contacto es requerido" },
      {
        type: "minlength",
        message: "El telefono de contacto debe ser de 10 digitos",
      },
    ],
    telefono: [
      { type: "required", message: "El telefono de contacto es requerido" },
      {
        type: "minlength",
        message: "El telefono de contacto debe ser de 10 digitos",
      },
    ],
    whatsapp: [
      { type: "required", message: "El telefono de mensajeria es requerido" },
      {
        type: "minlength",
        message: "El telefono de mensajeria debe ser de 10 digitos",
      },
    ],
    correocontacto: [
      { type: "required", message: "El correo de contacto es requerido" },
      {
        type: "pattern",
        message: "El correo de contacto no es un correo valido",
      },
    ],
    correoclinica: [
      { type: "required", message: "El correo de la clinica es requerido" },
      {
        type: "pattern",
        message: "El correo de la clinica no es un correo valido",
      },
    ],
    nombrecorto: [
      { type: "required", message: "El nombre corto es requerido" },
    ],
    logo: [{ type: "required", message: "El logo es requerido" }],
  };
  isEmail = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$";
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private enviandoImagen: SubirarchivoService,
    private sweet: AlertService,
    private http: HttpService
  ) {
    // obtener id de usuario
    this.idUsuario = String(this.authService.currentUserValue.id);
    const correo = this.authService.currentUserValue.username;
    this.roomForm = this.fb.group({
      nombreclinica: ["", Validators.required],
      rfc: ["", Validators.minLength(13)],
      dir: ["", Validators.required],
      codigoTelefono: ["", Validators.required],
      tel: ["", [Validators.required, Validators.minLength(10)]],
      codigoWhatsapp: ["", Validators.required],
      nummsjinst: ["", [Validators.required, Validators.minLength(10)]],
      emaildecontacto: [
        correo,
        [Validators.required, Validators.pattern(this.isEmail)],
      ],
      emaildeclinica: ["", Validators.pattern(this.isEmail)],
      nombrecorto: [""],
      fechaObjetivo: [""],
      objetivo: [""],
      logo: [""],
      calle: [""],
      numero_exterior: [""],
      numero_interior: [""],
      codpos: [""],
      colonia: [""],
      estado: [""],
      ciudad: [""],
      pais: [""],
    });
  }

  ngOnInit(): void {
    this.getCodPaises();
  }

  getCodPaises() {
    this.http.getCodPaises().subscribe((v: any) => {
      this.codigos = v;
      this.verificarClinica();
    });
  }

  public handleAddressChange(address: Address): void {
    let numero_exterior = "";
    let calle = "";
    let colonia = "";
    let ciudad = "";
    let estado = "";
    let codpos = "";
    let pais = "";
    for (let i = 0; i < address.address_components.length; i++) {
      const element = address.address_components[i].types[0];
      if (element === "street_number") {
        numero_exterior = address.address_components[i].long_name;
      }
      if (element === "route") {
        calle = address.address_components[i].long_name;
      }
      if (element === "sublocality_level_1") {
        colonia = address.address_components[i].long_name;
      }
      if (element === "locality") {
        ciudad = address.address_components[i].long_name;
      }
      if (element === "administrative_area_level_1") {
        estado = address.address_components[i].long_name;
      }
      if (element === "postal_code") {
        codpos = address.address_components[i].long_name;
      }
      if (element === "country") {
        pais = address.address_components[i].long_name;
      }
    }
    this.roomForm.patchValue({
      dir: address.formatted_address,
      calle,
      numero_exterior,
      codpos,
      colonia,
      estado,
      ciudad,
      pais,
    });
    this.roomForm.controls["calle"].disable();
    this.roomForm.controls["colonia"].disable();
    this.roomForm.controls["estado"].disable();
    this.roomForm.controls["ciudad"].disable();
    this.roomForm.controls["pais"].disable();
  }

  // Consulta si ya tiene clinica
  verificarClinica() {
    const idclinica = this.authService.currentUserValue.idclinica;
    if (idclinica === null) {
      this.sweet.alert("", "No se agregado ninguna clinica", "error");
    } else {
      this.http
        .post("clinicas.php", { caso: 0, idClinica: idclinica })
        .subscribe((respuesta) => {
          this.Datosclinica = respuesta;
          this.roomForm.patchValue({
            nombreclinica: this.Datosclinica[0].NombreClinica,
            rfc: this.Datosclinica[0].RFC,
            dir: this.Datosclinica[0].Direccion,
            tel: this.Datosclinica[0].Telefono,
            codigoTelefono: this.Datosclinica[0].codigoTelefono,
            nummsjinst: this.Datosclinica[0].Whatsapp,
            codigoWhatsapp: this.Datosclinica[0].codigoWhatsapp,
            emaildecontacto: this.Datosclinica[0].Email,
            emaildeclinica: this.Datosclinica[0].EmailClinica,
            nombrecorto: this.Datosclinica[0].NombreCorto,
            objetivo: this.Datosclinica[0].Objetivo,
            logo: this.Datosclinica[0].Logo,
            calle: this.Datosclinica[0].calle,
            numero_exterior: this.Datosclinica[0].numero_exterior,
            numero_interior: this.Datosclinica[0].numero_interior,
            codpos: this.Datosclinica[0].codpos,
            colonia: this.Datosclinica[0].colonia,
            estado: this.Datosclinica[0].estado,
            ciudad: this.Datosclinica[0].ciudad,
            pais: this.Datosclinica[0].pais,
          });
          this.idClinica = this.Datosclinica[0].idClinicas;
          this.valor = "1";
        });
    }
  }

  public confirmAdd() {
    if (this.valor === "") {
      this.cargandoImagen();
    }
    if (this.valor === "1") {
      this.editarinformacionclinica();
    }
  }

  onInputClick(event) {
    event.target.value = "";
  }

  //metdo para obtener la imagen de input file
  Subirimagen(img) {
    this.img = img;
    this.nombreImg = `${new Date().getTime()}${img.name}`;
  }

  //metodo para subir la imagen y guardar el resultado en base de datos
  cargandoImagen() {
    if (this.nombreImg === "") {
      this.roomForm.controls["logo"].setValue("");
      this.agregarClinica(
        this.roomForm.getRawValue(),
        this.idUsuario
      ).subscribe((res) => {
        if (res !== null) {
          console.log(res);
          this.nombreImg = "";
          this.sweet.alertClose();
          this.authService.currentUserValue.idclinica = res[0];
          this.authService.currentUserValue.sucursal = res[1];
          this.authService.currentUserValue.Logo_clinica = res[1];
          this.authService.currentUserValue.Nombre_clinica = res[1];
          const info = this.authService.currentUserValue;
          localStorage.setItem("datosusuarioclinica", JSON.stringify(info));
          this.sweet.alert("", "Clinica agregada correctamente", "success");
          this.verificarClinica();
        } else {
          this.sweet.alert(
            "",
            "Ocurrio un error, intente de nuevo por favor",
            "error"
          );
        }
      });
    } else {
      this.sweet.subiendo("su información");
      this.enviandoImagen
        .postFileImagen(this.img, this.nombreImg)
        .subscribe((response) => {
          this.respuestaImagenEnviada = response;
          if (this.respuestaImagenEnviada.msj === "Imagen subida") {
            this.roomForm.controls["logo"].setValue(this.nombreImg);
            this.agregarClinica(
              this.roomForm.getRawValue(),
              this.idUsuario
            ).subscribe((res) => {
              if (res !== null) {
                this.nombreImg = "";
                // this.authService.imgClinica$.emit(res[1].Logo_clinica);
                this.sweet.alertClose();
                this.authService.currentUserValue.idclinica = res[0];
                this.authService.currentUserValue.sucursal = res[1];
                this.authService.currentUserValue.Logo_clinica = res[1];
                this.authService.currentUserValue.Nombre_clinica = res[1];
                const info = this.authService.currentUserValue;
                localStorage.setItem(
                  "datosusuarioclinica",
                  JSON.stringify(info)
                );
                this.sweet.alert(
                  "",
                  "Clinica agregada correctamente",
                  "success"
                );
                this.verificarClinica();
              } else {
                this.sweet.alert(
                  "",
                  "Ocurrio un error, intente de nuevo por favor",
                  "error"
                );
              }
            });
          } else {
            this.sweet.alert(
              "",
              "Ocurrio un error, intente de nuevo por favor",
              "error"
            );
          }
        });
    }
  }

  // Agrega clinica
  agregarClinica(infoClinica, Uid) {
    const clinica = {
      telefonoCompleto: `${infoClinica.codigoTelefono}${infoClinica.tel}`,
      whatsappCompleto: `${infoClinica.codigoWhatsapp}${infoClinica.nummsjinst}`,
      ...infoClinica,
    };
    return this.http.post("clinicas.php", { caso: 1, clinica, Uid });
  }

  //metodo para editar informacion de clinica con condicion si se cambia imagen o no
  editarinformacionclinica() {
    if (this.nombreImg !== "") {
      this.cargarimageneditar();
    } else {
      this.ModificarCLinica(
        this.roomForm.getRawValue(),
        Number(this.idClinica)
      ).subscribe((respuesta) => {
        const res = respuesta;
        if (res.toString() === "Se modifico") {
          this.sweet.alertClose();
          this.sweet.alert("", "Clinica editada correctamente", "success");
          this.verificarClinica();
        } else {
          this.sweet.alert(
            "",
            "Ocurrio un error, intente de nuevo por favor",
            "error"
          );
        }
      });
    }
  }

  //metodo para subir la imagen y editar informacion de clinica
  public cargarimageneditar() {
    this.sweet.subiendo("imagen");
    this.enviandoImagen
      .postFileImagen(this.img, this.nombreImg)
      .subscribe((response) => {
        this.respuestaImagenEnviada = response;
        if (this.respuestaImagenEnviada.msj === "Imagen subida") {
          this.roomForm.controls["logo"].setValue(this.nombreImg);
          this.ModificarCLinica(
            this.roomForm.getRawValue(),
            Number(this.idClinica)
          ).subscribe((respuesta) => {
            const res = respuesta;
            if (res.toString() === "Se modifico") {
              this.nombreImg = "";
              this.sweet.alertClose();
              this.sweet.alert("", "Clinica editada correctamente", "success");
              this.verificarClinica();
            } else {
              this.sweet.alert(
                "",
                "Ocurrio un error, intente de nuevo por favor",
                "error"
              );
            }
          });
        } else {
          this.sweet.alert(
            "",
            "Ocurrio un error, intente de nuevo por favor",
            "error"
          );
        }
      });
  }

  // Modifica clinica
  ModificarCLinica(infoClinica, idClinica) {
    const clinica = {
      telefonoCompleto: `${infoClinica.codigoTelefono}${infoClinica.tel}`,
      whatsappCompleto: `${infoClinica.codigoWhatsapp}${infoClinica.nummsjinst}`,
      ...infoClinica,
    };
    this.router
      .navigateByUrl("/menuIzq", { skipLocationChange: true })
      .then(() => {
        this.router.navigate(["/admin/configuracion/clinica"]);
      });
    return this.http.post("clinicas.php", { caso: 2, clinica, idClinica });
  }

  firstLetterUpercase(id) {
    const texto = (<HTMLInputElement>document.getElementById(id)).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      (<HTMLInputElement>document.getElementById(id)).value = textofinal;
    }
  }

  convertirletraamayuscula() {
    const texto = (<HTMLInputElement>document.getElementById("rfc")).value;
    (<HTMLInputElement>document.getElementById("rfc")).value =
      texto.toUpperCase();
  }
}
