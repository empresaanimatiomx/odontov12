import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { AlertService } from "../../../core/service/alert.service";
import { HttpService } from "../../../core/service/http.service";

@Component({
  selector: "app-nueva-sucursal",
  templateUrl: "./nueva-sucursal.component.html",
  styles: [],
})
export class NuevaSucursalComponent implements OnInit {
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  sucursalForm: FormGroup;
  isEmail = /\S+@\S+\.\S+/;
  img = new FormData();
  nombreImg = "";
  codigos = [];

  constructor(
    private http: HttpService,
    private sweet: AlertService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.sucursalForm = this.fb.group({
      NombreSucursal: ["", Validators.required],
      Direccion: ["", Validators.required],
      codTelefono: [""],
      Telefono: ["", Validators.minLength(10)],
      codWhatsapp: [""],
      Whatsapp: ["", Validators.minLength(10)],
      Correo: ["", Validators.pattern(this.isEmail)],
      Nombrecorto: [""],
      Logo: [""],
    });
    this.getCodPaises();
  }

  getCodPaises() {
    this.http.getCodPaises().subscribe((v: any) => {
      this.codigos = v;
    });
  }

  public handleAddressChange(address: Address) {
    this.sucursalForm.patchValue({
      Direccion: address.formatted_address,
    });
  }

  preUpdate(sucursal: any) {
    if (this.nombreImg === "") {
      this.sweet.subiendo("su información");
      sucursal.Logo = "user_default.png";
      this.agregarSucursal(sucursal);
    } else {
      sucursal.Logo = this.nombreImg;
      this.subirImg(sucursal);
    }
  }

  agregarSucursal(sucursal: any) {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    const options: any = {
      caso: 1,
      idClinica: idclinica,
      suc: {
        ...sucursal,
        telCompleto: `${sucursal.codTelefono}${sucursal.Telefono}`,
        wspCompleto: `${sucursal.codWhatsapp}${sucursal.Whatsapp}`,
      },
    };
    this.http.post("sucursal.php", options).subscribe((res: any) => {
      if (res === "Se inserto") {
        this.sweet.alert("", "La sucursal se guardo correctamente", "success");
        this.img = new FormData();
        this.nombreImg = "";
        this.router.navigate(["/admin/configuracion/sucursal"]);
      } else {
        this.sweet.alert(
          "Error!",
          "Ocurrio un error, intentelo nuevamente",
          "error"
        );
      }
    });
  }

  subirImg(sucursal: any) {
    this.sweet.subiendo("su información");
    this.http.postFile("uploadfoto.php", this.img).subscribe((res: any) => {
      if (res.msj === "Imagen subida") {
        this.agregarSucursal(sucursal);
      } else {
        this.sweet.alert(
          "Error!",
          "Ocurrio un error, intentelo nuevamente",
          "error"
        );
      }
    });
  }

  cargarImagen(event) {
    if (event.target.files.length) {
      const file = event.target.files[0];
      this.nombreImg = `${new Date().getTime()}${file.name}`;
      this.img.append("imagenPropia", file, this.nombreImg);
    }
  }

  clearText = (str) => {
    const texto = str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    const textofinal = texto.replace(/[^a-zA-Z0-9]/g, "");
    return textofinal;
  };

  firstLetterUpperCase(name) {
    const texto = this.sucursalForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.sucursalForm.controls[name].setValue(textofinal);
    }
  }
}
