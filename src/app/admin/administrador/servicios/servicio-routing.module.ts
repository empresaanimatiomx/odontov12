import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoServiciosComponent } from './listado-servicios/listado-servicios.component';
import { NuevoServicioComponent } from './nuevo-servicio/nuevo-servicio.component';

const routes: Routes = [
    {
      path: 'nuevo-servicio',
      component: NuevoServicioComponent,
    },
    {
      path: 'listado-servicios',
      component: ListadoServiciosComponent,
    },
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class ServicioLevelRoutingModule {}