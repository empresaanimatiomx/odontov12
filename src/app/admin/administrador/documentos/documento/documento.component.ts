import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { Router } from "@angular/router";
import { AuthService } from "src/app/core/service/auth.service";
import { HttpService } from "src/app/core/service/http.service";
import { AlertService } from "src/app/core/service/alert.service";

@Component({
  selector: "app-documento",
  templateUrl: "./documento.component.html",
  styleUrls: [],
})
export class DocumentoComponent implements OnInit {
  @ViewChild("roleTemplate", { static: true }) roleTemplate: TemplateRef<any>;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  documentos: any = [];
  documentosFilter: any = [];
  idClinica: number;

  constructor(
    private http: HttpService,
    private sweet: AlertService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
  }

  ngOnInit() {
    if (this.idClinica > 0) {
      this.getDocumentos();
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
  }

  getDocumentos(refresh?): void {
    this.http
      .post("documentos.php", { caso: 0, idc: this.idClinica })
      .subscribe({
        next: (respuesta) => {
          this.documentos = respuesta;
          this.documentosFilter = respuesta;
          refresh && this.sweet.toast("los documentos");
        },
        error: (err) => console.log(err),
      });
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  filterDatatable(event) {
     if (event.target.value === "") {
       this.documentos = this.documentosFilter;
     }
     this.documentos = this.http.filter(event, this.documentosFilter);
     this.table.offset = 0;
  }

  deleteDocumento(row) {
    this.sweet.alertConfirm("eliminar este documento").then((result) => {
      if (result.isConfirmed) {
        this.http
          .post("documentos.php", { caso: 1, idd: row.idDocumentos })
          .subscribe((respuesta) => {
            const res = respuesta;
            if (res.toString() === "Se elimino") {
              this.getDocumentos();
              this.sweet.alert("", "se elimino el documento", "success");
            } else {
              this.sweet.alert(
                "",
                "Ocurrio un error intentelo de nuevo",
                "error"
              );
            }
          });
      }
    });
  }

  cargarArchivo(event) {
    if (event.target.files.length) {
      const file = event.target.files[0];
      const nombreFile = `${new Date().getTime()}${file.name}`;
      const fileData = new FormData();
      fileData.append("documentoPropio", file, nombreFile);
      this.subirArchivo(fileData, nombreFile);
    }
  }

  subirArchivo(file, nombreFile) {
    this.sweet.subiendo("su información");
    this.http.postFile("uploadarchivo2.php", file).subscribe({
      next: (res: any) => {
        if (res.msj === "Archivo subido") {
          this.agregarDoc(nombreFile);
        } else {
          this.sweet.alert("", "No se subio el archivo", "error");
        }
      },
      error: (err) => {
        this.sweet.alert("", "No se subio el archivo", "error");
        console.log(err);
      },
    });
  }

  agregarDoc(named) {
    const options: any = { caso: 2, named, idc: this.idClinica };
    this.http.post("documentos.php", options).subscribe({
      next: (res: any) => {
        if (res === "Se inserto") {
          this.getDocumentos();
          this.sweet.alert(
            "",
            "Se agrego el codumento corectamente",
            "success"
          );
        } else {
          this.sweet.alert("", "Ocurrio un error intentelo de nuevo", "error");
        }
      },
      error: (err) => console.log(err),
    });
  }
}
