import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubirArchivoService {
public url_servidor = "https://projectsbyanimatiomx.com/phps/odonto/uploadarchivo2.php";
public url_servidor1 = "https://projectsbyanimatiomx.com/phps/odonto/fotosdesdepaciente.php";
public url_servidor2 = "https://projectsbyanimatiomx.com/phps/odonto/uploadarchivo3.php";

constructor(private http: HttpClient) {
}

// eslint-disable-next-line
public postFileDoc(ParaSubir: File, nombre: string) {
  const formData = new FormData();
  formData.append('documentoPropio', ParaSubir, nombre);
  return this.http.post(this.url_servidor, formData);
}

public subirfoto(ParaSubir: File, nombre: string) {
  const formData = new FormData();
  formData.append('images', ParaSubir, nombre);
  return this.http.post(this.url_servidor1, formData);
}
// eslint-disable-next-line
public subirarchivo(ParaSubir: File, nombre: string) {
  const formData = new FormData();
  formData.append('documentoPropio', ParaSubir, nombre);
  return this.http.post(this.url_servidor2, formData);
}

}
