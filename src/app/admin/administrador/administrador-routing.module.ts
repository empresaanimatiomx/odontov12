import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Page404Component } from 'src/app/authentication/page404/page404.component';

const routes: Routes = [
  {
    path: 'usuario',
    loadChildren: () =>
      import('./usuarios/usuario.module').then(
        (m) => m.UsuarioLevelModule
      ),
  },
  {
    path: 'convenio',
    loadChildren: () =>
      import('./convenios/convenio.module').then(
        (m) => m.ConvenioLevelModule
      ),
  },
  {
    path: 'motivo',
    loadChildren: () =>
      import('./motivo/motivo.module').then(
        (m) => m.MotivoLevelModule
      ),
  },
  {
    path: 'categoria',
    loadChildren: () =>
      import('./categoria/categoria.module').then(
        (m) => m.CategoriaLevelModule
      ),
  },
  {
    path: 'servicio',
    loadChildren: () =>
      import('./servicios/servicio.module').then(
        (m) => m.ServicioLevelModule
      ),
  },
  {
    path: 'documento',
    loadChildren: () =>
      import('./documentos/documento.module').then(
        (m) => m.DocumentoLevelModule
      ),
  },
  {
    path: 'plantilla',
    loadChildren: () =>
      import('./plantilla/plantilla.module').then(
        (m) => m.PlantillaLevelModule
      ),
  },
  {
    path: 'laboratorio',
    loadChildren: () =>
      import('./laboratorio/laboratorio.module').then(
        (m) => m.LaboratorioModule
      ),
  },
  {
    path: 'radiologico',
    loadChildren: () =>
      import('./radiologico/radiologico.module').then(
        (m) => m.RadiologicoModule
      ),
  },

  { path: '**', component: Page404Component },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministradorRoutingModule {}