import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoUsuariosInactivosComponent } from './listado-usuarios-inactivos/listado-usuarios-inactivos.component';
import { ListadoUsuariosComponent } from './listado-usuarios/listado-usuarios.component';
import { NuevoUsuarioComponent } from './nuevo-usuario/nuevo-usuario.component';

const routes: Routes = [
    {
      path: 'nuevo-usuario',
      component: NuevoUsuarioComponent,
    },
    {
      path: 'listado-usuarios-activos',
      component: ListadoUsuariosComponent,
    },
    {
      path: 'listado-usuarios-inactivos',
      component: ListadoUsuariosInactivosComponent,
    },
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class UsuarioLevelRoutingModule {}