import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoPlantillasComponent } from './listado-plantillas/listado-plantillas.component';
import { NuevaPlantillaComponent } from './nueva-plantilla/nueva-plantilla.component';

const routes: Routes = [
    {
      path: 'nueva-plantilla',
      component: NuevaPlantillaComponent,
    },
    {
      path: 'listado-plantillas',
      component: ListadoPlantillasComponent,
    },
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class PlantillaLevelRoutingModule {}