import { Component, OnInit, ViewChild } from "@angular/core";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { AlertService } from "../../../../core/service/alert.service";
import { AuthService } from "../../../../core/service/auth.service";
import { HttpService } from "../../../../core/service/http.service";

@Component({
  selector: "app-convenios-inactivos",
  templateUrl: "./convenios-inactivos.component.html",
  styles: [],
})
export class ConveniosInactivosComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  dataconv = [];
  filterDataconv = [];
  idClinica;
  imagenclinicaPrint;

  constructor(
    private http: HttpService,
    private authService: AuthService,
    private sweet: AlertService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.imagenclinicaPrint =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
  }

  ngOnInit(): void {
    this.getDisabledConvenios();
  }

  getDisabledConvenios(refresh?) {
    const options: any = { caso: 15, idclinica: this.idClinica };
    this.http.post("convenios.php", options).subscribe({
      next: (respuesta: any) => {
        this.dataconv = respuesta;
        this.filterDataconv = respuesta;
        refresh && this.sweet.toast("los convenios");
      },
      error: (err: any) => console.log(err),
    });
  }

  enabledConvenio(idconv) {
    const options: any = { caso: 16, idconv };
    this.http.post("convenios.php", options).subscribe({
      next: (respuesta: any) => {
        if (respuesta === "Se modifico") {
          this.sweet.alert('','Se activo el convenio','success');
          this.getDisabledConvenios();
        }else{
          this.sweet.alert('','No se activo el convenio','error');
        }
      },
      error: (err: any) => console.log(err),
    });
  }

  filterDatatable(event: any) {
    if (event.target.value === "") {
      this.dataconv = this.filterDataconv;
    }
    this.dataconv = this.http.filter(event, this.filterDataconv);
    this.table.offset = 0;
  }

  exportToExcel() {
    this.http.export("Convenios_inactivos", this.filterDataconv);
  }
}
