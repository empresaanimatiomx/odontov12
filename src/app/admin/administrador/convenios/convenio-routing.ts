import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConveniosInactivosComponent } from './convenios-inactivos/convenios-inactivos.component';
import { ListadoConveniosComponent } from './listado-convenios/listado-convenios.component';
import { NuevoConvenioComponent } from './nuevo-convenio/nuevo-convenio.component';

const routes: Routes = [
    {
      path: 'nuevo-convenio',
      component: NuevoConvenioComponent,
    },
    {
      path: 'listado-convenios',
      component: ListadoConveniosComponent,
    },
    {
      path: 'convenios-inactivos',
      component:ConveniosInactivosComponent
    }
  ];

  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class ConvenioLevelRoutingModule {}