import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { AuthService } from "../../../../core/service/auth.service";
import { HttpService } from "../../../../core/service/http.service";
import { AlertService } from "src/app/core/service/alert.service";

@Component({
  selector: "app-listado-convenios",
  templateUrl: "./listado-convenios.component.html",
  styleUrls: [],
})
export class ListadoConveniosComponent implements OnInit {
  @ViewChild("placesRef")
  placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  @ViewChild("epltable", { static: false }) epltable: ElementRef;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  filtrodatosConv: any = [];
  dataconv: any = [];
  idClinica: number;
  idConv = "";
  doctores: any = [];
  sucursales: any = [];
  convenioForm: FormGroup;
  CancelaForm: FormGroup;
  historialconv: any = [];
  idUsuario: number;
  nombreConvenio;

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private http: HttpService,
    private sweet: AlertService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.idUsuario = this.authService.currentUserValue.id;
    this.convenioForm = this.fb.group({
      NombreConvenio: ["", [Validators.required, Validators.maxLength(80)]],
      Descuento: ["", [Validators.required]],
      idDoctor: [""],
      idSucursal: [""],
      sitio: ["", [Validators.required]],
      email: ["", [Validators.required]],
      telefono: ["", [Validators.required, Validators.minLength(10)]],
      direccion: ["", [Validators.required]],
      Notas: ["", [Validators.maxLength(80)]],
    });

    this.CancelaForm = this.fb.group({
      motivo: ["", [Validators.required, Validators.maxLength(80)]],
    });
  }

  ngOnInit() {
    if (this.idClinica > 0) {
      this.getConvenios();
      this.getDoctores();
      this.getSucursales();
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
  }

  public handleAddressChange(address: Address) {
    this.convenioForm.patchValue({
      direccion: address.formatted_address,
    });
  }

  exportToExcel() {
    this.http.export("listado_convenios", this.filtrodatosConv);
  }

  getDoctores() {
    this.http
      .post("convenios.php", { caso: 9, idclinica: this.idClinica })
      .subscribe((respuesta) => {
        this.doctores = respuesta;
      });
  }

  getSucursales() {
    this.http
      .post("categorias.php", { caso: 0, idclinica: this.idClinica })
      .subscribe((respuesta) => {
        this.sucursales = respuesta;
      });
  }

  desactivaDoctor(idsucursal) {
    this.convenioForm.get("nombreDoc").clearValidators();
    this.convenioForm.get("nombreDoc").updateValueAndValidity();
    this.convenioForm.controls["nombreDoc"].setValue("0");
  }

  cerrarmodal(form: FormGroup) {
    form.reset();
    this.modalService.dismissAll();
  }

  cerrarmodalhistorial() {
    this.modalService.dismissAll();
  }

  editarConvenio(form: any) {
    let idsucursal = form.sucursal;
    let iddoctor = form.nombreDoc;
    if (idsucursal === "0" || idsucursal === "" || idsucursal === null) {
      idsucursal = 0;
    } else {
      idsucursal = form.sucursal;
    }
    if (iddoctor === "0" || iddoctor === "" || iddoctor === null) {
      iddoctor = 0;
    } else {
      iddoctor = form.nombreDoc;
    }
    const options: any = {
      caso: 3,
      conv: form,
      iddoctor: iddoctor,
      ids: idsucursal,
      idconv: this.idConv,
      idUsuario: this.idUsuario,
    };
    this.http.post("convenios.php", options).subscribe((respuesta) => {
      const res = respuesta;
      if (res.toString() === "Se modifico") {
        this.getConvenios();
        this.modalService.dismissAll();
        this.sweet.alert("", "Convenio modificado correctamente", "success");
      } else {
        this.sweet.alert(
          "",
          "Ocurrio un error, intente de nuevo por favor",
          "error"
        );
      }
    });
  }

  editarConvenioModal(row, content) {
    this.idConv = row.id;
    this.nombreConvenio = row.NombreConvenio;
    let ngbModalOptions: NgbModalOptions = {
      centered: true,
      ariaLabelledBy: "modal-basic-title",
      size: "lg",
    };
    this.modalService.open(content, ngbModalOptions);
    this.convenioForm.patchValue(row);
    this.convenioForm.controls["direccion"].disable();
  }

  modalCancelar(row, content) {
    this.idConv = row.id;
    let ngbModalOptions: NgbModalOptions = {
      centered: true,
      ariaLabelledBy: "modal-basic-title",
    };
    this.modalService.open(content, ngbModalOptions);
  }

  modalHistorial(row, content) {
    this.idConv = row.id;
    this.nombreConvenio = row.NombreConvenio;
    this.getHistorial(this.idConv);
    let ngbModalOptions: NgbModalOptions = {
      size: "lg",
      centered: true,
      ariaLabelledBy: "modal-basic-title",
    };
    this.modalService.open(content, ngbModalOptions);
  }

  cancelConvenio(form: FormGroup, stat) {
    const options: any = {
      caso: 13,
      convc: this.CancelaForm.value,
      status: stat,
      idconv: this.idConv,
      idUsuario: this.idUsuario,
    };
    this.http.post("convenios.php", options).subscribe((res: any) => {
      if (res.toString() === "Se modifico") {
        this.getConvenios();
        form.reset();
        this.modalService.dismissAll();
        this.sweet.alert("", "Convenio desactivado correctamente", "success");
      } else {
        this.sweet.alert(
          "",
          "Ocurrio un error, intente de nuevo por favor",
          "error"
        );
      }
    });
  }

  deshabilitarConvenio(row) {
    this.sweet.alertConfirm("deshabilitar este convenio").then((result) => {
      if (result.isConfirmed) {
        const options: any = {
          caso: 14,
          idconv: row.id,
          idUsuario: this.idUsuario,
        };
        this.http.post("convenios.php", options).subscribe((res) => {
          this.getConvenios();
        });
      }
    });
  }

  filterDatatable(event) {
    if (event.target.value === "") {
      this.dataconv = this.filtrodatosConv;
    }
    this.dataconv = this.http.filter(event, this.filtrodatosConv);
    this.table.offset = 0;
  }

  getConvenios(refresh?): void {
    this.http
      .post("convenios.php", { caso: 2, idclinica: this.idClinica })
      .subscribe({
        next: (respuesta) => {
          this.dataconv = respuesta;
          this.filtrodatosConv = respuesta;
          refresh && this.sweet.toast("los convenios");
        },
        error: (err) => console.log(err),
      });
  }

  getHistorial(idc): void {
    this.http.post("convenios.php", { caso: 4, idconv: idc }).subscribe({
      next: (respuesta) => {
        this.historialconv = respuesta;
      },
      error: (err) => console.log(err),
    });
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }
  iraGenerar2() {
    this.router.navigate(["/admin/administrador/convenio/nuevo-convenio"]);
  }

  firstLetterUpperCase(name) {
    const texto = this.convenioForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.convenioForm.controls[name].setValue(textofinal);
    }
  }
}
