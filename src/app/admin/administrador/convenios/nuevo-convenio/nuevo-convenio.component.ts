import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { AuthService } from "../../../../core/service/auth.service";
import { HttpService } from "../../../../core/service/http.service";

@Component({
  selector: "app-nuevo-convenio",
  templateUrl: "./nuevo-convenio.component.html",
  styleUrls: [],
})
export class NuevoConvenioComponent implements OnInit {
  @ViewChild("placesRef")
  placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  idClinica: number;
  doctores: any = [];
  sucursales: any = [];
  convenioForm: FormGroup;
  idUsuario: number;

  constructor(
    private _snackBar: MatSnackBar,
    private http: HttpService,
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.idUsuario = this.authService.currentUserValue.id;
    this.convenioForm = this.fb.group({
      nombre: ["", [Validators.required]],
      descuento: [
        "",
        [Validators.required, Validators.max(100), Validators.min(0)],
      ],
      nombreDoc: ["", [Validators.required]],
      sucursal: ["", [Validators.required]],
      sitio: [""],
      email: ["", [Validators.required]],
      telefono: ["", [Validators.required, Validators.minLength(10)]],
      direccion: ["", [Validators.required]],
      notas: ["", [Validators.maxLength(80)]],
    });
  }

  ngOnInit() {
    if (this.idClinica < 0) {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
    this.getDoctores();
    this.getSucursales();
  }

  public handleAddressChange(address: Address) {
    this.convenioForm.patchValue({
      direccion: address.formatted_address,
    });
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  getDoctores() {
    this.http
      .post("convenios.php", { caso: 9, idclinica: this.idClinica })
      .subscribe((respuesta) => {
        this.doctores = respuesta;
      });
  }

  getSucursales() {
    this.http
      .post("categorias.php", { caso: 0, idclinica: this.idClinica })
      .subscribe((respuesta) => {
        this.sucursales = respuesta;
      });
  }

  firstLetterUpperCase(name) {
    const texto = this.convenioForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.convenioForm.controls[name].setValue(textofinal);
    }
  }

  desactivaDoctor() {
    this.convenioForm.get("nombreDoc").clearValidators();
    this.convenioForm.get("nombreDoc").updateValueAndValidity();
    this.convenioForm.controls["nombreDoc"].disable();
  }

  desactivaSucursal() {
    this.convenioForm.get("sucursal").clearValidators();
    this.convenioForm.get("sucursal").updateValueAndValidity();
    this.convenioForm.controls["sucursal"].disable();
  }

  agregarConvenio() {
    if (this.convenioForm.controls["nombreDoc"].value === "") {
      if (this.convenioForm.controls["sucursal"].value === "general") {
        this.convenioForm.controls.sucursal.setValue(0);
      }
      const options: any = {
        caso: 10,
        conv: this.convenioForm.value,
        idclinica: this.idClinica,
        idUsuario: this.idUsuario,
      };
      this.http.post("convenios.php", options).subscribe((respuesta) => {
        this.convenioForm.reset();
        this.showNotification(
          "snackbar-success",
          "Convenio registrado correctamente...!!!",
          "bottom",
          "center"
        );
        this.router.navigate([
          "/admin/administrador/convenio/listado-convenios",
        ]);
      });
    }
    if (this.convenioForm.controls["sucursal"].value === "") {
      const options: any = {
        caso: 1,
        conv: this.convenioForm.value,
        idclinica: this.idClinica,
        idUsuario: this.idUsuario,
      };
      this.http.post("convenios.php", options).subscribe((respuesta) => {
        this.convenioForm.reset();
        this.showNotification(
          "snackbar-success",
          "Convenio registrado correctamente...!!!",
          "bottom",
          "center"
        );
        this.router.navigate([
          "/admin/administrador/convenio/listado-convenios",
        ]);
      });
    }
  }

  showNotification(colorName, text, placementFrom, placementAlign) {
    this._snackBar.open(text, "", {
      duration: 2000,
      verticalPosition: placementFrom,
      horizontalPosition: placementAlign,
      panelClass: colorName,
    });
  }
}
