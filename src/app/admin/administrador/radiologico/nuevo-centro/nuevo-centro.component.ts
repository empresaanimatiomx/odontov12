import { Component, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { AlertService } from "../../../../core/service/alert.service";
import { HttpService } from "../../../../core/service/http.service";

@Component({
  selector: "app-nuevo-centro",
  templateUrl: "./nuevo-centro.component.html",
  styleUrls: [],
})
export class NuevoCentroComponent {
  centroForm: FormGroup;
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  isEmail = /\S+@\S+\.\S+/;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private sweet: AlertService,
    private router: Router
  ) {
    this.centroForm = this.fb.group({
      nombre: ["", Validators.required],
      correo: ["", [Validators.required, Validators.pattern(this.isEmail)]],
      telefono: ["", [Validators.required, Validators.minLength(10)]],
      direccion: ["", [Validators.required]],
      observaciones: [""],
    });
  }

  public handleAddressChange(address: Address) {
    this.centroForm.patchValue({
      direccion: address.formatted_address,
    });
  }

  guardarCentro(form: FormGroup) {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.http
      .post("recetas.php", { caso: 7, idclinica, centro: form.value })
      .subscribe({
        next: (data: any) => {
          if (data == "Se inserto") {
            this.sweet.alert(
              "",
              "Se agrego el centro radiologico correctamente",
              "success"
            );
            this.router.navigate([
              "/admin/administrador/radiologico/listado-centros",
            ]);
          } else {
            this.sweet.alert(
              "",
              "Ocurrio un error intentelo de nuevo",
              "error"
            );
          }
        },
        error: (err) => console.log(err),
      });
  }

  firstLetterUppercase(name) {
    const texto = this.centroForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.centroForm.controls[name].setValue(textofinal);
    }
  }
}
