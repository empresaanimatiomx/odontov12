import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { HttpService } from "../../../../core/service/http.service";
import { AlertService } from "../../../../core/service/alert.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-listado-ordenes",
  templateUrl: "./listado-ordenes.component.html",
  styles: [],
})
export class ListadoOrdenesComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  @ViewChild("epltable", { static: false }) epltable: ElementRef;
  ordenes = [];
  ordenesFilter = [];
  idCentro;


  constructor(
    private route: ActivatedRoute,
    private http: HttpService,
    private sweet: AlertService
  ) {
    this.route.params.subscribe({
      next: (params) => (this.idCentro = params.idCentro),
      error: (err) => console.log(err),
    });
  }

  ngOnInit() {
    this.getOrdenes();
  }

  getOrdenes(refresh?) {
    this.http
      .post("radiologia.php", { caso: 3, idCentro: this.idCentro })
      .subscribe({
        next: (ordenes: any) => {
          this.ordenes = ordenes;
          this.ordenesFilter = ordenes;
          refresh && this.sweet.toast("las ordenes");
        },
        error: (err) => console.log(err),
      });
  }

  filterDatatable(event) {
    if (event.target.value === "") {
      this.ordenes = this.ordenesFilter;
    }
    this.ordenes = this.http.filter(event, this.ordenesFilter);
    this.table.offset = 0;
  }

}
