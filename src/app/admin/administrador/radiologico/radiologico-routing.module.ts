import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetalleOrdenComponent } from './detalle-orden/detalle-orden.component';
import { ListadoCentrosComponent } from './listado-centros/listado-centros.component';
import { ListadoOrdenesComponent } from './listado-ordenes/listado-ordenes.component';
import { NuevoCentroComponent } from './nuevo-centro/nuevo-centro.component';

const routes: Routes = [
  {
    path: 'agregar-centro',
    component:NuevoCentroComponent
  },
  {
    path: 'listado-centros',
    component:ListadoCentrosComponent
  },
  {
    path: 'listado-odenes/:idCentro',
    component:ListadoOrdenesComponent
  },
  {
    path: 'detalle-orden/:idOrden',
    component:DetalleOrdenComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RadiologicoRoutingModule { }
