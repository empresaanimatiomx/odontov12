import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../../../../core/service/http.service';

@Component({
  selector: "app-detalle-orden",
  templateUrl: "./detalle-orden.component.html",
  styles: [],
})
export class DetalleOrdenComponent implements OnInit {
  centros = [];
  sucursales = [];
  ordenForm: FormGroup;
  idOrden;

  constructor(private route: ActivatedRoute, private fb: FormBuilder, private http: HttpService) {
    this.ordenForm = this.fb.group({
      idOrden: [""],
      sucursal: ["", Validators.required],
      centroRadiologico: ["", Validators.required],
      rxRetroalveolar: [""],
      rxRetroalveolarTotal: [""],
      rxBiteWing: [""],
      rxOclusal: [""],
      rxPanoramica: [""],
      rxteleradiografiaLateral: [""],
      rxTeleradiografiaFrontal: [""],
      rxDigitaldeMano: [""],
      maxilarSuperior: [""],
      maxilarInferior: [""],
      bimaxilar: [""],
      craneoCompleto: [""],
      ATMBocaCerrada: [""],
      ATMBocaAbierta: [""],
      zona: [""],
      especialidad: [""],
      rickets: [""],
      roth: [""],
      jarabak: [""],
      frontal: [""],
      tejidosBlandos: [""],
      conversionCefalome: [""],
      VTO: [""],
      modelos: [""],
      fotografias: [""],
      eje: [""],
      holta: [""],
      video: [""],
      articulados: [""],
      formaEntrga: [""],
      observaciones: [""],
    });
    this.route.params.subscribe({
      next: (params) => (this.idOrden = params.idOrden),
      error: (err) => console.log(err),
    });
  }

  ngOnInit(): void {
    this.getCentro();
  }

  getCentro() {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.http
      .post("recetas.php", { caso: 8, idclinica })
      .subscribe((centros: any) => {
        this.centros = centros;
        this.getSucursales();
      });
  }

  getSucursales() {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.http
      .post("sucursal.php", { caso: 0, idClinica: idclinica })
      .subscribe((sucursales: any) => {
        this.sucursales = sucursales;
        this.getOrden();
      });
  }

  getOrden() {
    this.http
      .post("radiologia.php", { caso: 4, idOrden: this.idOrden })
      .subscribe((orden: any) => {
        if (orden !== null) {
          this.ordenForm.patchValue(orden[0]);
          this.ordenForm.disable();
        }
      });
  }
}
