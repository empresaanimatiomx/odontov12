import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../../../../core/service/auth.service";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { HttpService } from "../../../../core/service/http.service";
import { AlertService } from "../../../../core/service/alert.service";

@Component({
  selector: "app-listado-centros",
  templateUrl: "./listado-centros.component.html",
  styles: [],
})
export class ListadoCentrosComponent implements OnInit {
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  @ViewChild("epltable", { static: false }) epltable: ElementRef;
  centroForm: FormGroup;
  centros = [];
  centrosFilter = [];
  idClinica;
  idCentro;
  isEmail = /\S+@\S+\.\S+/;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private http: HttpService,
    private sweet: AlertService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
  }

  ngOnInit() {
    this.centroForm = this.fb.group({
      nombre: ["", Validators.required],
      correo: ["", [Validators.required, Validators.pattern(this.isEmail)]],
      telefono: ["", [Validators.required, Validators.minLength(10)]],
      direccion: ["", [Validators.required]],
      observaciones: ["", [Validators.required]],
    });
    this.getCentros();
  }

  getCentros(refresh?) {
    this.http
      .post("recetas.php", { caso: 8, idclinica: this.idClinica })
      .subscribe({
        next: (data: any) => {
          this.centros = data;
          this.centrosFilter = data;
          refresh && this.sweet.toast("los centros");
        },
        error: (err) => console.log(err),
      });
  }

  public handleAddressChange(address: Address) {
    this.centroForm.patchValue({
      direccion: address.formatted_address,
    });
  }

  editModal(row, modal) {
    this.idCentro = row.idCentro;
    this.modalService.open(modal, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
      size: "lg",
    });
    this.centroForm.patchValue(row);
  }

  updateCentro(form: FormGroup) {
    this.http
      .post("recetas.php", {
        caso: 9,
        centro: form.value,
        idCentro: this.idCentro,
      })
      .subscribe({
        next: (data: any) => {
          if (data == "Se modifico") {
            this.sweet.alert(
              "",
              "Se agrego el centro radiologico correctamente",
              "success"
            );
            this.closeModal();
            form.reset();
          } else {
            this.sweet.alert(
              "",
              "Ocurrio un error intentelo de nuevo",
              "error"
            );
          }
        },
        error: (err) => console.log(err),
      });
  }

  deleteCentro(idCentro: any) {
    this.sweet.alertConfirm("eliminar este centro").then((res) => {
      if (res.isConfirmed) {
        this.http.post("recetas.php", { caso: 10, idCentro }).subscribe({
          next: (data: any) => {
            if (data == "Se elimino") {
              this.sweet.alert("", "Se elimino el centro", "success");
              this.ngOnInit();
            } else {
              this.sweet.alert(
                "",
                "Ocurrio un error intentelo de nuevo",
                "error"
              );
            }
          },
          error: (err) => console.log(err),
        });
      }
    });
  }

  firstLetterUppercase(name) {
    const texto = this.centroForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.centroForm.controls[name].setValue(textofinal);
    }
  }

  filterDatatable(event) {
    if (event.target.value === "") {
      this.centros = this.centrosFilter;
    }
    this.centros = this.http.filter(event, this.centrosFilter);
    this.table.offset = 0;
  }

  closeModal() {
    this.modalService.dismissAll();
    this.ngOnInit();
  }
}
