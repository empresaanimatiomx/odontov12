import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { RadiologicoRoutingModule } from "./radiologico-routing.module";
import { NuevoCentroComponent } from "./nuevo-centro/nuevo-centro.component";
import { ListadoCentrosComponent } from "./listado-centros/listado-centros.component";
import { ListadoOrdenesComponent } from './listado-ordenes/listado-ordenes.component';
import { DetalleOrdenComponent } from './detalle-orden/detalle-orden.component';
import { MaterialModule } from "src/app/shared/material.module";

@NgModule({
  declarations: [NuevoCentroComponent, ListadoCentrosComponent, ListadoOrdenesComponent, DetalleOrdenComponent],
  imports: [
    CommonModule,
    RadiologicoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    GooglePlaceModule,
  ],
})
export class RadiologicoModule {}
