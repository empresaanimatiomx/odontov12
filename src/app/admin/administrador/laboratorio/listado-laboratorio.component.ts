import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../../../core/service/auth.service";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { HttpService } from "../../../core/service/http.service";
import { AlertService } from "../../../core/service/alert.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-laboratorio",
  templateUrl: "./listado-laboratorio.component.html",
  styles: [],
})
export class LaboratorioComponent implements OnInit {
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  @ViewChild("epltable", { static: false }) epltable: ElementRef;
  laboratorios = [];
  laboratoriosFilter = [];
  laboratorio: FormGroup;
  idClinica;
  isEmail = /\S+@\S+\.\S+/;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private http: HttpService,
    private sweet: AlertService,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
  }

  ngOnInit() {
    this.laboratorio = this.fb.group({
      idLaboratorio: [""],
      Nombre: ["", [Validators.required]],
      Telefono: ["", [Validators.required]],
      Correo: ["", [Validators.required, Validators.pattern(this.isEmail)]],
      Direccion: ["", [Validators.required]],
      ProtesisFijaUnitaria: [""],
      ProtesisFijaPlural: [""],
      OperatoriaIndirecta: [""],
      Planos: [""],
      ProtesisRemovible: [""],
      Ortodoncia: [""],
      Otros: [""],
      Observaciones: [""],
    });
    this.getLaboratorio();
  }

  public handleAddressChange(address: Address) {
    this.laboratorio.patchValue({
      Direccion: address.formatted_address,
    });
  }

  editModal(row, modal) {
    this.modalService.open(modal, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
      size: "lg",
    });
    this.laboratorio.patchValue(row);
  }

  guardarLaboratorio(laboratorio: FormGroup) {
    this.http
      .post("laboratorio.php", { caso: 8, laboratorio })
      .subscribe((data: any) => {
        if (data == "Se modifico") {
          this.sweet.alert("", "Se modifico el laboratorio", "success");
          this.ngOnInit();
          this.modalService.dismissAll();
        } else {
          this.sweet.alert("", "Ocurrio un error intentelo de nuevo", "error");
        }
      });
  }

  deleteLab(idLaboratorio: any) {
    this.sweet.alertConfirm("eliminar el laboratorio").then((res) => {
      if (res.isConfirmed) {
        this.http
          .post("laboratorio.php", { caso: 9, idLaboratorio })
          .subscribe((data: any) => {
            if (data == "Se elimino") {
              this.sweet.alert("", "Se elimino el laboratorio", "success");
              this.ngOnInit();
            } else {
              this.sweet.alert(
                "",
                "Ocurrio un error intentelo de nuevo",
                "error"
              );
            }
          });
      }
    });
  }

  getLaboratorio(refresh?) {
    this.http
      .post("laboratorio.php", { caso: 0, idClinica: this.idClinica })
      .subscribe((respuesta: any) => {
        this.laboratorios = respuesta;
        this.laboratoriosFilter = respuesta;
        refresh && this.sweet.toast("los laboratorios");
      });
  }

  FirstUppercaseLetter(idname) {
    const texto = (<HTMLInputElement>document.getElementById(idname)).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      (<HTMLInputElement>document.getElementById(idname)).value = textofinal;
    }
  }

  filterDatatable(event) {
    this.table.offset = 0;
  }

  closeModal() {
    this.modalService.dismissAll();
    this.ngOnInit();
  }

  goToDetail(idLaboratorio) {
    this.router.navigate([
      "/admin/administrador/laboratorio/listado-ordenes",
      idLaboratorio,
    ]);
  }
}
