import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { AuthService } from "src/app/core/service/auth.service";
import { HttpService } from "../../../../core/service/http.service";

@Component({
  selector: "app-detalle-laboratorio",
  templateUrl: "./detalle-laboratorio.component.html",
  styles: [],
})
export class DetalleLaboratorioComponent implements OnInit {
  register: FormGroup;
  idOrden;
  idClinica;
  sucursales = [];
  doctores = [];
  laboratorios = [];

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private http: HttpService,
    private fb: FormBuilder
  ) {
    this.idOrden = this.route.snapshot.params.idOrden;
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.register = this.fb.group({
      idOrdenLab: [""],
      NombreOrden: ["", [Validators.required]],
      idSucursal: ["", [Validators.required]],
      idLaboratorio: ["", [Validators.required]],
      idDoctor: ["", [Validators.required]],
    });
    this.register.disable();
  }

  ngOnInit(): void {
    this.getLaboratorios();
  }

  getLaboratorios() {
    this.http
      .post("laboratorio.php", { caso: 0, idClinica: this.idClinica })
      .subscribe({
        next: (laboratorios: any) => {
          this.laboratorios = laboratorios;
          this.getDoctores();
        },
        error: (err) => console.log(err),
      });
  }

  getDoctores() {
    this.http
      .post("pacientes.php", { caso: 5, idClinica: this.idClinica })
      .subscribe({
        next: (doctores: any) => {
          this.doctores = doctores;
          this.getSucursales();
        },
        error: (err) => console.log(err),
      });
  }

  getSucursales() {
    this.http
      .post("recetas.php", { caso: 4, idclinica: this.idClinica })
      .subscribe({
        next: (sucursales: any) => {
          this.sucursales = sucursales;
          this.getOrden(this.idOrden);
        },
        error: (err) => console.log(err),
      });
  }

  getOrden(idOrden) {
    this.http.post("laboratorio.php", { caso: 10, idOrden }).subscribe({
      next: (orden: any) => {
        if (orden !== null) {
          this.register.patchValue(orden[0]);
        }
      },
      error: (err) => console.log(err),
    });
  }
}
