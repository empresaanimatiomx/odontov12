import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetalleLaboratorioComponent } from './detalle-laboratorio/detalle-laboratorio.component';
import { LaboratorioComponent } from './listado-laboratorio.component';
import { ListadoOrdenesComponent } from './listado-ordenes/listado-ordenes.component';
import { NuevoLaboratorioComponent } from './nuevo-laboratorio/nuevo-laboratorio.component';

const routes: Routes = [
  {
    path: "nuevo-laboratorio",
    component: NuevoLaboratorioComponent,
  },
  {
    path: "listado-laboratorio",
    component: LaboratorioComponent,
  },
  {
    path: "listado-ordenes/:idLaboratorio",
    component: ListadoOrdenesComponent,
  },
  {
    path: "detalle-laboratorio/:idOrden",
    component: DetalleLaboratorioComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LaboratorioRoutingModule { }
