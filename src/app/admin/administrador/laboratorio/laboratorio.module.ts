import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LaboratorioRoutingModule } from './laboratorio-routing.module';
import { MatIconModule } from "@angular/material/icon";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";


import { NuevoLaboratorioComponent } from './nuevo-laboratorio/nuevo-laboratorio.component';
import { LaboratorioComponent } from './listado-laboratorio.component';
import { DetalleLaboratorioComponent } from './detalle-laboratorio/detalle-laboratorio.component';
import { ListadoOrdenesComponent } from './listado-ordenes/listado-ordenes.component';


@NgModule({
  declarations: [NuevoLaboratorioComponent, LaboratorioComponent, DetalleLaboratorioComponent, ListadoOrdenesComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LaboratorioRoutingModule,
    MatIconModule,
    NgxDatatableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatCheckboxModule,
    GooglePlaceModule,
  ],
})
export class LaboratorioModule {}
