import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { AlertService } from "src/app/core/service/alert.service";
import { HttpService } from "src/app/core/service/http.service";

@Component({
  selector: "app-listado-ordenes",
  templateUrl: "./listado-ordenes.component.html",
  styles: [],
})
export class ListadoOrdenesComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  ordenes = [];
  ordenesFilter = [];
  idLaboratorio;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sweet: AlertService,
    private http: HttpService
  ) {
    this.route.params.subscribe({
      next: (params) => (this.idLaboratorio = params.idLaboratorio),
      error: (err) => console.log(err),
    });
  }

  ngOnInit(): void {
    this.getOrdenes();
  }

  getOrdenes(refresh?) {
    this.http
      .post("laboratorio.php", { caso: 11, idLaboratorio: this.idLaboratorio })
      .subscribe({
        next: (ordenes: any) => {
          this.ordenes = ordenes;
          this.ordenesFilter = ordenes;
          refresh && this.sweet.toast("las ordenes");
        },
        error: (err) => console.log(err),
      });
  }

  filterDatatable(event) {
    if (event.target.value === "") {
      this.ordenes = this.ordenesFilter;
    }
    this.ordenes = this.http.filter(event, this.ordenesFilter);
    this.table.offset = 0;
  }

  goToDetail(idOrden) {
    this.router.navigate([
      "/admin/administrador/laboratorio/detalle-laboratorio",
      idOrden,
    ]);
  }

}
