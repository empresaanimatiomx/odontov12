import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoCategoriasComponent } from './listado-categorias/listado-categorias.component';
import { NuevaCategoriaComponent } from './nueva-categoria/nueva-categoria.component';

const routes: Routes = [
    {
      path: 'nueva-categoria',
      component: NuevaCategoriaComponent,
    },
    {
      path: 'listado-categorias',
      component: ListadoCategoriasComponent,
    },
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class CategoriaLevelRoutingModule {}