import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../../../../core/service/auth.service";
import { HttpService } from "../../../../core/service/http.service";
import { AlertService } from "../../../../core/service/alert.service";

@Component({
  selector: "app-listado-categorias",
  templateUrl: "./listado-categorias.component.html",
  styleUrls: [],
})
export class ListadoCategoriasComponent implements OnInit {
  @ViewChild("epltable", { static: false }) epltable: ElementRef;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  idClinica: number;
  idCategoria;
  categorias = [];
  categoriasFilter = [];
  sucursales: any = [];
  categoriaForm: FormGroup;
  imagenclinicaPrint = "";

  constructor(
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private modalService: NgbModal,
    private authService: AuthService,
    private http: HttpService,
    private sweet: AlertService,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.imagenclinicaPrint =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
    this.categoriaForm = this.fb.group({
      Nombre: ["", [Validators.required]],
      idSucursal: ["", [Validators.required]],
      Deshabilitado: [false, []],
    });
  }

  ngOnInit() {
    if (this.idClinica > 0) {
      this.getCategorias();
      this.getSucursales();
    } else {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
  }

  getSucursales() {
    this.http
      .post("categorias.php", { caso: 0, idclinica: this.idClinica })
      .subscribe({
        next: (respuesta) => {
          this.sucursales = respuesta;
        },
        error: (err) => console.log(err),
      });
  }

  getCategorias(refresh?): void {
    this.http
      .post("AdminCategorias.php", { caso: 1, idclinica: this.idClinica })
      .subscribe({
        next: (respuesta: any) => {
          this.categorias = respuesta;
          this.categoriasFilter = respuesta;
          refresh && this.sweet.toast("los tratamientos");
        },
        error: (err) => console.log(err),
      });
  }

  editCategoriaModal(row, content) {
    this.idCategoria = row.idCategorias;
    const status = row.Deshabilitado === "1" ? true : false;
    let ngbModalOptions: NgbModalOptions = {
      centered: true,
      ariaLabelledBy: "modal-basic-title",
    };
    this.modalService.open(content, ngbModalOptions);
    this.categoriaForm.patchValue({
      ...row,
      Deshabilitado: status,
    });
  }

  updateCategoria(categoria) {
    categoria.Deshabilitado = categoria.Deshabilitado ? 1 : 0;
    const options: any = {
      caso: 2,
      categoria,
      idcategoria: this.idCategoria,
    };
    this.http.post("AdminCategorias.php", options).subscribe({
      next: (res: any) => {
        if (res === "Se actualizo") {
          this.modalService.dismissAll();
          this.getCategorias();
          this.sweet.alert("", "Categoría modificada correctamente", "success");
        } else {
          this.sweet.alert("", "Ocurrio un error intentelo de nuevo", "error");
        }
      },
      error: (err) => console.log(err),
    });
  }

  checkIncludeService(idcategoria): any {
    this.http
      .post("AdminCategorias.php", { caso: 11, idcategoria })
      .subscribe((res: any) => {
        if (res === null) {
          this.deleteCategoria(idcategoria);
        } else {
          this.sweet.alert(
            "",
            "No puede eliminar este tratamiento porque ya tiene servicios asociados",
            "error"
          );
        }
      });
  }

  deleteCategoria(idcategoria) {
    this.sweet.alertConfirm("eliminar este tratamiento?").then((result) => {
      if (result.isConfirmed) {
        this.http
          .post("AdminCategorias.php", { caso: 8, idcategoria })
          .subscribe((res: any) => {
            if (res.toString() === "Se elimino") {
              this.getCategorias();
              this.sweet.alert("", "Se elimino la categoria", "success");
            } else {
              this.sweet.alert(
                "",
                "Ocurrio un error, intente de nuevo por favor",
                "error"
              );
            }
          });
      }
    });
  }

  firstLetterUppercase(name) {
    const texto = this.categoriaForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.categoriaForm.controls[name].setValue(textofinal);
    }
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  goToAddCategoria() {
    this.router.navigate(["/admin/administrador/categoria/nueva-categoria"]);
  }

  filterDatatable(event) {
    if (event.target.value === "") {
      this.categorias = this.categoriasFilter;
    }
    this.categorias = this.http.filter(event, this.categoriasFilter);
    this.table.offset = 0;
  }

  exportToExcel() {
    this.http.export("listado_tratamientos", this.categoriasFilter);
  }
}
