import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { HttpService } from "../../../../core/service/http.service";
import { AuthService } from "../../../../core/service/auth.service";
import { AlertService } from "../../../../core/service/alert.service";

@Component({
  selector: "app-nueva-categoria",
  templateUrl: "./nueva-categoria.component.html",
  styleUrls: [],
})
export class NuevaCategoriaComponent implements OnInit {
  sucursales = [];
  convenios = [];
  idClinica: number;
  categoriaForm: FormGroup;

  constructor(
    private router: Router,
    private _snackBar: MatSnackBar,
    public http: HttpService,
    public sweet: AlertService,
    private authService: AuthService,
    private fb: FormBuilder
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.categoriaForm = this.fb.group({
      Nombre: ["", [Validators.required]],
      idSucursal: ["", [Validators.required]],
      idConvenio: [""],
      Deshabilitado: [false,[]],
    });
  }

  ngOnInit() {
    this.getConvenios();
    if (this.idClinica > 0) {
      this.getSucursales();
    } else {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
  }

  checkExist(categoria) {
    this.http.post("AdminCategorias.php", { caso: 10, categoria }).subscribe({
      next: (res: any) => {
        if (res !== null) {
          this.sweet.alert(
            "",
            "Ya existe un tratamiento con este nombre",
            "error"
            );
        } else {
          this.saveTratamiento(categoria);
        }
      },
      error: (err) => console.log(err),
    });
  }

  saveTratamiento(categoria) {
    categoria.Deshabilitado = categoria.Deshabilitado ? 1 : 0;
    this.http.post("AdminCategorias.php", { caso: 0, categoria }).subscribe({
      next: (res: any) => {
        if (res === "Se agrego") {
          this.sweet.alert("", "Tratamiento agregado correctamente", "success");
          this.router.navigate([
            "/admin/administrador/categoria/listado-categorias",
          ]);
        } else {
          this.sweet.alert("", "Ocurrio un error intentelo de nuevo", "error");
        }
      },
      error: (err) => console.log(err),
    });
  }

  firstLetterUppercase(name) {
    const texto = this.categoriaForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.categoriaForm.controls[name].setValue(textofinal);
    }
  }

  getConvenios() {
    this.http
      .post("convenios.php", { caso: 8, idclinica: this.idClinica })
      .subscribe({
        next: (convenios: any) => {
          this.convenios = convenios;
        },
        error: (err) => console.log(err),
      });
  }

  getSucursales() {
    this.http
      .post("categorias.php", { caso: 0, idclinica: this.idClinica })
      .subscribe({
        next: (sucursales: any) => {
          this.sucursales = sucursales ;
        },
        error: (err) => console.log(err)
      });
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }
}
