import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoMotivosComponent } from './listado-motivos/listado-motivos.component';
import { NuevoMotivoComponent } from './nuevo-motivo/nuevo-motivo.component';

const routes: Routes = [
    {
      path: 'nuevo-motivo',
      component: NuevoMotivoComponent,
    },
    {
      path: 'listado-motivos',
      component: ListadoMotivosComponent,
    },
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class MotivoLevelRoutingModule {}