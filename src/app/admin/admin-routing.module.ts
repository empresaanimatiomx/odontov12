import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PerfilComponent } from './perfil/perfil.component';

const routes: Routes = [
  {
    path: "dashboard",
    loadChildren: () =>
      import("./dashboard/dashboard.module").then((m) => m.DashboardModule),
  },
  {
    path: "configuracion",
    loadChildren: () =>
      import("./configuracion/configuracion.module").then(
        (m) => m.ConfiguracionModule
      ),
  },
  {
    path: "administrador",
    loadChildren: () =>
      import("./administrador/administrador.module").then(
        (m) => m.AdministradorModule
      ),
  },
  {
    path: "paciente",
    loadChildren: () =>
      import("./paciente/paciente.module").then((m) => m.PacienteModule),
  },
  {
    path: "agenda",
    loadChildren: () =>
      import("./agenda/agenda.module").then((m) => m.AgendaModule),
  },
  {
    path: "reportes",
    loadChildren: () =>
      import("./reportes/reporte.module").then((m) => m.ReportesModule),
  },
  {
    path: "videollamada",
    loadChildren: () =>
      import("./videollamada/videollamada.module").then(
        (m) => m.VideollamadaModule
      ),
  },
  {
    path: "email",
    loadChildren: () =>
      import("./email/email.module").then((m) => m.EmailModule),
  },
  {
    path: "perfil",
    component: PerfilComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
