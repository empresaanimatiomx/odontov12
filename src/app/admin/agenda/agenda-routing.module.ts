import { Page404Component } from './../../authentication/page404/page404.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalendarioComponent } from './calendario/calendario.component';
import { ListadoCitasComponent } from './listado-citas/listado-citas.component';
import { NuevaCitaComponent } from './nueva-cita/nueva-cita.component';

const routes: Routes = [
  {
    path: 'calendario',
    component: CalendarioComponent,
  },
  {
    path: 'nueva-cita',
    component: NuevaCitaComponent,
  },
  {
    path: 'listado-citas',
    component: ListadoCitasComponent,
  },

  { path: '**', component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgendaRoutingModule {}