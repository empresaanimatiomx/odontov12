import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Component } from "@angular/core";
import { Form, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import moment from "moment";
import { AuthService } from "src/app/core/service/auth.service";
import Swal from "sweetalert2";

const headers: any = new HttpHeaders({ "Content-Type": "application/json" });

@Component({
  selector: "app-nueva-cita",
  templateUrl: "./nueva-cita.component.html",
  styles: [],
})
export class NuevaCitaComponent {
  private baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  NuevaCitaForm: FormGroup;
  nuevoPacienteForm: FormGroup;
  hide3 = true;
  agree3 = false;
  pacientes: any = [];
  doctores: any = [];
  servicios: any = [];
  convenios: any = [];
  idClinica;

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    public httpClient: HttpClient,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.NuevaCitaForm = this.fb.group({
      paciente: ["", [Validators.required]],
      doctor: ["", [Validators.required]],
      fecha: ["", [Validators.required]],
      horario: ["", [Validators.required]],
      motivo: ["", [Validators.required]],
      servicio: ["", [Validators.required]],
      observaciones: [""],
    });

    this.nuevoPacienteForm = this.fb.group({
      nombre: ["", [Validators.required]],
      apellido: [""],
      correo: ["", [Validators.email]],
      doctor: [""],
      telefono: ["", [Validators.required]],
      convenio: [""],
    });

    this.GetPacientes();
    this.GetDoctores();
    this.GetServicios();
    this.obtenerconvenios(this.idClinica);
    if (this.idClinica < 0) {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
  }
  //guarda el contenido del formulario en la BD
  guardarNuevoUsuario(formCita: FormGroup) {
    const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    const options: any = {
      caso: 3,
      nuevacita: formCita.value,
      fecha,
      idclinica,
    };
    this.httpClient
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta != null) {
          Swal.fire("La cita se guardo correctamente", "", "success");
          this.router.navigateByUrl("/admin/agenda/listado-citas");
        } else {
          console.log(respuesta);
        }
      });
  }
  //obtiene el listado de pacientes
  GetPacientes() {
    const options: any = { caso: 0 };
    this.httpClient
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.pacientes = respuesta;
      });
  }
  //obtiene el listado de doctores
  GetDoctores() {
    const options: any = { caso: 5, idClinica: this.idClinica };
    this.httpClient
      .post(`${this.baseURL}pacientes.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.doctores = respuesta;
      });
  }
  //obtiene el listado de servicios
  GetServicios() {
    const options: any = { caso: 2 };
    this.httpClient
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.servicios = respuesta;
      });
  }

  // metodo para obtener los convenios
  obtenerconvenios(idclinica) {
    const options: any = { caso: 8, idclinica: idclinica };
    this.httpClient
      .post(`${this.baseURL}convenios.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        this.convenios = respuesta;
      });
  }
  //valida si el doctor esta disponible en esa fecha y horario
  ValidarHorario(formCita: FormGroup) {
    const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
    const options: any = { caso: 4, nuevacita: formCita.value, fecha: fecha };
    this.httpClient
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta == "se encontro") {
          Swal.fire("El doctor no esta disponible en ese horario", "", "error");
          this.NuevaCitaForm.controls["horario"].setValue("");
        }
        console.log("disponible");
      });
  }

  abrirModalNuevoPaciente(content) {
    setTimeout(() => {
      this.modalService.open(content, {
        ariaLabelledBy: "modal-basic-title",
        centered: true,
      });
    }, 0);
  }

  nuevoPaciente(formPaciente: FormGroup) {
    const options: any = {
      caso: 14,
      nuevoPaciente: formPaciente.value,
      idClinica: this.idClinica,
    };
    this.httpClient
      .post(`${this.baseURL}pacientes.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta >= 1) {
          this.modalService.dismissAll();
          Swal.fire("Se agrego el paciente correctamente", "", "success");
          this.GetPacientes();
        } else {
          Swal.fire(
            "Ocurrio un error al agregar paciente intentelo de nuevo",
            "",
            "error"
          );
        }
      });
  }

  FirstUppercaseLetter(idname) {
    var texto = (<HTMLInputElement>document.getElementById(idname)).value;
    var cadena = texto.toLowerCase().split(" ");
    for (var i = 0; i < cadena.length; i++) {
      cadena[i] = cadena[i].charAt(0).toUpperCase() + cadena[i].substring(1);
    }
    texto = cadena.join(" ");
    (<HTMLInputElement>document.getElementById(idname)).value = texto;
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }
}
