import {
  Component,
  ViewChild,
  OnInit,
  TemplateRef,
  ElementRef,
} from "@angular/core";
import { CalendarOptions } from "@fullcalendar/angular";
import { EventInput } from "@fullcalendar/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import listPlugin from "@fullcalendar/list";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatRadioChange } from "@angular/material/radio";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import moment from "moment";
import { AuthService } from "../../../core/service/auth.service";
moment.locale("es");
import esLocale from "@fullcalendar/core/locales/es";
import Swal from "sweetalert2";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";

const headers: any = new HttpHeaders({ "Content-Type": "application/json" });

@Component({
  selector: "app-calendario",
  templateUrl: "./calendario.component.html",
  styleUrls: [],
})
export class CalendarioComponent implements OnInit {
  fechaLimite = new Date();
  idCita = "";
  @ViewChild("abrirmodal", { static: false }) abrirmodal: ElementRef;
  @ViewChild("nuevacita", { static: false }) nuevacita: ElementRef;
  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  localMx = esLocale;
  calendarioinfo: any = [];
  @ViewChild("calendar", { static: false })
  public addCusForm: FormGroup;
  dialogTitle: string;
  filterOptions = "All";
  calendarData: any;
  idClinica: number;
  pacientes: any = [];
  doctores: any = [];
  servicios: any = [];
  convenios: any = [];
  motivosconsulta: any = [];
  infocita: any = [];
  infocae: any = [];
  calendarOptions: CalendarOptions;
  nombrepaciente = "";
  nombredoctor = "";
  nombremotivo = "";
  nombreservicio = "";
  nombrepaciente1 = "";
  nombredoctor1 = "";
  nombremotivo1 = "";
  nombreservicio1 = "";
  sms;
  public filters = [
    { class: "", name: "Todos", value: "Todos", checked: "true" },
    {
      class: "orangee",
      name: "No confirmado",
      value: "Por confirmar",
      checked: "false",
    },
    {
      class: "yelloww",
      name: "Confirmado",
      value: "Confirmado",
      checked: "false",
    },
    {
      class: "redd",
      name: "Hora cancelada",
      value: "Hora Cancelada",
      checked: "false",
    },
    {
      class: "greenn",
      name: "Atendido",
      value: "Finalizada",
      checked: "false",
    },
  ];
  arregloVacio: any = [
    {
      Nombre: "Vacio",
      estatus: "Confirmado",
      id: "0",
      idServicios: "0",
      start: "start",
      title: "   ",
    },
  ];
  calendarVisible = true;
  calendarPlugins = [
    dayGridPlugin,
    timeGridPlugin,
    interactionPlugin,
    listPlugin,
  ];
  calendarWeekends = true;
  @ViewChild("callAPIDialog", { static: false })
  callAPIDialog: TemplateRef<any>;
  calendarEvents: EventInput[];
  tempEvents: EventInput[];
  todaysEvents: EventInput[];
  NuevaCitaForm: FormGroup;
  nuevoPacienteForm: FormGroup;
  motivootro = false;
  valor = "";
  valorboolean = false;
  horariocita = "";
  motivocita = "";
  fechacita = "";
  serviciocita = "";
  estatuscita = "";
  observacionescita = "";
  recordaatorios = false;
  public pacientData;
  isEmail = /\S+@\S+\.\S+/;

  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private authService: AuthService,
    private modalService: NgbModal,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
  }

  ngOnInit(): void {
    this.NuevaCitaForm = this.fb.group({
      paciente: ["", [Validators.required]],
      doctor: ["", [Validators.required]],
      fecha: ["", [Validators.required]],
      horario: ["", [Validators.required]],
      motivo: ["", [Validators.required]],
      motivoo: [""],
      servicio: ["", [Validators.required]],
      estatus: ["", [Validators.required]],
      observaciones: [""],
      telefonoMovil: ["", Validators.minLength(10)],
      email: ["", Validators.pattern(this.isEmail)],
    });
    this.nuevoPacienteForm = this.fb.group({
      nombre: ["", [Validators.required]],
      apellido: [""],
      correo: ["", [Validators.email]],
      doctor: [""],
      telefono: ["", [Validators.required]],
      convenio: [""],
    });
    this.GetPacientes(this.idClinica);
    this.GetDoctores(this.idClinica);
    this.GetServicios(this.idClinica);
    this.obtenermotivos(this.idClinica);
    this.obtenerconvenios(this.idClinica);
    this.calendario();
    if (this.idClinica < 0) {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
  }

  changeCategory(e: MatRadioChange) {
    if (e.value === "Todos") {
      this.calendario();
    } else {
      this.calendarioFiltro(e.value);
    }
  }

  patchValue(paciente): any{
    this.NuevaCitaForm.patchValue({
      telefonoMovil: paciente.Celular,
      email: paciente.Correo,
    });
  }

  changeDoc(e: MatRadioChange) {
    if (e.value === "Todos") {
      this.calendario();
    } else {
      this.docFiltro(e.value);
    }
  }

  toggleRecordatorio(event) {
    this.recordaatorios = event.checked;
  }

  docFiltro(idD) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 15, doc: idD };
    const URL: any = this.baseURL + "citas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        if (respuesta.toString() === "Vacio") {
          this.infocae = this.arregloVacio;
        } else {
          this.infocae = respuesta;
          for (let index = 0; index < this.infocae.length; index++) {
            this.infocae[index].className = this.getClassNameValue(
              this.infocae[index].estatus,
              this.infocae[index].start
            );
            if (this.infocae[index].title === "") {
              this.infocae[index].title = "Videollamda";
            }
          }
        }
        this.calendarOptions = {
          headerToolbar: {
            left: "prev,next today",
            center: "title",
            right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek",
          },
          initialView: "dayGridMonth",
          weekends: true,
          editable: true,
          selectable: true,
          selectMirror: true,
          dayMaxEvents: true,
          events: this.infocae,
          locale: this.localMx,
          select: this.funcionnuevacita.bind(this),
          eventClick: this.eventClicked.bind(this),
        };
      });
  }

  calendarioFiltro(stat) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 14, idC: this.idClinica, status: stat };
    const URL: any = this.baseURL + "citas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        if (respuesta.toString() === "Vacio") {
          this.infocae = this.arregloVacio;
        } else {
          this.infocae = respuesta;
          for (let index = 0; index < this.infocae.length; index++) {
            this.infocae[index].className = this.getClassNameValue(
              this.infocae[index].estatus,
              this.infocae[index].start
            );
            if (this.infocae[index].title === "") {
              this.infocae[index].title = "Videollamda";
            }
          }
        }
        this.calendarOptions = {
          headerToolbar: {
            left: "prev,next today",
            center: "title",
            right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek",
          },
          initialView: "dayGridMonth",
          weekends: true,
          editable: true,
          selectable: true,
          selectMirror: true,
          dayMaxEvents: true,
          events: this.infocae,
          locale: this.localMx,
          select: this.funcionnuevacita.bind(this),
          eventClick: this.eventClicked.bind(this),
        };
      });
  }

  // Carga citas programadas
  calendario() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 5, idC: this.idClinica };
    const URL: any = this.baseURL + "citas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        this.infocae = respuesta;
        if (respuesta !== null) {
          for (let index = 0; index < this.infocae.length; index++) {
            this.infocae[index].color = this.getClassNameValue(
              this.infocae[index].estatus,
              this.infocae[index].start
            );
            if (this.infocae[index].title === "") {
              this.infocae[index].title = "Videollamda";
            }
          }
        }
        this.calendarOptions = {
          headerToolbar: {
            left: "prev,next today",
            center: "title",
            right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek",
          },
          initialView: "dayGridMonth",
          weekends: true,
          editable: true,
          selectable: true,
          selectMirror: true,
          dayMaxEvents: true,
          events: this.infocae,
          locale: this.localMx,
          select: this.funcionnuevacita.bind(this),
          eventClick: this.eventClicked.bind(this),
        };
      });
  }

  eventClicked(event, content) {
    this.idCita = event.event._def.publicId;
    const fechahoy = moment().format("YYYY-MM-DD");
    const fechaevento = moment(event.event.start).format("YYYY-MM-DD");
    if (fechaevento >= fechahoy) {
      this.valor = "1";
      this.valorboolean = false;
    } else {
      this.valor = "2";
      this.valorboolean = true;
    }
    const options: any = { caso: 13, idc: this.idCita };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.infocita = respuesta;
        console.log(this.infocita);
        this.fechacita = this.infocita[0].Fecha;
        this.horariocita = this.infocita[0].Horario;
        this.motivocita = this.infocita[0].Motivos;
        this.serviciocita = this.infocita[0].idServicios;
        this.estatuscita = this.infocita[0].EstatusCita;
        this.observacionescita = this.infocita[0].ObservacionesCita;
        let ngbModalOptions: NgbModalOptions = {
          backdrop: "static",
          keyboard: false,
          centered: true,
          ariaLabelledBy: "modal-basic-title",
          size: "lg",
        };
        this.modalService.open(this.nuevacita, ngbModalOptions);
        const fecha = moment(this.infocita[0].Fecha).format("MM/DD/YYYY");
        this.nombredoctor =
          this.infocita[0].NombreUsuario + " " + this.infocita[0].Apellidos;
        this.nombrepaciente =
          this.infocita[0].Nombre + " " + this.infocita[0].Apellido;
        this.nombremotivo1 = this.infocita[0].NombreMotivo;
        this.nombreservicio1 = this.infocita[0].NombreServicio;
        this.nombremotivo = this.infocita[0].NombreMotivo;
        this.nombreservicio = this.infocita[0].NombreServicio;
        if (this.infocita[0].idClinica !== "") {
          this.motivootro = false;
          this.NuevaCitaForm.setValue({
            paciente: this.infocita[0].idPacientes,
            doctor: this.infocita[0].idDoctores,
            fecha: new Date(fecha),
            horario: this.infocita[0].Horario,
            motivo: this.infocita[0].Motivos,
            motivoo: "",
            servicio: this.infocita[0].idServicios,
            estatus: this.infocita[0].EstatusCita,
            observaciones: this.infocita[0].ObservacionesCita,
            telefonoMovil: "",
            email: "",
          });
        } else {
          this.motivootro = true;
          this.NuevaCitaForm.setValue({
            paciente: this.infocita[0].idPacientes,
            doctor: this.infocita[0].idDoctores,
            fecha: new Date(fecha),
            horario: this.infocita[0].Horario,
            motivo: "Otro motivo de consulta de cita",
            motivoo: this.infocita[0].NombreMotivo,
            servicio: this.infocita[0].idServicios,
            estatus: this.infocita[0].EstatusCita,
            observaciones: this.infocita[0].ObservacionesCita,
            telefonoMovil: "",
            email: "",
          });
        }
      });
  }

  funcionnuevacita() {
    this.valor = "0";
    this.valorboolean = false;
    let ngbModalOptions: NgbModalOptions = {
      backdrop: "static",
      keyboard: false,
      centered: true,
      ariaLabelledBy: "modal-basic-title",
      size: "lg",
    };
    this.modalService.open(this.nuevacita, ngbModalOptions);
  }

  cerrarmodal(form: FormGroup) {
    form.reset();
    this.fechacita = "";
    this.horariocita = "";
    this.motivocita = "";
    this.serviciocita = "";
    this.estatuscita = "";
    this.observacionescita = "";
    this.modalService.dismissAll();
  }

  cerrarmodal1() {
    this.modalService.dismissAll();
  }

  otromotivo(valor) {
    this.motivootro = valor;
  }

  guardarNuevaCita(formCita: FormGroup) {
    if (this.valor === "0") {
      if (formCita.value.motivo === "Otro motivo de consulta de cita") {
        const options: any = {
          caso: 12,
          Nombredemotivo: formCita.value.motivoo,
          Mostrarenagenda: "SI",
        };
        this.http
          .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
          .subscribe((respuesta: any) => {
            if (respuesta != null) {
              const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
              const fecha1 = moment(formCita.value.fecha).format(
                "MMMM Do YYYY"
              );
              const hora = formCita.value.horario;
              const observaciones = formCita.value.observaciones;
              formCita.value.motivo = respuesta;
              const { idclinica } = JSON.parse(
                localStorage.getItem("datosusuarioclinica")
              );
              const options: any = {
                caso: 3,
                nuevacita: formCita.value,
                fecha: fecha,
                idclinica,
              };
              this.http
                .post(
                  `${this.baseURL}citas.php`,
                  JSON.stringify(options),
                  headers
                )
                .subscribe((respuesta: any) => {
                  if (respuesta != null) {
                    const options: any = {
                      caso: 18,
                      id_usuario: this.authService.currentUserValue.id,
                      motivo: "Nueva cita",
                      detalle: "Nueva cita",
                      fecha: moment().format("YYYY-MM-DD"),
                      hora: moment().format("HH:MM"),
                      idc: respuesta.toString(),
                    };
                    this.http
                      .post(
                        `${this.baseURL}citas.php`,
                        JSON.stringify(options),
                        headers
                      )
                      .subscribe((respuesta: any) => {
                        if (respuesta == "Se inserto") {
                          const telefono = "+522441049843";
                          const estatusc = formCita.value.estatus;
                          let link;
                          let mensaje = "";
                          if (estatusc === "Por confirmar") {
                            link = respuesta.toString();
                            if (observaciones) {
                              mensaje =
                                "Información de tu cita--salto--Doctor: " +
                                this.nombredoctor +
                                "--salto--Paciente: " +
                                this.nombrepaciente +
                                "--salto--Fecha y Hora: " +
                                fecha1 +
                                " " +
                                hora +
                                "--salto--Motivo: " +
                                this.nombremotivo +
                                "--salto--Servicio: " +
                                this.nombreservicio +
                                "--salto--Observaciones: " +
                                observaciones +
                                "--salto--Para confirmar o cancelar la cita da clic en el siguiente link: --salto--";
                            } else {
                              mensaje =
                                "Información de tu cita--salto--Doctor: " +
                                this.nombredoctor +
                                "--salto--Paciente: " +
                                this.nombrepaciente +
                                "--salto--Fecha y Hora: " +
                                fecha1 +
                                " " +
                                hora +
                                "--salto--Motivo: " +
                                this.nombremotivo +
                                "--salto--Servicio: " +
                                this.nombreservicio +
                                "--salto--Para confirmar o cancelar la cita da clic en el siguiente link: --salto--";
                            }
                          } else {
                            link = 0;
                            if (observaciones) {
                              mensaje =
                                "Información de tu cita--salto--Doctor: " +
                                this.nombredoctor +
                                "--salto--Paciente: " +
                                this.nombrepaciente +
                                "--salto--Fecha y Hora: " +
                                fecha1 +
                                " " +
                                hora +
                                "--salto--Motivo: " +
                                this.nombremotivo +
                                "--salto--Servicio: " +
                                this.nombreservicio +
                                "--salto--Observaciones: " +
                                observaciones;
                            } else {
                              mensaje =
                                "Información de tu cita--salto--Doctor: " +
                                this.nombredoctor +
                                "--salto--Paciente: " +
                                this.nombrepaciente +
                                "--salto--Fecha y Hora: " +
                                fecha1 +
                                " " +
                                hora +
                                "--salto--Motivo: " +
                                this.nombremotivo +
                                "--salto--Servicio: " +
                                this.nombreservicio;
                            }
                          }
                          this.http
                            .get(
                              "https://smsyemail.herokuapp.com/sms/" +
                                telefono +
                                "/" +
                                mensaje +
                                "/" +
                                link +
                                "/" +
                                0
                            )
                            .subscribe(
                              (data) => {
                                console.log(JSON.stringify(data));
                              },
                              (error) => {
                                console.log("err");
                                console.log(JSON.stringify(error));
                              }
                            );
                          formCita.reset();
                          this.modalService.dismissAll();
                          Swal.fire(
                            "La cita se guardo correctamente",
                            "",
                            "success"
                          );
                          this.calendario();
                        }
                      });
                  } else {
                    console.log(respuesta);
                  }
                });
            } else {
              console.log(respuesta);
            }
          });
      } else {
        const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
        const fecha1 = moment(formCita.value.fecha).format("MMMM Do YYYY");
        const hora = formCita.value.horario;
        const observaciones = formCita.value.observaciones;
        const { idclinica } = JSON.parse(
          localStorage.getItem("datosusuarioclinica")
        );
        const options: any = {
          caso: 3,
          nuevacita: formCita.value,
          fecha: fecha,
          idclinica,
        };
        this.http
          .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
          .subscribe((respuesta: any) => {
            if (respuesta != null) {
              const options: any = {
                caso: 18,
                id_usuario: this.authService.currentUserValue.id,
                motivo: "Nueva cita",
                detalle: "Nueva cita",
                fecha: moment().format("YYYY-MM-DD"),
                hora: moment().format("HH:MM"),
                idc: respuesta.toString(),
              };
              this.http
                .post(
                  `${this.baseURL}citas.php`,
                  JSON.stringify(options),
                  headers
                )
                .subscribe((respuesta: any) => {
                  if (respuesta == "Se inserto") {
                    const telefono = "+522441049843";
                    let link;
                    const estatusc = formCita.value.estatus;
                    let mensaje = "";
                    if (estatusc === "Por confirmar") {
                      link = respuesta.toString();
                      if (observaciones) {
                        mensaje =
                          "Información de tu cita--salto--Doctor: " +
                          this.nombredoctor +
                          "--salto--Paciente: " +
                          this.nombrepaciente +
                          "--salto--Fecha y Hora: " +
                          fecha1 +
                          " " +
                          hora +
                          "--salto--Motivo: " +
                          this.nombremotivo +
                          "--salto--Servicio: " +
                          this.nombreservicio +
                          "--salto--Observaciones: " +
                          observaciones +
                          "--salto--Para confirmar o cancelar la cita da clic en el siguiente link: --salto--";
                      } else {
                        mensaje =
                          "Información de tu cita--salto--Doctor: " +
                          this.nombredoctor +
                          "--salto--Paciente: " +
                          this.nombrepaciente +
                          "--salto--Fecha y Hora: " +
                          fecha1 +
                          " " +
                          hora +
                          "--salto--Motivo: " +
                          this.nombremotivo +
                          "--salto--Servicio: " +
                          this.nombreservicio +
                          "--salto--Para confirmar o cancelar la cita da clic en el siguiente link: --salto--";
                      }
                    } else {
                      link = 0;
                      if (observaciones) {
                        mensaje =
                          "Información de tu cita--salto--Doctor: " +
                          this.nombredoctor +
                          "--salto--Paciente: " +
                          this.nombrepaciente +
                          "--salto--Fecha y Hora: " +
                          fecha1 +
                          " " +
                          hora +
                          "--salto--Motivo: " +
                          this.nombremotivo +
                          "--salto--Servicio: " +
                          this.nombreservicio +
                          "--salto--Observaciones: " +
                          observaciones;
                      } else {
                        mensaje =
                          "Información de tu cita--salto--Doctor: " +
                          this.nombredoctor +
                          "--salto--Paciente: " +
                          this.nombrepaciente +
                          "--salto--Fecha y Hora: " +
                          fecha1 +
                          " " +
                          hora +
                          "--salto--Motivo: " +
                          this.nombremotivo +
                          "--salto--Servicio: " +
                          this.nombreservicio;
                      }
                    }

                    this.http
                      .get(
                        "https://smsyemail.herokuapp.com/sms/" +
                          telefono +
                          "/" +
                          mensaje +
                          "/" +
                          link +
                          "/" +
                          0
                      )
                      .subscribe(
                        (data) => {
                          console.log(JSON.stringify(data));
                        },
                        (error) => {
                          console.log("err");
                          console.log(JSON.stringify(error));
                        }
                      );
                    formCita.reset();
                    this.modalService.dismissAll();
                    Swal.fire("La cita se guardo correctamente", "", "success");
                    this.calendario();
                  }
                });
            } else {
              console.log(respuesta);
            }
          });
      }
    } else {
      if (formCita.value.motivo === "Otro motivo de consulta de cita") {
        const options: any = {
          caso: 12,
          Nombredemotivo: formCita.value.motivoo,
          Mostrarenagenda: "SI",
        };
        this.http
          .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
          .subscribe((respuesta: any) => {
            if (respuesta != null) {
              formCita.value.motivo = respuesta;
              let fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
              let horario = formCita.value.horario;
              let motivo = formCita.value.motivo;
              let servicio = formCita.value.servicio;
              let estatus = formCita.value.estatus;
              let observaciones = formCita.value.observaciones;
              let arraycita = [];
              let mensajemodificacion = "";
              let motivomodificacion = "";
              if (
                this.fechacita === fecha &&
                this.horariocita === horario &&
                this.motivocita === motivo &&
                this.serviciocita === servicio &&
                this.estatuscita === estatus &&
                this.observacionescita === observaciones
              ) {
                this.fechacita = "";
                this.horariocita = "";
                this.motivocita = "";
                this.serviciocita = "";
                this.estatuscita = "";
                this.observacionescita = "";
                this.modalService.dismissAll();
                Swal.fire("No hubo cambios por modificar", "", "success");
              } else {
                if (this.fechacita !== fecha) {
                  arraycita.push("fecha");
                  mensajemodificacion =
                    "La fecha de la cita se ha cambia de " +
                    this.fechacita +
                    " a " +
                    fecha;
                  motivomodificacion = "Modificación de fecha";
                }
                if (this.horariocita !== horario) {
                  arraycita.push("horario");
                  mensajemodificacion =
                    mensajemodificacion +
                    "\nLa hora de la cita se ha cambia de " +
                    this.horariocita +
                    " a " +
                    horario;
                  motivomodificacion =
                    motivomodificacion + "\nModificación de Hora";
                }
                if (this.motivocita !== motivo) {
                  arraycita.push("motivo");
                  mensajemodificacion =
                    mensajemodificacion +
                    "\nEl motivo de la cita se ha cambia de " +
                    this.nombremotivo1 +
                    " a " +
                    this.nombremotivo;
                  motivomodificacion =
                    motivomodificacion + "\nModificación de motivo";
                }
                if (this.serviciocita !== servicio) {
                  arraycita.push("servicio");
                  mensajemodificacion =
                    mensajemodificacion +
                    "\nEl servicio de la cita se ha cambia de " +
                    this.nombreservicio +
                    " a " +
                    this.nombreservicio1;
                  motivomodificacion =
                    motivomodificacion + "\nModificación de servicio";
                }
                if (this.estatuscita !== estatus) {
                  arraycita.push("estatus");
                  mensajemodificacion =
                    mensajemodificacion +
                    "\nEl estatus de la cita se ha cambia de " +
                    this.estatuscita +
                    " a " +
                    estatus;
                  motivomodificacion =
                    motivomodificacion + "\nModificación de estatus";
                }
                if (this.observacionescita !== observaciones) {
                  arraycita.push("observaciones");
                  mensajemodificacion =
                    mensajemodificacion +
                    "\nSe modifico la observación de la cita de " +
                    this.observacionescita +
                    " a " +
                    observaciones;
                  motivomodificacion =
                    motivomodificacion + "\nModificación de observaciones";
                }

                if (this.sms || estatus === "Por confirmar") {
                  const fecha = moment(formCita.value.fecha).format(
                    "YYYY-MM-DD"
                  );
                  const fecha1 = moment(formCita.value.fecha).format(
                    "MMMM Do YYYY"
                  );
                  const hora = formCita.value.horario;
                  const observaciones = formCita.value.observaciones;
                  const options: any = {
                    caso: 8,
                    nuevacita: formCita.value,
                    idc: this.infocita[0].idCitas,
                    fecha: fecha,
                  };
                  this.http
                    .post(
                      `${this.baseURL}citas.php`,
                      JSON.stringify(options),
                      headers
                    )
                    .subscribe((respuesta: any) => {
                      if (respuesta == "Se modifico") {
                        const options: any = {
                          caso: 18,
                          id_usuario: this.authService.currentUserValue.id,
                          motivo: motivomodificacion,
                          detalle: mensajemodificacion,
                          fecha: moment().format("YYYY-MM-DD"),
                          hora: moment().format("HH:MM"),
                          idc: this.infocita[0].idCitas,
                        };
                        this.http
                          .post(
                            `${this.baseURL}citas.php`,
                            JSON.stringify(options),
                            headers
                          )
                          .subscribe((respuesta: any) => {
                            if (respuesta == "Se inserto") {
                              const telefono = "+522441049843";
                              let link;
                              const estatusc = formCita.value.estatus;
                              let mensaje = "";
                              if (estatusc === "Por confirmar") {
                                link = this.infocita[0].idCitas;
                                if (observaciones) {
                                  mensaje =
                                    "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                                    this.nombredoctor +
                                    "--salto--Paciente: " +
                                    this.nombrepaciente +
                                    "--salto--Fecha y Hora: " +
                                    fecha1 +
                                    " " +
                                    hora +
                                    "--salto--Motivo: " +
                                    this.nombremotivo +
                                    "--salto--Servicio: " +
                                    this.nombreservicio +
                                    "--salto--Observaciones: " +
                                    observaciones +
                                    "--salto--Para confirmar o cancelar la cita da clic en el siguiente link: --salto--";
                                } else {
                                  mensaje =
                                    "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                                    this.nombredoctor +
                                    "--salto--Paciente: " +
                                    this.nombrepaciente +
                                    "--salto--Fecha y Hora: " +
                                    fecha1 +
                                    " " +
                                    hora +
                                    "--salto--Motivo: " +
                                    this.nombremotivo +
                                    "--salto--Servicio: " +
                                    this.nombreservicio +
                                    "--salto--Para confirmar o cancelar la cita da clic en el siguiente link: --salto--";
                                }
                              } else {
                                link = 0;
                                if (observaciones) {
                                  mensaje =
                                    "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                                    this.nombredoctor +
                                    "--salto--Paciente: " +
                                    this.nombrepaciente +
                                    "--salto--Fecha y Hora: " +
                                    fecha1 +
                                    " " +
                                    hora +
                                    "--salto--Motivo: " +
                                    this.nombremotivo +
                                    "--salto--Servicio: " +
                                    this.nombreservicio +
                                    "--salto--Observaciones: " +
                                    observaciones;
                                } else {
                                  mensaje =
                                    "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                                    this.nombredoctor +
                                    "--salto--Paciente: " +
                                    this.nombrepaciente +
                                    "--salto--Fecha y Hora: " +
                                    fecha1 +
                                    " " +
                                    hora +
                                    "--salto--Motivo: " +
                                    this.nombremotivo +
                                    "--salto--Servicio: " +
                                    this.nombreservicio;
                                }
                              }
                              this.http
                                .get(
                                  "https://smsyemail.herokuapp.com/sms/" +
                                    telefono +
                                    "/" +
                                    mensaje +
                                    "/" +
                                    link +
                                    "/" +
                                    0
                                )
                                .subscribe(
                                  (data) => {
                                    console.log(JSON.stringify(data));
                                  },
                                  (error) => {
                                    console.log("err");
                                    console.log(JSON.stringify(error));
                                  }
                                );
                              this.fechacita = "";
                              this.horariocita = "";
                              this.motivocita = "";
                              this.serviciocita = "";
                              this.estatuscita = "";
                              this.observacionescita = "";
                              formCita.reset();
                              this.sms = false;
                              this.modalService.dismissAll();
                              Swal.fire(
                                "Se actualizo la cita correctamente",
                                "",
                                "success"
                              );
                              this.calendario();
                            }
                          });
                      } else {
                        Swal.fire(
                          "Ocurrio un error intentelo de nuevo",
                          "",
                          "error"
                        );
                      }
                    });
                } else {
                  const fecha = moment(formCita.value.fecha).format(
                    "YYYY-MM-DD"
                  );
                  const options: any = {
                    caso: 8,
                    nuevacita: formCita.value,
                    idc: this.infocita[0].idCitas,
                    fecha: fecha,
                  };
                  this.http
                    .post(
                      `${this.baseURL}citas.php`,
                      JSON.stringify(options),
                      headers
                    )
                    .subscribe((respuesta: any) => {
                      if (respuesta == "Se modifico") {
                        const options: any = {
                          caso: 18,
                          id_usuario: this.authService.currentUserValue.id,
                          motivo: motivomodificacion,
                          detalle: mensajemodificacion,
                          fecha: moment().format("YYYY-MM-DD"),
                          hora: moment().format("HH:MM"),
                          idc: this.infocita[0].idCitas,
                        };
                        this.http
                          .post(
                            `${this.baseURL}citas.php`,
                            JSON.stringify(options),
                            headers
                          )
                          .subscribe((respuesta: any) => {
                            if (respuesta == "Se inserto") {
                              this.fechacita = "";
                              this.horariocita = "";
                              this.motivocita = "";
                              this.serviciocita = "";
                              this.estatuscita = "";
                              this.observacionescita = "";
                              formCita.reset();
                              this.modalService.dismissAll();
                              Swal.fire(
                                "Se actualizo la cita correctamente",
                                "",
                                "success"
                              );
                            }
                          });
                      } else {
                        Swal.fire(
                          "Ocurrio un error intentelo de nuevo",
                          "",
                          "error"
                        );
                      }
                      this.modalService.dismissAll();
                      this.ngOnInit();
                    });
                }
              }
            }
          });
      } else {
        let fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
        let horario = formCita.value.horario;
        let motivo = formCita.value.motivo;
        let servicio = formCita.value.servicio;
        let estatus = formCita.value.estatus;
        let observaciones = formCita.value.observaciones;
        let arraycita = [];
        let mensajemodificacion = "";
        let motivomodificacion = "";
        if (
          this.fechacita === fecha &&
          this.horariocita === horario &&
          this.motivocita === motivo &&
          this.serviciocita === servicio &&
          this.estatuscita === estatus &&
          this.observacionescita === observaciones
        ) {
          this.fechacita = "";
          this.horariocita = "";
          this.motivocita = "";
          this.serviciocita = "";
          this.estatuscita = "";
          this.observacionescita = "";
          this.modalService.dismissAll();
          Swal.fire("No hubo cambios por modificar", "", "success");
        } else {
          if (this.fechacita !== fecha) {
            arraycita.push("fecha");
            mensajemodificacion =
              "La fecha de la cita se ha cambia de " +
              this.fechacita +
              " a " +
              fecha;
            motivomodificacion = "Modificación de fecha";
          }
          if (this.horariocita !== horario) {
            arraycita.push("horario");
            mensajemodificacion =
              mensajemodificacion +
              "\nLa hora de la cita se ha cambia de " +
              this.horariocita +
              " a " +
              horario;
            motivomodificacion = motivomodificacion + "\nModificación de Hora";
          }
          if (this.motivocita !== motivo) {
            arraycita.push("motivo");
            mensajemodificacion =
              mensajemodificacion +
              "\nEl motivo de la cita se ha cambia de " +
              this.nombremotivo1 +
              " a " +
              this.nombremotivo;
            motivomodificacion =
              motivomodificacion + "\nModificación de motivo";
          }
          if (this.serviciocita !== servicio) {
            arraycita.push("servicio");
            mensajemodificacion =
              mensajemodificacion +
              "\nEl servicio de la cita se ha cambia de " +
              this.nombreservicio +
              " a " +
              this.nombreservicio1;
            motivomodificacion =
              motivomodificacion + "\nModificación de servicio";
          }
          if (this.estatuscita !== estatus) {
            arraycita.push("estatus");
            mensajemodificacion =
              mensajemodificacion +
              "\nEl estatus de la cita se ha cambia de " +
              this.estatuscita +
              " a " +
              estatus;
            motivomodificacion =
              motivomodificacion + "\nModificación de estatus";
          }
          if (this.observacionescita !== observaciones) {
            arraycita.push("observaciones");
            mensajemodificacion =
              mensajemodificacion +
              "\nSe modifico la observación de la cita de " +
              this.observacionescita +
              " a " +
              observaciones;
            motivomodificacion =
              motivomodificacion + "\nModificación de observaciones";
          }

          if (this.sms || estatus === "Por confirmar") {
            const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
            const fecha1 = moment(formCita.value.fecha).format("MMMM Do YYYY");
            const hora = formCita.value.horario;
            const observaciones = formCita.value.observaciones;
            const options: any = {
              caso: 8,
              nuevacita: formCita.value,
              idc: this.infocita[0].idCitas,
              fecha: fecha,
            };
            this.http
              .post(
                `${this.baseURL}citas.php`,
                JSON.stringify(options),
                headers
              )
              .subscribe((respuesta: any) => {
                if (respuesta == "Se modifico") {
                  const options: any = {
                    caso: 18,
                    id_usuario: this.authService.currentUserValue.id,
                    motivo: motivomodificacion,
                    detalle: mensajemodificacion,
                    fecha: moment().format("YYYY-MM-DD"),
                    hora: moment().format("HH:MM"),
                    idc: this.infocita[0].idCitas,
                  };
                  this.http
                    .post(
                      `${this.baseURL}citas.php`,
                      JSON.stringify(options),
                      headers
                    )
                    .subscribe((respuesta: any) => {
                      if (respuesta == "Se inserto") {
                        const telefono = "+522441049843";
                        let link;
                        const estatusc = formCita.value.estatus;
                        let mensaje = "";
                        if (estatusc === "Por confirmar") {
                          link = this.infocita[0].idCitas;
                          if (observaciones) {
                            mensaje =
                              "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                              this.nombredoctor +
                              "--salto--Paciente: " +
                              this.nombrepaciente +
                              "--salto--Fecha y Hora: " +
                              fecha1 +
                              " " +
                              hora +
                              "--salto--Motivo: " +
                              this.nombremotivo +
                              "--salto--Servicio: " +
                              this.nombreservicio +
                              "--salto--Observaciones: " +
                              observaciones +
                              "--salto--Para confirmar o cancelar la cita da clic en el siguiente link: --salto--";
                          } else {
                            mensaje =
                              "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                              this.nombredoctor +
                              "--salto--Paciente: " +
                              this.nombrepaciente +
                              "--salto--Fecha y Hora: " +
                              fecha1 +
                              " " +
                              hora +
                              "--salto--Motivo: " +
                              this.nombremotivo +
                              "--salto--Servicio: " +
                              this.nombreservicio +
                              "--salto--Para confirmar o cancelar la cita da clic en el siguiente link: --salto--";
                          }
                        } else {
                          link = 0;
                          if (observaciones) {
                            mensaje =
                              "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                              this.nombredoctor +
                              "--salto--Paciente: " +
                              this.nombrepaciente +
                              "--salto--Fecha y Hora: " +
                              fecha1 +
                              " " +
                              hora +
                              "--salto--Motivo: " +
                              this.nombremotivo +
                              "--salto--Servicio: " +
                              this.nombreservicio +
                              "--salto--Observaciones: " +
                              observaciones;
                          } else {
                            mensaje =
                              "Se ha Actualizado la información de tu cita--salto--Doctor: " +
                              this.nombredoctor +
                              "--salto--Paciente: " +
                              this.nombrepaciente +
                              "--salto--Fecha y Hora: " +
                              fecha1 +
                              " " +
                              hora +
                              "--salto--Motivo: " +
                              this.nombremotivo +
                              "--salto--Servicio: " +
                              this.nombreservicio;
                          }
                        }
                        this.http
                          .get(
                            "https://smsyemail.herokuapp.com/sms/" +
                              telefono +
                              "/" +
                              mensaje +
                              "/" +
                              link +
                              "/" +
                              0
                          )
                          .subscribe(
                            (data) => {
                              console.log(JSON.stringify(data));
                            },
                            (error) => {
                              console.log("err");
                              console.log(JSON.stringify(error));
                            }
                          );
                        this.fechacita = "";
                        this.horariocita = "";
                        this.motivocita = "";
                        this.serviciocita = "";
                        this.estatuscita = "";
                        this.observacionescita = "";
                        this.sms = false;
                        formCita.reset();
                        this.modalService.dismissAll();
                        Swal.fire(
                          "Se actualizo la cita correctamente",
                          "",
                          "success"
                        );
                        this.calendario();
                      }
                    });
                } else {
                  Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
                }
              });
          } else {
            const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
            const options: any = {
              caso: 8,
              nuevacita: formCita.value,
              idc: this.infocita[0].idCitas,
              fecha: fecha,
            };
            this.http
              .post(
                `${this.baseURL}citas.php`,
                JSON.stringify(options),
                headers
              )
              .subscribe((respuesta: any) => {
                if (respuesta == "Se modifico") {
                  const options: any = {
                    caso: 18,
                    id_usuario: this.authService.currentUserValue.id,
                    motivo: motivomodificacion,
                    detalle: mensajemodificacion,
                    fecha: moment().format("YYYY-MM-DD"),
                    hora: moment().format("HH:MM"),
                    idc: this.infocita[0].idCitas,
                  };
                  this.http
                    .post(
                      `${this.baseURL}citas.php`,
                      JSON.stringify(options),
                      headers
                    )
                    .subscribe((respuesta: any) => {
                      if (respuesta == "Se inserto") {
                        this.fechacita = "";
                        this.horariocita = "";
                        this.motivocita = "";
                        this.serviciocita = "";
                        this.estatuscita = "";
                        this.observacionescita = "";
                        formCita.reset();
                        this.modalService.dismissAll();
                        Swal.fire(
                          "Se actualizo la cita correctamente",
                          "",
                          "success"
                        );
                      }
                    });
                } else {
                  Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
                }
                this.modalService.dismissAll();
                this.ngOnInit();
              });
          }
        }
      }
    }
  }

  eliminarcita(formCita: FormGroup) {
    Swal.fire({
      title: "¿Esta seguro de eliminar esta cita?",
      showCancelButton: true,
      confirmButtonText: `Eliminar cita`,
      cancelButtonText: "Cancelar",
      confirmButtonColor: "#f44336",
      cancelButtonColor: "#007bff",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        const headers: any = new HttpHeaders({
          "Content-Type": "application/json",
        });
        const options: any = { caso: 21, idc: this.infocita[0].idCitas };
        const URL: any = this.baseURL + "citas.php";
        this.http
          .post(URL, JSON.stringify(options), headers)
          .subscribe((respuesta) => {
            const res = respuesta;
            if (res.toString() === "Se elimino") {
              this.calendario();
              this.fechacita = "";
              this.horariocita = "";
              this.motivocita = "";
              this.serviciocita = "";
              this.estatuscita = "";
              this.observacionescita = "";
              formCita.reset();
              this.modalService.dismissAll();
              this.swalalertmensaje("success", "Cita eliminada correctamente");
            } else {
              this.swalalertmensaje(
                "error",
                "Ocurrio un error, intente de nuevo por favor"
              );
            }
          });
      }
    });
  }

  iravista(idpaciente, apartadodepagina) {
    this.modalService.dismissAll();
    localStorage.setItem("perfilusuario", JSON.stringify(idpaciente));
    localStorage.setItem("tabs", JSON.stringify(apartadodepagina));
    this.router.navigate(["/admin/paciente/detalle-paciente"]);
  }

  swalalertmensaje(icon, mensaje) {
    Swal.fire({
      position: "center",
      icon: icon,
      title: mensaje,
      showConfirmButton: true,
      allowOutsideClick: false,
      confirmButtonText: "Aceptar",
    });
  }

  onToggle(event) {
    this.sms = event.checked;
  }

  //obtiene el listado de pacientes
  GetPacientes(idclinica) {
    const options: any = { caso: 10, idC: idclinica };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.pacientes = respuesta;
      });
  }

  GetDoctores(idC) {
    const options: any = { caso: 12, idC };
    this.http
      .post(`${this.baseURL}usuarios.php`, JSON.stringify(options), headers)
      .subscribe({
        next: (respuesta) => {
          this.doctores = respuesta;
        },
        error: (err) => console.log(err)
      });
  }

  //obtiene el listado de servicios
  GetServicios(idclinica) {
    const options: any = { caso: 0, idClinica: idclinica };
    this.http
      .post(`${this.baseURL}servicios.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.servicios = respuesta;
      });
  }

  // metodo para obtener los convenios
  obtenermotivos(idclinica) {
    const options: any = { caso: 11, idC: idclinica };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        this.motivosconsulta = respuesta;
      });
  }

  // metodo para obtener los convenios
  obtenerconvenios(idclinica) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 8, idclinica: idclinica };
    const URL: any = this.baseURL + "convenios.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.convenios = respuesta;
      });
  }

  //valida si el doctor esta disponible en esa fecha y horario
  ValidarHorario(formCita: FormGroup) {
    const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
    const options: any = { caso: 4, nuevacita: formCita.value, fecha: fecha };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta == "se encontro") {
          Swal.fire("El doctor no esta disponible en ese horario", "", "error");
          this.NuevaCitaForm.controls["horario"].setValue("");
        }
      });
  }

  FirstUppercaseLetter(id) {
    let texto = (<HTMLInputElement>document.getElementById(id)).value;
    if (texto !== "") {
      let textofinal = texto[0].toUpperCase() + texto.slice(1);
      (<HTMLInputElement>document.getElementById(id)).value = textofinal;
    }
  }

  abrirModalNuevoPaciente(content) {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: "static",
      keyboard: false,
      centered: true,
      ariaLabelledBy: "modal-basic-title",
      size: "lg",
    };
    setTimeout(() => {
      this.modalService.open(content, ngbModalOptions);
    }, 0);
  }

  nuevoPaciente(formPaciente: FormGroup) {
    const options: any = {
      caso: 14,
      nuevoPaciente: formPaciente.value,
      idClinica: this.idClinica,
    };
    this.http
      .post(`${this.baseURL}pacientes.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta >= 1) {
          formPaciente.reset();
          Swal.fire("Se agrego el paciente correctamente", "", "success");
          this.GetPacientes(this.idClinica);
        } else {
          Swal.fire(
            "Ocurrio un error al agregar paciente intentelo de nuevo",
            "",
            "error"
          );
        }
      });
  }

  getClassNameValue(estatus, fecha) {
    const fechahoy = moment().format("YYYY-MM-DD");
    const fechaevento = moment(fecha).format("YYYY-MM-DD");
    let className: string;
    if (estatus === "Por confirmar" && fechaevento >= fechahoy)
      className = "#000";
    else if (estatus === "Confirmado" && fechaevento >= fechahoy)
      className = "#000";
    else if (estatus === "Hora Cancelanda") className = ".fc-event-danger";
    else if (estatus === "Finalizada") className = ".fc-event-success";
    else className = ".fc-event-date";
    return className;
  }

  obtenernombrepaciente(option: any) {
    this.nombrepaciente = option.source.triggerValue;
  }

  obtenernombredoctor(option: any) {
    this.nombredoctor = option.source.triggerValue;
  }

  obtenernombremotivo(option: any) {
    this.nombremotivo = option.source.triggerValue;
  }

  obtenernombreservicio(option: any) {
    this.nombreservicio = option.source.triggerValue;
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  sendEmail() {
    const options: any = {
      email: "miradez28@gmail.com",
      enviado: "miguelicfiov@gmail.com",
    };
    this.http
      .post(`${this.baseURL}email.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta >= 1) {
          Swal.fire(
            "Enviado! Se envio el enlace a su correo para completar su registro!",
            "",
            "success"
          );
        } else {
          Swal.fire(
            `Error! Ocurrió un error intentelo de nuevo ${respuesta}`,
            "",
            "error"
          );
        }
      });
  }
}
