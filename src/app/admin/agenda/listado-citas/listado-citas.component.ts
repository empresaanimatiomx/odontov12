import { Component, OnInit, ViewChild, TemplateRef} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import moment from "moment";
import Swal from 'sweetalert2';
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { AuthService } from "src/app/core/service/auth.service";

const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });

@Component({
  selector: "app-listado-citas",
  templateUrl: "./listado-citas.component.html",
  styles: [],
})
export class ListadoCitasComponent implements OnInit {
  private baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  @ViewChild("roleTemplate", { static: true }) roleTemplate: TemplateRef<any>;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  rows = [];
  selectedRowData: selectRowInterface;
  data: any = [];
  filteredData: any = [];
  editForm: FormGroup;
  selectedOption: string;
  columns = [
    { name: "nombreP" },
    { name: "nombreD" },
    { name: "Fecha" },
    { name: "Horario" },
    { name: "Motivos" },
  ];
  pacientes: any = [];
  doctores: any = [];
  servicios: any = [];
  idClinica;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private modalService: NgbModal,
    public httpClient: HttpClient,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.editForm = this.fb.group({
      idCitas: ["", Validators.required],
      Paciente: ["", Validators.required],
      Doctor: ["", Validators.required],
      Fecha: ["", Validators.required],
      Hora: ["", Validators.required],
      Motivo: ["", Validators.required],
      Servicio: ["", Validators.required],
    });
  }
  ngOnInit() {
    if (this.idClinica < 0) {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
    this.GetPacientes();
    this.GetDoctores();
    this.GetServicios();
  }

  //obtiene el listado de pacientes
  GetPacientes() {
    const options: any = { caso: 0 };
    this.httpClient
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.pacientes = respuesta;
      });
  }
  //obtiene el listado de doctores
  GetDoctores() {
    const options: any = { caso: 1 };
    this.httpClient
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.doctores = respuesta;
      });
  }
  //obtiene el listado de servicios
  GetServicios() {
    const options: any = { caso: 2 };
    this.httpClient
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.servicios = respuesta;
      });
  }

  //valida si el doctor esta disponible en esa fecha y horario
  ValidarHorario(event) {
    const fecha = moment(this.editForm.value.Fecha).format("YYYY-MM-DD");
    const options: any = {
      caso: 4,
      nuevacita: this.editForm.value,
      fecha: fecha,
    };
    this.httpClient
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta == "se encontro") {
          Swal.fire("El doctor no esta disponible en ese horario", "", "error");
          this.editForm.controls["Hora"].setValue("");
        }
        console.log("disponible");
      });
  }

  editRow(row, rowIndex, content) {
    this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
    });
    this.editForm.setValue({
      idCitas: row.idCitas,
      Paciente: row.idPacientes,
      Doctor: row.idDoctores,
      Fecha: row.Fecha,
      Hora: row.Horario,
      Motivo: row.Motivos,
      Servicio: row.idServicios,
    });
    this.selectedRowData = row;
  }

  onEditSave(form: FormGroup) {
    const fecha = moment(form.value.Fecha).format("YYYY-MM-DD");
    const options: any = { caso: 8, nuevacita: form.value, fecha: fecha };
    this.httpClient
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta == "Se modifico") {
          Swal.fire("Se actualizo la cita correctamente", "", "success");
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
        this.modalService.dismissAll();
        this.ngOnInit();
      });
  }

  deleteRow(id) {
    const options: any = { caso: 9, idc: id };
    this.httpClient
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta == "Se elimino") {
          Swal.fire("Se elimino la cita correctamente", "", "success");
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
        this.ngOnInit();
      });
  }

  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }
}
export interface selectRowInterface {
  img: String;
  Nombres: String;
  Apellidos: String;

}
