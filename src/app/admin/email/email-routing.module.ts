import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrearEmailComponent } from './crear-email/crear-email.component';
import { InboxComponent } from './inbox/inbox.component';

const routes: Routes = [
  {
    path: "inbox",
    component: InboxComponent,
  },
  {
    path: "crear-email",
    component: CrearEmailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmailRoutingModule { }
