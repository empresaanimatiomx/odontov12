/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
import {
  Component,
  ViewChild,
  OnInit,
  ElementRef,
  ComponentFactoryResolver,
} from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "src/app/core/service/auth.service";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import moment from "moment";
import { DomSanitizer } from "@angular/platform-browser";
import Swal from "sweetalert2";
import { DynamicHostDirective } from "src/app/admin/directive/dynamic-host.directive";

const headers: any = new HttpHeaders({ "Content-Type": "application/json" });

@Component({
  selector: "app-inbox",
  templateUrl: "./inbox.component.html",
  styleUrls: ["./index.component.scss"],
})
export class InboxComponent implements OnInit {
  @ViewChild("nuevacategoria", { static: false }) nuevacategoria: ElementRef;
  @ViewChild("exampleModalCenter2", { static: false })
  exampleModalCenter2: ElementRef;

  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  plantillaSelect = "";
  categorias: any = [];
  categorias1: any = [];
  plantillasS: any = [];
  asunto = "";
  cate = "";
  asunto1 = "";
  cate1 = "";
  cate2 = "";
  asunto2 = "";
  mensaje = "";
  mensaje1;
  mensaje2;
  destinatario = "";
  correoSS: any = [];
  ver = "Correo";
  idClinica: number;
  valorcategoria = "";

  formcategoria: FormGroup;

  idcategoria = "";

  filtrodatosCategoria: any = [];
  filtrodatosBuzon: any = [];
  filtrodatosPlantilla: any = [];
  filtrodatosCorreo: any = [];

  columns: any[] = [{ name: "Motivo" }, { name: "Tiempo de Consulta" }];

  valorplantilla = "";

  config = {
    uiColor: "#ffffff",
    toolbarGroups: [
      { name: "clipboard", groups: ["clipboard", "undo"] },
      { name: "editing", groups: ["find", "selection", "spellchecker"] },
      { name: "links" },
      { name: "insert" },
      { name: "document", groups: ["mode", "document", "doctools"] },
      { name: "basicstyles", groups: ["basicstyles", "cleanup"] },
      { name: "paragraph", groups: ["list", "indent", "blocks", "align"] },
      { name: "styles" },
      { name: "colors" },
    ],
    skin: "kama",
    resize_enabled: false,
    removePlugins: "elementspath,save,magicline",
    extraPlugins: "divarea,smiley,justify,indentblock,colordialog",
    colorButton_foreStyle: {
      element: "font",
      attributes: { color: "#(color)" },
    },
    height: 500,
    removeDialogTabs: "image:advanced;link:advanced",
    removeButtons: "Subscript,Superscript,Anchor,Source,Table",
    format_tags: "p;h1;h2;h3;pre;div",
  };

  ckeConfig: any;
  log: string = "";
  @ViewChild(DynamicHostDirective) public dynamicHost: DynamicHostDirective;

  data: any = `<p>Hello, world!</p>`;
  config1 = {
    toolbar: [
      "undo",
      "redo",
      "|",
      "heading",
      "fontFamily",
      "fontSize",
      "|",
      "bold",
      "italic",
      "underline",
      "fontColor",
      "fontBackgroundColor",
      "highlight",
      "|",
      "link",
      "CKFinder",
      "imageUpload",
      "mediaEmbed",
      "|",
      "alignment",
      "bulletedList",
      "numberedList",
      "|",
      "indent",
      "outdent",
      "|",
      "insertTable",
      "blockQuote",
      "specialCharacters",
    ],
    language: "es",
    image: {
      toolbar: ["imageTextAlternative", "imageStyle:full", "imageStyle:side"],
    },
  };

  config2 = {
    toolbar: [],
    language: "es",
    image: {
      toolbar: ["imageTextAlternative", "imageStyle:full", "imageStyle:side"],
    },
  };

  idPlantilla;

  filtroCliente = "";
  clientes: any = [];
  destinatarios = [];
  nombres = [];
  nombrecompleto = "";
  posicionarreglo: any = [];
  correoDetalle: any = [];
  detalles: boolean = false;

  fechaEnvio = moment().format("YYYY-MM-DD hh:mm");

  imagenclinica = "";

  colorfiltrocategoria = "";
  nombrefiltrocategoria = "";
  colorcategoria1 = "";

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.formcategoria = this.fb.group({
      nombrecategoria: ["", [Validators.required]],
      colorcategoria: ["", [Validators.required]],
    });
  }
  ngOnInit() {
    this.categories();
    this.categories1();
    this.obtePlantillas();
    this.obtenCorreos();
    this.obtenClientes();
    this.ckeConfig = {
      allowedContent: false,
      extraPlugins: "divarea",
      forcePasteAsPlainText: true,
    };
  }

  categories() {
    const options: any = { caso: 0, id_clinica: this.idClinica };
    this.http
      .post(this.baseURL + "correos.php", JSON.stringify(options), headers)
      .subscribe((data: any) => {
        this.categorias = data;
      });
  }

  categories1() {
    const options: any = { caso: 17, id_clinica: this.idClinica };
    this.http
      .post(this.baseURL + "correos.php", JSON.stringify(options), headers)
      .subscribe((data: any) => {
        this.categorias1 = data;
        this.filtrodatosCategoria = data;
      });
  }

  obtePlantillas() {
    const options: any = { caso: 5, id_clinica: this.idClinica };
    this.http
      .post(this.baseURL + "correos.php", JSON.stringify(options), headers)
      .subscribe((data: any) => {
        this.plantillasS = data;
        this.filtrodatosPlantilla = data;
      });
  }

  obtenCorreos() {
    const options: any = { caso: 12, id_clinica: this.idClinica };
    this.http
      .post(this.baseURL + "correos.php", JSON.stringify(options), headers)
      .subscribe((data: any) => {
        this.correoSS = data;
        this.filtrodatosBuzon = data;
        this.nombrefiltrocategoria = "";
        this.colorfiltrocategoria = "";
      });
  }

  obtenClientes() {
    const options: any = { caso: 16, id_clinica: this.idClinica };
    this.http
      .post(this.baseURL + "correos.php", JSON.stringify(options), headers)
      .subscribe((data: any) => {
        this.clientes = data;
        this.filtrodatosCorreo = data;
      });
  }

  AgregaMensaje() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = {
      caso: 1,
      asun: this.asunto,
      message: this.mensaje,
      destino: this.destinatario,
      fechaE: this.fechaEnvio,
      catc: this.cate,
      idum: this.nombrecompleto,
      id_clinica: this.idClinica,
    };
    this.http
      .post(this.baseURL + "correos.php", JSON.stringify(options), headers)
      .subscribe((data: any) => {
        if (data == "Se inserto") {
          Swal.fire("Se envió correctamente!", "", "success").then((value) => {
            this.limpiarvariables();
            this.obtenCorreos();
            this.cambiarvista("Correo");
          });
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }

  obtePlan() {
    if (this.plantillaSelect) {
      const headers: any = new HttpHeaders({
        "Content-Type": "application/json",
      });
      const options: any = { caso: 9, idp: this.plantillaSelect };
      const URL: any = this.baseURL + "correos.php";
      this.http
        .post(URL, JSON.stringify(options), headers)
        .subscribe((respuesta) => {
          this.asunto = respuesta[0].Asunto;
          this.mensaje = respuesta[0].Mensaje;
          this.cate = respuesta[0].idCategoria;
        });
    } else {
      this.plantillaSelect = "";
      this.limpiarvariables();
    }
  }

  agregarplantilla() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    if (this.valorplantilla === "0") {
      const options: any = {
        caso: 8,
        asun: this.asunto1,
        catc: this.cate1,
        message: this.mensaje1,
        id_clinica: this.idClinica,
      };
      this.http
        .post(this.baseURL + `correos.php`, JSON.stringify(options), headers)
        .subscribe((data: any) => {
          if (data == "Se inserto") {
            Swal.fire("Se agregó correctamente!", "", "success");
            this.cate1 = "";
            this.asunto1 = "";
            this.mensaje1 = "";
            this.obtePlantillas();
            this.cambiarvista("Plantillas");
          } else {
            Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
          }
        });
    } else {
      const options: any = {
        caso: 10,
        asun: this.asunto1,
        catc: this.cate1,
        message: this.mensaje1,
        idp: this.idPlantilla,
      };
      this.http
        .post(this.baseURL + `correos.php`, JSON.stringify(options), headers)
        .subscribe((data: any) => {
          if (data == "Se modifico") {
            Swal.fire("Se modificó correctamente!", "", "success");
            this.cate1 = "";
            this.asunto1 = "";
            this.mensaje1 = "";
            this.obtePlantillas();
            this.cambiarvista("Plantillas");
          } else {
            Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
          }
        });
    }
  }

  EliminarP(id) {
    Swal.fire({
      title: "¿Seguro de eliminar la plantilla?",
      showCancelButton: true,
      confirmButtonText: `Eliminar`,
      cancelButtonText: `Cancelar`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.eliminarPlan(id);
      } else {
      }
    });
  }

  eliminarPlan(id) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 7, idp: id };
    const URL: any = this.baseURL + "correos.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((data: any) => {
        if (data == "Se elimino") {
          Swal.fire("Se ha eliminado correctamente!", "", "success");
          this.obtePlantillas();
          this.cambiarvista("Plantillas");
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }

  cambiarvista(vista) {
    this.ver = vista;
    this.plantillaSelect = "";
    this.limpiarvariables();
  }

  cambiarvistadetalle(vista, info) {
    this.limpiarvariables();
    this.cate2 = info.idCategoria;
    this.asunto2 = info.Asunto;
    this.mensaje2 = info.Mensaje;
    this.ver = vista;
  }

  cambiarvistadetalle1(vista, info) {
    this.imagenclinica =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
    this.limpiarvariables();
    const options: any = { caso: 13, idc: info.idCorreo };
    this.http
      .post(this.baseURL + "correos.php", JSON.stringify(options), headers)
      .subscribe((data: any) => {
        if (data !== null || data !== undefined) {
          this.correoDetalle = data;
          console.log(this.correoDetalle);
          this.ver = vista;
        }
      });
  }

  detallescorreo() {
    this.detalles = this.detalles ? false : true;
  }

  cambiarvistaplantilla(vista, mod, row) {
    this.limpiarvariables();
    this.ver = vista;
    this.valorplantilla = mod;
    if (this.valorplantilla === "1") {
      this.idPlantilla = row.idPlantillas;
      this.cate1 = row.idCategoria;
      this.asunto1 = row.Asunto;
      this.mensaje1 = row.Mensaje;
    }
  }

  Nuevacategoria() {
    this.valorcategoria = "0";
    let ngbModalOptions: NgbModalOptions = {
      backdrop: "static",
      keyboard: false,
      centered: true,
      ariaLabelledBy: "modal-basic-title",
    };
    this.modalService.open(this.nuevacategoria, ngbModalOptions);
  }

  modalusuarios() {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: "static",
      keyboard: false,
      centered: true,
      ariaLabelledBy: "modal-basic-title",
      size: "xl",
    };
    this.modalService.open(this.exampleModalCenter2, ngbModalOptions);
  }

  Editarcategoria(row) {
    this.idcategoria = row.idCategoria;
    this.valorcategoria = "1";
    let ngbModalOptions: NgbModalOptions = {
      backdrop: "static",
      keyboard: false,
      centered: true,
      ariaLabelledBy: "modal-basic-title",
    };
    this.modalService.open(this.nuevacategoria, ngbModalOptions);
    this.formcategoria.setValue({
      nombrecategoria: row.NombreCategoria,
      colorcategoria: row.Color,
    });
    this.colorcategoria1 = row.Color;
  }

  modalCategorias(modal) {
    this.modalService.open(modal, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
      size: "lg",
    });
  }

  cerrarmodal(form: FormGroup) {
    form.reset();
    this.modalService.dismissAll();
  }

  cerrarmodaldestinatarios() {
    this.modalService.dismissAll();
  }

  guardarclientes() {
    this.modalService.dismissAll();
  }

  Agregacategoria(form: FormGroup) {
    if (this.valorcategoria === "0") {
      const headers: any = new HttpHeaders({
        "Content-Type": "application/json",
      });
      const options: any = {
        caso: 3,
        formCategoria: form.getRawValue(),
        id_clinica: this.idClinica,
      };
      this.http
        .post(this.baseURL + "correos.php", JSON.stringify(options), headers)
        .subscribe((data: any) => {
          if (data == "Se inserto") {
            Swal.fire("Se agregó correctamente!", "", "success");
            form.reset();
            this.modalService.dismissAll();
            this.categories();
            this.categories1();
          } else {
            Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
          }
        });
    } else {
      const headers: any = new HttpHeaders({
        "Content-Type": "application/json",
      });
      const options: any = {
        caso: 2,
        formCategoria: form.getRawValue(),
        idc: this.idcategoria,
      };
      this.http
        .post(this.baseURL + "correos.php", JSON.stringify(options), headers)
        .subscribe((data: any) => {
          if (data == "Se modifico") {
            Swal.fire("Se actualizo correctamente!", "", "success");
            form.reset();
            this.modalService.dismissAll();
            this.categories();
            this.categories1();
          } else {
            Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
          }
        });
    }
  }

  eliminarcategoria(id, estatus, texto, texto1) {
    Swal.fire({
      title: "¿Estas seguro de " + texto + " la categoría?",
      showCancelButton: true,
      confirmButtonText: texto1,
      cancelButtonText: `Cancelar`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.metodoeliminarcategoria(id, estatus);
      } else {
      }
    });
  }

  metodoeliminarcategoria(id, estatus) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 6, idc: id, estatus: estatus };
    this.http
      .post(this.baseURL + "correos.php", JSON.stringify(options), headers)
      .subscribe((data: any) => {
        if (data == "Se elimino") {
          Swal.fire("Se actualizo correctamente!", "", "success");
          this.categories();
          this.categories1();
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }

  EliminarMail(id) {
    Swal.fire({
      title: "¿Seguro de eliminar el correo?",
      showCancelButton: true,
      confirmButtonText: `Eliminar`,
      cancelButtonText: `Cancelar`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.eliminarC(id);
      } else {
      }
    });
  }

  eliminarC(id) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 14, idc: id };
    const URL: any = this.baseURL + "correos.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((data: any) => {
        if (data == "Se elimino") {
          Swal.fire("Se ha eliminado correctamente!", "", "success");
          this.obtenCorreos();
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }

  filtrarcorreoporcategoria(categoria) {
    if (this.ver === "Correo") {
      this.nombrefiltrocategoria = categoria.NombreCategoria;
      this.colorfiltrocategoria = categoria.Color;
      const options: any = {
        caso: 15,
        idc: categoria.idCategoria,
        id_clinica: this.idClinica,
      };
      this.http
        .post(this.baseURL + "correos.php", JSON.stringify(options), headers)
        .subscribe((data: any) => {
          if (data == "vacio") {
            this.correoSS = [];
            this.filtrodatosBuzon = [];
          } else {
            this.correoSS = data;
            this.filtrodatosBuzon = data;
          }
        });
    }
  }

  FirstUppercaseLetter(idname) {
    var texto = (<HTMLInputElement>document.getElementById(idname)).value;
    var cadena = texto.toLowerCase().split(" ");
    for (var i = 0; i < cadena.length; i++) {
      cadena[i] = cadena[i].charAt(0).toUpperCase() + cadena[i].substring(1);
    }
    texto = cadena.join(" ");
    (<HTMLInputElement>document.getElementById(idname)).value = texto;
  }

  // metodo para convertir primera letra de cada palabra en mayuscula
  firstLetterUpperCase(id) {
    const texto = (<HTMLInputElement>document.getElementById(id)).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      (<HTMLInputElement>document.getElementById(id)).value = textofinal;
    }
  }

  filterDatatableCategoria(event, variable) {
    let val;
    if (variable === 0) {
      val = event.target.value.toLowerCase();
      console.log(val);
    } else {
      val = "";
    }
    const colsAmt = this.columns.length;
    const keys = Object.keys(this.filtrodatosCategoria[0]);
    this.categorias1 = this.filtrodatosCategoria.filter(function (item) {
      for (let i = 0; i < 6; i++) {
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          return true;
        }
      }
    });
  }

  filterDatatableCorreos(event) {
    let val;
    val = event.target.value.toLowerCase();
    const colsAmt = this.columns.length;
    const keys = Object.keys(this.filtrodatosCorreo[0]);
    this.clientes = this.filtrodatosCorreo.filter(function (item) {
      for (let i = 0; i < 6; i++) {
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          return true;
        }
      }
    });
  }

  filterDatatableBuzon(event, variable) {
    // let val;
    // if (variable === 0) {
    //   val = event.target.value.toLowerCase();
    //   console.log(val);
    // } else {
    //   val = "";
    // }
    // const colsAmt = this.columns.length;
    // const keys = Object.keys(this.filtrodatosBuzon[0]);
    // this.correoSS = this.filtrodatosBuzon.filter(function (item) {
    //   for (let i = 0; i < 13; i++) {
    //     if (
    //       item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
    //       !val
    //     ) {
    //       return true;
    //     }
    //   }
    // });
  }

  filterDatatablePlantilla(event, variable) {
    // let val;
    // if (variable === 0) {
    //   val = event.target.value.toLowerCase();
    //   console.log(val);
    // } else {
    //   val = "";
    // }
    // const colsAmt = this.columns.length;
    // const keys = Object.keys(this.filtrodatosPlantilla[0]);
    // this.plantillasS = this.filtrodatosPlantilla.filter(function (item) {
    //   for (let i = 0; i < 9; i++) {
    //     if (
    //       item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
    //       !val
    //     ) {
    //       return true;
    //     }
    //   }
    // });
  }

  transform(value) {
    return this.sanitizer.bypassSecurityTrustHtml(value);
  }

  transformarreglo(value) {
    var texto = this.sanitizer.bypassSecurityTrustHtml(value);
    var texto1 = this.stripHtml(
      texto["changingThisBreaksApplicationSecurity"]
    ).trim();
    var textofinal = this.truncate(texto1, 200, ".......");
    return textofinal;
  }

  truncate(string, length, delimiter) {
    delimiter = delimiter || "&hellip;";
    return string.length > length
      ? string.substr(0, length) + delimiter
      : string;
  }

  stripHtml(html) {
    // Crea un nuevo elemento div
    var temporalDivElement = document.createElement("div");
    // Establecer el contenido HTML con el dado
    temporalDivElement.innerHTML = html;
    // Recuperar la propiedad de texto del elemento (compatibilidad con varios navegadores)
    return temporalDivElement.textContent || temporalDivElement.innerText || "";
  }

  agregarDesti(correo, nombre, i) {
    this.destinatarios.push(correo);
    this.destinatario = this.destinatarios.toString();
    this.nombres.push(nombre);
    this.nombrecompleto = this.nombres.toString();
    this.posicionarreglo.push(i);
    console.log(this.nombrecompleto);
    console.log(this.destinatario);
    console.log(this.posicionarreglo);
  }

  quitarDesti(correo, nombre, j) {
    // this.destinatarios.splice(j, 1);
    // this.destinatario = this.destinatarios.toString();
    // this.nombres.splice(j, 1);
    // this.nombrecompleto = this.nombres.toString();

    var destinatiosarray = this.destinatarios;

    var destinatarioarrayfinal = destinatiosarray.filter(function (res) {
      return res !== correo;
    });

    this.destinatarios = destinatarioarrayfinal;

    this.destinatario = this.destinatarios.toString();

    var nombreclientes = this.nombres;

    var nombrecliente = nombreclientes.filter(function (res) {
      return res !== nombre;
    });

    this.nombres = nombrecliente;

    this.nombrecompleto = this.nombres.toString();

    var arreglo = this.posicionarreglo;

    var arreglofinal = arreglo.filter(function (res) {
      return res !== j;
    });

    this.posicionarreglo = arreglofinal;

    console.log(this.nombrecompleto);
    console.log(this.destinatario);
    console.log(this.destinatarios);
    console.log(this.nombres);
    console.log(this.posicionarreglo);
  }

  valorcheck(ob, correo, nombre, i) {
    console.log("checked: " + ob.checked);
    if (ob.checked) {
      this.agregarDesti(correo, nombre, i);
    } else {
      this.quitarDesti(correo, nombre, i);
    }
  }

  comprobarsiestanenarreglo(i) {
    return this.posicionarreglo.includes(i);
  }

  limpiarvariables() {
    this.cate = "";
    this.cate1 = "";
    this.cate2 = "";
    this.asunto1 = "";
    this.asunto2 = "";
    this.asunto = "";
    this.mensaje = "";
    this.mensaje1 = "";
    this.mensaje2 = "";
    this.nombrecompleto = "";
    this.destinatario = "";
    this.destinatarios = [];
    this.nombres = [];
    this.posicionarreglo = [];
    this.detalles = false;
    this.colorcategoria1 = "";
    // this.filterDatatableBuzon("", 1);
    // this.filterDatatablePlantilla("", 1);
    // this.filterDatatableCategoria("", 1);
  }

  colorcategoria(e) {
    this.formcategoria.controls["colorcategoria"].setValue(e);
    this.colorcategoria1 = e;
  }
}
