import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
  selector: "[appMyContainer]"
})
export class MyContainerDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}