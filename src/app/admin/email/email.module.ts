import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailRoutingModule } from './email-routing.module';
import { InboxComponent } from "./inbox/inbox.component";
import { MatMenuModule } from "@angular/material/menu";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSortModule } from "@angular/material/sort";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MaterialFileInputModule } from "ngx-material-file-input";
import { MatRadioModule } from '@angular/material/radio';
import { SearchPipe } from '../pipes/search.pipe';
// import { CKEditorModule } from 'ckeditor4-angular';
// import { MyckeditorComponent } from './myckeditor/myckeditor.component';
// import { DynamicHostDirective } from 'src/app/directive/dynamic-host.directive';
import { ColorPickerModule } from 'ngx-color-picker';
import { EmailEditorModule } from "angular-email-editor";
import { CrearEmailComponent } from './crear-email/crear-email.component';

@NgModule({
  declarations: [
    InboxComponent,
    SearchPipe,
    CrearEmailComponent,
  ],
  entryComponents: [],
  imports: [
    CommonModule,
    EmailRoutingModule,
    MatRadioModule,
    EmailRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatSortModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatSelectModule,
    MatMenuModule,
    MatCheckboxModule,
    MaterialFileInputModule,
    MatProgressSpinnerModule,
    NgxDatatableModule,
    ColorPickerModule,
    EmailEditorModule,
  ],
})
export class EmailModule {}
