import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/service/auth.service';
declare var JitsiMeetExternalAPI: any;

@Component({
  selector: 'app-videollamada',
  templateUrl: './videollamada.component.html',
  styleUrls: ['./videollamada.component.css']
})
export class VideollamadaComponent implements OnInit, AfterViewInit {

  domain: string = "meet.jit.si";
  options: any;
  api: any;
  user: any;
  // For Custom Controls
  isAudioMuted = false;
  isVideoMuted = false;

  idadministrador;
  nombreclinica = '';
  nombreadministrador: any;

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {

    var num = Math.floor((Math.random() * 100000) + 1);

    this.idadministrador = this.authService.currentUserValue.id;
    this.nombreclinica = this.authService.currentUserValue.Nombre_clinica + this.idadministrador +num;
    this.nombreadministrador = this.authService.currentUserValue.firstName + ' ' + this.authService.currentUserValue.lastName;
    this.user = {
        name: this.nombreadministrador // set your username
    }

    console.log(this.idadministrador, this.nombreadministrador, this.nombreclinica, this.user);

  }

  ngAfterViewInit(): void {
    this.options = {
        roomName: this.nombreclinica,
        width: 1000,
        height: 700,
        configOverwrite: { prejoinPageEnabled: false },
        interfaceConfigOverwrite: {
            // overwrite interface properties
        },
        parentNode: document.querySelector('#jitsi-iframe'),
        userInfo: {
            displayName: this.user.name
        }
    }

    this.api = new JitsiMeetExternalAPI(this.domain, this.options);

    this.api.addEventListeners({
        readyToClose: this.handleClose,
        participantLeft: this.handleParticipantLeft,
        participantJoined: this.handleParticipantJoined,
        videoConferenceJoined: this.handleVideoConferenceJoined,
        videoConferenceLeft: this.handleVideoConferenceLeft,
        audioMuteStatusChanged: this.handleMuteStatus,
        videoMuteStatusChanged: this.handleVideoStatus
    });
  }

  handleClose = () => {
    console.log("handleClose");
  }

  handleParticipantLeft = async (participant) => {
    console.log("handleParticipantLeft", participant); // { id: "2baa184e" }
    const data = await this.getParticipants();
  }

  handleParticipantJoined = async (participant) => {
    console.log("handleParticipantJoined", participant); // { id: "2baa184e", displayName: "Shanu Verma", formattedDisplayName: "Shanu Verma" }
    const data = await this.getParticipants();
  }

  handleVideoConferenceJoined = async (participant) => {
    console.log("handleVideoConferenceJoined", participant); // { roomName: "bwb-bfqi-vmh", id: "8c35a951", displayName: "Akash Verma", formattedDisplayName: "Akash Verma (me)"}
    const data = await this.getParticipants();
  }

  handleVideoConferenceLeft = () => {
    console.log("handleVideoConferenceLeft");
    this.router.navigate(['/admin/dashboard/main']);
  }

  handleMuteStatus = (audio) => {
    console.log("handleMuteStatus", audio); // { muted: true }
  }

  handleVideoStatus = (video) => {
    console.log("handleVideoStatus", video); // { muted: true }
  }

  getParticipants() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(this.api.getParticipantsInfo()); // get all participants
        }, 500)
    });
  }

  // custom events
  executeCommand(command: string) {
    this.api.executeCommand(command);;
    if(command == 'hangup') {
        this.router.navigate(['/admin/dashboard/main']);
        return;
    }

    if(command == 'toggleAudio') {
        this.isAudioMuted = !this.isAudioMuted;
    }

    if(command == 'toggleVideo') {
        this.isVideoMuted = !this.isVideoMuted;
    }
  }

}
