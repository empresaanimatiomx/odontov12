import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VideollamadaComponent } from './videollamada.component';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { HistorialComponent } from './historial/historial.component';
import { Page404Component } from 'src/app/authentication/page404/page404.component';
import { VideollamadaUsuarioComponent } from './videollamada-usuario/videollamada-usuario.component';

const routes: Routes = [
  {
    path: 'videollamada_general',
    component: VideollamadaComponent,
  },
  {
    path: 'solicitudes',
    component: SolicitudesComponent,
  },
  {
    path: 'historial',
    component: HistorialComponent,
  },
  {
    path: 'videollamada_usuario/:idU/:Ref/:idC',
    component: VideollamadaUsuarioComponent,
  },
  { path: '**', component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideollamadaRoutingModule { }
