import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import moment from "moment";
moment.locale("es");
import { Chart } from "chart.js";
const headers: any = new HttpHeaders({ "Content-Type": "application/json" });
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import Swal from "sweetalert2";
import { AuthService } from "src/app/core/service/auth.service";
import { ActivatedRoute, Router } from "@angular/router";

declare var JitsiMeetExternalAPI: any;

@Component({
  selector: 'app-videollamada-usuario',
  templateUrl: './videollamada-usuario.component.html',
  styleUrls: ['./videollamada-usuario.component.css']
})
export class VideollamadaUsuarioComponent implements OnInit {

  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  infopaciente: any = [];
  idpaciente: number;
  idClinica: number;
  yearselect = "";
  citaspacientetablafinalizadas: any = [];
  citaspaciente: any = [];
  chart: any;
  arreglodeyear: any = [];

  api: any;
  domain: string = "meet.jit.si";
  options: any;
  user: any;

  isAudioMuted = false;
  isVideoMuted = false;

  nombreclinica = '';
  nombreadministrador: any;
  referencia = '';
  idcita = '';

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.idpaciente = this.route.snapshot.params.idU;
    this.referencia = this.route.snapshot.params.Ref;
    this.idcita = this.route.snapshot.params.idC;
    this.nombreadministrador = this.authService.currentUserValue.firstName + ' ' + this.authService.currentUserValue.lastName;
    this.arreglodeyear = [];
    for (var i = 2020; i <= 2050; i++) {
      this.arreglodeyear.push(i);
    }
  }

  ngOnInit(): void {
    var num = Math.floor((Math.random() * 100000) + 1);
    this.informacionpaciente();
    this.datos();
    this.user = {
      name: this.nombreadministrador // set your username
    }
    this.cambiarestatus();
  }

  ngAfterViewInit(): void {
    this.options = {
        roomName: this.referencia,
        width: 1000,
        height: 700,
        configOverwrite: { prejoinPageEnabled: false },
        interfaceConfigOverwrite: {
            // overwrite interface properties
        },
        parentNode: document.querySelector('#jitsi-iframe'),
        userInfo: {
            displayName: this.user.name
        }
    }

    this.api = new JitsiMeetExternalAPI(this.domain, this.options);

    this.api.addEventListeners({
        readyToClose: this.handleClose,
        participantLeft: this.handleParticipantLeft,
        participantJoined: this.handleParticipantJoined,
        videoConferenceJoined: this.handleVideoConferenceJoined,
        videoConferenceLeft: this.handleVideoConferenceLeft,
        audioMuteStatusChanged: this.handleMuteStatus,
        videoMuteStatusChanged: this.handleVideoStatus
    });
  }

  cambiarestatus(){
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 26, idc: this.idcita, status: 1 };
    const URL: any = this.baseURL + "citas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta: any) => {
        console.log(respuesta);
      }
    );
  }

  //obtener informacion del paciente
  informacionpaciente() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 13, idPaciente: this.idpaciente };
    const URL: any = this.baseURL + "pacientes.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta: any) => {
        console.log(respuesta);
        this.infopaciente = respuesta;
      }
    );
  }

  datos() {
    if (this.yearselect) {
      this.yearselect = this.yearselect;
    } else {
      this.yearselect = String(new Date().getFullYear());
    }
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = {
      caso: 0,
      id_usuario: this.idpaciente,
      year: this.yearselect,
    };
    const URL: any = this.baseURL + "graficas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        if (respuesta) {
          this.citaspaciente = respuesta;
          this.obtenercitasfinalizadas();
        } else {
          this.citaspaciente = [];
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }

  obtenercitasfinalizadas() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 3, id_usuario: this.idpaciente };
    const URL: any = this.baseURL + "graficas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        if (respuesta.toString() !== "Vacio") {
          this.citaspacientetablafinalizadas = respuesta;
        } else {
          this.citaspacientetablafinalizadas = [];
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }


  executeCommand(command: string) {
    this.api.executeCommand(command);;
    if(command == 'hangup') {
      const headers: any = new HttpHeaders({
        "Content-Type": "application/json",
      });
      const options: any = { caso: 29, idc: this.idcita };
      const URL: any = this.baseURL + "citas.php";
      this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
        (respuesta: any) => {
          console.log(respuesta);
        }
      );
        this.router.navigate(['/admin/dashboard/main']);
        return;
    }

    if(command == 'toggleAudio') {
        this.isAudioMuted = !this.isAudioMuted;
    }

    if(command == 'toggleVideo') {
        this.isVideoMuted = !this.isVideoMuted;
    }
  }

  handleClose = () => {
    console.log("handleClose");
  }

  handleParticipantLeft = async (participant) => {
    console.log("handleParticipantLeft", participant); // { id: "2baa184e" }
    const data = await this.getParticipants();
  }

  handleParticipantJoined = async (participant) => {
    console.log("handleParticipantJoined", participant); // { id: "2baa184e", displayName: "Shanu Verma", formattedDisplayName: "Shanu Verma" }
    const data = await this.getParticipants();
  }

  handleVideoConferenceJoined = async (participant) => {
    console.log("handleVideoConferenceJoined", participant); // { roomName: "bwb-bfqi-vmh", id: "8c35a951", displayName: "Akash Verma", formattedDisplayName: "Akash Verma (me)"}
    const data = await this.getParticipants();
  }

  handleVideoConferenceLeft = () => {
    console.log("handleVideoConferenceLeft");
    this.router.navigate(['/admin/videollamada/historial']);
  }

  handleMuteStatus = (audio) => {
    console.log("handleMuteStatus", audio); // { muted: true }
  }

  handleVideoStatus = (video) => {
    console.log("handleVideoStatus", video); // { muted: true }
  }

  getParticipants() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(this.api.getParticipantsInfo()); // get all participants
        }, 500)
    });
  }

}
