import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideollamadaUsuarioComponent } from './videollamada-usuario.component';

describe('VideollamadaUsuarioComponent', () => {
  let component: VideollamadaUsuarioComponent;
  let fixture: ComponentFixture<VideollamadaUsuarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideollamadaUsuarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideollamadaUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
