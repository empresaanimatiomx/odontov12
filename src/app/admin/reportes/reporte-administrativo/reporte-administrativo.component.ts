import { Component, OnInit } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { AlertService } from "src/app/core/service/alert.service";
import { AuthService } from "src/app/core/service/auth.service";
import { HttpService } from "src/app/core/service/http.service";

@Component({
  selector: "app-reporte-administrativo",
  templateUrl: "./reporte-administrativo.component.html",
  styleUrls: [],
})
export class ReporteAdministrativoComponent implements OnInit {
  doctores: any = [];
  sucursales: any = [];
  reportes: any = [];
  idClinica;

  tipoReporte = "";
  fechaInicial;
  fechaFinal;
  sucursal;
  Total;
  Pagos;
  Pendiente;
  // doctor;

  constructor(
    private _snackBar: MatSnackBar,
    private authService: AuthService,
    private sweet: AlertService,
    private http: HttpService,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
  }

  ngOnInit(): void {
    if (this.idClinica < 0) {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
    // this.getDoctores(this.idClinica);
    this.getSucursales(this.idClinica);
  }

  selectReport() {
    switch (this.tipoReporte) {
      case "recaudacion":
        this.getReporteRecaudado(
          this.idClinica,
          this.formatDate(this.fechaInicial),
          this.formatDate(this.fechaFinal),
          this.sucursal
        );
        break;
      case "estadoCitas":
        this.getReportesCitas(
          this.idClinica,
          this.fechaInicial,
          this.fechaFinal,
          this.sucursal
        );
        break;
      case "motivos":
        this.getReportesCitas(
          this.idClinica,
          this.fechaInicial,
          this.fechaFinal,
          this.sucursal
        );
        break;
      case "presupuestos":
        this.getReportesPresupuestos(
          this.idClinica,
          this.fechaInicial,
          this.fechaFinal,
          this.sucursal
        );
        break;
      default:
        break;
    }
  }

  getReportesPresupuestos(
    idClinica: any,
    fechaInicial: any,
    fechaFinal: any,
    sucursal: any
  ) {
    if (
      fechaInicial === undefined ||
      fechaFinal === undefined ||
      sucursal === undefined
    ) {
      this.sweet.alert(
        "",
        "Debe seleccionar un valor en todos los filtros",
        "error"
      );
      return;
    }
    this.http
      .post("reportes.php", {
        caso: 7,
        idClinica,
        fechaInicial,
        fechaFinal,
        sucursal,
      })
      .subscribe({
        next: (reportes: any) => {
          this.reportes = reportes;
        },
        error: (e) => console.error(e),
      });
  }

  // getReportesMotivos(
  //   idClinica: any,
  //   fechaInicial: any,
  //   fechaFinal: any,
  //   sucursal: any
  // ) {}

  getReportesCitas(
    idClinica: any,
    fechaInicial: any,
    fechaFinal: any,
    sucursal: any
  ) {
    if (
      fechaInicial === undefined ||
      fechaFinal === undefined ||
      sucursal === undefined
    ) {
      this.sweet.alert(
        "",
        "Debe seleccionar un valor en todos los filtros",
        "error"
      );
      return;
    }
    this.http
      .post("reportes.php", {
        caso: 8,
        idClinica,
        fechaInicial,
        fechaFinal,
        sucursal,
      })
      .subscribe({
        next: (reportes: any) => {
          this.reportes = reportes;
        },
        error: (e) => console.error(e),
      });
  }

  getReporteRecaudado(idClinica, fechaInicial?, fechaFinal?, sucursal?) {
    if (
      fechaInicial === undefined ||
      fechaFinal === undefined ||
      sucursal === undefined
    ) {
      this.sweet.alert(
        "",
        "Debe seleccionar un valor en todos los filtros",
        "error"
      );
      return;
    }
    this.http
      .post("reportes.php", {
        caso: 7,
        idClinica,
        fechaInicial,
        fechaFinal,
        sucursal,
      })
      .subscribe({
        next: (reportes: any) => {
          if (reportes !== null) {
            this.Total = 0;
            this.Pagos = 0;
            this.Pendiente = 0;
            for (let i = 0; i < reportes.length; i++) {
              if (reportes[i].Total !== "") {
                this.Total += parseFloat(reportes[i].Total);
              }
              if (reportes[i].pago !== "") {
                this.Pagos += parseFloat(reportes[i].pago);
              }
              if (reportes[i].pago !== "") {
                this.Pendiente += parseFloat(reportes[i].pendiente);
              }
            }
          }
        },
        error: (e) => console.error(e),
      });
  }

  getSucursales(idclinica) {
    this.http.post("categorias.php", { caso: 0, idclinica }).subscribe({
      next: (sucursales: any) => {
        this.sucursales = sucursales;
      },
      error: (e) => console.error(e),
    });
  }

  limpiarFiltros() {
    this.fechaInicial = undefined;
    this.fechaFinal = undefined;
    this.sucursal = undefined;
  }

  formatDate(date) {
    if (date === undefined) {
      return date;
    }

    let d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  // getDoctores(idClinica) {
  //   this.http.post("pacientes.php", { caso: 5, idClinica }).subscribe({
  //     next: (doctores: any) => {
  //       this.doctores = doctores;
  //     },
  //     error: (e) => console.error(e),
  //   });
  // }
}
