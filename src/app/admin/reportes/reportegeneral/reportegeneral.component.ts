import { Component, OnInit } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { EChartsOption } from "echarts";
import { AlertService } from "src/app/core/service/alert.service";
import { AuthService } from "src/app/core/service/auth.service";
import { HttpService } from "src/app/core/service/http.service";
@Component({
  selector: "app-reportegeneral",
  templateUrl: "./reportegeneral.component.html",
  styleUrls: [],
})
export class ReportegeneralComponent implements OnInit {
  // line bar chart
  line_bar_chart: EChartsOption = {
    grid: {
      top: "6",
      right: "0",
      bottom: "17",
      left: "25",
    },
    xAxis: {
      data: ["Julio", "Agosto", "Septiembre", "Octubre", "Noviembre"],
      axisLine: {
        lineStyle: {
          color: "#eaeaea",
        },
      },
      axisLabel: {
        fontSize: 10,
        color: "#9aa0ac",
      },
    },
    tooltip: {
      show: true,
      showContent: true,
      alwaysShowContent: false,
      triggerOn: "mousemove",
      trigger: "axis",
    },
    yAxis: {
      splitLine: {
        lineStyle: {
          color: "#eaeaea",
        },
      },
      axisLine: {
        lineStyle: {
          color: "#eaeaea",
        },
      },
      axisLabel: {
        fontSize: 10,
        color: "#9aa0ac",
      },
    },
    color: ["#9f78ff", "#3FA7DC", "#F6A025"],
  };
  grafica = false;
  idClinica;

  constructor(
    private authService: AuthService,
    private _snackBar: MatSnackBar,
    private sweet: AlertService,
    private http: HttpService,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
  }

  ngOnInit(): void {
    if (this.idClinica < 0) {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
    this.getPacientes(this.idClinica);
  }

  getPacientes(idClinica) {
    this.http.post("reportes.php", { caso: 11, idClinica }).subscribe({
      next: (pacientes: any) => {
        this.getCitas(idClinica, pacientes);
      },
      error: (e) => console.error(e),
    });
  }

  getCitas(idClinica, pacientes) {
    this.http.post("reportes.php", { caso: 12, idClinica }).subscribe({
      next: (citas: any) => {
        this.getPresupuestos(idClinica, pacientes, citas);
      },
      error: (e) => console.error(e),
    });
  }

  getPresupuestos(idClinica, pacientes, citas) {
    this.http.post("reportes.php", { caso: 13, idClinica }).subscribe({
      next: (presupuestos: any) => {
        console.log(pacientes, citas, presupuestos);
        let pacientesArr = [];
        let citasArr = [];
        let presupuestosArr = [];
        if (pacientes) {
          switch (pacientes.length) {
            case 1:
              pacientesArr = [0, 0, 0, 0];
              break;
            case 2:
              pacientesArr = [0, 0, 0];
              break;
            case 3:
              pacientesArr = [0, 0];
              break;
            case 4:
              pacientesArr = [0];
              break;
            case 5:
              pacientesArr = [];
              break;
            default:
              pacientesArr = [];
              break;
          }
          for (let i = 0; i < pacientes.length; i++) {
            pacientesArr.push(parseInt(pacientes[i].total));
          }
        }
        if (citas) {
          switch (citas.length) {
            case 1:
              citasArr = [0, 0, 0, 0];
              break;
            case 2:
              citasArr = [0, 0, 0];
              break;
            case 3:
              citasArr = [0, 0];
              break;
            case 4:
              citasArr = [0];
              break;
            case 5:
              citasArr = [];
              break;
            default:
              citasArr = [];
              break;
          }
          for (let i = 0; i < citas.length; i++) {
            citasArr.push(parseInt(citas[i].total));
          }
        }
        if (presupuestos) {
          switch (presupuestos.length) {
            case 1:
              presupuestosArr = [0, 0, 0, 0];
              break;
            case 2:
              presupuestosArr = [0, 0, 0];
              break;
            case 3:
              presupuestosArr = [0, 0];
              break;
            case 4:
              presupuestosArr = [0];
              break;
            case 5:
              presupuestosArr = [];
              break;
            default:
              presupuestosArr = [];
              break;
          }
          for (let i = 0; i < presupuestos.length; i++) {
            presupuestosArr.push(parseInt(presupuestos[i].total));
          }
        }

        this.line_bar_chart = {
          ...this.line_bar_chart,
          series: [
            {
              name: "pacientes",
              type: "bar",
              data: pacientesArr,
            },
            {
              name: "Citas",
              type: "bar",
              data: citasArr,
            },
            {
              name: "Presupuestos",
              type: "line",
              smooth: true,
              lineStyle: {
                width: 3,
                shadowColor: "rgba(0,0,0,0.4)",
                shadowBlur: 10,
                shadowOffsetY: 10,
              },
              data: presupuestosArr,
              symbolSize: 10,
            },
          ],
        };
        this.grafica = true;
      },
      error: (e) => console.error(e),
    });
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }
}