import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Page404Component } from 'src/app/authentication/page404/page404.component';
import { DetalleReporteComponent } from './detalle-reporte/detalle-reporte.component';
import { ReporteAdministrativoComponent } from './reporte-administrativo/reporte-administrativo.component';
import { ReportePacienteComponent } from './reporte-paciente/reporte-paciente.component';
import { ReportegeneralComponent } from './reportegeneral/reportegeneral.component';

const routes: Routes = [
    {
        path: 'resumen-reporte',
        component: ReportegeneralComponent,
      },
      {
        path: 'reporte-administrativo',
        component: ReporteAdministrativoComponent,
      },
      {
        path: 'reporte-paciente',
        component: ReportePacienteComponent,
      },
      {
        path: 'detalle-reporte',
        component: DetalleReporteComponent,
      },
      {
        path: 'detalle-reporte/:idc/:ids/:idd/:m',
        component: DetalleReporteComponent,
      },

      { path: '**', component: Page404Component },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportesRoutingModule {}