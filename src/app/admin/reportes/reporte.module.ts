import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";


import { MatMenuModule } from "@angular/material/menu";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSortModule } from "@angular/material/sort";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MaterialFileInputModule } from "ngx-material-file-input";
import { ReportesRoutingModule } from "./reporte-routing.module";
import { NgxEchartsModule } from "ngx-echarts";


import { ReportegeneralComponent } from "./reportegeneral/reportegeneral.component";
import { ReporteAdministrativoComponent } from "./reporte-administrativo/reporte-administrativo.component";
import { ReportePacienteComponent } from "./reporte-paciente/reporte-paciente.component";
import { DetalleReporteComponent } from './detalle-reporte/detalle-reporte.component';
import { NgApexchartsModule } from "ng-apexcharts";

@NgModule({
  declarations: [ReportegeneralComponent, ReporteAdministrativoComponent, ReportePacienteComponent, DetalleReporteComponent],
  imports: [
    CommonModule,
    ReportesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatSortModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatSelectModule,
    MatMenuModule,
    MatCheckboxModule,
    MaterialFileInputModule,
    MatProgressSpinnerModule,
    NgxDatatableModule,
    NgApexchartsModule,
    NgxEchartsModule.forRoot({
      echarts: () => import("echarts"),
    }),
  ],
  providers: [],
})
export class ReportesModule {}