import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from "@angular/common/http";
import { AuthService } from "src/app/core/service/auth.service";
import { Router } from "@angular/router";
import moment from "moment";
import { AlertService } from "src/app/core/service/alert.service";
import { HttpService } from "src/app/core/service/http.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import * as xlsx from 'xlsx';
const headers: any = new HttpHeaders({ "Content-Type": "application/json" });
@Component({
  selector: "app-reporte-paciente",
  templateUrl: "./reporte-paciente.component.html",
  styleUrls: [],
})
export class ReportePacienteComponent implements OnInit {
  @ViewChild("epltable", { static: false }) epltable: ElementRef;
  doctores: any = [];
  sucursales: any = [];
  pacientes: any = [];
  idClinica: number;
  cuenta: boolean = false;
  citas: boolean = false;
  tipoReporte;

  constructor(
    private authService: AuthService,
    private sweet: AlertService,
    private http: HttpService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
  }

  ngOnInit(): void {
    if (this.idClinica < 0) {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
    this.GetDoctores(this.idClinica);
    this.GetSuc();
  }

  selectReport() {
    switch (this.tipoReporte) {
      case "1":
        this.listadoPacientes();
        break;
      case "2":
        this.pacientesAtendidos(0, this.idClinica);
        break;
      case "3":
        break;
      case "4":
        break;
      case "5":
        this.pacientesAtendidos(1, this.idClinica);
        break;
      case "6":
        break;
      case "7":
        this.pacientesAtendidos(2, this.idClinica);
        break;
      case "8":
        this.pacientesAtendidos(3, this.idClinica);
        break;
      case "9":
        break;
      case "10":
        break;
      case "11":
        this.pacientesAtendidos(4, this.idClinica, true);
        break;
      case "12":
        break;
      case "13":
        this.pacientesAtendidos(5, this.idClinica);
        break;
      case "14":
        this.pacientesAtendidos(6, this.idClinica, false, true);
        break;
      default:
        this.sweet.alert("", "No seleciono un tipo de reporte valido", "error");
        break;
    }
  }

  pacientesAtendidos(caso, idClinica, cuenta = false, citas = false) {
    this.http.post("reportes.php", { caso, idClinica }).subscribe({
      next: (pacientes: any) => {
        this.pacientes = pacientes;
        this.cuenta = cuenta;
        this.citas = citas;
        console.log(this.pacientes);
      },
      error: (e) => console.error(e),
    });
  }

  listadoPacientes() {
    this.http
      .post("pacientes.php", { caso: 0, idClinica: this.idClinica })
      .subscribe({
        next: (pacientes: any) => {
          this.pacientes = pacientes;
        },
        error: (e) => console.error(e),
      });
  }

  GetDoctores(idclinica) {
    this.http
      .post("pacientes.php", { caso: 5, idClinica: idclinica })
      .subscribe({
        next: (doctores: any) => {
          this.doctores = doctores;
        },
        error: (e) => console.error(e),
      });
  }

  GetSuc() {
    this.http
      .post("categorias.php", { caso: 0, idclinica: this.idClinica })
      .subscribe({
        next: (sucursales: any) => {
          this.sucursales = sucursales;
        },
        error: (e) => console.error(e),
      });
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  exportToExcel() {
    const ws: xlsx.WorkSheet = xlsx.utils.table_to_sheet(
      this.epltable.nativeElement
    );
    const wb: xlsx.WorkBook = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    xlsx.writeFile(wb, "ListadoPacientes.xlsx");
  }

  // GenReport() {
  //   let fechaI;
  //   let fechaF;
  //   fechaI = moment(this.fechai).format("YYYY-MM-DD");
  //   fechaF = moment(this.fechaf).format("YYYY-MM-DD");
  //   console.log(fechaI + " es la fecha inicial");
  //   console.log(fechaF + " es la fecha fnal");
  //   const options: any = { caso: 0, ids: this.sucu, fi: fechaI, ff: fechaF };
  //   const URL: any = this.baseURL + "reportes.php";
  //   this.http
  //     .post(URL, JSON.stringify(options), headers)
  //     .subscribe((respuesta) => {
  //       this.citasAgendadas = respuesta;
  //     });
  // }
}
