import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { Router, ActivatedRoute } from '@angular/router';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexTitleSubtitle,
  ApexDataLabels,
  ApexPlotOptions,
  ApexYAxis,
  ApexLegend,
  ApexStroke,
  ApexFill,
  ApexTooltip
} from 'ng-apexcharts';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/core/service/auth.service';
export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  xaxis: ApexXAxis;
  fill: ApexFill;
  tooltip: ApexTooltip;
  stroke: ApexStroke;
  legend: ApexLegend;
};
const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
declare const ApexCharts: any;

@Component({
  selector: "app-detalle-reporte",
  templateUrl: "./detalle-reporte.component.html",
  styles: [],
})
export class DetalleReporteComponent implements OnInit {
  private baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  @ViewChild("roleTemplate", { static: true }) roleTemplate: TemplateRef<any>;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  caso = "";
  ids = "";
  iddoc = "";
  mes = "";
  data: any = [];
  rows = [];
  selectedRowData: selectRowInterface;
  newUserImg = "assets/images/user_default.png";
  detalle: any = [];
  filteredData: any = [];
  editForm: FormGroup;
  register: FormGroup;
  selectedOption: string;
  columns = [
    { name: "idRecetas" },
    { name: "Fecha" },
    { name: "Sucursal" },
    { name: "Nº de Citas Agendadas" },
    { name: "Nº de Citas Atendidas" },
    { name: "% Atendidas vs. Agendadas" },
  ];

  public barChartOptions: Partial<ChartOptions>;
  public areaChartOptions: any;
  idClinica;

  constructor(
    private authService: AuthService,
    private http: HttpClient,
    public route: ActivatedRoute,
    private router: Router,
    private _snackBar: MatSnackBar,
    private fb: FormBuilder,
    private httpClient: HttpClient
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.route.params.subscribe((parametros) => {
      this.caso = parametros["idc"];
      this.ids = parametros["ids"];
      this.iddoc = parametros["idd"];
      this.mes = parametros["m"];
    });
  }

  ngOnInit(): void {
    if (this.idClinica < 0) {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
    this.chart1();
    this.chart6();
    this.GenReport2();
  }

  fetch(cb) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 0, idPaciente: 2 };
    this.httpClient
      .post(`${this.baseURL}recetas.php`, JSON.stringify(options), headers)
      .subscribe((data) => {
        this.data = data;
        this.filteredData = data;
        cb(data);
      });
  }

  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  private chart1() {
    this.barChartOptions = {
      series: [
        {
          name: "Net Profit",
          data: [44, 55, 57, 56, 61, 58, 63, 60, 66],
        },
        {
          name: "Revenue",
          data: [76, 85, 101, 98, 87, 105, 91, 114, 94],
        },
        {
          name: "Free Cash Flow",
          data: [35, 41, 36, 26, 45, 48, 52, 53, 41],
        },
      ],
      chart: {
        type: "bar",
        height: 350,
        foreColor: "#9aa0ac",
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: "55%",
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        show: true,
        width: 2,
        colors: ["transparent"],
      },
      xaxis: {
        categories: [
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
        ],
        labels: {
          style: {
            colors: "#9aa0ac",
          },
        },
      },
      yaxis: {
        title: {
          text: "$ (thousands)",
        },
      },
      fill: {
        opacity: 1,
      },
      tooltip: {
        theme: "dark",
        marker: {
          show: true,
        },
        x: {
          show: true,
        },
      },
    };
  }

  private chart6() {
    this.areaChartOptions = {
      chart: {
        height: 350,
        type: "area",
        foreColor: "#9aa0ac",
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: "smooth",
      },
      series: [
        {
          name: "series1",
          data: [31, 40, 28, 51, 42, 109, 100],
        },
        {
          name: "series2",
          data: [11, 32, 45, 32, 34, 52, 41],
        },
      ],

      xaxis: {
        type: "datetime",
        categories: [
          "2018-09-19T00:00:00",
          "2018-09-19T01:30:00",
          "2018-09-19T02:30:00",
          "2018-09-19T03:30:00",
          "2018-09-19T04:30:00",
          "2018-09-19T05:30:00",
          "2018-09-19T06:30:00",
        ],
        labels: {
          style: {
            colors: "#9aa0ac",
          },
        },
      },
      yaxis: {
        labels: {
          style: {
            color: "#9aa0ac",
          },
        },
      },
      tooltip: {
        x: {
          format: "dd/MM/yy HH:mm",
        },
      },
    };
  }
  GenReport2() {
    if (this.mes === "3") {
      const options: any = { caso: this.caso, ids: this.ids, doc: this.iddoc };
      const URL: any = this.baseURL + "reportes.php";
      this.http
        .post(URL, JSON.stringify(options), headers)
        .subscribe((respuesta) => {
          this.detalle = respuesta;
        });
    }
    if (this.mes === "6") {
      const options: any = { caso: 1.1, ids: this.ids, doc: this.iddoc };
      const URL: any = this.baseURL + "reportes.php";
      this.http
        .post(URL, JSON.stringify(options), headers)
        .subscribe((respuesta) => {
          this.detalle = respuesta;
        });
    }
    if (this.mes === "12") {
      const options: any = { caso: 1.2, ids: this.ids, doc: this.iddoc };
      const URL: any = this.baseURL + "reportes.php";
      this.http
        .post(URL, JSON.stringify(options), headers)
        .subscribe((respuesta) => {
          this.detalle = respuesta;
        });
    }
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }
}
export interface selectRowInterface {
  idRecetas: number;
  Fecha: String;
  Descripcion: String;
}