import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Chart } from "chart.js";
import { HttpService } from "src/app/core/service/http.service";
import { AuthService } from "../../core/service/auth.service";

@Component({
  selector: "app-perfil",
  templateUrl: "./perfil.component.html",
  styles: [],
})
export class PerfilComponent implements OnInit {
  @ViewChild("nuevacita", { static: false }) nuevacita: ElementRef;
  idpaciente;
  infopaciente: any = [];
  public areaChartOptions: any;
  arreglodeyear: any = [];
  yearselect = "";
  citaspaciente: any = [];
  citaspacientetablaproximas: any = [];
  citaspacientetablaultimas: any = [];
  citaspacientetablafinalizadas: any = [];
  colorScheme = {
    domain: ["#9370DB", "#87CEFA", "#FA8072", "#FF7F50", "#90EE90", "#9370DB"],
  };
  public single = [
    {
      name: "China",
      value: 2243772,
    },
    {
      name: "USA",
      value: 1826000,
    },
    {
      name: "India",
      value: 1173657,
    },
    {
      name: "Japan",
      value: 857363,
    },
    {
      name: "Germany",
      value: 496750,
    },
    {
      name: "France",
      value: 204617,
    },
  ];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  showYAxisLabel = true;
  legendPosition = "right";
  hbarxAxisLabel = "Sales";
  hbaryAxisLabel = "Country";
  chart: any;
  idCita = "";
  valor = "";
  valorboolean = false;
  infocita: any = [];
  nombrepaciente = "";
  nombredoctor = "";
  nombremotivo = "";
  nombreservicio = "";
  nombrepaciente1 = "";
  nombredoctor1 = "";
  nombremotivo1 = "";
  nombreservicio1 = "";
  horariocita = "";
  motivocita = "";
  fechacita = "";
  serviciocita = "";
  estatuscita = "";
  observacionescita = "";
  motivootro = false;
  pacientes: any = [];
  doctores: any = [];
  idClinica: number;
  servicios: any = [];
  convenios: any = [];
  motivosconsulta: any = [];
  filterDoc: any = [];
  sms;

  constructor(
    private http: HttpService,
    private authService: AuthService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.idpaciente = id;
    this.arreglodeyear = [];
    for (var i = 2022; i <= 2050; i++) {
      this.arreglodeyear.push(i);
    }
  }

  ngOnInit() {
    this.informacionpaciente();
    this.datos();
    this.GetPacientes(this.idClinica);
    this.GetDoctores(this.idClinica);
    this.GetServicios(this.idClinica);
    this.obtenermotivos(this.idClinica);
  }

  //obtener informacion del paciente
  informacionpaciente() {
    this.http
      .post("pacientes.php", { caso: 13, idPaciente: this.idpaciente })
      .subscribe({
        next: (respuesta: any) => {
          this.infopaciente = respuesta;
        },
        error: (err) => console.log(err)
      });
  }

  grafica() {
    this.chart = new Chart("canvas", {
      type: "horizontalBar",
      data: {
        labels: [
          "ENE",
          "FEB",
          "MAR",
          "ABR",
          "MAY",
          "JUN",
          "JUL",
          "AGO",
          "SEP",
          "OCT",
          "NOV",
          "DIC",
        ],
        datasets: [
          {
            label: "",
            data: this.citaspaciente,
            backgroundColor: [
              "rgba(111, 172, 83, 1)",
              "rgba(247, 224, 13, 1)",
              "rgba(222, 145, 47, 1)",
              "rgba(55, 167, 156, 1)",
              "rgba(255, 191, 166, 1)",
              "rgba(102, 168, 222, 1)",
              "rgba(203, 34, 40, 1)",
              "rgba(63, 63, 191, 1)",
              "rgba(221, 115, 207, .67)",
              "rgba(159, 69, 211, 1)",
              "rgba(51, 233, 167, 1)",
              "rgba(222, 16, 27, 0.38)",
            ],
            borderColor: [
              "rgba(111, 172, 83, 1)",
              "rgba(247, 224, 13, 1)",
              "rgba(222, 145, 47, 1)",
              "rgba(55, 167, 156, 1)",
              "rgba(255, 191, 166, 1)",
              "rgba(102, 168, 222, 1)",
              "rgba(203, 34, 40, 1)",
              "rgba(63, 63, 191, 1)",
              "rgba(221, 115, 207, .67)",
              "rgba(159, 69, 211, 1)",
              "rgba(51, 233, 167, 1)",
              "rgba(222, 16, 27, 0.38)",
            ],
            borderWidth: 1,
          },
        ],
      },
      options: {
        title: {
          display: false,
        },
        legend: {
          display: false,
        },
        scales: {
          yAxes: [{}],
          xAxes: [
            {
              gridLines: {
                zeroLineWidth: 0,
                zeroLineColor: "rgba(166, 197, 212, 1)",
              },
            },
          ],
        },
      },
    });
  }

  datos() {
    if (this.yearselect) {
      this.yearselect = this.yearselect;
    } else {
      this.yearselect = String(new Date().getFullYear());
    }
    const options: any = {
      caso: 0,
      id_usuario: this.idpaciente,
      year: this.yearselect,
    };
    this.http.post("graficas.php", options).subscribe({
      next: (respuesta) => {
        if (respuesta) {
          this.citaspaciente = respuesta;
          //this.graficacita();
          this.grafica();
          this.obtenercitasproximas();
          this.obtenercitasultimas();
          this.obtenercitasfinalizadas();
        } else {
          this.citaspaciente = [];
          //this.graficacita();
          this.grafica();
        }
      },
      error: (err) => console.log(err),
    });
  }
  obtenercitasproximas() {
    this.http
      .post("graficas.php", { caso: 1, id_usuario: this.idpaciente })
      .subscribe({
        next: (respuesta) => {
          if (respuesta.toString() !== "Vacio") {
            this.citaspacientetablaproximas = respuesta;
          } else {
            this.citaspacientetablaproximas = [];
          }
        },
        error: (err) => console.log(err),
      });
  }
  obtenercitasultimas() {
    this.http
      .post("graficas.php", { caso: 2, id_usuario: this.idpaciente })
      .subscribe({
        next: (respuesta) => {
          if (respuesta.toString() !== "Vacio") {
            this.citaspacientetablaultimas = respuesta;
          } else {
            this.citaspacientetablaultimas = [];
          }
        },
        error: (err) => console.log(err),
      });
  }
  obtenercitasfinalizadas() {
    this.http.post("graficas.php", { caso: 3, id_usuario: this.idpaciente }).subscribe({
      next: (respuesta) => {
        if (respuesta.toString() !== "Vacio") {
          this.citaspacientetablafinalizadas = respuesta;
        } else {
          this.citaspacientetablafinalizadas = [];
        }
      },
      error: (err) => console.log(err),
    });
  }

  obtenernombrepaciente(option: any) {
    this.nombrepaciente = option.source.triggerValue;
  }

  obtenernombredoctor(option: any) {
    this.nombredoctor = option.source.triggerValue;
  }

  obtenernombremotivo(option: any) {
    this.nombremotivo = option.source.triggerValue;
  }

  obtenernombreservicio(option: any) {
    this.nombreservicio = option.source.triggerValue;
  }

  //obtiene el listado de pacientes
  GetPacientes(idclinica) {
    this.http.post("citas.php", { caso: 10, idC: idclinica }).subscribe({
      next: (respuesta) => {
        if (respuesta) {
          this.pacientes = respuesta;
        } else {
          this.pacientes = [];
        }
      },
      error: (err) => console.log(err),
    });
  }

  //obtiene el listado de doctores

  GetDoctores(idclinica) {
    this.http
      .post("pacientes.php", { caso: 5, idClinica: idclinica })
      .subscribe({
        next: (respuesta) => {
          this.doctores = respuesta;
        },
        error: (err) => console.log(err),
      });
  }

  //obtiene el listado de servicios
  GetServicios(idclinica) {
    this.http
      .post("servicios.php", { caso: 0, idClinica: idclinica })
      .subscribe({
        next: (respuesta) => {
          this.servicios = respuesta;
        },
        error: (err) => console.log(err),
      });
  }

  // metodo para obtener los doctores de la clinica
  obtenerDocs(idclinica) {
    this.http.post("citas.php", { caso: 0.1, idC: idclinica }).subscribe({
      next: (respuesta) => {
        if (respuesta) {
          this.filterDoc = respuesta;
        } else {
          this.filterDoc = [];
        }
      },
      error: (err) => console.log(err),
    });
  }

  // metodo para obtener los convenios
  obtenermotivos(idclinica) {
    this.http.post("citas.php", { caso: 11, idC: idclinica }).subscribe({
      next: (respuesta) => {
        if (respuesta) {
          this.motivosconsulta = respuesta;
        } else {
          this.motivosconsulta = [];
        }
      },
      error: (err) => console.log(err),
    });
  }

  // metodo para obtener los convenios
  obtenerconvenios(idclinica) {
    this.http
      .post("convenios.php", { caso: 8, idclinica: idclinica })
      .subscribe({
        next: (respuesta) => {
          this.convenios = respuesta;
        },
        error: (err) => console.log(err),
      });
  }

}
