import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Clipboard } from "@angular/cdk/clipboard";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthService } from "../../core/service/auth.service";
import { HttpService } from "../../core/service/http.service";
import { AlertService } from "../../core/service/alert.service";

@Component({
  selector: "app-documentacion",
  templateUrl: "./documentacion.component.html",
  styles: [],
})
export class DocumentacionComponent implements OnInit {
  arreglodocumentos: any = [];
  idClinica: number;
  idPaciente: number;
  ifNumero = false;
  numero = "";
  datos = "";

  constructor(
    private router: Router,
    private authService: AuthService,
    private http: HttpService,
    private sweet: AlertService,
    private clipboard: Clipboard,
    private modalService: NgbModal
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.idPaciente = id;
  }
  ngOnInit() {
    this.obtenerdocumentos();
  }

  obtenerdocumentos(): void {
    this.http
      .post("documentos.php", { caso: 9, idPaciente: this.idPaciente })
      .subscribe({
        next: (respuesta) => {
          this.arreglodocumentos = respuesta;
        },
        error: (err) => console.log(err),
      });
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  cargarArchivo(event) {
    if (event.target.files.length) {
      const file = event.target.files[0];
      const nombreImg = `${new Date().getTime()}${file.name}`;
      const img = new FormData();
      img.append("documentoPropio", file, nombreImg);
      this.subirArchivo(img, nombreImg);
    }
  }

  subirArchivo(img, nombreImg) {
    this.sweet.subiendo("la imagen");
    this.http.postFile("uploadarchivo3.php", img).subscribe({
      next: (response: any) => {
        if (response.msj === "Archivo subido") {
          this.guardarArchivoBD(nombreImg);
        } else if (response.msj === "No se recibio ningun archivo") {
          this.sweet.alert(
            "",
            "No es un formato valido, intente de nuevo por favor",
            "error"
          );
        } else {
          this.sweet.alert(
            "",
            "Ocurrio un error, intente de nuevo por favor",
            "error"
          );
        }
      },
      error: (err) => console.log(err),
    });
  }

  guardarArchivoBD(named) {
    const options: any = {
      caso: 6,
      named,
      idc: this.idClinica,
      idPaciente: this.idPaciente,
    };
    this.http.post("documentos.php", options).subscribe((res) => {
      if (res.toString() === "Se inserto") {
        this.obtenerdocumentos();
        this.sweet.alert("", "Archivo subido correctamente", "success");
      } else {
        this.sweet.alert(
          "",
          "Ocurrio un error, intente de nuevo por favor",
          "error"
        );
      }
    });
  }

  eliminardocumento(nombre, id) {
    this.http
      .post("documentos.php", { caso: 5, named: nombre, idd: id })
      .subscribe((res) => {
        if (res.toString() === "Se elimino") {
          this.obtenerdocumentos();
          this.sweet.alert("", "Archivo eliminado correctamente", "success");
        } else {
          this.sweet.alert(
            "",
            "Ocurrio un error, intente de nuevo por favor",
            "error"
          );
        }
      });
  }

  saberquetipodearchivoes(nombre) {
    if (nombre.includes(".doc") || nombre.includes(".docx")) {
      return "word";
    } else if (
      nombre.includes(".xml") ||
      nombre.includes(".xls") ||
      nombre.includes(".xlsx")
    ) {
      return "excel";
    } else if (nombre.includes(".pptx")) {
      return "powerpoint";
    } else if (nombre.includes(".pdf")) {
      return "pdf";
    } else {
      return "archivo";
    }
  }

  onToggle(e) {
    this.ifNumero = e.checked;
  }

  compartir(modal, info?) {
    this.datos = `https://projectsbyanimatiomx.com/phps/odonto/fotospaciente/${info.Nombre}`;
    this.clipboard.copy(this.datos);
    this.modalService.open(modal, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
    });
  }
}
