import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FacturasEnviadasComponent } from './facturas-enviadas.component';

const routes: Routes = [
  {
    path: "",
    component: FacturasEnviadasComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FacturasEnviadasRoutingModule { }
