import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FacturasEnviadasRoutingModule } from './facturas-enviadas-routing.module';
import { FacturasEnviadasComponent } from './facturas-enviadas.component';


@NgModule({
  declarations: [FacturasEnviadasComponent],
  imports: [
    CommonModule,
    FacturasEnviadasRoutingModule
  ]
})
export class FacturasEnviadasModule { }
