import { Component, OnInit, ViewChild } from "@angular/core";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { HttpService } from "../../core/service/http.service";

@Component({
  selector: "app-examenes-laboratorio",
  templateUrl: "./examenes-laboratorio.component.html",
  styles: [],
})
export class ExamenesLaboratorioComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  data = [];
  filteredData = [];
  laboratorios = [];
  doctores = [];
  sucursales = [];
  datosPaciente = [];
  idPaciente;
  idClinica;

  constructor(private http: HttpService) {
    const { id, idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.idPaciente = id;
    this.idClinica = idclinica;
  }

  ngOnInit() {
    this.getOrdenes();
    this.getLaboratorio();
    this.GetDoctores();
    this.GetSucursales();
    this.getDatosPaciente();
  }

  getOrdenes() {
    this.http
      .post("laboratorio.php", { caso: 12, idPaciente: this.idPaciente })
      .subscribe({
        next: (ordenes: any) => {
          this.data = ordenes;
        },
        error: (err) => console.log(err),
      });
  }

  getLaboratorio() {
    this.http
      .post("laboratorio.php", { caso: 0, idClinica: this.idClinica })
      .subscribe({
        next: (respuesta: any) => {
          this.laboratorios = respuesta;
        },
        error: (err) => console.log(err),
      });
  }

  GetDoctores() {
    this.http
      .post("pacientes.php", { caso: 5, idClinica: this.idClinica })
      .subscribe({
        next: (respuesta: any) => {
          this.doctores = respuesta;
        },
        error: (err) => console.log(err),
      });
  }

  GetSucursales() {
    this.http
      .post("recetas.php", { caso: 4, idclinica: this.idClinica })
      .subscribe({
        next: (respuesta: any) => {
          this.sucursales = respuesta;
        },
        error: (err) => console.log(err),
      });
  }

  getDatosPaciente() {
    this.http
      .post("recetas.php", { caso: 3, idPaciente: this.idPaciente })
      .subscribe({
        next: (respuesta: any) => {
          this.datosPaciente = respuesta[0];
        },
        error: (err) => console.log(err),
      });
  }

  FirstUppercaseLetter(idname) {
    const texto = (<HTMLInputElement>document.getElementById(idname)).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      (<HTMLInputElement>document.getElementById(idname)).value = textofinal;
    }
  }

  filterDatatable(event) {
    if (event.target.value === "") {
      this.data = this.filteredData;
    }
    this.data = this.http.filter(event, this.filteredData);
    this.table.offset = 0;
  }
}