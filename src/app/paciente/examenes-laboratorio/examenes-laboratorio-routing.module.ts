import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExamenesLaboratorioComponent } from './examenes-laboratorio.component';

const routes: Routes = [
  {
    path: "",
    component: ExamenesLaboratorioComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamenesLaboratorioRoutingModule { }
