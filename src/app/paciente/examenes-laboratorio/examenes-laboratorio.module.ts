import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExamenesLaboratorioRoutingModule } from './examenes-laboratorio-routing.module';
import { ExamenesLaboratorioComponent } from './examenes-laboratorio.component';
import { MaterialModule } from 'src/app/shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';


@NgModule({
  declarations: [ExamenesLaboratorioComponent],
  imports: [
    CommonModule,
    ExamenesLaboratorioRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    GooglePlaceModule
  ]
})
export class ExamenesLaboratorioModule { }
