import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanesTratamientoRoutingModule } from './planes-tratamiento-routing.module';
import { PlanesTratamientoComponent } from './planes-tratamiento.component';
import { MaterialModule } from 'src/app/shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgApexchartsModule } from 'ng-apexcharts';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';


@NgModule({
  declarations: [PlanesTratamientoComponent],
  imports: [
    CommonModule,
    PlanesTratamientoRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxChartsModule,
    NgApexchartsModule,
    GooglePlaceModule,
  ],
})
export class PlanesTratamientoModule {}
