import { Component, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import Swal from "sweetalert2";
import * as shape from "d3-shape";
import { AuthService } from "src/app/core/service/auth.service";
import { AlertService } from "src/app/core/service/alert.service";

const headers: any = new HttpHeaders({ "Content-Type": "application/json" });

@Component({
  selector: "app-planes-tratamiento",
  templateUrl: "./planes-tratamiento.component.html",
  styles: [],
})
export class PlanesTratamientoComponent implements OnInit {
  private baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  totalUsuario: any;
  totalCompletado: any;
  totalPagado: any;
  completadoNoPagado: any;
  presupuestadoNoPagado: any;
  Presupuestos: any = [];
  PresupuestosServ: any = [];
  PresupuestosServid: any = [];
  editForm: FormGroup;
  register: FormGroup;
  cheque: boolean = false;
  plantilla: boolean = false;
  tarjeta: boolean = false;
  pagare: boolean = false;
  idPaciente;
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  showYAxisLabel = false;
  legendPosition = "right";
  timeline = true;
  colorScheme = {
    domain: ["#9370DB", "#87CEFA", "#FA8072"],
  };
  showLabels = true;
  public single = [
    {
      name: "Total",
      value: 500,
    },
    {
      name: "Total Completado",
      value: 500,
    },
    {
      name: "Abonos",
      value: 500,
    },
  ];
  public multi = [
    {
      name: "China",
      series: [
        {
          name: "2010",
          value: 500,
        },
        {
          name: "2011",
          value: 500,
        },
      ],
    },
    {
      name: "USA",
      series: [
        {
          name: "2010",
          value: 500,
        },
        {
          name: "2011",
          value: 500,
        },
      ],
    },
    {
      name: "India",
      series: [
        {
          name: "2010",
          value: 500,
        },
        {
          name: "2011",
          value: 520,
        },
      ],
    },
  ];
  vbarxAxisLabel = "Pagos";
  vbaryAxisLabel = "Sales";
  hbarxAxisLabel = "Sales";
  hbaryAxisLabel = "Pagos";
  autoScale = true;
  linexAxisLabel = "Year";
  lineyAxisLabel = "Sales";
  areaxAxisLabel = "Year";
  areayAxisLabel = "Sales";
  shapeChartCurve = shape.curveBasis;
  imagenclinicaPrint = "";

  constructor(
    private sweet: AlertService,
    private authService: AuthService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private httpClient: HttpClient
  ) {
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.idPaciente = id;
    this.editForm = this.fb.group({
      idPlanTratamiento: new FormControl(),
      Fecha: new FormControl(),
      Procedimiento: new FormControl(),
      Doctor: new FormControl(),
      Subtotal: new FormControl(),
      Total: new FormControl(),
    });
  }
  ngOnInit() {
    this.getTotalUsuario();
    this.getTotalCompletado();
    this.getTotalPagado();
    this.getCompletadoNoPagado();
    this.getPresupuestadoNoPagado();
    this.getPresupuestos();
    this.getPresupuestosServicios();
    this.register = this.fb.group({
      preserv: ["", Validators.required],
      Monto: ["", [Validators.required]],
      Mpago: ["", [Validators.required]],
      Comprobante: ["", [Validators.required]],
      NoComprobante: ["", [Validators.required]],
      CantidadCheque: [0],
      Banco: [""],
      NoCheque: [0],
      FechaCheque: [""],
      MontoCheque: [""],
      NoDocumento: [""],
      NoCuotas: [""],
      NoTarjeta: [""],
      TipoTarjeta: [""],
    });
    this.imagenclinicaPrint =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
  }
  getTotalUsuario(refresh?) {
    const options: any = { caso: 1, idPaciente: this.idPaciente };
    this.httpClient
      .post(`${this.baseURL}pagos.php`, JSON.stringify(options), headers)
      .subscribe((data) => {
        if (data != null) {
          this.totalUsuario = data[0].Total;
          refresh && this.sweet.toast("los planes");
        }
      });
  }

  getTotalCompletado() {
    const options: any = { caso: 2, idPaciente: this.idPaciente };
    this.httpClient
      .post(`${this.baseURL}pagos.php`, JSON.stringify(options), headers)
      .subscribe((data) => {
        if (data != null) {
          this.totalCompletado = data[0].Total;
        }
      });
  }

  getTotalPagado() {
    const options: any = { caso: 3, idPaciente: this.idPaciente };
    this.httpClient
      .post(`${this.baseURL}pagos.php`, JSON.stringify(options), headers)
      .subscribe((data) => {
        if (data != null) {
          this.totalPagado = data[0].Pagado;
        }
      });
  }

  getCompletadoNoPagado() {
    const options: any = { caso: 4, idPaciente: this.idPaciente };
    this.httpClient
      .post(`${this.baseURL}pagos.php`, JSON.stringify(options), headers)
      .subscribe((data) => {
        if (data != null) {
          this.completadoNoPagado = data[0].pendiente;
        }
      });
  }
  getPresupuestadoNoPagado() {
    const options: any = { caso: 5, idPaciente: this.idPaciente };
    this.httpClient
      .post(`${this.baseURL}pagos.php`, JSON.stringify(options), headers)
      .subscribe((data) => {
        if (data != null) {
          this.presupuestadoNoPagado = data[0].presupuestado;
        }
      });
  }

  getPresupuestos() {
    const options: any = { caso: 0, idPaciente: this.idPaciente };
    this.httpClient
      .post(`${this.baseURL}evoluciones.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.Presupuestos = respuesta;
      });
  }
  getPresupuestosServicios() {
    const options: any = { caso: 1, idPaciente: this.idPaciente };
    this.httpClient
      .post(`${this.baseURL}evoluciones.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.PresupuestosServ = respuesta;
      });
  }

  abrirModalPagos(content, data) {
    this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
      size: "lg",
    });
    this.getPresupuestosServiciosid(data);
  }

  getPresupuestosServiciosid(id) {
    const options: any = { caso: 1.1, idPresupuesto: id };
    this.httpClient
      .post(`${this.baseURL}evoluciones.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.PresupuestosServid = respuesta;
      });
  }

  onAddRowSave(form: FormGroup) {
    const options: any = { caso: 7, Presupuesto: form.value };
    this.httpClient
      .post(`${this.baseURL}pagos.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta == "Se inserto") {
          Swal.fire("Se guardo el pago correctamente", "", "success");
          this.ngOnInit();
          this.modalService.dismissAll();
          this.tarjeta = false;
          this.plantilla = false;
          this.cheque = false;
          this.pagare = false;
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
          console.log(respuesta);
          this.tarjeta = false;
          this.plantilla = false;
          this.cheque = false;
          this.pagare = false;
        }
      });
  }
  activarMetodo() {
    if (this.register.controls["Mpago"].value == "Cheque") {
      this.cheque = true;
      this.plantilla = false;
      this.pagare = false;
      this.tarjeta = false;
    } else if (this.register.controls["Mpago"].value == "plantilla") {
      this.plantilla = true;
      this.cheque = false;
      this.pagare = false;
      this.tarjeta = false;
    } else if (this.register.controls["Mpago"].value == "Pagare") {
      this.pagare = true;
      this.plantilla = false;
      this.cheque = false;
      this.tarjeta = false;
    } else if (this.register.controls["Mpago"].value == "Tarjeta de credito") {
      this.tarjeta = true;
      this.plantilla = false;
      this.cheque = false;
      this.pagare = false;
    } else {
      this.tarjeta = false;
      this.plantilla = false;
      this.cheque = false;
      this.pagare = false;
    }
  }
}
