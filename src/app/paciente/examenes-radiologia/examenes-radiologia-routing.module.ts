import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExamenesRadiologiaComponent } from './examenes-radiologia.component';

const routes: Routes = [
  {
    path: "",
    component: ExamenesRadiologiaComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamenesRadiologiaRoutingModule { }
