import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExamenesRadiologiaRoutingModule } from './examenes-radiologia-routing.module';
import { ExamenesRadiologiaComponent } from './examenes-radiologia.component';
import { MaterialModule } from 'src/app/shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ExamenesRadiologiaComponent],
  imports: [
    CommonModule,
    ExamenesRadiologiaRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ExamenesRadiologiaModule { }
