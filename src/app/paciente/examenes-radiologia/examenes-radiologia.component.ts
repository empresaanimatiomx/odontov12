import { Component, OnInit, ViewChild } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../../core/service/auth.service";
import Swal from "sweetalert2";
import { AlertService } from "../../core/service/alert.service";
import { HttpService } from "../../core/service/http.service";

@Component({
  selector: "app-examenes-radiologia",
  templateUrl: "./examenes-radiologia.component.html",
  styles: [],
})
export class ExamenesRadiologiaComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  data = [];
  filteredData = [];
  ordenForm: FormGroup;
  imagenclinicaPrint = "";
  centros = [];
  sucursales = [];
  edit: boolean = false;

  constructor(
    private sweet: AlertService,
    private authService: AuthService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private http: HttpService
  ) {}
  ngOnInit() {
    this.getOrdenes();
    this.getCentro();
    this.obtenersucursales();
    this.imagenclinicaPrint =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
    this.ordenForm = this.fb.group({
      idOrden: [""],
      sucursal: ["", Validators.required],
      centroRadiologico: ["", Validators.required],
      rxRetroalveolar: [""],
      rxRetroalveolarTotal: [""],
      rxBiteWing: [""],
      rxOclusal: [""],
      rxPanoramica: [""],
      rxteleradiografiaLateral: [""],
      rxTeleradiografiaFrontal: [""],
      rxDigitaldeMano: [""],
      maxilarSuperior: [""],
      maxilarInferior: [""],
      bimaxilar: [""],
      craneoCompleto: [""],
      ATMBocaCerrada: [""],
      ATMBocaAbierta: [""],
      zona: [""],
      especialidad: [""],
      rickets: [""],
      roth: [""],
      jarabak: [""],
      frontal: [""],
      tejidosBlandos: [""],
      conversionCefalome: [""],
      VTO: [""],
      modelos: [""],
      fotografias: [""],
      eje: [""],
      holta: [""],
      video: [""],
      articulados: [""],
      formaEntrga: [""],
      observaciones: [""],
    });
  }

  obtenersucursales() {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.http
      .post("sucursal.php", { caso: 0, idClinica: idclinica })
      .subscribe({
        next: (respuesta: any) => {
          this.sucursales = respuesta;
        },
        error: (err) => console.log(err),
      });
  }

  getCentro() {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.http.post("recetas.php", { caso: 8, idclinica }).subscribe({
      next: (data: any) => {
        this.centros = data;
      },
      error: (err) => console.log(err),
    });
  }

  getOrdenes(refresh?) {
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.http.post("radiologia.php", { caso: 5, idPaciente: id }).subscribe({
      next: (data: any) => {
        this.data = data;
        this.filteredData = data;
        refresh && this.sweet.toast("las ordenes");
      },
      error: (err) => console.log(err),
    });
  }

  editRow(row, content) {
    this.edit = true;
    this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
      size: "lg",
    });
    this.ordenForm.setValue(row);
  }

  addRow(content) {
    this.edit = false;
    this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
      size: "lg",
    });
  }

  selectLab(value, modal) {
    if (value === "si") {
      setTimeout(() => {
        this.modalService.open(modal, {
          ariaLabelledBy: "modal-basic-title",
          centered: true,
          size: "lg",
        });
      }, 100);
    }
  }

  modalPrevisulizar(modal) {
    setTimeout(() => {
      this.modalService.open(modal, {
        ariaLabelledBy: "modal-basic-title",
        centered: true,
        size: "lg",
      });
    }, 100);
  }

  preOrder(form: FormGroup) {
    if (form.value.idOrden !== "") {
      this.onEditSave(form);
    } else {
      this.saveOrden(form);
    }
  }

  onEditSave(form: FormGroup) {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    const options: any = { caso: 2, orden: form.value, idclinica };
    this.http.post("radiologia.php", options).subscribe((respuesta: any) => {
      if (respuesta === "Se modifico") {
        this.ngOnInit();
        Swal.fire("Se modifico la orden correctamente", "", "success");
        form.reset();
        this.modalService.dismissAll();
      } else {
        Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
      }
    });
  }

  saveOrden(form: FormGroup) {
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    const options: any = { caso: 1, orden: form.value, idclinica };
    this.http.post("radiologia.php", options).subscribe((respuesta: any) => {
      if (respuesta === "Se inserto") {
        this.ngOnInit();
        Swal.fire("Se agrego la orden correctamente", "", "success");
        form.reset();
        this.modalService.dismissAll();
      } else {
        Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
      }
    });
  }

  closeAndOpenModal(modal) {
    this.modalService.dismissAll();
    setTimeout(() => {
      this.modalService.open(modal, {
        ariaLabelledBy: "modal-basic-title",
        centered: true,
        size: "lg",
      });
    }, 0);
  }

  filterDatatable(event) {
    if (event.target.value === "") {
      this.data = this.filteredData;
    }
    this.data = this.http.filter(event, this.filteredData);
    this.table.offset = 0;
  }
}
