import { Component, OnInit } from "@angular/core";
import { HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import Swal from "sweetalert2";
import { SubirArchivoService } from "src/app/admin/administrador/documentos/documento/subir-archivo.service";
import { AuthService } from "../../core/service/auth.service";
import { HttpService } from "../../core/service/http.service";

@Component({
  selector: "app-imagenes",
  templateUrl: "./imagenes.component.html",
  styles: [],
})
export class ImagenesComponent implements OnInit {
  arregloimagenes: any = [];
  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  public respuestaImagenEnviada;
  Archivoseleccionado: null;
  nombreArchivo: any;
  Dataarchivo: File = null;
  archivo = "";
  archivoFinal: any;
  idClinica: number;
  idPaciente: number;
  currentIndex: any = -1;
  showFlag: any = false;

  constructor(
    private http: HttpService,
    private router: Router,
    private enviandoImagen: SubirArchivoService,
    private authService: AuthService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.idPaciente = id;
  }
  ngOnInit() {
    this.obtenerfotos();
  }

  showLightbox(index) {
    this.currentIndex = index;
    this.showFlag = true;
  }

  closeEventHandler() {
    this.showFlag = false;
    this.currentIndex = -1;
  }

  /** obtener fotos */
  obtenerfotos(): void {
    this.http
      .post("documentos.php", { caso: 8, idPaciente: this.idPaciente })
      .subscribe({
        next: (respuesta) => {
          this.arregloimagenes = respuesta;
        },
        error: (err) => console.log(err),
      });
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  // metdo para obtener la imagen de input file
  Subirimagen(event, files: FileList, fileInput: any) {
    console.log("Entro");
    const Numero1 = Math.floor(Math.random() * 10001).toString();
    const Numero2 = Math.floor(Math.random() * 1001).toString();
    // Nombre del archivo
    this.Archivoseleccionado = event.target.files[0];
    this.nombreArchivo = event.target.files[0].name;
    this.archivo = Numero1 + Numero2 + this.nombreArchivo.replace(/ /g, "");
    this.archivoFinal = files;
    this.Dataarchivo = <File>fileInput.target.files[0];
    this.cargandoImagen();
  }
  // metodo para subir la imagen y guardar el resultado en base de datos
  cargandoImagen() {
    this.mensajesubiendoinf();
    if (this.archivo === "") {
      this.swalalertmensaje("error", "Debe seleccionar un archivo");
    } else {
      this.enviandoImagen
        .subirfoto(this.archivoFinal[0], this.archivo)
        .subscribe({
          next: (response) => {
            this.respuestaImagenEnviada = response;
            if (this.respuestaImagenEnviada.msj === "Imagen subida") {
              const options: any = {
                caso: 3,
                named: this.archivo,
                idc: this.idClinica,
                idPaciente: this.idPaciente,
              };
              this.http.post("documentos.php", options).subscribe({
                next: (res) => {
                  if (res.toString() === "Se inserto") {
                    this.obtenerfotos();
                    this.swalalertmensaje(
                      "success",
                      "Imagen subida correctamente"
                    );
                  } else {
                    this.swalalertmensaje(
                      "error",
                      "Ocurrio un error, intente de nuevo por favor"
                    );
                  }
                },
                error: (err) => console.log(err),
              });
            } else if (
              this.respuestaImagenEnviada.msj === "Formato no soportado"
            ) {
              this.swalalertmensaje(
                "error",
                "No es un formato valido, intente de nuevo por favor"
              );
            } else {
              this.swalalertmensaje(
                "error",
                "Ocurrio un error, intente de nuevo por favor"
              );
            }
          },
          error: (err) => console.log(err),
        });
    }
  }

  eliminarfoto(nombre, id) {
    this.http
      .post("documentos.php", { caso: 5, named: nombre, idd: id })
      .subscribe({
        next: (res) => {
          if (res.toString() === "Se elimino") {
            this.obtenerfotos();
            this.swalalertmensaje("success", "Imagen eliminada correctamente");
          } else {
            this.swalalertmensaje(
              "error",
              "Ocurrio un error, intente de nuevo por favor"
            );
          }
        },
        error: (err) => console.log(err),
      });
  }

  mensajesubiendoinf() {
    Swal.fire({
      title: "Subiendo imagen ",
      html: "Espere un momento por favor",
      allowOutsideClick: false,
      showCancelButton: false,
      showConfirmButton: false,
    });
  }

  swalalertmensaje(icon, mensaje) {
    Swal.fire({
      position: "center",
      icon: icon,
      title: mensaje,
      showConfirmButton: true,
      allowOutsideClick: false,
      confirmButtonText: "Aceptar",
    });
  }
}
