import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "documentacion",
    loadChildren: () =>
      import("./documentacion/documentacion.module").then((m) => m.DocumentacionModule),
  },
  {
    path: "evoluciones",
    loadChildren: () =>
      import("./evoluciones/evoluciones.module").then((m) => m.EvolucionesModule),
  },
  {
    path: "examenes-laboratorio",
    loadChildren: () =>
      import("./examenes-laboratorio/examenes-laboratorio.module").then((m) => m.ExamenesLaboratorioModule),
  },
  {
    path: "examenes-radiologia",
    loadChildren: () =>
      import("./examenes-radiologia/examenes-radiologia.module").then((m) => m.ExamenesRadiologiaModule),
  },
  {
    path: "facturas-enviadas",
    loadChildren: () =>
      import("./facturas-enviadas/facturas-enviadas.module").then((m) => m.FacturasEnviadasModule),
  },
  {
    path: "historial-clinico",
    loadChildren: () =>
      import("./historial-clinico/historial-clinico.module").then((m) => m.HistorialClinicoModule),
  },
  {
    path: "imagenes",
    loadChildren: () =>
      import("./imagenes/imagenes.module").then((m) => m.ImagenesModule),
  },
  {
    path: "perfil",
    loadChildren: () =>
      import("./perfil/perfil.module").then((m) => m.PerfilModule),
  },
  {
    path: "planes-tratamiento",
    loadChildren: () =>
      import("./planes-tratamiento/planes-tratamiento.module").then((m) => m.PlanesTratamientoModule),
  },
  {
    path: "recetas",
    loadChildren: () =>
      import("./recetas/recetas.module").then((m) => m.RecetasModule),
  },
  {
    path: "videollamadas",
    loadChildren: () =>
      import("./videollamada/videollamada.module").then((m) => m.VideollamadaModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacienteRoutingModule { }
