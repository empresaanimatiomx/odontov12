import { Component, OnInit } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
} from "@angular/common/http";
import { AuthService } from "src/app/core/service/auth.service";
import Swal from "sweetalert2";
import { AlertService } from "src/app/core/service/alert.service";

const headers: any = new HttpHeaders({ "Content-Type": "application/json" });

@Component({
  selector: "app-evoluciones",
  templateUrl: "./evoluciones.component.html",
  styles: [],
})
export class EvolucionesComponent implements OnInit {
  private baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  panelOpenState = true;
  step = 0;
  setStep(index: number) {
    this.step = index;
  }
  nextStep() {
    this.step++;
  }
  prevStep() {
    this.step--;
  }
  idClinica;
  idPaciente;
  Presupuestos: any = [];
  PresupuestosServ: any = [];
  constructor(
    private authService: AuthService,
    private httpClient: HttpClient,
    private sweet: AlertService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.idPaciente = id;
  }
  ngOnInit() {
    this.getPresupuestos();
    this.getPresupuestosServicios();
  }

  getPresupuestos(refresh?) {
    const options: any = { caso: 0, idPaciente: this.idPaciente };
    this.httpClient
      .post(`${this.baseURL}evoluciones.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.Presupuestos = respuesta;
        refresh && this.sweet.toast("las evoluciones");
      });
  }
  getPresupuestosServicios() {
    const options: any = { caso: 1, idPaciente: this.idPaciente };
    this.httpClient
      .post(`${this.baseURL}evoluciones.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.PresupuestosServ = respuesta;
      });
  }
  cambiarstatus(Estatus, id) {
    const options: any = { caso: 3, Estatus: Estatus, idPresupuesto: id };
    this.httpClient
      .post(`${this.baseURL}evoluciones.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta == "Se modifico") {
          Swal.fire("Se modifico el presupuesto correctamente", "", "success");
          this.ngOnInit();
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }
}
