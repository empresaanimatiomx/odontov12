import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EvolucionesComponent } from './evoluciones.component';

const routes: Routes = [
  {
    path: "",
    component: EvolucionesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvolucionesRoutingModule { }
