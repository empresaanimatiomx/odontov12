import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EvolucionesRoutingModule } from './evoluciones-routing.module';
import { EvolucionesComponent } from './evoluciones.component';
import { MaterialModule } from 'src/app/shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    EvolucionesComponent
  ],
  imports: [
    CommonModule,
    EvolucionesRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EvolucionesModule { }
