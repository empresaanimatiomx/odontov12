import { Component, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { FormBuilder, FormGroup } from "@angular/forms";
import Swal from "sweetalert2";

const headers: any = new HttpHeaders({ "Content-Type": "application/json" });

@Component({
  selector: "app-historial-clinico",
  templateUrl: "./historial-clinico.component.html",
  styles: [],
})
export class HistorialClinicoComponent implements OnInit {
  private baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  historialPaciente: any = [];
  historialForm: FormGroup;
  form = false;
  editar: boolean;
  isedit: boolean;
  idPaciente;
  disabled = true;

  constructor(private httpClient: HttpClient, private fb: FormBuilder) {
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.idPaciente = id;
  }

  ngOnInit(): void {
    this.ObtenerHistorial(this.idPaciente, true);
    this.historialForm = this.fb.group({
      motivo: [""],
      enfermedadActual: [""],
      enfermedades: [""],
      antecedentesqrs: [""],
      alergias: [""],
      medicamentos: [""],
      habitos: [""],
      antecedentesfam: [""],
      linemediadentalsuperior: [""],
      milsuperior: [""],
      lineamediadentalinferior: [""],
      milinferior: [""],
      apinamientosuperior: [""],
      apinamientoinferior: [""],
      obsapinamiento: [""],
      obsanalisis: [""],
      perdidainser: [""],
      movilidadinser: [""],
      obserinsert: [""],
      pulpafrio: [""],
      pulpacaliente: [""],
      perhorizontal: [""],
      pervertical: [""],
      perobservaciones: [""],
      Fuma: [""],
      Embarazo: [""],
      Alcohol: [""],
      Cepilladodiario: [""],
      Fumar: [""],
      Golosinas: [""],
      perniciosolabio: [""],
      perniciosolengua: [""],
      Higienebucal: [""],
      Artritisreumatoide: [""],
      Asma: [""],
      Autoinmune: [""],
      Cancer: [""],
      Cardiaco: [""],
      Diabetes: [""],
      ETS: [""],
      Gastritis: [""],
      Hepatitis: [""],
      Hipotiroidismo: [""],
      Sarampion: [""],
      Varicela: [""],
      Hospitalizaciones: [""],
      Operacionadenoides: [""],
      AcidoAcetilsalicilico: [""],
      Alimentos: [""],
      Aneteico: [""],
      Loratadina: [""],
      Metamizol: [""],
      Paracetamol: [""],
      Penicilina: [""],
      Sulfonamidas: [""],
      CambiosPisoBoca: [""],
      lasMejillas: [""],
      labios: [""],
      paladar: [""],
      frenillo: [""],
      lengua: [""],
      CambiosEsmalteDentina: [""],
      observaestoma: [""],
      succionDigital: [""],
      deglucionAti: [""],
      onychophogia: [""],
      bruxismo: [""],
      resoral: [""],
      musculo: [""],
      masticarenfer: [""],
      atm: [""],
      enciasinfla: [""],
      placaperio: [""],
      calculoperio: [""],
      pruebaelec: [""],
      recegingival: [""],
    });
    this.historialForm.disable();
  }

  //trae la inf del historial clinico desde la BD
  ObtenerHistorial = (id, activo) => {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 1, idPaciente: id };
    this.httpClient
      .post(`${this.baseURL}historial.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.historialPaciente = respuesta;
        if (this.historialPaciente == null) {
          this.editar = false;
          this.form = true;
        } else {
          this.historialForm.patchValue({
            motivo: this.historialPaciente[0].Motivo,
            enfermedadActual: this.historialPaciente[0].EnfermedadActual,
            enfermedades: this.historialPaciente[0].Enfermedades,
            antecedentesqrs: this.historialPaciente[0].AntecedentesQuiru,
            alergias: this.historialPaciente[0].Alergias,
            medicamentos: this.historialPaciente[0].Medicamentos,
            habitos: this.historialPaciente[0].Habitos,
            antecedentesfam: this.historialPaciente[0].AntecedentesFamil,
            linemediadentalsuperior:
              this.historialPaciente[0].LineaMediaDentalSuperior,
            milsuperior: this.historialPaciente[0].LineaMediaDentalInfMM,
            lineamediadentalinferior:
              this.historialPaciente[0].LineaMediaDentalInferior,
            milinferior: this.historialPaciente[0].LineaMediaDentalInfMM,
            apinamientosuperior: this.historialPaciente[0].ApiSuperior,
            apinamientoinferior: this.historialPaciente[0].ApiInferior,
            obsapinamiento: this.historialPaciente[0].ApiObservacion,
            obsanalisis: this.historialPaciente[0].ApiObservacion,
            perdidainser: this.historialPaciente[0].PerdidaInsercion,
            movilidadinser: this.historialPaciente[0].Movilidad,
            obserinsert: this.historialPaciente[0].ObservPeriodontal,
            pulpafrio: this.historialPaciente[0].Frio,
            pulpacaliente: this.historialPaciente[0].Caliente,
            perhorizontal: this.historialPaciente[0].PercuHorizontal,
            pervertical: this.historialPaciente[0].PercuVertica,
            perobservaciones: this.historialPaciente[0].ObservacionesSensibi,
            Fuma: this.historialPaciente[0].Fuma,
            Embarazo: this.historialPaciente[0].Embarazo,
            Alcohol: this.historialPaciente[0].Alcohol,
            Cepilladodiario: this.historialPaciente[0].Cepilladodiario,
            Fumar: this.historialPaciente[0].Fumar,
            Golosinas: this.historialPaciente[0].Golosinas,
            perniciosolabio: this.historialPaciente[0].perniciosolabio,
            perniciosolengua: this.historialPaciente[0].perniciosolengua,
            Higienebucal: this.historialPaciente[0].Higienebucal,
            Artritisreumatoide: this.historialPaciente[0].Artritisreumatoide,
            Asma: this.historialPaciente[0].Asma,
            Autoinmune: this.historialPaciente[0].Autoinmune,
            Cancer: this.historialPaciente[0].Cancer,
            Cardiaco: this.historialPaciente[0].Cardiaco,
            Diabetes: this.historialPaciente[0].Diabetes,
            ETS: this.historialPaciente[0].ETS,
            Gastritis: this.historialPaciente[0].Gastritis,
            Hepatitis: this.historialPaciente[0].Hepatitis,
            Hipotiroidismo: this.historialPaciente[0].Hipotiroidismo,
            Sarampion: this.historialPaciente[0].Sarampion,
            Varicela: this.historialPaciente[0].Varicela,
            Hospitalizaciones: this.historialPaciente[0].Hospitalizaciones,
            Operacionadenoides: this.historialPaciente[0].Operacionadenoides,
            AcidoAcetilsalicilico:
              this.historialPaciente[0].AcidoAcetilsalicilico,
            Alimentos: this.historialPaciente[0].Alimentos,
            Aneteico: this.historialPaciente[0].Aneteico,
            Loratadina: this.historialPaciente[0].Loratadina,
            Metamizol: this.historialPaciente[0].Metamizol,
            Paracetamol: this.historialPaciente[0].Paracetamol,
            Penicilina: this.historialPaciente[0].Penicilina,
            Sulfonamidas: this.historialPaciente[0].Sulfonamidas,
            CambiosPisoBoca: this.historialPaciente[0].CambiosEnElPisoDeLaBoca,
            lasMejillas: this.historialPaciente[0].Mejillas,
            labios: this.historialPaciente[0].Labios,
            paladar: this.historialPaciente[0].Paladar,
            frenillo: this.historialPaciente[0].Frenillos,
            lengua: this.historialPaciente[0].Lengua,
            CambiosEsmalteDentina:
              this.historialPaciente[0].CambioEnElEsmalteYLaDentina,
            observaestoma: this.historialPaciente[0].ObservacionesEstoma,
            succionDigital: this.historialPaciente[0].SuccionDigi,
            deglucionAti: this.historialPaciente[0].DeglucionAti,
            onychophogia: this.historialPaciente[0].Onychophogia,
            bruxismo: this.historialPaciente[0].Bruxismo,
            resoral: this.historialPaciente[0].RespiracionOral,
            musculo: this.historialPaciente[0].Musculo,
            masticarenfer: this.historialPaciente[0].MasticarEnfermedad,
            atm: this.historialPaciente[0].ATM,
            enciasinfla: this.historialPaciente[0].EnciasInflamadas,
            placaperio: this.historialPaciente[0].Placa,
            calculoperio: this.historialPaciente[0].Calculo,
            pruebaelec: this.historialPaciente[0].PruebaElectr,
            recegingival: this.historialPaciente[0].RecesionGingi,
          });
          this.editar = true;
          this.form = true;
          this.historialForm.disable();
        }
      });
  };

  activarform() {
    this.historialForm.enable();
    this.isedit = true;
    this.disabled = false;
  }

  desactivarform() {
    Swal.fire({
      title: "Estas seguro que deseas cancelar?",
      text: "se perderán los cambios realizados",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        this.ObtenerHistorial(this.idPaciente, true);
        this.historialForm.disable();
        this.disabled = true;
      }
    });
  }

  // inserta la inf del historial clinico en BD
  historialSubmit() {
    const options: any = {
      caso: 0,
      Historial: this.historialForm.value,
      idPaciente: this.idPaciente,
    };
    this.httpClient
      .post(`${this.baseURL}historial.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        if (respuesta == null) {
          this.swalalertmensaje(
            "success",
            "Historial clinico guardado correctamente"
          );
          this.ObtenerHistorial(this.idPaciente, true);
          this.isedit = false;
          this.historialForm.disable();
          this.disabled = true;
        }
      });
  }

  //Editar el formulario de historial clinico y enviarlo a BD
  actualizar() {
    console.log(this.historialForm.value);
    const options: any = {
      caso: 2,
      Historial: this.historialForm.value,
      idPaciente: this.idPaciente,
    };
    this.httpClient
      .post(`${this.baseURL}historial.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        if (respuesta == null) {
          this.swalalertmensaje(
            "success",
            "Historial clinico actualizado correctamente"
          );
          this.ObtenerHistorial(this.idPaciente, true);
          this.isedit = false;
          this.historialForm.disable();
          this.disabled = true;
        }
      });
  }

  onBlurMethod(name) {
    const texto = this.historialForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.historialForm.controls[name].setValue(textofinal);
    }
  }

  //aleta de mensaje
  swalalertmensaje(icon, mensaje) {
    Swal.fire({
      position: "center",
      icon: icon,
      title: mensaje,
      showConfirmButton: true,
      allowOutsideClick: false,
      confirmButtonText: "Aceptar",
    });
  }
}
