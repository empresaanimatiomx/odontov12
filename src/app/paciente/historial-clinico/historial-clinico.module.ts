import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistorialClinicoRoutingModule } from './historial-clinico-routing.module';
import { MaterialModule } from 'src/app/shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HistorialClinicoComponent } from './historial-clinico.component';


@NgModule({
  declarations: [HistorialClinicoComponent],
  imports: [
    CommonModule,
    HistorialClinicoRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class HistorialClinicoModule { }
