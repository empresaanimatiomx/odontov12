import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HistorialClinicoComponent } from './historial-clinico.component';

const routes: Routes = [
  {
    path: "",
    component: HistorialClinicoComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistorialClinicoRoutingModule { }
