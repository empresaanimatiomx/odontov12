import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IniciarVideollamadaRoutingModule } from './iniciar-videollamada-routing.module';
import { IniciarVideollamadaComponent } from './iniciar-videollamada.component';
import { MaterialModule } from 'src/app/shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    IniciarVideollamadaComponent
  ],
  imports: [
    CommonModule,
    IniciarVideollamadaRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class IniciarVideollamadaModule { }
