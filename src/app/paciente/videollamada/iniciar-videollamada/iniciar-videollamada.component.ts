import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from "@angular/forms";
import { AlertService } from "../../../core/service/alert.service";
import { HttpService } from "../../../core/service/http.service";

@Component({
  selector: "app-iniciar-videollamada",
  templateUrl: "./iniciar-videollamada.component.html",
  styles: [],
})
export class IniciarVideollamadaComponent implements OnInit {
  doctores = [];
  idPaciente;
  idClinica;
  solicitudForm: FormGroup;

  constructor(private http: HttpService, private fb: FormBuilder, private sweet: AlertService) {
    const { id, idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.idPaciente = id;
    this.idClinica = idclinica;
    this.solicitudForm = this.fb.group({
      nombreUsuario: ["", Validators.required],
      idDoctor: ["", Validators.required],
      fecha: ["", Validators.required],
      hora: ["", Validators.required],
      motivo: ["", Validators.required],
      observaciones: [""],
    });
  }

  ngOnInit(): void {
    this.GetDoctores();
  }

  GetDoctores() {
    this.http
      .post("pacientes.php", { caso: 5, idClinica: this.idClinica })
      .subscribe({
        next: (respuesta: any) => {
          this.doctores = respuesta;
          this.getUserInfo();
        },
        error: (err) => console.log(err),
      });
  }

  getUserInfo() {
    this.http
      .post("pacientes.php", { caso: 18, idP: this.idPaciente })
      .subscribe({
        next: (respuesta: any) => {
          if (respuesta !== null) {
            const idDoctor = respuesta[0].idDoctor;
            this.solicitudForm.patchValue({
              idDoctor,
            });
            this.solicitudForm.controls["idDoctor"].disable();
          }
        },
        error: (err) => console.log(err),
      });
  }

  enviarSolicitud(solicitud, form: FormGroupDirective) {
    solicitud.idPaciente = this.idPaciente;
    this.http
      .post("pacientes.php", { caso: 19, solicitud, })
      .subscribe({
        next: (respuesta: any) => {
          if (respuesta === "Se inserto") {
            this.sweet.alert("","Se envio su solicitud","success");
            form.resetForm();
            this.GetDoctores();
          }else{
            this.sweet.alert("", "Ocurrio un error intente de nuevo", "error");
          }
        },
        error: (err) => console.log(err),
      });
  }

  firstLetterUppercase(name) {
    const texto = this.solicitudForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.solicitudForm.controls[name].setValue(textofinal);
    }
  }
}
