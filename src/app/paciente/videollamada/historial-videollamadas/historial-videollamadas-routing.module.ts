import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HistorialVideollamadasComponent } from './historial-videollamadas.component';

const routes: Routes = [
  {
    path:'',
    component: HistorialVideollamadasComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistorialVideollamadasRoutingModule { }
