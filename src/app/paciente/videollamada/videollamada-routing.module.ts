import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "historial-videollamadas",
    loadChildren: () =>
      import("./historial-videollamadas/historial-videollamadas.module").then(
        (m) => m.HistorialVideollamadasModule
      ),
  },
  {
    path: "iniciar-videollamada",
    loadChildren: () =>
      import("./iniciar-videollamada/iniciar-videollamada.module").then(
        (m) => m.IniciarVideollamadaModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideollamadaRoutingModule { }
