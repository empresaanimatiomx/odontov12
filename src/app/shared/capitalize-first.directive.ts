import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appCapitalizeFirst]'
})
export class CapitalizeFirstDirective {

    constructor(private el: ElementRef) {
  }

  @HostListener('blur') onBlur(){
  if (this.el.nativeElement.value) {
    var cadena = this.el.nativeElement.value.toLowerCase().split(' ');
    for (var i = 0; i < cadena.length; i++) {
     cadena[i] = cadena[i].charAt(0).toUpperCase() + cadena[i].substring(1);
    }
    this.el.nativeElement.value = cadena.join(' ');
 }
}

}
