import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "usuarios",
    loadChildren: () =>
      import("./usuarios/usuarios.module").then((m) => m.UsuariosModule),
  },
  {
    path: "convenios",
    loadChildren: () =>
      import("./convenios/convenios.module").then((m) => m.ConveniosModule),
  },
  {
    path: "motivos-consulta",
    loadChildren: () =>
      import("./motivos-consulta/motivos-consulta.module").then(
        (m) => m.MotivosConsultaModule
      ),
  },
  {
    path: "tratamientos",
    loadChildren: () =>
      import("./tratamientos/tratamientos.module").then(
        (m) => m.TratamientosModule
      ),
  },
  {
    path: "servicios",
    loadChildren: () =>
      import("./servicios/servicios.module").then((m) => m.ServiciosModule),
  },
  {
    path: "laboratorios",
    loadChildren: () =>
      import("./laboratorios/laboratorios.module").then(
        (m) => m.LaboratoriosModule
      ),
  },
  {
    path: "documentos",
    loadChildren: () =>
      import("./documentos/documentos.module").then((m) => m.DocumentosModule),
  },
  {
    path: "plantilla-receta",
    loadChildren: () =>
      import("./plantilla-receta/plantilla-receta.module").then(
        (m) => m.PlantillaRecetaModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministracionRoutingModule {}
