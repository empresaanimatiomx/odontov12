import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { HttpService } from "../../../../core/service/http.service";
import { AuthService } from "../../../../core/service/auth.service";
import { AlertService } from "../../../../core/service/alert.service";

@Component({
  selector: "app-nuevo-tratamiento",
  templateUrl: "./nuevo-tratamiento.component.html",
  styles: [],
})
export class NuevoTratamientoComponent implements OnInit {
  sucursales: any = [];
  idClinica: number;
  categoriaForm: FormGroup;

  constructor(
    private router: Router,
    private _snackBar: MatSnackBar,
    public http: HttpService,
    public sweet: AlertService,
    private authService: AuthService,
    private fb: FormBuilder
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.categoriaForm = this.fb.group({
      Nombre: ["", [Validators.required]],
      idSucursal: ["", [Validators.required]],
      Deshabilitado: [false, []],
    });
  }

  ngOnInit() {
    if (this.idClinica > 0) {
      this.getSucursales();
    } else {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
  }

  checkExist(categoria) {
    this.http.post("AdminCategorias.php", { caso: 10, categoria }).subscribe({
      next: (res: any) => {
        if (res !== null) {
          this.sweet.alert(
            "",
            "Ya existe un tratamiento con este nombre",
            "error"
          );
        } else {
          this.saveTratamiento(categoria);
        }
      },
      error: (err) => console.log(err),
    });
  }

  saveTratamiento(categoria) {
    categoria.Deshabilitado = categoria.Deshabilitado ? 1 : 0;
    this.http.post("AdminCategorias.php", { caso: 0, categoria }).subscribe({
      next: (res: any) => {
        if (res === "Se agrego") {
          this.sweet.alert("", "Tratamiento agregado correctamente", "success");
          this.router.navigate([
            "/admin/administrador/categoria/listado-categorias",
          ]);
        } else {
          this.sweet.alert("", "Ocurrio un error intentelo de nuevo", "error");
        }
      },
      error: (err) => console.log(err),
    });
  }

  firstLetterUppercase(name) {
    const texto = this.categoriaForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.categoriaForm.controls[name].setValue(textofinal);
    }
  }

  getSucursales() {
    this.http
      .post("categorias.php", { caso: 0, idclinica: this.idClinica })
      .subscribe((respuesta) => {
        this.sucursales = respuesta;
      });
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }
}
