import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NuevoTratamientoRoutingModule } from './nuevo-tratamiento-routing.module';
import { NuevoTratamientoComponent } from './nuevo-tratamiento.component';
import { MaterialModule } from "../../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    NuevoTratamientoComponent
  ],
  imports: [
    CommonModule,
    NuevoTratamientoRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class NuevoTratamientoModule { }
