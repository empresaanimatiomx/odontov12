import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NuevoTratamientoComponent } from './nuevo-tratamiento.component';

const routes: Routes = [
  {
    path:'',
    component:NuevoTratamientoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NuevoTratamientoRoutingModule { }
