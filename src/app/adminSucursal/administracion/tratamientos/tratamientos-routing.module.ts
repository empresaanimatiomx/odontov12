import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "listado-tratamientos",
    loadChildren: () =>
      import("./listado-tratamientos/listado-tratamientos.module").then(
        (m) => m.ListadoTratamientosModule
      ),
  },
  {
    path: "nuevo-tratamiento",
    loadChildren: () =>
      import("./nuevo-tratamiento/nuevo-tratamiento.module").then(
        (m) => m.NuevoTratamientoModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TratamientosRoutingModule { }
