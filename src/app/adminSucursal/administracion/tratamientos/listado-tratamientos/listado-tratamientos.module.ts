import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListadoTratamientosRoutingModule } from './listado-tratamientos-routing.module';
import { ListadoTratamientosComponent } from './listado-tratamientos.component';
import { MaterialModule } from "../../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ListadoTratamientosComponent
  ],
  imports: [
    CommonModule,
    ListadoTratamientosRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ListadoTratamientosModule { }
