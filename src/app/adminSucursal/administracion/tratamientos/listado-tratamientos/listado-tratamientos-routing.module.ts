import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListadoTratamientosComponent } from './listado-tratamientos.component';

const routes: Routes = [
  {
    path:'',
    component: ListadoTratamientosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListadoTratamientosRoutingModule { }
