import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListadoLaboratoriosComponent } from './listado-laboratorios.component';

const routes: Routes = [
  {
    path:'',
    component: ListadoLaboratoriosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListadoLaboratoriosRoutingModule { }
