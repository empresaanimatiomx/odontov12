import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListadoLaboratoriosRoutingModule } from './listado-laboratorios-routing.module';
import { ListadoLaboratoriosComponent } from './listado-laboratorios.component';
import { MaterialModule } from "../../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';

@NgModule({
  declarations: [
    ListadoLaboratoriosComponent
  ],
  imports: [
    CommonModule,
    ListadoLaboratoriosRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    GooglePlaceModule
  ]
})
export class ListadoLaboratoriosModule { }
