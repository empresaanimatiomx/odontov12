/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import Swal from "sweetalert2";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { AuthService } from "../../../../core/service/auth.service";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";

const headers: any = new HttpHeaders({ "Content-Type": "application/json" });

@Component({
  selector: "app-nuevo-laboratorio",
  templateUrl: "./nuevo-laboratorio.component.html",
  styles: [],
})
export class NuevoLaboratorioComponent implements OnInit {
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  private baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  laboratorio: FormGroup;
  idClinica;
  isEmail = /\S+@\S+\.\S+/;

  constructor(
    private authService: AuthService,
    private _snackBar: MatSnackBar,
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
  }

  ngOnInit(): void {
    if (this.idClinica < 0) {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
    this.laboratorio = this.fb.group({
      Nombre: ["", [Validators.required]],
      Telefono: ["", [Validators.required]],
      Correo: ["", [Validators.required, Validators.pattern(this.isEmail)]],
      Direccion: ["", [Validators.required]],
      ProtesisFijaUnitaria: [""],
      ProtesisFijaPlural: [""],
      OperatoriaIndirecta: [""],
      Planos: [""],
      ProtesisRemovible: [""],
      Ortodoncia: [""],
      Otros: [""],
      Observaciones: [""],
    });
  }

  public handleAddressChange(address: Address) {
    this.laboratorio.patchValue({
      Direccion: address.formatted_address,
    });
  }

  guardarLaboratorio(form: FormGroup) {
    const options: any = {
      caso: 1,
      laboratorio: form.value,
      idClinica: this.idClinica,
    };
    this.httpClient
      .post(`${this.baseURL}laboratorio.php`, JSON.stringify(options), headers)
      .subscribe((data: any) => {
        if (data == "Se inserto") {
          Swal.fire("Se agrego el laboratorio correctamente", "", "success");
          this.router.navigate([
            "/admin/administrador/laboratorio/listado-laboratorio",
          ]);
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }

  FirstUppercaseLetter(idname) {
    const texto = (<HTMLInputElement>document.getElementById(idname)).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      (<HTMLInputElement>document.getElementById(idname)).value = textofinal;
    }
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }
}
