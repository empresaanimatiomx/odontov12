import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NuevoLaboratorioComponent } from './nuevo-laboratorio.component';

const routes: Routes = [
  {
    path:'',
    component: NuevoLaboratorioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NuevoLaboratorioRoutingModule { }
