import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NuevoLaboratorioRoutingModule } from './nuevo-laboratorio-routing.module';
import { NuevoLaboratorioComponent } from './nuevo-laboratorio.component';
import { MaterialModule } from "../../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';

@NgModule({
  declarations: [NuevoLaboratorioComponent],
  imports: [
    CommonModule,
    NuevoLaboratorioRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    GooglePlaceModule,
  ],
})
export class NuevoLaboratorioModule {}
