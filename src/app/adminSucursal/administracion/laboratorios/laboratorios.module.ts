import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LaboratoriosRoutingModule } from './laboratorios-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LaboratoriosRoutingModule
  ]
})
export class LaboratoriosModule { }
