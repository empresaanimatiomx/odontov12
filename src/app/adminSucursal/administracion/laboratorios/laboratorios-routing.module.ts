import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "listado-laboratorios",
    loadChildren: () =>
      import("./listado-laboratorios/listado-laboratorios.module").then(
        (m) => m.ListadoLaboratoriosModule
      ),
  },
  {
    path: "nuevo-laboratorio",
    loadChildren: () =>
      import("./nuevo-laboratorio/nuevo-laboratorio.module").then(
        (m) => m.NuevoLaboratorioModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LaboratoriosRoutingModule { }
