import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MotivosConsultaRoutingModule } from './motivos-consulta-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MotivosConsultaRoutingModule
  ]
})
export class MotivosConsultaModule { }
