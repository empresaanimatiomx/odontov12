import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NuevoMotivoRoutingModule } from './nuevo-motivo-routing.module';
import { NuevoMotivoComponent } from './nuevo-motivo.component';
import { MaterialModule } from "../../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    NuevoMotivoComponent
  ],
  imports: [
    CommonModule,
    NuevoMotivoRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class NuevoMotivoModule { }
