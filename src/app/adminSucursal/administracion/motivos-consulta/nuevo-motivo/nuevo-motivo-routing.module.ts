import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NuevoMotivoComponent } from './nuevo-motivo.component';

const routes: Routes = [
  {
    path:'',
    component:NuevoMotivoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NuevoMotivoRoutingModule { }
