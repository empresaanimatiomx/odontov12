import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { AuthService } from "../../../../core/service/auth.service";
import { HttpService } from "../../../../core/service/http.service";
import { AlertService } from "../../../../core/service/alert.service";

@Component({
  selector: "app-nuevo-motivo",
  templateUrl: "./nuevo-motivo.component.html",
  styles: [],
})
export class NuevoMotivoComponent implements OnInit {
  motivoForm: FormGroup;
  idClinica: number;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private sweet: AlertService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.motivoForm = this.fb.group({
      NombreMotivo: ["", [Validators.required]],
      Tiempo: [
        "",
        [Validators.required, Validators.max(120), Validators.min(1)],
      ],
      MostrarAgenda: [""],
    });
  }

  ngOnInit(): void {
    if (this.idClinica > 0) {
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraclinica());
    }
  }

  iraclinica() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  checkExist(motivo) {
    const data = {
      caso: 4,
      idClinica: this.idClinica,
      Nombredemotivo: motivo.NombreMotivo,
    };
    this.http.post("motivos.php", data).subscribe({
      next: (respuesta) => {
        if (respuesta === null) {
          this.addMotivo(motivo, this.idClinica);
        } else {
          this.sweet.alert("", "Ya existe un motivo igual", "error");
        }
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  addMotivo(motivo, idClinica: Number) {
    let mostraagenda = "";
    if (motivo.MostrarAgenda === true) {
      mostraagenda = "SI";
    } else {
      mostraagenda = "NO";
    }
    const options: any = {
      caso: 1,
      Nombredemotivo: motivo.NombreMotivo,
      Tiempo: motivo.Tiempo,
      Mostrarenagenda: mostraagenda,
      idClinica,
    };
    this.http.post("motivos.php", options).subscribe((respuesta) => {
      const res = respuesta;
      if (res.toString() === "Se inserto") {
        this.sweet.alert(
          "",
          "Motivo de consulta guardado correctamente",
          "success"
        );
        this.router.navigate(["/admin/administrador/motivo/listado-motivos"]);
      } else {
        this.sweet.alert(
          "",
          "Ocurrio un error, intente de nuevo por favor",
          "error"
        );
      }
    });
  }

  firstLetterUppercase(name) {
    const texto = this.motivoForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.motivoForm.controls[name].setValue(textofinal);
    }
  }
}
