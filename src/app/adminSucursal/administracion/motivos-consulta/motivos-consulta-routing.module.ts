import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "listado-motivos",
    loadChildren: () =>
      import("./listado-motivos/listado-motivos.module").then(
        (m) => m.ListadoMotivosModule
      ),
  },
  {
    path: "nuevo-motivo",
    loadChildren: () =>
      import("./nuevo-motivo/nuevo-motivo.module").then(
        (m) => m.NuevoMotivoModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MotivosConsultaRoutingModule { }
