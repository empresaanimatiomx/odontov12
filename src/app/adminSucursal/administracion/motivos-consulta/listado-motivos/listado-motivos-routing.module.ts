import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListadoMotivosComponent } from './listado-motivos.component';

const routes: Routes = [
  {
    path:'',
    component:ListadoMotivosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListadoMotivosRoutingModule { }
