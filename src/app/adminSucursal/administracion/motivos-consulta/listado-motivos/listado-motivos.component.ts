import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../../../../core/service/auth.service";
import { HttpService } from "../../../../core/service/http.service";
import { AlertService } from "../../../../core/service/alert.service";

@Component({
  selector: "app-listado-motivos",
  templateUrl: "./listado-motivos.component.html",
  styles: [],
})
export class ListadoMotivosComponent implements OnInit {
  @ViewChild("epltable", { static: false }) epltable: ElementRef;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  motivoForm: FormGroup;
  imagenclinicaPrint = "";
  idClinica: number;
  motivos = [];
  motivosFilter = [];
  idmotivo = "";

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private http: HttpService,
    private authService: AuthService,
    private sweet: AlertService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.imagenclinicaPrint =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.motivoForm = this.fb.group({
      NombreMotivo: ["", [Validators.required]],
      Tiempo: [
        "",
        [Validators.required, Validators.max(120), Validators.min(1)],
      ],
      MostrarAgenda: [""],
    });
  }

  ngOnInit() {
    if (this.idClinica > 0) {
      this.getMotivos();
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraclinica());
    }
  }

  iraclinica() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  getMotivos(refresh?) {
    this.http
      .post("motivos.php", { caso: 0, idClinica: this.idClinica })
      .subscribe({
        next: (respuesta: any) => {
          this.motivos = respuesta;
          this.motivosFilter = respuesta;
          refresh && this.sweet.toast("los motivos");
        },
        error: (err) => console.log(err),
      });
  }

  editMotivoModal(row, content) {
    let mostragen;
    this.idmotivo = row.idMotivo;
    let ngbModalOptions: NgbModalOptions = {
      centered: true,
      ariaLabelledBy: "modal-basic-title",
      size: "lg",
    };
    if (row.MostrarAgenda === "NO") {
      mostragen = false;
    } else {
      mostragen = true;
    }
    this.modalService.open(content, ngbModalOptions);
    this.motivoForm.patchValue({
      ...row,
      MostrarAgenda: mostragen,
    });
  }

  eliminarMotivo(row) {
    this.sweet.alertConfirm("eliminar este motivo").then((result) => {
      if (result.isConfirmed) {
        this.http
          .post("motivos.php", { caso: 3, idMotivo: row.idMotivo })
          .subscribe((respuesta) => {
            const res = respuesta;
            if (res.toString() === "Se elimino") {
              this.getMotivos();
              this.sweet.alert(
                "",
                "Motivo de consulta eliminado correctamente",
                "success"
              );
            } else {
              this.sweet.alert(
                "",
                "Ocurrio un error, intente de nuevo por favor",
                "error"
              );
            }
          });
      }
    });
  }

  updateMotivo(motivo) {
    let mostraagenda = "";
    if (motivo.MostrarAgenda === true) {
      mostraagenda = "SI";
    } else {
      mostraagenda = "NO";
    }
    const options: any = {
      caso: 2,
      Nombredemotivo: motivo.NombreMotivo,
      Tiempo: motivo.Tiempo,
      Mostrarenagenda: mostraagenda,
      idMotivo: this.idmotivo,
    };
    this.http.post("motivos.php", options).subscribe({
      next: (res: any) => {
        if (res === "Se modifico") {
          this.getMotivos();
          this.cerrarmodal();
          this.sweet.alert(
            "",
            "Motivo de consulta modificado correctamente",
            "success"
          );
        } else {
          this.sweet.alert(
            "",
            "Ocurrio un error, intente de nuevo por favor",
            "error"
          );
        }
      },
      error: (err) => console.log(err),
    });
  }

  filterDatatable(event) {
    if (event.target.value === "") {
      this.motivos = this.motivosFilter;
    }
    this.motivos = this.http.filter(event, this.motivosFilter);
    this.table.offset = 0;
  }

  firstLetterUpperCase(name) {
    const texto = this.motivoForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.motivoForm.controls[name].setValue(textofinal);
    }
  }

  cerrarmodal() {
    this.modalService.dismissAll();
  }
}
