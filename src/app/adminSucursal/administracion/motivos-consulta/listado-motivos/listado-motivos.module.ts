import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListadoMotivosRoutingModule } from './listado-motivos-routing.module';
import { ListadoMotivosComponent } from './listado-motivos.component';
import { MaterialModule } from "../../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ListadoMotivosComponent
  ],
  imports: [
    CommonModule,
    ListadoMotivosRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ListadoMotivosModule { }
