import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListadoConveniosRoutingModule } from './listado-convenios-routing.module';
import { ListadoConveniosComponent } from './listado-convenios.component';
import { MaterialModule } from '../../../../shared/material.module';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ListadoConveniosComponent],
  imports: [
    CommonModule,
    ListadoConveniosRoutingModule,
    MaterialModule,
    GooglePlaceModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class ListadoConveniosModule {}
