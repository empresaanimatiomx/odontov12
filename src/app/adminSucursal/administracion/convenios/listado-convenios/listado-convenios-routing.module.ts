import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListadoConveniosComponent } from './listado-convenios.component';

const routes: Routes = [
  {
    path:'',
    component:ListadoConveniosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListadoConveniosRoutingModule { }
