import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "listado-convenios",
    loadChildren: () =>
      import("./listado-convenios/listado-convenios.module").then((m) => m.ListadoConveniosModule),
  },
  {
    path: "nuevo-convenio",
    loadChildren: () =>
      import("./nuevo-convenio/nuevo-convenio.module").then((m) => m.NuevoConvenioModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConveniosRoutingModule { }
