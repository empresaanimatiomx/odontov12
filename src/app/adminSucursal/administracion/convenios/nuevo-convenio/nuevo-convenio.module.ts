import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NuevoConvenioRoutingModule } from './nuevo-convenio-routing.module';
import { NuevoConvenioComponent } from './nuevo-convenio.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { MaterialModule } from "../../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [NuevoConvenioComponent],
  imports: [
    CommonModule,
    NuevoConvenioRoutingModule,
    MaterialModule,
    GooglePlaceModule,
    FormsModule,
    ReactiveFormsModule
  ],
})
export class NuevoConvenioModule {}
