import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NuevoConvenioComponent } from './nuevo-convenio.component';

const routes: Routes = [
  {
    path:'',
    component:NuevoConvenioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NuevoConvenioRoutingModule { }
