import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AlertService } from "src/app/core/service/alert.service";
import { AuthService } from "src/app/core/service/auth.service";
import { HttpService } from "src/app/core/service/http.service";

@Component({
  selector: "app-listado-servicios",
  templateUrl: "./listado-servicios.component.html",
  styles: [],
})
export class ListadoServiciosComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  @ViewChild("epltable", { static: false }) epltable: ElementRef;
  servicios: any = [];
  serviciosFilter: any = [];
  categorias: any = [];
  servicioForm: FormGroup;
  idClinica: number;
  idServicio;
  imagenclinicaPrint = "";

  constructor(
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private modalService: NgbModal,
    private http: HttpService,
    private authService: AuthService,
    private router: Router,
    private swal: AlertService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.imagenclinicaPrint =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
    this.servicioForm = this.fb.group({
      Nombre: ["", [Validators.required]],
      idCategorias: ["", [Validators.required]],
      Descripcion: ["", [Validators.maxLength(250)]],
      Costo: ["", [Validators.required]],
      Deshabilitar: [false, []],
    });
  }

  ngOnInit() {
    if (this.idClinica > 0) {
      this.getCategorias();
      this.getServicios();
    } else {
      let snack = this._snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraclinica());
    }
  }

  iraclinica() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  getServicios(refresh?) {
    this.http
      .post("servicios.php", { caso: 0, idClinica: this.idClinica })
      .subscribe({
        next: (respuesta) => {
          this.servicios = respuesta;
          this.serviciosFilter = respuesta;
          refresh && this.swal.toast("los servicios");
        },
        error: (res) => console.log(res),
      });
  }

  exportToExcel() {
    this.http.export("listado_servicios", this.serviciosFilter);
  }

  getCategorias() {
    this.http
      .post("servicios.php", { caso: 1, idClinica: this.idClinica })
      .subscribe((respuesta) => {
        this.categorias = respuesta;
      });
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  editServicioModal(row, content) {
    this.idServicio = row.idServicios;
    const status = row.Deshabilitar === "1" ? true : false;
    let ngbModalOptions: NgbModalOptions = {
      centered: true,
      ariaLabelledBy: "modal-basic-title",
    };
    this.modalService.open(content, ngbModalOptions);
    this.servicioForm.patchValue({
      ...row,
      Deshabilitar: status,
    });
  }

  updateServicio(servicio) {
    servicio.Costo = Number(servicio.Costo).toFixed(2);
    servicio.Deshabilitar = servicio.Deshabilitar ? 1 : 0;
    const options: any = {
      caso: 3,
      servicio,
      idservicio: this.idServicio,
    };
    this.http.post("servicios.php", options).subscribe((respuesta) => {
      const res = respuesta;
      if (res.toString() === "Se modifico") {
        this.getServicios();
        this.modalService.dismissAll();
        this.swal.alert("", "Servicio editado correctamente", "success");
      } else {
        this.swal.alert(
          "",
          "Ocurrio un error, intente de nuevo por favor",
          "error"
        );
      }
    });
  }

  filterDatatable(event) {
    if (event.target.value === "") {
      this.servicios = this.serviciosFilter;
    }
    this.servicios = this.http.filter(event, this.serviciosFilter);
    this.table.offset = 0;
  }

  firstLetterUpperCase(name) {
    const texto = this.servicioForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.servicioForm.controls[name].setValue(textofinal);
    }
  }

  deleteServicio(row) {
    this.swal
      .alertConfirm("eliminar este servicio", "¡Eliminar!")
      .then((action) => {
        if (action.isConfirmed) {
          this.http
            .post("servicios.php", { caso: 5, idservicio: row.idServicios })
            .subscribe((respuesta) => {
              const res = respuesta;
              if (res.toString() === "Se elimino") {
                this.swal.alert("", "Se elimino el servicio", "success");
                this.getServicios();
              } else {
                this.swal.alert(
                  "",
                  "Ocurrio un error, intente de nuevo por favor",
                  "error"
                );
              }
            });
        }
      });
  }
}
