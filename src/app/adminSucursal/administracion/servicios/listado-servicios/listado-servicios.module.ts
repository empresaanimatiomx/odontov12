import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListadoServiciosRoutingModule } from './listado-servicios-routing.module';
import { ListadoServiciosComponent } from './listado-servicios.component';
import { MaterialModule } from "../../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ListadoServiciosComponent
  ],
  imports: [
    CommonModule,
    ListadoServiciosRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ListadoServiciosModule { }
