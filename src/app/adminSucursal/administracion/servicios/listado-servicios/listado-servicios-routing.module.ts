import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListadoServiciosComponent } from './listado-servicios.component';

const routes: Routes = [
  {
    path:'',
    component: ListadoServiciosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListadoServiciosRoutingModule { }
