import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "nuevo-servicio",
    loadChildren: () =>
      import("./nuevo-servicio/nuevo-servicio.module").then(
        (m) => m.NuevoServicioModule
      ),
  },
  {
    path: "listado-servicios",
    loadChildren: () =>
      import("./listado-servicios/listado-servicios.module").then(
        (m) => m.ListadoServiciosModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiciosRoutingModule { }
