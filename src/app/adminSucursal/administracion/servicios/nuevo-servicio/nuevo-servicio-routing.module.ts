import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NuevoServicioComponent } from './nuevo-servicio.component';

const routes: Routes = [
  {
    path:'',
    component: NuevoServicioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NuevoServicioRoutingModule { }
