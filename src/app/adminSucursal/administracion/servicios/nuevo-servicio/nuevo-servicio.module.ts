import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NuevoServicioRoutingModule } from './nuevo-servicio-routing.module';
import { NuevoServicioComponent } from './nuevo-servicio.component';
import { MaterialModule } from "../../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    NuevoServicioComponent
  ],
  imports: [
    CommonModule,
    NuevoServicioRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class NuevoServicioModule { }
