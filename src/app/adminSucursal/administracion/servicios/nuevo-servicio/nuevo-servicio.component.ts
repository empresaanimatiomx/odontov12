import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { AlertService } from "../../../../core/service/alert.service";
import { AuthService } from "../../../../core/service/auth.service";
import { HttpService } from "../../../../core/service/http.service";

@Component({
  selector: "app-nuevo-servicio",
  templateUrl: "./nuevo-servicio.component.html",
  styles: [],
})
export class NuevoServicioComponent implements OnInit {
  idClinica: number;
  categorias: any = [];
  servicioForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private sweet: AlertService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.servicioForm = this.fb.group({
      Nombre: ["", [Validators.required]],
      idCategorias: ["", [Validators.required]],
      Descripcion: ["", [Validators.maxLength(250)]],
      Costo: ["", [Validators.required]],
      Deshabilitar: [false, []],
    });
  }

  ngOnInit(): void {
    if (this.idClinica < 0) {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.goToClinica());
    }
    this.getCategorias();
  }

  goToClinica() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  getCategorias() {
    this.http
      .post("servicios.php", { caso: 1, idClinica: this.idClinica })
      .subscribe((respuesta) => {
        this.categorias = respuesta;
      });
  }

  agregarServicio(servicio) {
    servicio.Costo = Number(servicio.Costo).toFixed(2);
    servicio.Deshabilitar = servicio.Deshabilitar ? 1 : 0;
    const options: any = {
      caso: 2,
      servicio,
    };
    this.http.post("servicios.php", options).subscribe({
      next: (res: any) => {
        if (res === "Se inserto") {
          this.sweet.alert("", "Servicio guardado correctamente", "success");
          this.router.navigate([
            "/admin/administrador/servicio/listado-servicios",
          ]);
        } else {
          this.sweet.alert(
            "",
            "Ocurrio un error, intente de nuevo por favor",
            "error"
          );
        }
      },
      error: (err) => console.log(err),
    });
  }

  firstLetterUpperCase(name) {
    const texto = this.servicioForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.servicioForm.controls[name].setValue(textofinal);
    }
  }
}
