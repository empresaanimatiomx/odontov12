import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubirDocumentoRoutingModule } from './subir-documento-routing.module';
import { SubirDocumentoComponent } from './subir-documento.component';
import { MaterialModule } from "../../../../shared/material.module";

@NgModule({
  declarations: [
    SubirDocumentoComponent
  ],
  imports: [
    CommonModule,
    SubirDocumentoRoutingModule,
    MaterialModule
  ]
})
export class SubirDocumentoModule { }
