import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubirDocumentoComponent } from './subir-documento.component';

const routes: Routes = [
  {
    path:'',
    component:SubirDocumentoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubirDocumentoRoutingModule { }
