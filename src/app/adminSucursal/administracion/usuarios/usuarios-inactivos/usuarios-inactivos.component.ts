import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { AuthService } from "src/app/core/service/auth.service";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { Router } from "@angular/router";
import Swal from "sweetalert2";
import { AlertService } from "src/app/core/service/alert.service";

@Component({
  selector: "app-usuarios-inactivos",
  templateUrl: "./usuarios-inactivos.component.html",
  styles: [],
})
export class UsuariosInactivosComponent implements OnInit {
  @ViewChild("roleTemplate", { static: true }) roleTemplate: TemplateRef<any>;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  idUser = "";
  filtrodatosUserI: any = [];
  dataUserI: any = [];
  idClinica: number;
  rows = [];
  selectedRowData: selectRowInterface;
  newUserImg = "assets/images/user_default.png";
  data = [];
  filteredData = [];
  editForm: FormGroup;
  register: FormGroup;
  selectedOption: string;
  columns = [
    { name: "img" },
    { name: "nombres" },
    { name: "apellidos" },
    { name: "perfil" },
    { name: "perfil" },
  ];
  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router,
    private sweet: AlertService
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.editForm = this.fb.group({
      id: new FormControl(),
      img: new FormControl(),
      Nombres: new FormControl(),
      Apellidos: new FormControl(),
      Celular: new FormControl(),
      Genero: new FormControl(),
      Doctor: new FormControl(),
    });
  }
  ngOnInit() {
    if (this.idClinica > 0) {
      this.getAllUsers();
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
    this.register = this.fb.group({
      id: [""],
      img: [""],
      Nombres: ["", [Validators.required, Validators.pattern("[a-zA-Z]+")]],
      Apellidos: [""],
      Celular: ["", [Validators.required]],
      Genero: ["", [Validators.required]],
      Doctor: [
        "",
        [Validators.required, Validators.email, Validators.minLength(5)],
      ],
    });
  }

  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset
    const keys = Object.keys(this.filtrodatosUserI[0]);
    // assign filtered matches to the active datatable
    this.dataUserI = this.filtrodatosUserI.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  /** Select convenios */
  getAllUsers(refresh?): void {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 13, idC: this.idClinica };
    const URL: any = this.baseURL + "usuarios.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        if (respuesta) {
          this.dataUserI = respuesta;
          this.filtrodatosUserI = respuesta;
          refresh && this.sweet.toast("los usuarios");
        } else {
          this.dataUserI = [];
          this.filtrodatosUserI = [];
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }
  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }
  iraGenerar2() {
    this.router.navigate(["/admin/administrador/usuario/nuevo-usuario"]);
  }
  // Activa usuario
  cambiarestatus(row) {
    var mensaje;
    var mensaje1;
    var mensaje2;
    var color;
    var status;
    console.log(row);
    this.idUser = row.idUsuario;
    mensaje = "¿Estas seguro de habilitar a este usuario?";
    mensaje1 = "Usuario habilitado correctamente";
    mensaje2 = "HABILITAR USUARIO";
    color = "#50C400";
    status = "0";
    Swal.fire({
      title: mensaje,
      showCancelButton: true,
      confirmButtonText: mensaje2,
      cancelButtonText: "Cancelar",
      confirmButtonColor: color,
      cancelButtonColor: "#007bff",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        this.activaUser(Number(this.idUser)).subscribe((respuesta) => {
          const res = respuesta;
          if (res.toString() === "Se modifico") {
            this.getAllUsers();
            this.swalalertmensaje("success", mensaje1);
          } else {
            this.swalalertmensaje(
              "error",
              "Ocurrio un error, intente de nuevo por favor"
            );
          }
        });
      }
    });
  }
  activaUser(id: number) {
    console.log(id);
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 11, idUsuario: id, idActivo: 0 };
    const URL: any = this.baseURL + "usuarios.php";
    return this.httpClient.post(URL, JSON.stringify(options), headers);
  }
  // alerta de mensaje
  swalalertmensaje(icon, mensaje) {
    Swal.fire({
      position: "center",
      icon: icon,
      title: mensaje,
      showConfirmButton: true,
      allowOutsideClick: false,
      confirmButtonText: "Aceptar",
    });
  }
}
export interface selectRowInterface {
  img: String;
  Nombres: String;
  Apellidos: String;
}
