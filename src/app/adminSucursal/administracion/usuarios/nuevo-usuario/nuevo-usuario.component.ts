import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "src/app/core/service/auth.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { SubirarchivoService } from "src/app/core/service/subirarchivo.service";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { Router } from "@angular/router";
import Swal from "sweetalert2";
import moment from "moment";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";

@Component({
  selector: "app-nuevo-usuario",
  templateUrl: "./nuevo-usuario.component.html",
  styleUrls: ["./nuevo-usuario.component.scss"],
})
export class NuevoUsuarioComponent implements OnInit {
  @ViewChild("placesRef")
  placesRef: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  roomForm: FormGroup;
  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  idClinica: number;
  edad: number;
  sucursales: any = [];
  foto = "";
  fotoFinal: any;
  Archivoseleccionado: null;
  nombrefoto: any;
  Dataarchivo: File = null;
  public respuestaImagenEnviada;
  public resultadoCarga;
  verpassword = true;
  especialidades = [
    "Endodoncia",
    "Odontología Pediátrica",
    "Odontología Restauradora",
    "Ortodoncia",
    "Periodoncia",
    "Cirugía oral y maxilofacial",
    "Radiología oral y maxilofacial",
    "Prostodoncia",
    "Patología oral y maxilofacial",
  ];
  celdaseleccionada = "background: #23395b";
  celdanoseleccionada = "background: #e2e2e2";
  tdlunes6a7 = this.celdanoseleccionada;
  tdlunes7a8 = this.celdanoseleccionada;
  tdlunes8a9 = this.celdanoseleccionada;
  tdlunes9a10 = this.celdanoseleccionada;
  tdlunes10a11 = this.celdanoseleccionada;
  tdlunes11a12 = this.celdanoseleccionada;
  tdlunes12a13 = this.celdanoseleccionada;
  tdlunes13a14 = this.celdanoseleccionada;
  tdlunes14a15 = this.celdanoseleccionada;
  tdlunes15a16 = this.celdanoseleccionada;
  tdlunes16a17 = this.celdanoseleccionada;
  tdlunes17a18 = this.celdanoseleccionada;
  tdlunes18a19 = this.celdanoseleccionada;
  tdlunes19a20 = this.celdanoseleccionada;
  tdlunes20a21 = this.celdanoseleccionada;
  tdmartes6a7 = this.celdanoseleccionada;
  tdmartes7a8 = this.celdanoseleccionada;
  tdmartes8a9 = this.celdanoseleccionada;
  tdmartes9a10 = this.celdanoseleccionada;
  tdmartes10a11 = this.celdanoseleccionada;
  tdmartes11a12 = this.celdanoseleccionada;
  tdmartes12a13 = this.celdanoseleccionada;
  tdmartes13a14 = this.celdanoseleccionada;
  tdmartes14a15 = this.celdanoseleccionada;
  tdmartes15a16 = this.celdanoseleccionada;
  tdmartes16a17 = this.celdanoseleccionada;
  tdmartes17a18 = this.celdanoseleccionada;
  tdmartes18a19 = this.celdanoseleccionada;
  tdmartes19a20 = this.celdanoseleccionada;
  tdmartes20a21 = this.celdanoseleccionada;
  tdmiercoles6a7 = this.celdanoseleccionada;
  tdmiercoles7a8 = this.celdanoseleccionada;
  tdmiercoles8a9 = this.celdanoseleccionada;
  tdmiercoles9a10 = this.celdanoseleccionada;
  tdmiercoles10a11 = this.celdanoseleccionada;
  tdmiercoles11a12 = this.celdanoseleccionada;
  tdmiercoles12a13 = this.celdanoseleccionada;
  tdmiercoles13a14 = this.celdanoseleccionada;
  tdmiercoles14a15 = this.celdanoseleccionada;
  tdmiercoles15a16 = this.celdanoseleccionada;
  tdmiercoles16a17 = this.celdanoseleccionada;
  tdmiercoles17a18 = this.celdanoseleccionada;
  tdmiercoles18a19 = this.celdanoseleccionada;
  tdmiercoles19a20 = this.celdanoseleccionada;
  tdmiercoles20a21 = this.celdanoseleccionada;
  tdjueves6a7 = this.celdanoseleccionada;
  tdjueves7a8 = this.celdanoseleccionada;
  tdjueves8a9 = this.celdanoseleccionada;
  tdjueves9a10 = this.celdanoseleccionada;
  tdjueves10a11 = this.celdanoseleccionada;
  tdjueves11a12 = this.celdanoseleccionada;
  tdjueves12a13 = this.celdanoseleccionada;
  tdjueves13a14 = this.celdanoseleccionada;
  tdjueves14a15 = this.celdanoseleccionada;
  tdjueves15a16 = this.celdanoseleccionada;
  tdjueves16a17 = this.celdanoseleccionada;
  tdjueves17a18 = this.celdanoseleccionada;
  tdjueves18a19 = this.celdanoseleccionada;
  tdjueves19a20 = this.celdanoseleccionada;
  tdjueves20a21 = this.celdanoseleccionada;
  tdviernes6a7 = this.celdanoseleccionada;
  tdviernes7a8 = this.celdanoseleccionada;
  tdviernes8a9 = this.celdanoseleccionada;
  tdviernes9a10 = this.celdanoseleccionada;
  tdviernes10a11 = this.celdanoseleccionada;
  tdviernes11a12 = this.celdanoseleccionada;
  tdviernes12a13 = this.celdanoseleccionada;
  tdviernes13a14 = this.celdanoseleccionada;
  tdviernes14a15 = this.celdanoseleccionada;
  tdviernes15a16 = this.celdanoseleccionada;
  tdviernes16a17 = this.celdanoseleccionada;
  tdviernes17a18 = this.celdanoseleccionada;
  tdviernes18a19 = this.celdanoseleccionada;
  tdviernes19a20 = this.celdanoseleccionada;
  tdviernes20a21 = this.celdanoseleccionada;
  tdsabado6a7 = this.celdanoseleccionada;
  tdsabado7a8 = this.celdanoseleccionada;
  tdsabado8a9 = this.celdanoseleccionada;
  tdsabado9a10 = this.celdanoseleccionada;
  tdsabado10a11 = this.celdanoseleccionada;
  tdsabado11a12 = this.celdanoseleccionada;
  tdsabado12a13 = this.celdanoseleccionada;
  tdsabado13a14 = this.celdanoseleccionada;
  tdsabado14a15 = this.celdanoseleccionada;
  tdsabado15a16 = this.celdanoseleccionada;
  tdsabado16a17 = this.celdanoseleccionada;
  tdsabado17a18 = this.celdanoseleccionada;
  tdsabado18a19 = this.celdanoseleccionada;
  tdsabado19a20 = this.celdanoseleccionada;
  tdsabado20a21 = this.celdanoseleccionada;
  tddomingo6a7 = this.celdanoseleccionada;
  tddomingo7a8 = this.celdanoseleccionada;
  tddomingo8a9 = this.celdanoseleccionada;
  tddomingo9a10 = this.celdanoseleccionada;
  tddomingo10a11 = this.celdanoseleccionada;
  tddomingo11a12 = this.celdanoseleccionada;
  tddomingo12a13 = this.celdanoseleccionada;
  tddomingo13a14 = this.celdanoseleccionada;
  tddomingo14a15 = this.celdanoseleccionada;
  tddomingo15a16 = this.celdanoseleccionada;
  tddomingo16a17 = this.celdanoseleccionada;
  tddomingo17a18 = this.celdanoseleccionada;
  tddomingo18a19 = this.celdanoseleccionada;
  tddomingo19a20 = this.celdanoseleccionada;
  tddomingo20a21 = this.celdanoseleccionada;

  //valor check
  checklunes6a7 = false;
  checklunes7a8 = false;
  checklunes8a9 = false;
  checklunes9a10 = false;
  checklunes10a11 = false;
  checklunes11a12 = false;
  checklunes12a13 = false;
  checklunes13a14 = false;
  checklunes14a15 = false;
  checklunes15a16 = false;
  checklunes16a17 = false;
  checklunes17a18 = false;
  checklunes18a19 = false;
  checklunes19a20 = false;
  checklunes20a21 = false;
  checkmartes6a7 = false;
  checkmartes7a8 = false;
  checkmartes8a9 = false;
  checkmartes9a10 = false;
  checkmartes10a11 = false;
  checkmartes11a12 = false;
  checkmartes12a13 = false;
  checkmartes13a14 = false;
  checkmartes14a15 = false;
  checkmartes15a16 = false;
  checkmartes16a17 = false;
  checkmartes17a18 = false;
  checkmartes18a19 = false;
  checkmartes19a20 = false;
  checkmartes20a21 = false;
  checkmiercoles6a7 = false;
  checkmiercoles7a8 = false;
  checkmiercoles8a9 = false;
  checkmiercoles9a10 = false;
  checkmiercoles10a11 = false;
  checkmiercoles11a12 = false;
  checkmiercoles12a13 = false;
  checkmiercoles13a14 = false;
  checkmiercoles14a15 = false;
  checkmiercoles15a16 = false;
  checkmiercoles16a17 = false;
  checkmiercoles17a18 = false;
  checkmiercoles18a19 = false;
  checkmiercoles19a20 = false;
  checkmiercoles20a21 = false;
  checkjueves6a7 = false;
  checkjueves7a8 = false;
  checkjueves8a9 = false;
  checkjueves9a10 = false;
  checkjueves10a11 = false;
  checkjueves11a12 = false;
  checkjueves12a13 = false;
  checkjueves13a14 = false;
  checkjueves14a15 = false;
  checkjueves15a16 = false;
  checkjueves16a17 = false;
  checkjueves17a18 = false;
  checkjueves18a19 = false;
  checkjueves19a20 = false;
  checkjueves20a21 = false;
  checkviernes6a7 = false;
  checkviernes7a8 = false;
  checkviernes8a9 = false;
  checkviernes9a10 = false;
  checkviernes10a11 = false;
  checkviernes11a12 = false;
  checkviernes12a13 = false;
  checkviernes13a14 = false;
  checkviernes14a15 = false;
  checkviernes15a16 = false;
  checkviernes16a17 = false;
  checkviernes17a18 = false;
  checkviernes18a19 = false;
  checkviernes19a20 = false;
  checkviernes20a21 = false;
  checksabado6a7 = false;
  checksabado7a8 = false;
  checksabado8a9 = false;
  checksabado9a10 = false;
  checksabado10a11 = false;
  checksabado11a12 = false;
  checksabado12a13 = false;
  checksabado13a14 = false;
  checksabado14a15 = false;
  checksabado15a16 = false;
  checksabado16a17 = false;
  checksabado17a18 = false;
  checksabado18a19 = false;
  checksabado19a20 = false;
  checksabado20a21 = false;
  checkdomingo6a7 = false;
  checkdomingo7a8 = false;
  checkdomingo8a9 = false;
  checkdomingo9a10 = false;
  checkdomingo10a11 = false;
  checkdomingo11a12 = false;
  checkdomingo12a13 = false;
  checkdomingo13a14 = false;
  checkdomingo14a15 = false;
  checkdomingo15a16 = false;
  checkdomingo16a17 = false;
  checkdomingo17a18 = false;
  checkdomingo18a19 = false;
  checkdomingo19a20 = false;
  checkdomingo20a21 = false;
  todoeldialunes = false;
  todoeldiamartes = false;
  todoeldiamiercoles = false;
  todoeldiajueves = false;
  todoeldiaviernes = false;
  todoeldiasabado = false;
  todoeldiadomingo = false;
  dia = "";
  servicio = "";
  horainicio = "";
  horafin = "";
  horainicio1 = "";
  horafin1 = "";

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private enviandoImagen: SubirarchivoService,
    private modalService: NgbModal
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.roomForm = this.fb.group({
      nombreusuario: ["", [Validators.required]],
      apellidosusuario: ["", [Validators.required]],
      RFCUsuario: ["", [Validators.minLength(12), Validators.maxLength(13)]],
      direccion: ["", []],
      telefonousuario: [
        "",
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
        ],
      ],
      email: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ]),
      ],
      fechanaci: ["", []],
      edad: [{ value: "", disabled: true }, [Validators.maxLength(3)]],
      perfil: ["", [Validators.required]],
      sucursal: ["", [Validators.required]],
      gruposanguineo: ["", []],
      genero: ["", [Validators.required]],
      estadocivil: ["", []],
      titulo: ["", [Validators.maxLength(80)]],
      especialidad: ["", [Validators.required]],
      cedula: ["", [Validators.minLength(7), Validators.maxLength(10)]],
      uploadImg: ["", []],
    });
  }

  mensajes_validacion = {
    nombreusuario: [
      { type: "required", message: "El nombre de usuario es requerido" },
    ],
    apellidosusuario: [
      { type: "required", message: "Los apellidos son requeridos" },
    ],
    RFCUsuario: [
      { type: "maxlength", message: "El RFC debe de ser de 12 a 13 digitos" },
      { type: "minlength", message: "El RFC debe de ser de 12 a 13 digitos" },
    ],
    direccion: [{ type: "required", message: "La dirección es requerida" }],
    telefonousuario: [
      { type: "required", message: "El telefono es requerido" },
      { type: "minlength", message: "El telefono debe de ser de 10 digitos" },
      { type: "maxlength", message: "El telefono debe de ser de 10 digitos" },
    ],
    correocontacto: [
      { type: "required", message: "El correo es requerido" },
      { type: "pattern", message: "El correo no es un correo valido" },
    ],
    edad: [
      { type: "required", message: "La edad es requerida" },
      {
        type: "maxlength",
        message: "La edad no debe superar los tres digitos",
      },
    ],
    perfil: [{ type: "required", message: "El perfil es requerido" }],
    sucursal: [{ type: "required", message: "La sucursal es requerida" }],
    gruposanguineo: [
      { type: "required", message: "El grupo sanguineo es requerido" },
    ],
    genero: [{ type: "required", message: "El genero es requerido" }],
    estadocivil: [
      { type: "required", message: "El estado civil es requerido" },
    ],
    titulo: [
      { type: "required", message: "El titulo es requerido" },
      {
        type: "maxlength",
        message: "El titulo debe ser de un maximo de 80 caracteres",
      },
    ],
    especialidad: [
      { type: "required", message: "La especialidad es requerida" },
    ],
    cedula: [
      { type: "required", message: "La cedula es requerida" },
      { type: "maxlength", message: "La cedula debe ser de maximo 8 digitos" },
      { type: "minlength", message: "La cedula debe ser de minimo 7 digitos" },
    ],
  };

  ngOnInit() {
    if (this.idClinica > 0) {
      this.GetSuc();
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
  }

  public handleAddressChange(address: Address) {
    this.roomForm.patchValue({
      direccion: address.formatted_address,
    });
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  // select de sucursales
  GetSuc() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 0, idclinica: this.idClinica };
    const URL: any = this.baseURL + "categorias.php";
    this.httpClient
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.sucursales = respuesta;
      });
  }

  //comprobar si el horario esta lleno o vacio
  comprobarhorario() {
    if (
      this.checklunes6a7 !== false ||
      this.checklunes7a8 !== false ||
      this.checklunes8a9 !== false ||
      this.checklunes9a10 !== false ||
      this.checklunes10a11 !== false ||
      this.checklunes11a12 !== false ||
      this.checklunes12a13 !== false ||
      this.checklunes13a14 !== false ||
      this.checklunes14a15 !== false ||
      this.checklunes15a16 !== false ||
      this.checklunes16a17 !== false ||
      this.checklunes17a18 !== false ||
      this.checklunes18a19 !== false ||
      this.checklunes19a20 !== false ||
      this.checklunes20a21 !== false ||
      this.checkmartes6a7 !== false ||
      this.checkmartes7a8 !== false ||
      this.checkmartes8a9 !== false ||
      this.checkmartes9a10 !== false ||
      this.checkmartes10a11 !== false ||
      this.checkmartes11a12 !== false ||
      this.checkmartes12a13 !== false ||
      this.checkmartes13a14 !== false ||
      this.checkmartes14a15 !== false ||
      this.checkmartes15a16 !== false ||
      this.checkmartes16a17 !== false ||
      this.checkmartes17a18 !== false ||
      this.checkmartes18a19 !== false ||
      this.checkmartes19a20 !== false ||
      this.checkmartes20a21 !== false ||
      this.checkmiercoles6a7 !== false ||
      this.checkmiercoles7a8 !== false ||
      this.checkmiercoles8a9 !== false ||
      this.checkmiercoles9a10 !== false ||
      this.checkmiercoles10a11 !== false ||
      this.checkmiercoles11a12 !== false ||
      this.checkmiercoles12a13 !== false ||
      this.checkmiercoles13a14 !== false ||
      this.checkmiercoles14a15 !== false ||
      this.checkmiercoles15a16 !== false ||
      this.checkmiercoles16a17 !== false ||
      this.checkmiercoles17a18 !== false ||
      this.checkmiercoles18a19 !== false ||
      this.checkmiercoles19a20 !== false ||
      this.checkmiercoles20a21 !== false ||
      this.checkjueves6a7 !== false ||
      this.checkjueves7a8 !== false ||
      this.checkjueves8a9 !== false ||
      this.checkjueves9a10 !== false ||
      this.checkjueves10a11 !== false ||
      this.checkjueves11a12 !== false ||
      this.checkjueves12a13 !== false ||
      this.checkjueves13a14 !== false ||
      this.checkjueves14a15 !== false ||
      this.checkjueves15a16 !== false ||
      this.checkjueves16a17 !== false ||
      this.checkjueves17a18 !== false ||
      this.checkjueves18a19 !== false ||
      this.checkjueves19a20 !== false ||
      this.checkjueves20a21 !== false ||
      this.checkviernes6a7 !== false ||
      this.checkviernes7a8 !== false ||
      this.checkviernes8a9 !== false ||
      this.checkviernes9a10 !== false ||
      this.checkviernes10a11 !== false ||
      this.checkviernes11a12 !== false ||
      this.checkviernes12a13 !== false ||
      this.checkviernes13a14 !== false ||
      this.checkviernes14a15 !== false ||
      this.checkviernes15a16 !== false ||
      this.checkviernes16a17 !== false ||
      this.checkviernes17a18 !== false ||
      this.checkviernes18a19 !== false ||
      this.checkviernes19a20 !== false ||
      this.checkviernes20a21 !== false ||
      this.checksabado6a7 !== false ||
      this.checksabado7a8 !== false ||
      this.checksabado8a9 !== false ||
      this.checksabado9a10 !== false ||
      this.checksabado10a11 !== false ||
      this.checksabado11a12 !== false ||
      this.checksabado12a13 !== false ||
      this.checksabado13a14 !== false ||
      this.checksabado14a15 !== false ||
      this.checksabado15a16 !== false ||
      this.checksabado16a17 !== false ||
      this.checksabado17a18 !== false ||
      this.checksabado18a19 !== false ||
      this.checksabado19a20 !== false ||
      this.checksabado20a21 !== false ||
      this.checkdomingo6a7 !== false ||
      this.checkdomingo7a8 !== false ||
      this.checkdomingo8a9 !== false ||
      this.checkdomingo9a10 !== false ||
      this.checkdomingo10a11 !== false ||
      this.checkdomingo11a12 !== false ||
      this.checkdomingo12a13 !== false ||
      this.checkdomingo13a14 !== false ||
      this.checkdomingo14a15 !== false ||
      this.checkdomingo15a16 !== false ||
      this.checkdomingo16a17 !== false ||
      this.checkdomingo17a18 !== false ||
      this.checkdomingo18a19 !== false ||
      this.checkdomingo19a20 !== false ||
      this.checkdomingo20a21 !== false
    ) {
      this.crearhorario().subscribe(
        (res) => {
          if (res !== null) {
            if (
              res.toString() === "Ya existe un usuario con los mismos datos"
            ) {
              this.swalalertmensaje(
                "error",
                "Ya existe un usuario con los mismos datos"
              );
            } else {
              this.cargandoImagen(14, res.toString());
            }
          } else {
            this.swalalertmensaje(
              "error",
              "Ocurrio un error, intente de nuevo por favor"
            );
          }
        },
        (error) => {
          this.swalalertmensaje(
            "error",
            "Ocurrio un error, intente de nuevo por favor"
          );
        }
      );
    } else {
      this.cargandoImagen(15, 0);
    }
  }

  //metodo para llenar el horario
  crearhorario() {
    console.log("Form Value", this.roomForm.value);
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = {
      caso: 0,
      formU: this.roomForm.value,
      lunes6a7: this.checklunes6a7,
      lunes7a8: this.checklunes7a8,
      lunes8a9: this.checklunes8a9,
      lunes9a10: this.checklunes9a10,
      lunes10a11: this.checklunes10a11,
      lunes11a12: this.checklunes11a12,
      lunes12a13: this.checklunes12a13,
      lunes13a14: this.checklunes13a14,
      lunes14a15: this.checklunes14a15,
      lunes15a16: this.checklunes15a16,
      lunes16a17: this.checklunes16a17,
      lunes17a18: this.checklunes17a18,
      lunes18a19: this.checklunes18a19,
      lunes19a20: this.checklunes19a20,
      lunes20a21: this.checklunes20a21,
      martes6a7: this.checkmartes6a7,
      martes7a8: this.checkmartes7a8,
      martes8a9: this.checkmartes8a9,
      martes9a10: this.checkmartes9a10,
      martes10a11: this.checkmartes10a11,
      martes11a12: this.checkmartes11a12,
      martes12a13: this.checkmartes12a13,
      martes13a14: this.checkmartes13a14,
      martes14a15: this.checkmartes14a15,
      martes15a16: this.checkmartes15a16,
      martes16a17: this.checkmartes16a17,
      martes17a18: this.checkmartes17a18,
      martes18a19: this.checkmartes18a19,
      martes19a20: this.checkmartes19a20,
      martes20a21: this.checkmartes20a21,
      miercoles6a7: this.checkmiercoles6a7,
      miercoles7a8: this.checkmiercoles7a8,
      miercoles8a9: this.checkmiercoles8a9,
      miercoles9a10: this.checkmiercoles9a10,
      miercoles10a11: this.checkmiercoles10a11,
      miercoles11a12: this.checkmiercoles11a12,
      miercoles12a13: this.checkmiercoles12a13,
      miercoles13a14: this.checkmiercoles13a14,
      miercoles14a15: this.checkmiercoles14a15,
      miercoles15a16: this.checkmiercoles15a16,
      miercoles16a17: this.checkmiercoles16a17,
      miercoles17a18: this.checkmiercoles17a18,
      miercoles18a19: this.checkmiercoles18a19,
      miercoles19a20: this.checkmiercoles19a20,
      miercoles20a21: this.checkmiercoles20a21,
      jueves6a7: this.checkjueves6a7,
      jueves7a8: this.checkjueves7a8,
      jueves8a9: this.checkjueves8a9,
      jueves9a10: this.checkjueves9a10,
      jueves10a11: this.checkjueves10a11,
      jueves11a12: this.checkjueves11a12,
      jueves12a13: this.checkjueves12a13,
      jueves13a14: this.checkjueves13a14,
      jueves14a15: this.checkjueves14a15,
      jueves15a16: this.checkjueves15a16,
      jueves16a17: this.checkjueves16a17,
      jueves17a18: this.checkjueves17a18,
      jueves18a19: this.checkjueves18a19,
      jueves19a20: this.checkjueves19a20,
      jueves20a21: this.checkjueves20a21,
      viernes6a7: this.checkviernes6a7,
      viernes7a8: this.checkviernes7a8,
      viernes8a9: this.checkviernes8a9,
      viernes9a10: this.checkviernes9a10,
      viernes10a11: this.checkviernes10a11,
      viernes11a12: this.checkviernes11a12,
      viernes12a13: this.checkviernes12a13,
      viernes13a14: this.checkviernes13a14,
      viernes14a15: this.checkviernes14a15,
      viernes15a16: this.checkviernes15a16,
      viernes16a17: this.checkviernes16a17,
      viernes17a18: this.checkviernes17a18,
      viernes18a19: this.checkviernes18a19,
      viernes19a20: this.checkviernes19a20,
      viernes20a21: this.checkviernes20a21,
      sabado6a7: this.checksabado6a7,
      sabado7a8: this.checksabado7a8,
      sabado8a9: this.checksabado8a9,
      sabado9a10: this.checksabado9a10,
      sabado10a11: this.checksabado10a11,
      sabado11a12: this.checksabado11a12,
      sabado12a13: this.checksabado12a13,
      sabado13a14: this.checksabado13a14,
      sabado14a15: this.checksabado14a15,
      sabado15a16: this.checksabado15a16,
      sabado16a17: this.checksabado16a17,
      sabado17a18: this.checksabado17a18,
      sabado18a19: this.checksabado18a19,
      sabado19a20: this.checksabado19a20,
      sabado20a21: this.checksabado20a21,
      domingo6a7: this.checkdomingo6a7,
      domingo7a8: this.checkdomingo7a8,
      domingo8a9: this.checkdomingo8a9,
      domingo9a10: this.checkdomingo9a10,
      domingo10a11: this.checkdomingo10a11,
      domingo11a12: this.checkdomingo11a12,
      domingo12a13: this.checkdomingo12a13,
      domingo13a14: this.checkdomingo13a14,
      domingo14a15: this.checkdomingo14a15,
      domingo15a16: this.checkdomingo15a16,
      domingo16a17: this.checkdomingo16a17,
      domingo17a18: this.checkdomingo17a18,
      domingo18a19: this.checkdomingo18a19,
      domingo19a20: this.checkdomingo19a20,
      domingo20a21: this.checkdomingo20a21,
    };
    const URL: any = this.baseURL + "horariosusuarios.php";
    return this.httpClient.post(URL, JSON.stringify(options), headers);
  }

  // metodo para subir la imagen y guardar el resultado en base de datos
  cargandoImagen(caso, idH) {
    this.mensajesubiendoinf();
    if (this.roomForm.get("fechanaci").value) {
      this.roomForm.controls["fechanaci"].setValue(
        moment(this.roomForm.controls["fechanaci"].value).format("YYYY-MM-DD")
      );
    } else {
      this.roomForm.controls["fechanaci"].setValue("");
    }
    if (this.foto === "") {
      this.roomForm.controls["uploadImg"].setValue("user_default.png");
      this.agregarUsuario(caso, idH).subscribe(
        (res) => {
          if (res !== null) {
            if (
              res.toString() === "Ya existe un usuario con los mismos datos"
            ) {
              this.swalalertmensaje(
                "error",
                "Ya existe un usuario con los mismos datos"
              );
            } else {
              Swal.close();
              this.swalalertmensaje(
                "success",
                "Usuario agregado correctamente"
              );
              this.router.navigate([
                "/admin/administrador/usuario/listado-usuarios-activos",
              ]);
            }
          } else {
            this.swalalertmensaje(
              "error",
              "Ocurrio un error, intente de nuevo por favor"
            );
          }
        },
        (error) => {
          this.swalalertmensaje(
            "error",
            "Ocurrio un error, intente de nuevo por favor"
          );
        }
      );
    } else {
      this.enviandoImagen
        .postFileImagen(this.fotoFinal[0], this.foto)
        .subscribe((response) => {
          this.respuestaImagenEnviada = response;
          if (this.respuestaImagenEnviada.msj === "Imagen subida") {
            this.roomForm.controls["uploadImg"].setValue(this.foto);
            this.agregarUsuario(caso, idH).subscribe(
              (res) => {
                if (res !== null) {
                  if (
                    res.toString() ===
                    "Ya existe un usuario con los mismos datos"
                  ) {
                    this.swalalertmensaje(
                      "error",
                      "Ya existe un usuario con los mismos datos"
                    );
                  } else {
                    Swal.close();
                    this.swalalertmensaje(
                      "success",
                      "Usuario agregado correctamente"
                    );
                    this.router.navigate([
                      "/admin/administrador/usuario/listado-usuarios-activos",
                    ]);
                  }
                } else {
                  this.swalalertmensaje(
                    "error",
                    "Ocurrio un error, intente de nuevo por favor"
                  );
                }
              },
              (error) => {
                this.swalalertmensaje(
                  "error",
                  "Ocurrio un error, intente de nuevo por favor"
                );
              }
            );
          } else {
            this.swalalertmensaje(
              "error",
              "Ocurrio un error, intente de nuevo por favor"
            );
          }
        });
    }
  }
  onInputClick(event) {
    event.target.value = "";
  }
  //metdo para obtener la imagen de input file
  Subirimagen(event, files: FileList, fileInput: any) {
    const Numero1 = Math.floor(Math.random() * 10001).toString();
    const Numero2 = Math.floor(Math.random() * 1001).toString();
    // Nombre del archivo
    this.Archivoseleccionado = event.target.files[0];
    this.nombrefoto = event.target.files[0].name;
    this.foto = Numero1 + Numero2 + this.nombrefoto.replace(/ /g, "");
    this.fotoFinal = files;
    this.Dataarchivo = <File>fileInput.target.files[0];
  }
  // agrega usuario con fecha de nacimient
  agregarUsuario(caso, idH) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = {
      caso: caso,
      formU: this.roomForm.getRawValue(),
      esp: this.roomForm.value.especialidad,
      idhorario: idH,
    };
    const URL: any = this.baseURL + "usuarios.php";
    return this.httpClient.post(URL, JSON.stringify(options), headers);
  }

  mensajesubiendoinf() {
    Swal.fire({
      title: "Subiendo información",
      html: "Espere un momento por favor",
      allowOutsideClick: false,
      showCancelButton: false,
      showConfirmButton: false,
    });
  }
  // Calcula la edad del cliente
  public CalculateAge(): void {
    if (this.roomForm.get("fechanaci").value) {
      let timeDiff = Math.abs(
        Date.now() - new Date(this.roomForm.get("fechanaci").value).getTime()
      );
      this.edad = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
      this.roomForm.controls.edad.setValue(this.edad);
    }
  }
  // aleta de mensaje
  swalalertmensaje(icon, mensaje) {
    Swal.fire({
      position: "center",
      icon: icon,
      title: mensaje,
      showConfirmButton: true,
      allowOutsideClick: false,
      confirmButtonText: "Aceptar",
    });
  }

  // metodo para convertir primera letra de cada palabra en mayuscula
  firstLetterUpperCase(id) {
    const texto = (<HTMLInputElement>document.getElementById(id)).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      (<HTMLInputElement>document.getElementById(id)).value = textofinal;
    }
  }

  toUpperCase(id) {
    let texto = (<HTMLInputElement>document.getElementById(id)).value;
    let cadena = texto.toUpperCase().split(" ");
    for (let i = 0; i < cadena.length; i++) {
      cadena[i] = cadena[i].charAt(0).toUpperCase() + cadena[i].substring(1);
    }
    texto = cadena.join(" ");
    (<HTMLInputElement>document.getElementById(id)).value = texto;
  }

  firstWordUpercase(id) {
    let texto = (<HTMLInputElement>document.getElementById(id)).value;
    let cadena = texto.toLowerCase().split(" ");
    for (let i = 0; i < cadena.length; i++) {
      cadena[i] = cadena[i].charAt(0).toUpperCase() + cadena[i].substring(1);
    }
    texto = cadena.join(" ");
    (<HTMLInputElement>document.getElementById(id)).value = texto;
  }

  //limpiar horario de usuario
  valorespordefectohorario() {
    this.tdlunes6a7 = this.celdanoseleccionada;
    this.tdlunes7a8 = this.celdanoseleccionada;
    this.tdlunes8a9 = this.celdanoseleccionada;
    this.tdlunes9a10 = this.celdanoseleccionada;
    this.tdlunes10a11 = this.celdanoseleccionada;
    this.tdlunes11a12 = this.celdanoseleccionada;
    this.tdlunes12a13 = this.celdanoseleccionada;
    this.tdlunes13a14 = this.celdanoseleccionada;
    this.tdlunes14a15 = this.celdanoseleccionada;
    this.tdlunes15a16 = this.celdanoseleccionada;
    this.tdlunes16a17 = this.celdanoseleccionada;
    this.tdlunes17a18 = this.celdanoseleccionada;
    this.tdlunes18a19 = this.celdanoseleccionada;
    this.tdlunes19a20 = this.celdanoseleccionada;
    this.tdlunes20a21 = this.celdanoseleccionada;
    this.tdmartes6a7 = this.celdanoseleccionada;
    this.tdmartes7a8 = this.celdanoseleccionada;
    this.tdmartes8a9 = this.celdanoseleccionada;
    this.tdmartes9a10 = this.celdanoseleccionada;
    this.tdmartes10a11 = this.celdanoseleccionada;
    this.tdmartes11a12 = this.celdanoseleccionada;
    this.tdmartes12a13 = this.celdanoseleccionada;
    this.tdmartes13a14 = this.celdanoseleccionada;
    this.tdmartes14a15 = this.celdanoseleccionada;
    this.tdmartes15a16 = this.celdanoseleccionada;
    this.tdmartes16a17 = this.celdanoseleccionada;
    this.tdmartes17a18 = this.celdanoseleccionada;
    this.tdmartes18a19 = this.celdanoseleccionada;
    this.tdmartes19a20 = this.celdanoseleccionada;
    this.tdmartes20a21 = this.celdanoseleccionada;
    this.tdmiercoles6a7 = this.celdanoseleccionada;
    this.tdmiercoles7a8 = this.celdanoseleccionada;
    this.tdmiercoles8a9 = this.celdanoseleccionada;
    this.tdmiercoles9a10 = this.celdanoseleccionada;
    this.tdmiercoles10a11 = this.celdanoseleccionada;
    this.tdmiercoles11a12 = this.celdanoseleccionada;
    this.tdmiercoles12a13 = this.celdanoseleccionada;
    this.tdmiercoles13a14 = this.celdanoseleccionada;
    this.tdmiercoles14a15 = this.celdanoseleccionada;
    this.tdmiercoles15a16 = this.celdanoseleccionada;
    this.tdmiercoles16a17 = this.celdanoseleccionada;
    this.tdmiercoles17a18 = this.celdanoseleccionada;
    this.tdmiercoles18a19 = this.celdanoseleccionada;
    this.tdmiercoles19a20 = this.celdanoseleccionada;
    this.tdmiercoles20a21 = this.celdanoseleccionada;
    this.tdjueves6a7 = this.celdanoseleccionada;
    this.tdjueves7a8 = this.celdanoseleccionada;
    this.tdjueves8a9 = this.celdanoseleccionada;
    this.tdjueves9a10 = this.celdanoseleccionada;
    this.tdjueves10a11 = this.celdanoseleccionada;
    this.tdjueves11a12 = this.celdanoseleccionada;
    this.tdjueves12a13 = this.celdanoseleccionada;
    this.tdjueves13a14 = this.celdanoseleccionada;
    this.tdjueves14a15 = this.celdanoseleccionada;
    this.tdjueves15a16 = this.celdanoseleccionada;
    this.tdjueves16a17 = this.celdanoseleccionada;
    this.tdjueves17a18 = this.celdanoseleccionada;
    this.tdjueves18a19 = this.celdanoseleccionada;
    this.tdjueves19a20 = this.celdanoseleccionada;
    this.tdjueves20a21 = this.celdanoseleccionada;
    this.tdviernes6a7 = this.celdanoseleccionada;
    this.tdviernes7a8 = this.celdanoseleccionada;
    this.tdviernes8a9 = this.celdanoseleccionada;
    this.tdviernes9a10 = this.celdanoseleccionada;
    this.tdviernes10a11 = this.celdanoseleccionada;
    this.tdviernes11a12 = this.celdanoseleccionada;
    this.tdviernes12a13 = this.celdanoseleccionada;
    this.tdviernes13a14 = this.celdanoseleccionada;
    this.tdviernes14a15 = this.celdanoseleccionada;
    this.tdviernes15a16 = this.celdanoseleccionada;
    this.tdviernes16a17 = this.celdanoseleccionada;
    this.tdviernes17a18 = this.celdanoseleccionada;
    this.tdviernes18a19 = this.celdanoseleccionada;
    this.tdviernes19a20 = this.celdanoseleccionada;
    this.tdviernes20a21 = this.celdanoseleccionada;
    this.tdsabado6a7 = this.celdanoseleccionada;
    this.tdsabado7a8 = this.celdanoseleccionada;
    this.tdsabado8a9 = this.celdanoseleccionada;
    this.tdsabado9a10 = this.celdanoseleccionada;
    this.tdsabado10a11 = this.celdanoseleccionada;
    this.tdsabado11a12 = this.celdanoseleccionada;
    this.tdsabado12a13 = this.celdanoseleccionada;
    this.tdsabado13a14 = this.celdanoseleccionada;
    this.tdsabado14a15 = this.celdanoseleccionada;
    this.tdsabado15a16 = this.celdanoseleccionada;
    this.tdsabado16a17 = this.celdanoseleccionada;
    this.tdsabado17a18 = this.celdanoseleccionada;
    this.tdsabado18a19 = this.celdanoseleccionada;
    this.tdsabado19a20 = this.celdanoseleccionada;
    this.tdsabado20a21 = this.celdanoseleccionada;
    this.tddomingo6a7 = this.celdanoseleccionada;
    this.tddomingo7a8 = this.celdanoseleccionada;
    this.tddomingo8a9 = this.celdanoseleccionada;
    this.tddomingo9a10 = this.celdanoseleccionada;
    this.tddomingo10a11 = this.celdanoseleccionada;
    this.tddomingo11a12 = this.celdanoseleccionada;
    this.tddomingo12a13 = this.celdanoseleccionada;
    this.tddomingo13a14 = this.celdanoseleccionada;
    this.tddomingo14a15 = this.celdanoseleccionada;
    this.tddomingo15a16 = this.celdanoseleccionada;
    this.tddomingo16a17 = this.celdanoseleccionada;
    this.tddomingo17a18 = this.celdanoseleccionada;
    this.tddomingo18a19 = this.celdanoseleccionada;
    this.tddomingo19a20 = this.celdanoseleccionada;
    this.tddomingo20a21 = this.celdanoseleccionada;
    this.checklunes6a7 = false;
    this.checklunes7a8 = false;
    this.checklunes8a9 = false;
    this.checklunes9a10 = false;
    this.checklunes10a11 = false;
    this.checklunes11a12 = false;
    this.checklunes12a13 = false;
    this.checklunes13a14 = false;
    this.checklunes14a15 = false;
    this.checklunes15a16 = false;
    this.checklunes16a17 = false;
    this.checklunes17a18 = false;
    this.checklunes18a19 = false;
    this.checklunes19a20 = false;
    this.checklunes20a21 = false;
    this.checkmartes6a7 = false;
    this.checkmartes7a8 = false;
    this.checkmartes8a9 = false;
    this.checkmartes9a10 = false;
    this.checkmartes10a11 = false;
    this.checkmartes11a12 = false;
    this.checkmartes12a13 = false;
    this.checkmartes13a14 = false;
    this.checkmartes14a15 = false;
    this.checkmartes15a16 = false;
    this.checkmartes16a17 = false;
    this.checkmartes17a18 = false;
    this.checkmartes18a19 = false;
    this.checkmartes19a20 = false;
    this.checkmartes20a21 = false;
    this.checkmiercoles6a7 = false;
    this.checkmiercoles7a8 = false;
    this.checkmiercoles8a9 = false;
    this.checkmiercoles9a10 = false;
    this.checkmiercoles10a11 = false;
    this.checkmiercoles11a12 = false;
    this.checkmiercoles12a13 = false;
    this.checkmiercoles13a14 = false;
    this.checkmiercoles14a15 = false;
    this.checkmiercoles15a16 = false;
    this.checkmiercoles16a17 = false;
    this.checkmiercoles17a18 = false;
    this.checkmiercoles18a19 = false;
    this.checkmiercoles19a20 = false;
    this.checkmiercoles20a21 = false;
    this.checkjueves6a7 = false;
    this.checkjueves7a8 = false;
    this.checkjueves8a9 = false;
    this.checkjueves9a10 = false;
    this.checkjueves10a11 = false;
    this.checkjueves11a12 = false;
    this.checkjueves12a13 = false;
    this.checkjueves13a14 = false;
    this.checkjueves14a15 = false;
    this.checkjueves15a16 = false;
    this.checkjueves16a17 = false;
    this.checkjueves17a18 = false;
    this.checkjueves18a19 = false;
    this.checkjueves19a20 = false;
    this.checkjueves20a21 = false;
    this.checkviernes6a7 = false;
    this.checkviernes7a8 = false;
    this.checkviernes8a9 = false;
    this.checkviernes9a10 = false;
    this.checkviernes10a11 = false;
    this.checkviernes11a12 = false;
    this.checkviernes12a13 = false;
    this.checkviernes13a14 = false;
    this.checkviernes14a15 = false;
    this.checkviernes15a16 = false;
    this.checkviernes16a17 = false;
    this.checkviernes17a18 = false;
    this.checkviernes18a19 = false;
    this.checkviernes19a20 = false;
    this.checkviernes20a21 = false;
    this.checksabado6a7 = false;
    this.checksabado7a8 = false;
    this.checksabado8a9 = false;
    this.checksabado9a10 = false;
    this.checksabado10a11 = false;
    this.checksabado11a12 = false;
    this.checksabado12a13 = false;
    this.checksabado13a14 = false;
    this.checksabado14a15 = false;
    this.checksabado15a16 = false;
    this.checksabado16a17 = false;
    this.checksabado17a18 = false;
    this.checksabado18a19 = false;
    this.checksabado19a20 = false;
    this.checksabado20a21 = false;
    this.checkdomingo6a7 = false;
    this.checkdomingo7a8 = false;
    this.checkdomingo8a9 = false;
    this.checkdomingo9a10 = false;
    this.checkdomingo10a11 = false;
    this.checkdomingo11a12 = false;
    this.checkdomingo12a13 = false;
    this.checkdomingo13a14 = false;
    this.checkdomingo14a15 = false;
    this.checkdomingo15a16 = false;
    this.checkdomingo16a17 = false;
    this.checkdomingo17a18 = false;
    this.checkdomingo18a19 = false;
    this.checkdomingo19a20 = false;
    this.checkdomingo20a21 = false;
    this.todoeldialunes = false;
    this.todoeldiamartes = false;
    this.todoeldiamiercoles = false;
    this.todoeldiajueves = false;
    this.todoeldiaviernes = false;
    this.todoeldiasabado = false;
    this.todoeldiadomingo = false;
  }

  //abrir modal de calendario

  abrirmodal(content) {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: "static",
      keyboard: false,
      centered: true,
      ariaLabelledBy: "modal-basic-title",
      size: "xl",
    };
    this.modalService.open(content, ngbModalOptions);
  }

  cerrarmodal(valor) {
    if (valor === 1) {
      this.valorespordefectohorario();
    }
    this.modalService.dismissAll();
  }

  cambiarcheck(valor) {
    if (valor === 1) {
      if (this.checklunes6a7 === false) {
        this.tdlunes6a7 = this.celdanoseleccionada;
      } else {
        this.tdlunes6a7 = this.celdaseleccionada;
      }
    } else if (valor === 2) {
      if (this.checkmartes6a7 === false) {
        this.tdmartes6a7 = this.celdanoseleccionada;
      } else {
        this.tdmartes6a7 = this.celdaseleccionada;
      }
    } else if (valor === 3) {
      if (this.checkmiercoles6a7 === false) {
        this.tdmiercoles6a7 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles6a7 = this.celdaseleccionada;
      }
    } else if (valor === 4) {
      if (this.checkjueves6a7 === false) {
        this.tdjueves6a7 = this.celdanoseleccionada;
      } else {
        this.tdjueves6a7 = this.celdaseleccionada;
      }
    } else if (valor === 5) {
      if (this.checkviernes6a7 === false) {
        this.tdviernes6a7 = this.celdanoseleccionada;
      } else {
        this.tdviernes6a7 = this.celdaseleccionada;
      }
    } else if (valor === 6) {
      if (this.checksabado6a7 === false) {
        this.tdsabado6a7 = this.celdanoseleccionada;
      } else {
        this.tdsabado6a7 = this.celdaseleccionada;
      }
    } else if (valor === 7) {
      if (this.checkdomingo6a7 === false) {
        this.tddomingo6a7 = this.celdanoseleccionada;
      } else {
        this.tddomingo6a7 = this.celdaseleccionada;
      }
    } else if (valor === 8) {
      if (this.checklunes7a8 === false) {
        this.tdlunes7a8 = this.celdanoseleccionada;
      } else {
        this.tdlunes7a8 = this.celdaseleccionada;
      }
    } else if (valor === 9) {
      if (this.checkmartes7a8 === false) {
        this.tdmartes7a8 = this.celdanoseleccionada;
      } else {
        this.tdmartes7a8 = this.celdaseleccionada;
      }
    } else if (valor === 10) {
      if (this.checkmiercoles7a8 === false) {
        this.tdmiercoles7a8 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles7a8 = this.celdaseleccionada;
      }
    } else if (valor === 11) {
      if (this.checkjueves7a8 === false) {
        this.tdjueves7a8 = this.celdanoseleccionada;
      } else {
        this.tdjueves7a8 = this.celdaseleccionada;
      }
    } else if (valor === 12) {
      if (this.checkviernes7a8 === false) {
        this.tdviernes7a8 = this.celdanoseleccionada;
      } else {
        this.tdviernes7a8 = this.celdaseleccionada;
      }
    } else if (valor === 13) {
      if (this.checksabado7a8 === false) {
        this.tdsabado7a8 = this.celdanoseleccionada;
      } else {
        this.tdsabado7a8 = this.celdaseleccionada;
      }
    } else if (valor === 14) {
      if (this.checkdomingo7a8 === false) {
        this.tddomingo7a8 = this.celdanoseleccionada;
      } else {
        this.tddomingo7a8 = this.celdaseleccionada;
      }
    } else if (valor === 15) {
      if (this.checklunes8a9 === false) {
        this.tdlunes8a9 = this.celdanoseleccionada;
      } else {
        this.tdlunes8a9 = this.celdaseleccionada;
      }
    } else if (valor === 16) {
      if (this.checkmartes8a9 === false) {
        this.tdmartes8a9 = this.celdanoseleccionada;
      } else {
        this.tdmartes8a9 = this.celdaseleccionada;
      }
    } else if (valor === 17) {
      if (this.checkmiercoles8a9 === false) {
        this.tdmiercoles8a9 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles8a9 = this.celdaseleccionada;
      }
    } else if (valor === 18) {
      if (this.checkjueves8a9 === false) {
        this.tdjueves8a9 = this.celdanoseleccionada;
      } else {
        this.tdjueves8a9 = this.celdaseleccionada;
      }
    } else if (valor === 19) {
      if (this.checkviernes8a9 === false) {
        this.tdviernes8a9 = this.celdanoseleccionada;
      } else {
        this.tdviernes8a9 = this.celdaseleccionada;
      }
    } else if (valor === 20) {
      if (this.checksabado8a9 === false) {
        this.tdsabado8a9 = this.celdanoseleccionada;
      } else {
        this.tdsabado8a9 = this.celdaseleccionada;
      }
    } else if (valor === 21) {
      if (this.checkdomingo8a9 === false) {
        this.tddomingo8a9 = this.celdanoseleccionada;
      } else {
        this.tddomingo8a9 = this.celdaseleccionada;
      }
    } else if (valor === 22) {
      if (this.checklunes9a10 === false) {
        this.tdlunes9a10 = this.celdanoseleccionada;
      } else {
        this.tdlunes9a10 = this.celdaseleccionada;
      }
    } else if (valor === 23) {
      if (this.checkmartes9a10 === false) {
        this.tdmartes9a10 = this.celdanoseleccionada;
      } else {
        this.tdmartes9a10 = this.celdaseleccionada;
      }
    } else if (valor === 24) {
      if (this.checkmiercoles9a10 === false) {
        this.tdmiercoles9a10 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles9a10 = this.celdaseleccionada;
      }
    } else if (valor === 25) {
      if (this.checkjueves9a10 === false) {
        this.tdjueves9a10 = this.celdanoseleccionada;
      } else {
        this.tdjueves9a10 = this.celdaseleccionada;
      }
    } else if (valor === 26) {
      if (this.checkviernes9a10 === false) {
        this.tdviernes9a10 = this.celdanoseleccionada;
      } else {
        this.tdviernes9a10 = this.celdaseleccionada;
      }
    } else if (valor === 27) {
      if (this.checksabado9a10 === false) {
        this.tdsabado9a10 = this.celdanoseleccionada;
      } else {
        this.tdsabado9a10 = this.celdaseleccionada;
      }
    } else if (valor === 28) {
      if (this.checkdomingo9a10 === false) {
        this.tddomingo9a10 = this.celdanoseleccionada;
      } else {
        this.tddomingo9a10 = this.celdaseleccionada;
      }
    } else if (valor === 29) {
      if (this.checklunes10a11 === false) {
        this.tdlunes10a11 = this.celdanoseleccionada;
      } else {
        this.tdlunes10a11 = this.celdaseleccionada;
      }
    } else if (valor === 30) {
      if (this.checkmartes10a11 === false) {
        this.tdmartes10a11 = this.celdanoseleccionada;
      } else {
        this.tdmartes10a11 = this.celdaseleccionada;
      }
    } else if (valor === 31) {
      if (this.checkmiercoles10a11 === false) {
        this.tdmiercoles10a11 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles10a11 = this.celdaseleccionada;
      }
    } else if (valor === 32) {
      if (this.checkjueves10a11 === false) {
        this.tdjueves10a11 = this.celdanoseleccionada;
      } else {
        this.tdjueves10a11 = this.celdaseleccionada;
      }
    } else if (valor === 33) {
      if (this.checkviernes10a11 === false) {
        this.tdviernes10a11 = this.celdanoseleccionada;
      } else {
        this.tdviernes10a11 = this.celdaseleccionada;
      }
    } else if (valor === 34) {
      if (this.checksabado10a11 === false) {
        this.tdsabado10a11 = this.celdanoseleccionada;
      } else {
        this.tdsabado10a11 = this.celdaseleccionada;
      }
    } else if (valor === 35) {
      if (this.checkdomingo10a11 === false) {
        this.tddomingo10a11 = this.celdanoseleccionada;
      } else {
        this.tddomingo10a11 = this.celdaseleccionada;
      }
    } else if (valor === 36) {
      if (this.checklunes11a12 === false) {
        this.tdlunes11a12 = this.celdanoseleccionada;
      } else {
        this.tdlunes11a12 = this.celdaseleccionada;
      }
    } else if (valor === 37) {
      if (this.checkmartes11a12 === false) {
        this.tdmartes11a12 = this.celdanoseleccionada;
      } else {
        this.tdmartes11a12 = this.celdaseleccionada;
      }
    } else if (valor === 38) {
      if (this.checkmiercoles11a12 === false) {
        this.tdmiercoles11a12 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles11a12 = this.celdaseleccionada;
      }
    } else if (valor === 39) {
      if (this.checkjueves11a12 === false) {
        this.tdjueves11a12 = this.celdanoseleccionada;
      } else {
        this.tdjueves11a12 = this.celdaseleccionada;
      }
    } else if (valor === 40) {
      if (this.checkviernes11a12 === false) {
        this.tdviernes11a12 = this.celdanoseleccionada;
      } else {
        this.tdviernes11a12 = this.celdaseleccionada;
      }
    } else if (valor === 41) {
      if (this.checksabado11a12 === false) {
        this.tdsabado11a12 = this.celdanoseleccionada;
      } else {
        this.tdsabado11a12 = this.celdaseleccionada;
      }
    } else if (valor === 42) {
      if (this.checkdomingo11a12 === false) {
        this.tddomingo11a12 = this.celdanoseleccionada;
      } else {
        this.tddomingo11a12 = this.celdaseleccionada;
      }
    } else if (valor === 43) {
      if (this.checklunes12a13 === false) {
        this.tdlunes12a13 = this.celdanoseleccionada;
      } else {
        this.tdlunes12a13 = this.celdaseleccionada;
      }
    } else if (valor === 44) {
      if (this.checkmartes12a13 === false) {
        this.tdmartes12a13 = this.celdanoseleccionada;
      } else {
        this.tdmartes12a13 = this.celdaseleccionada;
      }
    } else if (valor === 45) {
      if (this.checkmiercoles12a13 === false) {
        this.tdmiercoles12a13 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles12a13 = this.celdaseleccionada;
      }
    } else if (valor === 46) {
      if (this.checkjueves12a13 === false) {
        this.tdjueves12a13 = this.celdanoseleccionada;
      } else {
        this.tdjueves12a13 = this.celdaseleccionada;
      }
    } else if (valor === 47) {
      if (this.checkviernes12a13 === false) {
        this.tdviernes12a13 = this.celdanoseleccionada;
      } else {
        this.tdviernes12a13 = this.celdaseleccionada;
      }
    } else if (valor === 48) {
      if (this.checksabado12a13 === false) {
        this.tdsabado12a13 = this.celdanoseleccionada;
      } else {
        this.tdsabado12a13 = this.celdaseleccionada;
      }
    } else if (valor === 49) {
      if (this.checkdomingo12a13 === false) {
        this.tddomingo12a13 = this.celdanoseleccionada;
      } else {
        this.tddomingo12a13 = this.celdaseleccionada;
      }
    } else if (valor === 50) {
      if (this.checklunes13a14 === false) {
        this.tdlunes13a14 = this.celdanoseleccionada;
      } else {
        this.tdlunes13a14 = this.celdaseleccionada;
      }
    } else if (valor === 51) {
      if (this.checkmartes13a14 === false) {
        this.tdmartes13a14 = this.celdanoseleccionada;
      } else {
        this.tdmartes13a14 = this.celdaseleccionada;
      }
    } else if (valor === 52) {
      if (this.checkmiercoles13a14 === false) {
        this.tdmiercoles13a14 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles13a14 = this.celdaseleccionada;
      }
    } else if (valor === 53) {
      if (this.checkjueves13a14 === false) {
        this.tdjueves13a14 = this.celdanoseleccionada;
      } else {
        this.tdjueves13a14 = this.celdaseleccionada;
      }
    } else if (valor === 54) {
      if (this.checkviernes13a14 === false) {
        this.tdviernes13a14 = this.celdanoseleccionada;
      } else {
        this.tdviernes13a14 = this.celdaseleccionada;
      }
    } else if (valor === 55) {
      if (this.checksabado13a14 === false) {
        this.tdsabado13a14 = this.celdanoseleccionada;
      } else {
        this.tdsabado13a14 = this.celdaseleccionada;
      }
    } else if (valor === 56) {
      if (this.checkdomingo13a14 === false) {
        this.tddomingo13a14 = this.celdanoseleccionada;
      } else {
        this.tddomingo13a14 = this.celdaseleccionada;
      }
    } else if (valor === 57) {
      if (this.checklunes14a15 === false) {
        this.tdlunes14a15 = this.celdanoseleccionada;
      } else {
        this.tdlunes14a15 = this.celdaseleccionada;
      }
    } else if (valor === 58) {
      if (this.checkmartes14a15 === false) {
        this.tdmartes14a15 = this.celdanoseleccionada;
      } else {
        this.tdmartes14a15 = this.celdaseleccionada;
      }
    } else if (valor === 59) {
      if (this.checkmiercoles14a15 === false) {
        this.tdmiercoles14a15 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles14a15 = this.celdaseleccionada;
      }
    } else if (valor === 60) {
      if (this.checkjueves14a15 === false) {
        this.tdjueves14a15 = this.celdanoseleccionada;
      } else {
        this.tdjueves14a15 = this.celdaseleccionada;
      }
    } else if (valor === 61) {
      if (this.checkviernes14a15 === false) {
        this.tdviernes14a15 = this.celdanoseleccionada;
      } else {
        this.tdviernes14a15 = this.celdaseleccionada;
      }
    } else if (valor === 62) {
      if (this.checksabado14a15 === false) {
        this.tdsabado14a15 = this.celdanoseleccionada;
      } else {
        this.tdsabado14a15 = this.celdaseleccionada;
      }
    } else if (valor === 63) {
      if (this.checkdomingo14a15 === false) {
        this.tddomingo14a15 = this.celdanoseleccionada;
      } else {
        this.tddomingo14a15 = this.celdaseleccionada;
      }
    } else if (valor === 64) {
      if (this.checklunes15a16 === false) {
        this.tdlunes15a16 = this.celdanoseleccionada;
      } else {
        this.tdlunes15a16 = this.celdaseleccionada;
      }
    } else if (valor === 65) {
      if (this.checkmartes15a16 === false) {
        this.tdmartes15a16 = this.celdanoseleccionada;
      } else {
        this.tdmartes15a16 = this.celdaseleccionada;
      }
    } else if (valor === 66) {
      if (this.checkmiercoles15a16 === false) {
        this.tdmiercoles15a16 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles15a16 = this.celdaseleccionada;
      }
    } else if (valor === 67) {
      if (this.checkjueves15a16 === false) {
        this.tdjueves15a16 = this.celdanoseleccionada;
      } else {
        this.tdjueves15a16 = this.celdaseleccionada;
      }
    } else if (valor === 68) {
      if (this.checkviernes15a16 === false) {
        this.tdviernes15a16 = this.celdanoseleccionada;
      } else {
        this.tdviernes15a16 = this.celdaseleccionada;
      }
    } else if (valor === 69) {
      if (this.checksabado15a16 === false) {
        this.tdsabado15a16 = this.celdanoseleccionada;
      } else {
        this.tdsabado15a16 = this.celdaseleccionada;
      }
    } else if (valor === 70) {
      if (this.checkdomingo15a16 === false) {
        this.tddomingo15a16 = this.celdanoseleccionada;
      } else {
        this.tddomingo15a16 = this.celdaseleccionada;
      }
    } else if (valor === 71) {
      if (this.checklunes16a17 === false) {
        this.tdlunes16a17 = this.celdanoseleccionada;
      } else {
        this.tdlunes16a17 = this.celdaseleccionada;
      }
    } else if (valor === 72) {
      if (this.checkmartes16a17 === false) {
        this.tdmartes16a17 = this.celdanoseleccionada;
      } else {
        this.tdmartes16a17 = this.celdaseleccionada;
      }
    } else if (valor === 73) {
      if (this.checkmiercoles16a17 === false) {
        this.tdmiercoles16a17 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles16a17 = this.celdaseleccionada;
      }
    } else if (valor === 74) {
      if (this.checkjueves16a17 === false) {
        this.tdjueves16a17 = this.celdanoseleccionada;
      } else {
        this.tdjueves16a17 = this.celdaseleccionada;
      }
    } else if (valor === 75) {
      if (this.checkviernes16a17 === false) {
        this.tdviernes16a17 = this.celdanoseleccionada;
      } else {
        this.tdviernes16a17 = this.celdaseleccionada;
      }
    } else if (valor === 76) {
      if (this.checksabado16a17 === false) {
        this.tdsabado16a17 = this.celdanoseleccionada;
      } else {
        this.tdsabado16a17 = this.celdaseleccionada;
      }
    } else if (valor === 77) {
      if (this.checkdomingo16a17 === false) {
        this.tddomingo16a17 = this.celdanoseleccionada;
      } else {
        this.tddomingo16a17 = this.celdaseleccionada;
      }
    } else if (valor === 78) {
      if (this.checklunes17a18 === false) {
        this.tdlunes17a18 = this.celdanoseleccionada;
      } else {
        this.tdlunes17a18 = this.celdaseleccionada;
      }
    } else if (valor === 79) {
      if (this.checkmartes17a18 === false) {
        this.tdmartes17a18 = this.celdanoseleccionada;
      } else {
        this.tdmartes17a18 = this.celdaseleccionada;
      }
    } else if (valor === 80) {
      if (this.checkmiercoles17a18 === false) {
        this.tdmiercoles17a18 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles17a18 = this.celdaseleccionada;
      }
    } else if (valor === 81) {
      if (this.checkjueves17a18 === false) {
        this.tdjueves17a18 = this.celdanoseleccionada;
      } else {
        this.tdjueves17a18 = this.celdaseleccionada;
      }
    } else if (valor === 82) {
      if (this.checkviernes17a18 === false) {
        this.tdviernes17a18 = this.celdanoseleccionada;
      } else {
        this.tdviernes17a18 = this.celdaseleccionada;
      }
    } else if (valor === 83) {
      if (this.checksabado17a18 === false) {
        this.tdsabado17a18 = this.celdanoseleccionada;
      } else {
        this.tdsabado17a18 = this.celdaseleccionada;
      }
    } else if (valor === 84) {
      if (this.checkdomingo17a18 === false) {
        this.tddomingo17a18 = this.celdanoseleccionada;
      } else {
        this.tddomingo17a18 = this.celdaseleccionada;
      }
    } else if (valor === 85) {
      if (this.checklunes18a19 === false) {
        this.tdlunes18a19 = this.celdanoseleccionada;
      } else {
        this.tdlunes18a19 = this.celdaseleccionada;
      }
    } else if (valor === 86) {
      if (this.checkmartes18a19 === false) {
        this.tdmartes18a19 = this.celdanoseleccionada;
      } else {
        this.tdmartes18a19 = this.celdaseleccionada;
      }
    } else if (valor === 87) {
      if (this.checkmiercoles18a19 === false) {
        this.tdmiercoles18a19 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles18a19 = this.celdaseleccionada;
      }
    } else if (valor === 88) {
      if (this.checkjueves18a19 === false) {
        this.tdjueves18a19 = this.celdanoseleccionada;
      } else {
        this.tdjueves18a19 = this.celdaseleccionada;
      }
    } else if (valor === 89) {
      if (this.checkviernes18a19 === false) {
        this.tdviernes18a19 = this.celdanoseleccionada;
      } else {
        this.tdviernes18a19 = this.celdaseleccionada;
      }
    } else if (valor === 90) {
      if (this.checksabado18a19 === false) {
        this.tdsabado18a19 = this.celdanoseleccionada;
      } else {
        this.tdsabado18a19 = this.celdaseleccionada;
      }
    } else if (valor === 91) {
      if (this.checkdomingo18a19 === false) {
        this.tddomingo18a19 = this.celdanoseleccionada;
      } else {
        this.tddomingo18a19 = this.celdaseleccionada;
      }
    } else if (valor === 92) {
      if (this.checklunes19a20 === false) {
        this.tdlunes19a20 = this.celdanoseleccionada;
      } else {
        this.tdlunes19a20 = this.celdaseleccionada;
      }
    } else if (valor === 93) {
      if (this.checkmartes19a20 === false) {
        this.tdmartes19a20 = this.celdanoseleccionada;
      } else {
        this.tdmartes19a20 = this.celdaseleccionada;
      }
    } else if (valor === 94) {
      if (this.checkmiercoles19a20 === false) {
        this.tdmiercoles19a20 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles19a20 = this.celdaseleccionada;
      }
    } else if (valor === 95) {
      if (this.checkjueves19a20 === false) {
        this.tdjueves19a20 = this.celdanoseleccionada;
      } else {
        this.tdjueves19a20 = this.celdaseleccionada;
      }
    } else if (valor === 96) {
      if (this.checkviernes19a20 === false) {
        this.tdviernes19a20 = this.celdanoseleccionada;
      } else {
        this.tdviernes19a20 = this.celdaseleccionada;
      }
    } else if (valor === 97) {
      if (this.checksabado19a20 === false) {
        this.tdsabado19a20 = this.celdanoseleccionada;
      } else {
        this.tdsabado19a20 = this.celdaseleccionada;
      }
    } else if (valor === 98) {
      if (this.checkdomingo19a20 === false) {
        this.tddomingo19a20 = this.celdanoseleccionada;
      } else {
        this.tddomingo19a20 = this.celdaseleccionada;
      }
    } else if (valor === 99) {
      if (this.checklunes20a21 === false) {
        this.tdlunes20a21 = this.celdanoseleccionada;
      } else {
        this.tdlunes20a21 = this.celdaseleccionada;
      }
    } else if (valor === 100) {
      if (this.checkmartes20a21 === false) {
        this.tdmartes20a21 = this.celdanoseleccionada;
      } else {
        this.tdmartes20a21 = this.celdaseleccionada;
      }
    } else if (valor === 101) {
      if (this.checkmiercoles20a21 === false) {
        this.tdmiercoles20a21 = this.celdanoseleccionada;
      } else {
        this.tdmiercoles20a21 = this.celdaseleccionada;
      }
    } else if (valor === 102) {
      if (this.checkjueves20a21 === false) {
        this.tdjueves20a21 = this.celdanoseleccionada;
      } else {
        this.tdjueves20a21 = this.celdaseleccionada;
      }
    } else if (valor === 103) {
      if (this.checkviernes20a21 === false) {
        this.tdviernes20a21 = this.celdanoseleccionada;
      } else {
        this.tdviernes20a21 = this.celdaseleccionada;
      }
    } else if (valor === 104) {
      if (this.checksabado20a21 === false) {
        this.tdsabado20a21 = this.celdanoseleccionada;
      } else {
        this.tdsabado20a21 = this.celdaseleccionada;
      }
    } else if (valor === 105) {
      if (this.checkdomingo20a21 === false) {
        this.tddomingo20a21 = this.celdanoseleccionada;
      } else {
        this.tddomingo20a21 = this.celdaseleccionada;
      }
    }
  }

  checktruetodoeldia(valor) {
    if (valor === 1) {
      if (this.todoeldialunes === false) {
        this.todoeldialunes = true;
        this.checklunes6a7 = true;
        this.checklunes7a8 = true;
        this.checklunes8a9 = true;
        this.checklunes9a10 = true;
        this.checklunes10a11 = true;
        this.checklunes11a12 = true;
        this.checklunes12a13 = true;
        this.checklunes13a14 = true;
        this.checklunes14a15 = true;
        this.checklunes15a16 = true;
        this.checklunes16a17 = true;
        this.checklunes17a18 = true;
        this.checklunes18a19 = true;
        this.checklunes19a20 = true;
        this.checklunes20a21 = true;
        this.tdlunes6a7 = this.celdaseleccionada;
        this.tdlunes7a8 = this.celdaseleccionada;
        this.tdlunes8a9 = this.celdaseleccionada;
        this.tdlunes9a10 = this.celdaseleccionada;
        this.tdlunes10a11 = this.celdaseleccionada;
        this.tdlunes11a12 = this.celdaseleccionada;
        this.tdlunes12a13 = this.celdaseleccionada;
        this.tdlunes13a14 = this.celdaseleccionada;
        this.tdlunes14a15 = this.celdaseleccionada;
        this.tdlunes15a16 = this.celdaseleccionada;
        this.tdlunes16a17 = this.celdaseleccionada;
        this.tdlunes17a18 = this.celdaseleccionada;
        this.tdlunes18a19 = this.celdaseleccionada;
        this.tdlunes19a20 = this.celdaseleccionada;
        this.tdlunes20a21 = this.celdaseleccionada;
      } else {
        this.todoeldialunes = false;
        this.checklunes6a7 = false;
        this.checklunes7a8 = false;
        this.checklunes8a9 = false;
        this.checklunes9a10 = false;
        this.checklunes10a11 = false;
        this.checklunes11a12 = false;
        this.checklunes12a13 = false;
        this.checklunes13a14 = false;
        this.checklunes14a15 = false;
        this.checklunes15a16 = false;
        this.checklunes16a17 = false;
        this.checklunes17a18 = false;
        this.checklunes18a19 = false;
        this.checklunes19a20 = false;
        this.checklunes20a21 = false;
        this.tdlunes6a7 = this.celdanoseleccionada;
        this.tdlunes7a8 = this.celdanoseleccionada;
        this.tdlunes8a9 = this.celdanoseleccionada;
        this.tdlunes9a10 = this.celdanoseleccionada;
        this.tdlunes10a11 = this.celdanoseleccionada;
        this.tdlunes11a12 = this.celdanoseleccionada;
        this.tdlunes12a13 = this.celdanoseleccionada;
        this.tdlunes13a14 = this.celdanoseleccionada;
        this.tdlunes14a15 = this.celdanoseleccionada;
        this.tdlunes15a16 = this.celdanoseleccionada;
        this.tdlunes16a17 = this.celdanoseleccionada;
        this.tdlunes17a18 = this.celdanoseleccionada;
        this.tdlunes18a19 = this.celdanoseleccionada;
        this.tdlunes19a20 = this.celdanoseleccionada;
        this.tdlunes20a21 = this.celdanoseleccionada;
      }
    } else if (valor === 2) {
      if (this.todoeldiamartes === false) {
        this.todoeldiamartes = true;
        this.checkmartes6a7 = true;
        this.checkmartes7a8 = true;
        this.checkmartes8a9 = true;
        this.checkmartes9a10 = true;
        this.checkmartes10a11 = true;
        this.checkmartes11a12 = true;
        this.checkmartes12a13 = true;
        this.checkmartes13a14 = true;
        this.checkmartes14a15 = true;
        this.checkmartes15a16 = true;
        this.checkmartes16a17 = true;
        this.checkmartes17a18 = true;
        this.checkmartes18a19 = true;
        this.checkmartes19a20 = true;
        this.checkmartes20a21 = true;
        this.tdmartes6a7 = this.celdaseleccionada;
        this.tdmartes7a8 = this.celdaseleccionada;
        this.tdmartes8a9 = this.celdaseleccionada;
        this.tdmartes9a10 = this.celdaseleccionada;
        this.tdmartes10a11 = this.celdaseleccionada;
        this.tdmartes11a12 = this.celdaseleccionada;
        this.tdmartes12a13 = this.celdaseleccionada;
        this.tdmartes13a14 = this.celdaseleccionada;
        this.tdmartes14a15 = this.celdaseleccionada;
        this.tdmartes15a16 = this.celdaseleccionada;
        this.tdmartes16a17 = this.celdaseleccionada;
        this.tdmartes17a18 = this.celdaseleccionada;
        this.tdmartes18a19 = this.celdaseleccionada;
        this.tdmartes19a20 = this.celdaseleccionada;
        this.tdmartes20a21 = this.celdaseleccionada;
      } else {
        this.todoeldiamartes = false;
        this.checkmartes6a7 = false;
        this.checkmartes7a8 = false;
        this.checkmartes8a9 = false;
        this.checkmartes9a10 = false;
        this.checkmartes10a11 = false;
        this.checkmartes11a12 = false;
        this.checkmartes12a13 = false;
        this.checkmartes13a14 = false;
        this.checkmartes14a15 = false;
        this.checkmartes15a16 = false;
        this.checkmartes16a17 = false;
        this.checkmartes17a18 = false;
        this.checkmartes18a19 = false;
        this.checkmartes19a20 = false;
        this.checkmartes20a21 = false;
        this.tdmartes6a7 = this.celdanoseleccionada;
        this.tdmartes7a8 = this.celdanoseleccionada;
        this.tdmartes8a9 = this.celdanoseleccionada;
        this.tdmartes9a10 = this.celdanoseleccionada;
        this.tdmartes10a11 = this.celdanoseleccionada;
        this.tdmartes11a12 = this.celdanoseleccionada;
        this.tdmartes12a13 = this.celdanoseleccionada;
        this.tdmartes13a14 = this.celdanoseleccionada;
        this.tdmartes14a15 = this.celdanoseleccionada;
        this.tdmartes15a16 = this.celdanoseleccionada;
        this.tdmartes16a17 = this.celdanoseleccionada;
        this.tdmartes17a18 = this.celdanoseleccionada;
        this.tdmartes18a19 = this.celdanoseleccionada;
        this.tdmartes19a20 = this.celdanoseleccionada;
        this.tdmartes20a21 = this.celdanoseleccionada;
      }
    } else if (valor === 3) {
      if (this.todoeldiamiercoles === false) {
        this.todoeldiamiercoles = true;
        this.checkmiercoles6a7 = true;
        this.checkmiercoles7a8 = true;
        this.checkmiercoles8a9 = true;
        this.checkmiercoles9a10 = true;
        this.checkmiercoles10a11 = true;
        this.checkmiercoles11a12 = true;
        this.checkmiercoles12a13 = true;
        this.checkmiercoles13a14 = true;
        this.checkmiercoles14a15 = true;
        this.checkmiercoles15a16 = true;
        this.checkmiercoles16a17 = true;
        this.checkmiercoles17a18 = true;
        this.checkmiercoles18a19 = true;
        this.checkmiercoles19a20 = true;
        this.checkmiercoles20a21 = true;
        this.tdmiercoles6a7 = this.celdaseleccionada;
        this.tdmiercoles7a8 = this.celdaseleccionada;
        this.tdmiercoles8a9 = this.celdaseleccionada;
        this.tdmiercoles9a10 = this.celdaseleccionada;
        this.tdmiercoles10a11 = this.celdaseleccionada;
        this.tdmiercoles11a12 = this.celdaseleccionada;
        this.tdmiercoles12a13 = this.celdaseleccionada;
        this.tdmiercoles13a14 = this.celdaseleccionada;
        this.tdmiercoles14a15 = this.celdaseleccionada;
        this.tdmiercoles15a16 = this.celdaseleccionada;
        this.tdmiercoles16a17 = this.celdaseleccionada;
        this.tdmiercoles17a18 = this.celdaseleccionada;
        this.tdmiercoles18a19 = this.celdaseleccionada;
        this.tdmiercoles19a20 = this.celdaseleccionada;
        this.tdmiercoles20a21 = this.celdaseleccionada;
      } else {
        this.todoeldiamiercoles = false;
        this.checkmiercoles6a7 = false;
        this.checkmiercoles7a8 = false;
        this.checkmiercoles8a9 = false;
        this.checkmiercoles9a10 = false;
        this.checkmiercoles10a11 = false;
        this.checkmiercoles11a12 = false;
        this.checkmiercoles12a13 = false;
        this.checkmiercoles13a14 = false;
        this.checkmiercoles14a15 = false;
        this.checkmiercoles15a16 = false;
        this.checkmiercoles16a17 = false;
        this.checkmiercoles17a18 = false;
        this.checkmiercoles18a19 = false;
        this.checkmiercoles19a20 = false;
        this.checkmiercoles20a21 = false;
        this.tdmiercoles6a7 = this.celdanoseleccionada;
        this.tdmiercoles7a8 = this.celdanoseleccionada;
        this.tdmiercoles8a9 = this.celdanoseleccionada;
        this.tdmiercoles9a10 = this.celdanoseleccionada;
        this.tdmiercoles10a11 = this.celdanoseleccionada;
        this.tdmiercoles11a12 = this.celdanoseleccionada;
        this.tdmiercoles12a13 = this.celdanoseleccionada;
        this.tdmiercoles13a14 = this.celdanoseleccionada;
        this.tdmiercoles14a15 = this.celdanoseleccionada;
        this.tdmiercoles15a16 = this.celdanoseleccionada;
        this.tdmiercoles16a17 = this.celdanoseleccionada;
        this.tdmiercoles17a18 = this.celdanoseleccionada;
        this.tdmiercoles18a19 = this.celdanoseleccionada;
        this.tdmiercoles19a20 = this.celdanoseleccionada;
        this.tdmiercoles20a21 = this.celdanoseleccionada;
      }
    } else if (valor === 4) {
      if (this.todoeldiajueves === false) {
        this.todoeldiajueves = true;
        this.checkjueves6a7 = true;
        this.checkjueves7a8 = true;
        this.checkjueves8a9 = true;
        this.checkjueves9a10 = true;
        this.checkjueves10a11 = true;
        this.checkjueves11a12 = true;
        this.checkjueves12a13 = true;
        this.checkjueves13a14 = true;
        this.checkjueves14a15 = true;
        this.checkjueves15a16 = true;
        this.checkjueves16a17 = true;
        this.checkjueves17a18 = true;
        this.checkjueves18a19 = true;
        this.checkjueves19a20 = true;
        this.checkjueves20a21 = true;
        this.tdjueves6a7 = this.celdaseleccionada;
        this.tdjueves7a8 = this.celdaseleccionada;
        this.tdjueves8a9 = this.celdaseleccionada;
        this.tdjueves9a10 = this.celdaseleccionada;
        this.tdjueves10a11 = this.celdaseleccionada;
        this.tdjueves11a12 = this.celdaseleccionada;
        this.tdjueves12a13 = this.celdaseleccionada;
        this.tdjueves13a14 = this.celdaseleccionada;
        this.tdjueves14a15 = this.celdaseleccionada;
        this.tdjueves15a16 = this.celdaseleccionada;
        this.tdjueves16a17 = this.celdaseleccionada;
        this.tdjueves17a18 = this.celdaseleccionada;
        this.tdjueves18a19 = this.celdaseleccionada;
        this.tdjueves19a20 = this.celdaseleccionada;
        this.tdjueves20a21 = this.celdaseleccionada;
      } else {
        this.todoeldiajueves = false;
        this.checkjueves6a7 = false;
        this.checkjueves7a8 = false;
        this.checkjueves8a9 = false;
        this.checkjueves9a10 = false;
        this.checkjueves10a11 = false;
        this.checkjueves11a12 = false;
        this.checkjueves12a13 = false;
        this.checkjueves13a14 = false;
        this.checkjueves14a15 = false;
        this.checkjueves15a16 = false;
        this.checkjueves16a17 = false;
        this.checkjueves17a18 = false;
        this.checkjueves18a19 = false;
        this.checkjueves19a20 = false;
        this.checkjueves20a21 = false;
        this.tdjueves6a7 = this.celdanoseleccionada;
        this.tdjueves7a8 = this.celdanoseleccionada;
        this.tdjueves8a9 = this.celdanoseleccionada;
        this.tdjueves9a10 = this.celdanoseleccionada;
        this.tdjueves10a11 = this.celdanoseleccionada;
        this.tdjueves11a12 = this.celdanoseleccionada;
        this.tdjueves12a13 = this.celdanoseleccionada;
        this.tdjueves13a14 = this.celdanoseleccionada;
        this.tdjueves14a15 = this.celdanoseleccionada;
        this.tdjueves15a16 = this.celdanoseleccionada;
        this.tdjueves16a17 = this.celdanoseleccionada;
        this.tdjueves17a18 = this.celdanoseleccionada;
        this.tdjueves18a19 = this.celdanoseleccionada;
        this.tdjueves19a20 = this.celdanoseleccionada;
        this.tdjueves20a21 = this.celdanoseleccionada;
      }
    } else if (valor === 5) {
      if (this.todoeldiaviernes === false) {
        this.todoeldiaviernes = true;
        this.checkviernes6a7 = true;
        this.checkviernes7a8 = true;
        this.checkviernes8a9 = true;
        this.checkviernes9a10 = true;
        this.checkviernes10a11 = true;
        this.checkviernes11a12 = true;
        this.checkviernes12a13 = true;
        this.checkviernes13a14 = true;
        this.checkviernes14a15 = true;
        this.checkviernes15a16 = true;
        this.checkviernes16a17 = true;
        this.checkviernes17a18 = true;
        this.checkviernes18a19 = true;
        this.checkviernes19a20 = true;
        this.checkviernes20a21 = true;
        this.tdviernes6a7 = this.celdaseleccionada;
        this.tdviernes7a8 = this.celdaseleccionada;
        this.tdviernes8a9 = this.celdaseleccionada;
        this.tdviernes9a10 = this.celdaseleccionada;
        this.tdviernes10a11 = this.celdaseleccionada;
        this.tdviernes11a12 = this.celdaseleccionada;
        this.tdviernes12a13 = this.celdaseleccionada;
        this.tdviernes13a14 = this.celdaseleccionada;
        this.tdviernes14a15 = this.celdaseleccionada;
        this.tdviernes15a16 = this.celdaseleccionada;
        this.tdviernes16a17 = this.celdaseleccionada;
        this.tdviernes17a18 = this.celdaseleccionada;
        this.tdviernes18a19 = this.celdaseleccionada;
        this.tdviernes19a20 = this.celdaseleccionada;
        this.tdviernes20a21 = this.celdaseleccionada;
      } else {
        this.todoeldiaviernes = false;
        this.checkviernes6a7 = false;
        this.checkviernes7a8 = false;
        this.checkviernes8a9 = false;
        this.checkviernes9a10 = false;
        this.checkviernes10a11 = false;
        this.checkviernes11a12 = false;
        this.checkviernes12a13 = false;
        this.checkviernes13a14 = false;
        this.checkviernes14a15 = false;
        this.checkviernes15a16 = false;
        this.checkviernes16a17 = false;
        this.checkviernes17a18 = false;
        this.checkviernes18a19 = false;
        this.checkviernes19a20 = false;
        this.checkviernes20a21 = false;
        this.tdviernes6a7 = this.celdanoseleccionada;
        this.tdviernes7a8 = this.celdanoseleccionada;
        this.tdviernes8a9 = this.celdanoseleccionada;
        this.tdviernes9a10 = this.celdanoseleccionada;
        this.tdviernes10a11 = this.celdanoseleccionada;
        this.tdviernes11a12 = this.celdanoseleccionada;
        this.tdviernes12a13 = this.celdanoseleccionada;
        this.tdviernes13a14 = this.celdanoseleccionada;
        this.tdviernes14a15 = this.celdanoseleccionada;
        this.tdviernes15a16 = this.celdanoseleccionada;
        this.tdviernes16a17 = this.celdanoseleccionada;
        this.tdviernes17a18 = this.celdanoseleccionada;
        this.tdviernes18a19 = this.celdanoseleccionada;
        this.tdviernes19a20 = this.celdanoseleccionada;
        this.tdviernes20a21 = this.celdanoseleccionada;
      }
    } else if (valor === 6) {
      if (this.todoeldiasabado === false) {
        this.todoeldiasabado = true;
        this.checksabado6a7 = true;
        this.checksabado7a8 = true;
        this.checksabado8a9 = true;
        this.checksabado9a10 = true;
        this.checksabado10a11 = true;
        this.checksabado11a12 = true;
        this.checksabado12a13 = true;
        this.checksabado13a14 = true;
        this.checksabado14a15 = true;
        this.checksabado15a16 = true;
        this.checksabado16a17 = true;
        this.checksabado17a18 = true;
        this.checksabado18a19 = true;
        this.checksabado19a20 = true;
        this.checksabado20a21 = true;
        this.tdsabado6a7 = this.celdaseleccionada;
        this.tdsabado7a8 = this.celdaseleccionada;
        this.tdsabado8a9 = this.celdaseleccionada;
        this.tdsabado9a10 = this.celdaseleccionada;
        this.tdsabado10a11 = this.celdaseleccionada;
        this.tdsabado11a12 = this.celdaseleccionada;
        this.tdsabado12a13 = this.celdaseleccionada;
        this.tdsabado13a14 = this.celdaseleccionada;
        this.tdsabado14a15 = this.celdaseleccionada;
        this.tdsabado15a16 = this.celdaseleccionada;
        this.tdsabado16a17 = this.celdaseleccionada;
        this.tdsabado17a18 = this.celdaseleccionada;
        this.tdsabado18a19 = this.celdaseleccionada;
        this.tdsabado19a20 = this.celdaseleccionada;
        this.tdsabado20a21 = this.celdaseleccionada;
      } else {
        this.todoeldiasabado = false;
        this.checksabado6a7 = false;
        this.checksabado7a8 = false;
        this.checksabado8a9 = false;
        this.checksabado9a10 = false;
        this.checksabado10a11 = false;
        this.checksabado11a12 = false;
        this.checksabado12a13 = false;
        this.checksabado13a14 = false;
        this.checksabado14a15 = false;
        this.checksabado15a16 = false;
        this.checksabado16a17 = false;
        this.checksabado17a18 = false;
        this.checksabado18a19 = false;
        this.checksabado19a20 = false;
        this.checksabado20a21 = false;
        this.tdsabado6a7 = this.celdanoseleccionada;
        this.tdsabado7a8 = this.celdanoseleccionada;
        this.tdsabado8a9 = this.celdanoseleccionada;
        this.tdsabado9a10 = this.celdanoseleccionada;
        this.tdsabado10a11 = this.celdanoseleccionada;
        this.tdsabado11a12 = this.celdanoseleccionada;
        this.tdsabado12a13 = this.celdanoseleccionada;
        this.tdsabado13a14 = this.celdanoseleccionada;
        this.tdsabado14a15 = this.celdanoseleccionada;
        this.tdsabado15a16 = this.celdanoseleccionada;
        this.tdsabado16a17 = this.celdanoseleccionada;
        this.tdsabado17a18 = this.celdanoseleccionada;
        this.tdsabado18a19 = this.celdanoseleccionada;
        this.tdsabado19a20 = this.celdanoseleccionada;
        this.tdsabado20a21 = this.celdanoseleccionada;
      }
    } else if (valor === 7) {
      if (this.todoeldiadomingo === false) {
        this.todoeldiadomingo = true;
        this.checkdomingo6a7 = true;
        this.checkdomingo7a8 = true;
        this.checkdomingo8a9 = true;
        this.checkdomingo9a10 = true;
        this.checkdomingo10a11 = true;
        this.checkdomingo11a12 = true;
        this.checkdomingo12a13 = true;
        this.checkdomingo13a14 = true;
        this.checkdomingo14a15 = true;
        this.checkdomingo15a16 = true;
        this.checkdomingo16a17 = true;
        this.checkdomingo17a18 = true;
        this.checkdomingo18a19 = true;
        this.checkdomingo19a20 = true;
        this.checkdomingo20a21 = true;
        this.tddomingo6a7 = this.celdaseleccionada;
        this.tddomingo7a8 = this.celdaseleccionada;
        this.tddomingo8a9 = this.celdaseleccionada;
        this.tddomingo9a10 = this.celdaseleccionada;
        this.tddomingo10a11 = this.celdaseleccionada;
        this.tddomingo11a12 = this.celdaseleccionada;
        this.tddomingo12a13 = this.celdaseleccionada;
        this.tddomingo13a14 = this.celdaseleccionada;
        this.tddomingo14a15 = this.celdaseleccionada;
        this.tddomingo15a16 = this.celdaseleccionada;
        this.tddomingo16a17 = this.celdaseleccionada;
        this.tddomingo17a18 = this.celdaseleccionada;
        this.tddomingo18a19 = this.celdaseleccionada;
        this.tddomingo19a20 = this.celdaseleccionada;
        this.tddomingo20a21 = this.celdaseleccionada;
      } else {
        this.todoeldiadomingo = false;
        this.checkdomingo6a7 = false;
        this.checkdomingo7a8 = false;
        this.checkdomingo8a9 = false;
        this.checkdomingo9a10 = false;
        this.checkdomingo10a11 = false;
        this.checkdomingo11a12 = false;
        this.checkdomingo12a13 = false;
        this.checkdomingo13a14 = false;
        this.checkdomingo14a15 = false;
        this.checkdomingo15a16 = false;
        this.checkdomingo16a17 = false;
        this.checkdomingo17a18 = false;
        this.checkdomingo18a19 = false;
        this.checkdomingo19a20 = false;
        this.checkdomingo20a21 = false;
        this.tddomingo6a7 = this.celdanoseleccionada;
        this.tddomingo7a8 = this.celdanoseleccionada;
        this.tddomingo8a9 = this.celdanoseleccionada;
        this.tddomingo9a10 = this.celdanoseleccionada;
        this.tddomingo10a11 = this.celdanoseleccionada;
        this.tddomingo11a12 = this.celdanoseleccionada;
        this.tddomingo12a13 = this.celdanoseleccionada;
        this.tddomingo13a14 = this.celdanoseleccionada;
        this.tddomingo14a15 = this.celdanoseleccionada;
        this.tddomingo15a16 = this.celdanoseleccionada;
        this.tddomingo16a17 = this.celdanoseleccionada;
        this.tddomingo17a18 = this.celdanoseleccionada;
        this.tddomingo18a19 = this.celdanoseleccionada;
        this.tddomingo19a20 = this.celdanoseleccionada;
        this.tddomingo20a21 = this.celdanoseleccionada;
      }
    }
  }
}
