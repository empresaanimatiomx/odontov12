import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListadoUsuariosComponent } from './listado-usuarios/listado-usuarios.component';
import { NuevoUsuarioComponent } from './nuevo-usuario/nuevo-usuario.component';
import { UsuariosInactivosComponent } from './usuarios-inactivos/usuarios-inactivos.component';

const routes: Routes = [
  {
    path: "nuevo-usuario",
    component: NuevoUsuarioComponent
  },
  {
    path: "listado-usuarios",
    component: ListadoUsuariosComponent
  },
  {
    path: "usuarios-inactivos",
    component: UsuariosInactivosComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
