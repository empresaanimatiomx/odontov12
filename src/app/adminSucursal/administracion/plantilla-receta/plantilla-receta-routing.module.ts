import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "listado-plantillas",
    loadChildren: () =>
      import("./listado-plantillas/listado-plantillas.module").then(
        (m) => m.ListadoPlantillasModule
      ),
  },
  {
    path: "nueva-plantilla",
    loadChildren: () =>
      import("./nueva-plantilla/nueva-plantilla.module").then(
        (m) => m.NuevaPlantillaModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlantillaRecetaRoutingModule { }
