import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListadoPlantillasComponent } from './listado-plantillas.component';

const routes: Routes = [
  {
    path:'',
    component: ListadoPlantillasComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListadoPlantillasRoutingModule { }
