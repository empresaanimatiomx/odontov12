import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListadoPlantillasRoutingModule } from './listado-plantillas-routing.module';
import { ListadoPlantillasComponent } from './listado-plantillas.component';
import { MaterialModule } from "../../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ListadoPlantillasComponent
  ],
  imports: [
    CommonModule,
    ListadoPlantillasRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ListadoPlantillasModule { }
