import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../../../../core/service/auth.service";
import { HttpService } from "../../../../core/service/http.service";
import { AlertService } from "../../../../core/service/alert.service";

@Component({
  selector: "app-listado-plantillas",
  templateUrl: "./listado-plantillas.component.html",
  styles: [],
})
export class ListadoPlantillasComponent implements OnInit {
  @ViewChild("epltable", { static: false }) epltable: ElementRef;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  plantillas = [];
  plantillasFilter = [];
  plantillaForm: FormGroup;
  imagenclinicaPrint = "";
  idPlantilla;
  idClinica;

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private http: HttpService,
    private sweet: AlertService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.imagenclinicaPrint =
      "https://projectsbyanimatiomx.com/phps/odonto/imagenes/" +
      this.authService.currentUserValue.Logo_clinica;
    this.idClinica = this.authService.currentUserValue.idclinica;

    this.plantillaForm = this.fb.group({
      Titulo: ["", [Validators.required]],
      Notas: ["", [Validators.required]],
    });
  }

  ngOnInit() {
    if (this.idClinica > 0) {
      this.getPlantillas();
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraGenerar());
    }
  }

  exportToExcel() {
    this.http.export("Listado_plantillas_receta", this.plantillasFilter);
  }

  filterDatatable(event) {
    if (event.target.value === "") {
      this.plantillas = this.plantillasFilter;
    }
    this.plantillas = this.http.filter(event, this.plantillasFilter);
    this.table.offset = 0;
  }

  getPlantillas(refresh?): void {
    this.http
      .post("AdminCategorias.php", { caso: 4, idclinica: this.idClinica })
      .subscribe({
        next: (respuesta: any) => {
          this.plantillas = respuesta;
          this.plantillasFilter = respuesta;
          refresh && this.sweet.toast("las plantillas");
        },
        error: (err) => console.log(err),
      });
  }

  iraGenerar() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  iraGenerar2() {
    this.router.navigate(["/admin/administrador/plantilla/nueva-plantilla"]);
  }

  editarPlantillaModal(row, content) {
    this.idPlantilla = row.idPlantilla;
    let ngbModalOptions: NgbModalOptions = {
      centered: true,
      ariaLabelledBy: "modal-basic-title",
    };
    this.modalService.open(content, ngbModalOptions);
    this.plantillaForm.patchValue(row);
  }

  updatePlantilla(plantilla) {
    const options: any = {
      caso: 5,
      nomrec: plantilla.Titulo,
      notarec: plantilla.Notas,
      idplantilla: this.idPlantilla,
    };
    this.http.post("AdminCategorias.php", options).subscribe({
      next: (res: any) => {
        if (res === "Se modifico") {
          this.getPlantillas();
          this.modalService.dismissAll();
          this.sweet.alert("", "Plantilla modificada correctamente", "success");
        } else {
          this.sweet.alert(
            "",
            "Ocurrio un error, intente de nuevo por favor",
            "error"
          );
        }
      },
      error: (err) => console.log(err),
    });
  }

  deletePlantilla(idplantilla) {
    this.sweet.alertConfirm("eliminar esta plantilla").then((result) => {
      if (result.isConfirmed) {
        this.http
          .post("AdminCategorias.php", { caso: 9, idplantilla })
          .subscribe({
            next: (res: any) => {
              if (res.toString() === "Se elimino") {
                this.getPlantillas();
                this.sweet.alert("", "Se elimino la plantilla", "success");
              } else {
                this.sweet.alert(
                  "",
                  "Ocurrio un error, intente de nuevo",
                  "error"
                );
              }
            },
            error: (err) => console.log(err),
          });
      }
    });
  }

  cerrarmodal() {
    this.modalService.dismissAll();
  }

  firstLetterUpperCase(name) {
    const texto = this.plantillaForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.plantillaForm.controls[name].setValue(textofinal);
    }
  }
}
