import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NuevaPlantillaRoutingModule } from './nueva-plantilla-routing.module';
import { NuevaPlantillaComponent } from './nueva-plantilla.component';
import { MaterialModule } from "../../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    NuevaPlantillaComponent
  ],
  imports: [
    CommonModule,
    NuevaPlantillaRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class NuevaPlantillaModule { }
