import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { AlertService } from "../../../../core/service/alert.service";
import { AuthService } from "../../../../core/service/auth.service";
import { HttpService } from "../../../../core/service/http.service";

@Component({
  selector: "app-nueva-plantilla",
  templateUrl: "./nueva-plantilla.component.html",
  styles: [],
})
export class NuevaPlantillaComponent implements OnInit {
  idClinica: number;
  plantillaForm: FormGroup;
  idUsuario: number;

  constructor(
    private fb: FormBuilder,
    private http: HttpService,
    private sweet: AlertService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.idUsuario = this.authService.currentUserValue.id;
    this.plantillaForm = this.fb.group({
      Titulo: ["", [Validators.required]],
      Notas: ["", [Validators.required]],
    });
  }

  ngOnInit(): void {
    if (this.idClinica > 0) {
    } else {
      let snack = this.snackBar.open(
        "Debes agregar una clinica",
        "Agregar Clinica",
        { duration: 4000 }
      );
      snack.onAction().subscribe(() => this.iraclinica());
    }
  }

  iraclinica() {
    this.router.navigate(["/admin/configuracion/clinica"]);
  }

  addReceta(receta) {
    const options: any = {
      caso: 3,
      nomrec: receta.Titulo,
      notarec: receta.Notas,
      idclinica: this.idClinica,
      usuario: this.idUsuario,
    };
    this.http.post("AdminCategorias.php", options).subscribe({
      next: (respuesta) => {
        const res = respuesta;
        if (res.toString() === "Se inserto") {
          this.router.navigate([
            "/admin/administrador/plantilla/listado-plantillas",
          ]);
          this.sweet.alert(
            "",
            "Plantilla de receta guardada correctamente",
            "success"
          );
        } else {
          this.sweet.alert(
            "",
            "Ocurrio un error, intente de nuevo por favor",
            "error"
          );
        }
      },
      error: (err) => console.log(err),
    });
  }

  firstLetterUpperCase(name) {
    const texto = this.plantillaForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.plantillaForm.controls[name].setValue(textofinal);
    }
  }
}
