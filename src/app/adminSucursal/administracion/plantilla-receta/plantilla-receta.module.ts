import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlantillaRecetaRoutingModule } from './plantilla-receta-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PlantillaRecetaRoutingModule
  ]
})
export class PlantillaRecetaModule { }
