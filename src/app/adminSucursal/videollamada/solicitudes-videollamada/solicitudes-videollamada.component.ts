import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { AuthService } from "src/app/core/service/auth.service";
import moment from "moment";
moment.locale("es");
import Swal from "sweetalert2";
import { AlertService } from "src/app/core/service/alert.service";

const headers: any = new HttpHeaders({ "Content-Type": "application/json" });

@Component({
  selector: "app-solicitudes-videollamada",
  templateUrl: "./solicitudes-videollamada.component.html",
  styles: [],
})
export class SolicitudesVideollamadaComponent implements OnInit {
  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  data: any = [];
  columns = [
    { name: "Nombres" },
    { name: "Apellidos" },
    { name: "Celular" },
    { name: "Genero" },
    { name: "Doctor" },
  ];
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  rows = [];
  selectedOption: string;
  filteredData: any = [];
  idClinica;
  idCita;
  infocita;
  NuevaCitaForm: FormGroup;
  pacientes: any = [];
  doctores: any = [];
  servicios: any = [];
  motivosconsulta: any = [];
  historialcita: any = [];
  nombrepaciente = "";
  nombredoctor = "";
  horariocita = "";

  constructor(
    private sweet: AlertService,
    private http: HttpClient,
    private authService: AuthService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.NuevaCitaForm = this.fb.group({
      paciente: [{ value: "", disabled: true }, [Validators.required]],
      doctor: [{ value: "", disabled: true }, [Validators.required]],
      fecha: [{ value: "", disabled: true }, [Validators.required]],
      horario: [{ value: "", disabled: true }, [Validators.required]],
      motivo: [{ value: "", disabled: true }, [Validators.required]],
      estatus: ["", [Validators.required]],
      observaciones: [""],
      servicio: [{ value: "0", disabled: true }],
    });
  }

  ngOnInit(): void {
    this.solicitudes();
    this.GetPacientes(this.idClinica);
    this.GetDoctores(this.idClinica);
  }

  solicitudes(refresh?) {
    const options: any = { caso: 23, idC: this.idClinica };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta.toString() === "Vacio") {
          this.data = [];
        } else {
          this.data = respuesta;
          this.filteredData = respuesta;
          refresh && this.sweet.toast("las solicitudes");
        }
      });
  }

  GetPacientes(idclinica) {
    const options: any = { caso: 10, idC: idclinica };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.pacientes = respuesta;
      });
  }

  GetDoctores(idclinica) {
    const options: any = { caso: 5, idClinica: idclinica };
    this.http
      .post(`${this.baseURL}pacientes.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.doctores = respuesta;
      });
  }

  filterDatatable(event) {
    const val = event.target.value.toLowerCase();
    const colsAmt = this.columns.length;
    const keys = Object.keys(this.filteredData[0]);
    this.data = this.filteredData.filter(function (item) {
      for (let i = 0; i < 50; i++) {
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          return true;
        }
      }
    });
  }

  modalDetalle(cita: any, content) {
    this.idCita = cita.idCitas;
    const options: any = { caso: 13, idc: this.idCita };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.infocita = respuesta;
        let ngbModalOptions: NgbModalOptions = {
          backdrop: "static",
          keyboard: false,
          centered: true,
          ariaLabelledBy: "modal-basic-title",
          size: "lg",
        };
        this.nombredoctor =
          this.infocita[0].NombreUsuario + " " + this.infocita[0].Apellidos;
        this.nombrepaciente =
          this.infocita[0].Nombre + " " + this.infocita[0].Apellido;
        this.horariocita = this.infocita[0].Horario;
        this.modalService.open(content, ngbModalOptions);
        this.NuevaCitaForm.setValue({
          paciente: this.infocita[0].idPacientes,
          doctor: this.infocita[0].idDoctores,
          fecha: this.infocita[0].Fecha,
          horario: this.infocita[0].Horario,
          motivo: this.infocita[0].Motivos,
          estatus: this.infocita[0].EstatusCita,
          observaciones: this.infocita[0].Observaciones,
          servicio: 0,
        });
      });
  }

  modificarcita(formCita: FormGroup, estatu) {
    formCita.controls["estatus"].setValue(estatu);
    let fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
    const fecha1 = moment(formCita.value.fecha).format("MMMM Do YYYY");
    const horario = this.horariocita;
    var enviar = 0;

    let observaciones = formCita.value.observaciones;
    let detallecita = "";
    let mensajealert = "";
    let mensaje = "";
    let contra = "";
    if (estatu === "Confirmado") {
      detallecita = "Confirmacion de videollamada";
      mensajealert = "Se confirmo la solicitud correctamente";
    } else {
      detallecita = "Cancelación de videollamada";
      mensajealert = "Se cancelo la solicitud correctamente";
    }
    console.log(formCita.value);
    const options: any = {
      caso: 8,
      nuevacita: formCita.getRawValue(),
      idc: this.infocita[0].idCitas,
      fecha: fecha,
    };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta != "") {
          contra = respuesta.toString();
          const options: any = {
            caso: 18,
            id_usuario: this.authService.currentUserValue.id,
            motivo: "Videollamada",
            detalle: detallecita,
            fecha: moment().format("YYYY-MM-DD"),
            hora: moment().format("HH:MM"),
            idc: this.infocita[0].idCitas,
          };
          this.http
            .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
            .subscribe((respuesta: any) => {
              if (respuesta == "Se inserto") {
                if (estatu === "Confirmado") {
                  enviar = 1;
                  if (observaciones) {
                    mensaje =
                      "Información de solicitud de videollamada--salto--Doctor: " +
                      this.nombredoctor +
                      "--salto--Paciente: " +
                      this.nombrepaciente +
                      "--salto--Fecha y Hora: " +
                      fecha1 +
                      " " +
                      horario +
                      "--salto--Motivo: Videollamada" +
                      "--salto--Observaciones: " +
                      observaciones +
                      "--salto--Nota: Para entrar a la videollamada debes ingresar al siguiente link, y ingresar la siguiente contraseña: " +
                      contra +
                      "--salto--";
                  } else {
                    mensaje =
                      "Información de solicitud de videollamada--salto--Doctor: " +
                      this.nombredoctor +
                      "--salto--Paciente: " +
                      this.nombrepaciente +
                      "--salto--Fecha y Hora: " +
                      fecha1 +
                      " " +
                      horario +
                      "--salto--Motivo: Videollamada" +
                      "--salto--Nota: Para entrar a la videollamada debes ingresar al siguiente link, y ingresar la siguiente contraseña: " +
                      contra +
                      "--salto--";
                  }
                } else {
                  enviar = 2;
                  if (observaciones) {
                    mensaje =
                      "Información de solicitud de videollamada--salto--Doctor: " +
                      this.nombredoctor +
                      "--salto--Paciente: " +
                      this.nombrepaciente +
                      "--salto--Fecha y Hora: " +
                      fecha1 +
                      " " +
                      horario +
                      "--salto--Motivo: Videollamada" +
                      "--salto--Observaciones: " +
                      observaciones +
                      "--salto--Nota: Tu solicitud de videollamada no fue aceptada por favor comuníquese con la clínica --salto--";
                  } else {
                    mensaje =
                      "Información de solicitud de videollamada--salto--Doctor: " +
                      this.nombredoctor +
                      "--salto--Paciente: " +
                      this.nombrepaciente +
                      "--salto--Fecha y Hora: " +
                      fecha1 +
                      " " +
                      horario +
                      "--salto--Motivo: Videollamada" +
                      "--salto--Nota: Tu solicitud de videollamada no fue aceptada por favor comuníquese con la clínica --salto--";
                  }
                }

                const telefono = "+52" + this.infocita[0].CelularPaciente;

                this.http
                  .get(
                    "https://smsyemail.herokuapp.com/sms/" +
                      telefono +
                      "/" +
                      mensaje +
                      "/" +
                      0 +
                      "/" +
                      enviar
                  )
                  .subscribe(
                    (data) => {
                      console.log(JSON.stringify(data));
                    },
                    (error) => {
                      console.log("err");
                      console.log(JSON.stringify(error));
                    }
                  );

                formCita.reset();
                this.modalService.dismissAll();
                Swal.fire(mensajealert, "", "success");
                this.solicitudes();
              }
            });
        } else {
          Swal.fire("Ocurrio un error intentelo de nuevo", "", "error");
        }
      });
  }
}
