import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SolicitudesVideollamadaRoutingModule } from './solicitudes-videollamada-routing.module';
import { SolicitudesVideollamadaComponent } from './solicitudes-videollamada.component';
import { MaterialModule } from "../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SolicitudesVideollamadaComponent
  ],
  imports: [
    CommonModule,
    SolicitudesVideollamadaRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SolicitudesVideollamadaModule { }
