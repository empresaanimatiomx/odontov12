import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IniciarVideollamadaComponent } from './iniciar-videollamada.component';

const routes: Routes = [
  {
    path:'',
    component: IniciarVideollamadaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IniciarVideollamadaRoutingModule { }
