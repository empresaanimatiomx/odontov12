import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IniciarVideollamadaRoutingModule } from './iniciar-videollamada-routing.module';
import { IniciarVideollamadaComponent } from './iniciar-videollamada.component';
import { MaterialModule } from "../../../shared/material.module";

@NgModule({
  declarations: [
    IniciarVideollamadaComponent
  ],
  imports: [
    CommonModule,
    IniciarVideollamadaRoutingModule,
    MaterialModule
  ]
})
export class IniciarVideollamadaModule { }
