import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "dashboard",
    loadChildren: () =>
      import("./dashboard/dashboard.module").then((m) => m.DashboardModule),
  },
  {
    path: "administracion",
    loadChildren: () =>
      import("./administracion/administracion.module").then(
        (m) => m.AdministracionModule
      ),
  },
  {
    path: "agenda",
    loadChildren: () =>
      import("./agenda/agenda.module").then((m) => m.AgendaModule),
  },
  {
    path: "campana",
    loadChildren: () =>
      import("./campana/campana.module").then((m) => m.CampanaModule),
  },
  {
    path: "pacientes",
    loadChildren: () =>
      import("./pacientes/pacientes.module").then((m) => m.PacientesModule),
  },
  {
    path: "videollamada",
    loadChildren: () =>
      import("./videollamada/videollamada.module").then(
        (m) => m.VideollamadaModule
      ),
  },
  {
    path: "perfil",
    loadChildren: () =>
      import("./perfil/perfil.module").then((m) => m.PerfilModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminSucursalRoutingModule {}
