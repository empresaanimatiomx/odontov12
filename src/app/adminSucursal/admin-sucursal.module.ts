import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminSucursalRoutingModule } from './admin-sucursal-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminSucursalRoutingModule
  ]
})
export class AdminSucursalModule { }
