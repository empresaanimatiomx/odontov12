import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListadoPacientesRoutingModule } from './listado-pacientes-routing.module';
import { ListadoPacientesComponent } from './listado-pacientes.component';
import { MaterialModule } from "../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ListadoPacientesComponent
  ],
  imports: [
    CommonModule,
    ListadoPacientesRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ListadoPacientesModule { }
