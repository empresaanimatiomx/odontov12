import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "listado-pacientes",
    loadChildren: () =>
      import("./listado-pacientes/listado-pacientes.module").then(
        (m) => m.ListadoPacientesModule
      ),
  },
  {
    path: "nuevo-paciente",
    loadChildren: () =>
      import("./nuevo-paciente/nuevo-paciente.module").then(
        (m) => m.NuevoPacienteModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacientesRoutingModule { }
