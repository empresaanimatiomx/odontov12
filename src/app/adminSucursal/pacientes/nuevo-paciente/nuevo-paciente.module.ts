import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NuevoPacienteRoutingModule } from './nuevo-paciente-routing.module';
import { NuevoPacienteComponent } from './nuevo-paciente.component';
import { MaterialModule } from '../../../shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';


@NgModule({
  declarations: [NuevoPacienteComponent],
  imports: [
    CommonModule,
    NuevoPacienteRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    GooglePlaceModule,
    AutocompleteLibModule,
  ],
})
export class NuevoPacienteModule {}
