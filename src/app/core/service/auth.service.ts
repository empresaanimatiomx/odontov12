import { EventEmitter, Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable, of } from "rxjs";
import { map } from "rxjs/operators";
import { User } from "../models/user";
import { HttpService } from "./http.service";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  imgUser$ = new EventEmitter();
  imgClinica$ = new EventEmitter();
  nameUser$ = new EventEmitter();

  constructor(private http: HttpClient, private httpSvc: HttpService) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem("datosusuarioclinica"))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string, tipoUsuario: string) {
    let caso = tipoUsuario === "Paciente" ? 13 : 1;
    return this.http
      .post<any>(this.baseURL + "usuario.php", {
        caso,
        email: username,
        Password: password,
      })
      .pipe(
        map((user) => {
          localStorage.setItem("datosusuarioclinica", JSON.stringify(user));
          this.currentUserSubject.next(user);
          return user;
        })
      );
  }

  checkExiste(email) {
    this.httpSvc.post("usuario.php", { caso: 12, email }).subscribe({
      next: (pacientes: any) => {
        if (pacientes !== null) {
          return true
        } else {
          return false;
        }
      },
      error: (e) => console.error(e),
    });
  }

  logout() {
    localStorage.removeItem("datosusuarioclinica");
    localStorage.removeItem("perfilusuario");
    localStorage.removeItem("tabs");
    this.currentUserSubject.next(null);
    return of({ success: false });
  }
}
