import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import Swal, { SweetAlertIcon } from "sweetalert2";

@Injectable({
  providedIn: "root",
})
export class AlertService {
  alertConf: any = {
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#1584c6",
    cancelButtonColor: "#f44336",
    allowOutsideClick: false,
  };

  alertAloneConfirm: any = {
    icon: "warning",
    confirmButtonColor: "#1584c6",
    cancelButtonColor: "#28a745",
    allowOutsideClick: false,
  };

  constructor(private snackBar: MatSnackBar) {}

  subiendo(msg) {
    Swal.fire({
      title: `Subiendo ${msg}...`,
      html: "Espere un momento por favor",
      allowOutsideClick: false,
      showCancelButton: false,
      showConfirmButton: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
  }

  alert(title: string, msg: string, icon: SweetAlertIcon) {
    Swal.fire({
      title,
      html: msg,
      icon,
      confirmButtonText: "Aceptar",
      confirmButtonColor: "#1584c6",
    });
  }

  alertClose() {
    Swal.close();
  }

  alertConfirm(
    mensaje,
    title?,
    confirmButtonText = "¡Si, confirmar!",
    cancelButtonText = "¡No, cancelar!"
  ) {
    return Swal.fire({
      title,
      text: `¿Estas seguro de ${mensaje}?`,
      confirmButtonText,
      cancelButtonText,
      ...this.alertConf,
    });
  }

  alertConfirm1(mensaje, confirmButtonText = "¡Si, entendido!") {
    return Swal.fire({
      text: `${mensaje} aun no tiene equipos registrados`,
      confirmButtonText,
      ...this.alertConf,
    });
  }

  alertConfirmYes(
    html,
    confirmButtonText = "Aceptar",
    cancelButtonText = "Cancelar",
    cancel = false
  ) {
    return Swal.fire({
      html,
      confirmButtonText,
      cancelButtonText,
      showCancelButton: cancel,
      ...this.alertAloneConfirm,
    });
  }

  toast(msg) {
    this.snackBar.open(`Se actualizaron ${msg}`, "Aceptar", {
      duration: 3000,
    });
  }
}
