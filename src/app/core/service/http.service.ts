import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import * as XLSX from "xlsx";

const headers: any = new HttpHeaders({ "Content-Type": "application/json" });

@Injectable({
  providedIn: "root",
})
export class HttpService {
  public baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";

  constructor(private http: HttpClient) {}

  post(archivo, options) {
    return this.http.post(
      `${this.baseURL}${archivo}`,
      JSON.stringify(options),
      headers
    );
  }

  postpdf(archivo, options) {
    return this.http.post(
      `${this.baseURL}${archivo}`,
      JSON.stringify(options),
      headers
    );
  }

  postFile(archivo, file) {
    return this.http.post(`${this.baseURL}${archivo}`, file);
  }

  getCodPaises(): any {
    return this.http.get("assets/data/phone-code.json");
  }

  getActividades(): any {
    return this.http.get("assets/data/actividades.json");
  }

  filter(event: any, data: any): any {
    const val = event.target.value.toLowerCase();
    const keys = Object.keys(data[0]);
    const FilteredData = data.filter((item: any): any => {
      for (let i = 0; i < keys.length; i++) {
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          return true;
        }
      }
    });
    return FilteredData;
  }

  export(filename: string, data: any) {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "shopper");
    XLSX.writeFile(wb, `${filename}.xlsx`);
  }
}
