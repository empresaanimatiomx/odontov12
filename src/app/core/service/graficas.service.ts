import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class GraficasService {
  single = [
    {
      name: "Ene-Feb",
      value: 0.1,
    },
    {
      name: "Mar-Abr",
      value: 0.1,
    },
    {
      name: "May-Jun",
      value: 0.1,
    },
    {
      name: "Jul-Ago",
      value: 0.1,
    },
    {
      name: "Sep-Oct",
      value: 0.1,
    },
    {
      name: "Nov-Dic",
      value: 0.1,
    },
  ];
  single2 = [
    {
      name: "Periodico",
      value: 0.1,
    },
    {
      name: "Redes Sociales",
      value: 0.1,
    },
    {
      name: "Motor de Busqueda",
      value: 0.1,
    },
    {
      name: "Recomendacion",
      value: 0.1,
    },
    {
      name: "Otro",
      value: 0.1,
    },
  ];
  anio = new Date().getFullYear();
  meses = [
    new Date(`${this.anio}-01-01`).getTime(),
    new Date(`${this.anio}-02-28`).getTime(),
    new Date(`${this.anio}-03-01`).getTime(),
    new Date(`${this.anio}-04-30`).getTime(),
    new Date(`${this.anio}-05-01`).getTime(),
    new Date(`${this.anio}-06-30`).getTime(),
    new Date(`${this.anio}-07-01`).getTime(),
    new Date(`${this.anio}-08-31`).getTime(),
    new Date(`${this.anio}-09-01`).getTime(),
    new Date(`${this.anio}-10-31`).getTime(),
    new Date(`${this.anio}-11-01`).getTime(),
    new Date(`${this.anio}-12-31`).getTime(),
  ];

  constructor() {}

  public patientByAge(patients: any[]): any {
    let mujeresArr = [0, 0, 0, 0, 0, 0, 0, 0];
    let hombresArr = [0, 0, 0, 0, 0, 0, 0, 0];
    patients.map((paciente) => {
      if (paciente.Edad !== "") {
        const edadNumber = Number(paciente.Edad);
        if (edadNumber >= 0 && edadNumber < 5) {
          paciente.Genero === "Masculino" ? hombresArr[0]++ : mujeresArr[0]++;
        } else if (edadNumber >= 5 && edadNumber < 18) {
          paciente.Genero === "Masculino" ? hombresArr[1]++ : mujeresArr[1]++;
        } else if (edadNumber >= 18 && edadNumber < 30) {
          paciente.Genero === "Masculino" ? hombresArr[2]++ : mujeresArr[2]++;
        } else if (edadNumber >= 30 && edadNumber < 40) {
          paciente.Genero === "Masculino" ? hombresArr[3]++ : mujeresArr[3]++;
        } else if (edadNumber >= 40 && edadNumber < 50) {
          paciente.Genero === "Masculino" ? hombresArr[4]++ : mujeresArr[4]++;
        } else if (edadNumber >= 50 && edadNumber < 60) {
          paciente.Genero === "Masculino" ? hombresArr[5]++ : mujeresArr[5]++;
        } else if (edadNumber >= 60 && edadNumber < 70) {
          paciente.Genero === "Masculino" ? hombresArr[6]++ : mujeresArr[6]++;
        } else if (edadNumber >= 70) {
          paciente.Genero === "Masculino" ? hombresArr[7]++ : mujeresArr[7]++;
        }
      }
    });
    return { mujeresArr, hombresArr };
  }

  public citasByEstatus(citas: any[]): any {
    let atendidas = 0,
      noAtendidas = 0,
      canceladas = 0;
    if (citas !== null) {
      citas.map((cita) => {
        const status = cita.EstatusCita.replace(/\s+/g, "");
        if (status === "Finalizada") {
          atendidas++;
        } else if (status === "Confirmado" || status === "Porconfirmar") {
          noAtendidas++;
        } else if (status === "HoraCancelada" || status === "HoraCancelanda") {
          canceladas++;
        }
      });
    }

    return { atendidas, noAtendidas, canceladas };
  }

  public patientsByCreatedAt(patients: any[]): any {
    let montsArr = [...this.single];
    patients.map((paciente) => {
      if (paciente.fechaRegistro !== "") {
        const fecha = new Date(paciente.fechaRegistro).getTime();
        if (fecha >= this.meses[0] && fecha <= this.meses[1]) {
          montsArr[0].value++;
        } else if (fecha >= this.meses[2] && fecha <= this.meses[3]) {
          montsArr[1].value++;
        } else if (fecha >= this.meses[4] && fecha <= this.meses[5]) {
          montsArr[2].value++;
        } else if (fecha >= this.meses[6] && fecha <= this.meses[7]) {
          montsArr[3].value++;
        } else if (fecha >= this.meses[8] && fecha <= this.meses[9]) {
          montsArr[4].value++;
        } else if (fecha >= this.meses[10] && fecha <= this.meses[11]) {
          montsArr[5].value++;
        }
      }
    });
    return { montsArr };
  }

  public patientsByEncontraste(patients: any[]): any {
    let EncontrasteArr = [...this.single2];
    patients.map((paciente) => {
      if (paciente.Encontraste !== "") {
        const estatus = paciente.Encontraste;
        if (estatus === "Periodico") {
          EncontrasteArr[0].value++;
        } else if (estatus === "Redes Sociales") {
          EncontrasteArr[1].value++;
        } else if (estatus === "Motor de Busqueda") {
          EncontrasteArr[2].value++;
        } else if (estatus === "Recomendacion") {
          EncontrasteArr[3].value++;
        } else if (estatus === "Otro") {
          EncontrasteArr[4].value++;
        }
      }
    });
    return { EncontrasteArr };
  }
}
