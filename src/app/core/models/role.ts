export enum Role {
  All = 'All',
  Admin = 'Admin',
  Doctor = 'Doctor',
  Asistente = 'Asistente',
  AdminSucursal = 'AdminSucursal',
  Paciente = 'Paciente',
}
