import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "agenda",
    loadChildren: () =>
      import("./agenda/agenda.module").then((m) => m.AgendaModule),
  },
  {
    path: "equipo",
    loadChildren: () =>
      import("./equipo/equipo.module").then((m) => m.EquipoModule),
  },
  {
    path: "pacientes",
    loadChildren: () =>
      import("./pacientes/pacientes.module").then((m) => m.PacientesModule),
  },
  {
    path: "perfil",
    loadChildren: () =>
      import("./perfil/perfil.module").then((m) => m.PerfilModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsistenteRoutingModule {}
