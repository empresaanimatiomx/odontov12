import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetallePacienteRoutingModule } from './detalle-paciente-routing.module';
import { DetallePacienteComponent } from './detalle-paciente.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../../shared/material.module';


@NgModule({
  declarations: [DetallePacienteComponent],
  imports: [
    CommonModule,
    DetallePacienteRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
  ],
})
export class DetallePacienteModule {}
