import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetallePacienteComponent } from './detalle-paciente.component';

const routes: Routes = [
  {
    path:'',
    component: DetallePacienteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetallePacienteRoutingModule { }
