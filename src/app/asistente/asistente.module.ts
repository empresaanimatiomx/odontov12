import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsistenteRoutingModule } from './asistente-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AsistenteRoutingModule
  ]
})
export class AsistenteModule { }
