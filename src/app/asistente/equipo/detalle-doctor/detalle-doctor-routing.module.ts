import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetalleDoctorComponent } from './detalle-doctor.component';

const routes: Routes = [
  {
    path:'',
    component: DetalleDoctorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetalleDoctorRoutingModule { }
