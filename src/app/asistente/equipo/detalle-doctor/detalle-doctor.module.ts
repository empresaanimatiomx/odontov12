import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetalleDoctorRoutingModule } from './detalle-doctor-routing.module';
import { DetalleDoctorComponent } from './detalle-doctor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../../shared/material.module';


@NgModule({
  declarations: [DetalleDoctorComponent],
  imports: [
    CommonModule,
    DetalleDoctorRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class DetalleDoctorModule {}
