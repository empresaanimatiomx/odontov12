import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListadoDoctoresComponent } from './listado-doctores.component';

const routes: Routes = [
  {
    path:'',
    component: ListadoDoctoresComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListadoDoctoresRoutingModule { }
