import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "src/app/core/service/auth.service";
const headers: any = new HttpHeaders({ "Content-Type": "application/json" });
import Swal from "sweetalert2";
import moment from "moment";
moment.locale("es");
import esLocale from "@fullcalendar/core/locales/es";

@Component({
  selector: "app-crear-cita",
  templateUrl: "./crear-cita.component.html",
  styleUrls: [],
})
export class CrearCitaComponent implements OnInit {
  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  NuevaCitaForm: FormGroup;
  infocita: any = [];
  nombredoctor = "";
  doctores: any = [];
  fechaLimite = new Date();
  nombremotivo = "";
  motivosconsulta: any = [];
  motivootro = false;
  idclinica;
  nombrepaciente = "";
  pacientes: any = [];
  doctorvalor = false;
  nombre_sucursal = "";

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private route: ActivatedRoute
  ) {
    this.idclinica = this.route.snapshot.params.idc;
  }

  ngOnInit(): void {
    this.NuevaCitaForm = this.formBuilder.group({
      paciente: ["", [Validators.required]],
      doctor: ["", [Validators.required]],
      fecha: ["", [Validators.required]],
      horario: ["", [Validators.required]],
      motivo: ["Solicitar un video consulta", [Validators.required]],
      observaciones: [""],
      estatus: ["Por confirmar"],
      servicio: [0],
    });
    this.swalalertmensaje(this.idclinica);
  }

  onSubmit() {}

  obtenernombredoctor(option: any) {
    this.nombredoctor = option.source.triggerValue;
    this.doctorvalor = true;
  }

  obtenernombremotivo(option: any) {
    this.nombremotivo = option.source.triggerValue;
  }

  obtenernombrepaciente(option: any) {
    this.nombrepaciente = option.source.triggerValue;
  }

  ObtenerDoctores(idclinica: any, idPaciente: any) {
    const options: any = { caso: 5, idClinica: idclinica };
    this.http
      .post(`${this.baseURL}pacientes.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.doctores = respuesta;
        this.obtenerpacientes(this.idclinica, idPaciente);
      });
  }

  // metodo para obtener los convenios
  obtenermotivos(idclinica) {
    const options: any = { caso: 11, idC: idclinica };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        this.motivosconsulta = respuesta;
      });
  }

  otromotivo(valor) {
    this.motivootro = valor;
  }

  FirstUppercaseLetter(name: string) {
    const texto = this.NuevaCitaForm.get(name).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      this.NuevaCitaForm.controls[name].setValue(textofinal);
    }
  }

  swalalertmensaje(idclinica) {
    Swal.fire({
      title: "Ingresa tu numero de teléfono",
      input: "number",
      inputAttributes: {
        autocapitalize: "off",
      },
      showDenyButton: true,
      confirmButtonText: "Verificar",
      showLoaderOnConfirm: true,
      denyButtonText: "Cancelar",
      preConfirm: (num) => {
        if (num.length === 0) {
          Swal.showValidationMessage("El numero no puede estar vacío");
        } else if (num.length === 10) {
          const options: any = { caso: 22, idC: idclinica, numero: num };
          this.http
            .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
            .subscribe((respuesta: any) => {
              this.nombre_sucursal = respuesta[0].NombreSucursal;
              if (respuesta.toString() === "Vacio") {
                this.router.navigate(["/authentication/signin"]);
                Swal.fire(
                  `El numero no esta registrado ponte en contacto con la clínica por favor`,
                  "",
                  "error"
                );
              } else {
                this.ObtenerDoctores(this.idclinica, respuesta[0].idPacientes);
                this.obtenermotivos(this.idclinica);
              }
            });
        } else {
          Swal.showValidationMessage(
            `El numero debe ser menor o igual 10 dígitos`
          );
        }
      },
      preDeny: () => {
        this.router.navigate(["/authentication/signin"]);
      },
      allowOutsideClick: () => !Swal.isLoading(),
    });
  }

  obtenerpacientes(idclinica, idP) {
    const options: any = { caso: 10, idC: idclinica };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.pacientes = respuesta;
        this.NuevaCitaForm.controls["paciente"].setValue(idP);
        const [{ idDoctor }] = this.pacientes.filter(
          (paciente) => paciente.idPacientes === idP
        );
        this.NuevaCitaForm.controls["doctor"].setValue(idDoctor);
        this.NuevaCitaForm.controls["doctor"].disable();
      });
  }

  ValidarHorario(formCita: FormGroup) {
    const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
    const options: any = { caso: 4, nuevacita: formCita.value, fecha: fecha };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta == "se encontro") {
          Swal.fire("El doctor no esta disponible en ese horario", "", "error");
          this.NuevaCitaForm.controls["horario"].setValue("");
        }
      });
  }

  cancelarevento() {
    this.router.navigate(["/authentication/signin"]);
  }

  crearcita(formCita: FormGroup) {
    const fecha = moment(formCita.value.fecha).format("YYYY-MM-DD");
    const options: any = {
      caso: 25,
      nuevacita: formCita.value,
      fecha: fecha,
      nombrelink:
        this.nombre_sucursal + formCita.value.paciente + formCita.value.doctor,
      idclinica:"0",
    };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        if (respuesta != null) {
          this.router.navigate(["/authentication/signin"]);
          Swal.fire(
            "Se envió la solicitud de videollamada correctamente a la clínica",
            "",
            "success"
          );
        } else {
          console.log(respuesta);
        }
      });
  }
}
