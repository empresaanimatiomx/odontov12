import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { isNullOrUndefined } from "@swimlane/ngx-datatable";
import { UnsubscribeOnDestroyAdapter } from "../../shared/UnsubscribeOnDestroyAdapter";
import { AuthService } from "../../core/service/auth.service";
import { Role } from "../../core/models/role";
import { AlertService } from "../../core/service/alert.service";
import { HttpService } from "../../core/service/http.service";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html",
  styleUrls: [],
})
export class SigninComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  authForm: FormGroup;
  submitted = false;
  error = "";
  hide = true;
  mensajes_validacion = {
    tipoUsuario: [
      { type: "required", message: "El nombre de usuario es requerido" },
    ],
    username: [
      { type: "required", message: "El nombre de usuario es requerido" },
      {
        type: "pattern",
        message: "El nombre de usuario no es un correo valido",
      },
    ],
    password: [
      { type: "required", message: "La contraseña es requerida" },
      {
        type: "minlength",
        message: "La contraseña debe de ser mínimo de 6 caracteres",
      },
    ],
  };

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private sweet: AlertService,
    private http: HttpService
  ) {
    super();
    if (this.authService.currentUserValue?.role) {
      const role = this.authService.currentUserValue?.role;
      if (role === Role.All || role === Role.Admin) {
        this.router.navigate(["/admin/dashboard/main"]);
      } else if (role === Role.Doctor) {
        this.router.navigate(["/doctor/dashboard"]);
      } else if (role === Role.Asistente) {
        this.router.navigate(["/asistente/agenda"]);
      } else if (role === Role.AdminSucursal) {
        this.router.navigate(["/adminSucursal/dashboard"]);
      } else if (role === Role.Paciente) {
        this.router.navigate(["/paciente/documentacion"]);
      } else {
        this.router.navigate(["/admin/dashboard/main"]);
      }
    }
  }

  ngOnInit() {
    this.authForm = this.formBuilder.group({
      tipoUsuario: [
        "",
        Validators.compose([
          Validators.required,
        ]),
      ],
      username: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ]),
      ],
      password: [
        "",
        Validators.compose([Validators.required, Validators.minLength(6)]),
      ],
    });
  }

  get username() {
    return this.authForm.get("username");
  }

  get f() {
    return this.authForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.error = "";
    this.authService
      .login(
        this.f.username.value,
        this.f.password.value,
        this.f.tipoUsuario.value
      )
      .subscribe({
        next: (res: any) => {
          if (
            res !== "La contrasena es incorrecta" &&
            res !== "El usuario no esta registrado"
          ) {
            const role = this.authService.currentUserValue?.role;
            const clinica = this.authService.currentUserValue?.idclinica;
            if (role === Role.All || role === Role.Admin) {
              if (isNullOrUndefined(clinica)) {
                this.router.navigate(["/admin/configuracion/clinica"]);
              } else {
                this.router.navigate(["/admin/dashboard/main"]);
              }
            } else if (role === Role.Doctor) {
              this.router.navigate(["/doctor/dashboard"]);
            } else if (role === Role.Asistente) {
              this.router.navigate(["/asistente/agenda"]);
            } else if (role === Role.AdminSucursal) {
              this.router.navigate(["/adminSucursal/dashboard"]);
            } else if (role === Role.Paciente) {
              this.router.navigate(["/paciente/documentacion"]);
            } else {
              this.router.navigate(["/authentication/signin"]);
            }
          } else {
            this.error = res;
          }
        },
        error: (e) => {
          this.error = e;
          this.submitted = false;
        },
      });
  }

  getPayment(idUser) {
    this.http
      .post("usuario.php", { caso: 2, idUser })
      .subscribe(([{ statusSubscripcion }]: any) => {
        if (statusSubscripcion === "pendiente") {
          this.sweet.alert(
            "",
            "Su pago esta pendiente puede seguir utilizando el sistema, si en los proximos 2 dias no se ve reflejado su pago se le negara el acceso al sistema",
            "warning"
          );
        } else if (statusSubscripcion === "no pagado") {
          this.sweet
            .alertConfirmYes(
              "No se ah podido realizar el cobro, no podra usar el sistema!, si ya efectuo el pago click en el boton para consultar su estatus o comuniquese con el admin del sistema",
              "Consultar"
            )
            .then((action) => {
              if (action.isConfirmed) {
                this.getPayment(idUser);
              }
            });
        } else if (statusSubscripcion === "suscripcion cancelada") {
          this.sweet
            .alertConfirmYes(
              "Su suscripción fue cancelada, no podra usar el sistema!, si ya efectuo el pago click en el boton para consultar o puede reanudar su suscripción",
              "Consultar",
              "Reanudar suscripción",
              true
            )
            .then((action) => {
              if (action.isConfirmed) {
                this.getPayment(idUser);
              } else {
                this.authService.logout().subscribe((res) => {
                  if (!res.success) {
                    this.router.navigate(["/authentication/signin"]);
                  }
                });
              }
            });
        } else if (statusSubscripcion === "cobro expirado") {
          this.sweet
            .alertConfirmYes(
              "El cobro de la factura actual no pudo realizarse, si en los proximos 2 dias no se ve reflejado se le negara el acceso!, si ya efectuo el pago click en el boton para consultar o comuniquese con el Admin del sistema",
              "Consultar"
            )
            .then((action) => {
              if (action.isConfirmed) {
                this.getPayment(idUser);
              } else {
                this.authService.logout().subscribe((res) => {
                  if (!res.success) {
                    this.router.navigate(["/authentication/signin"]);
                  }
                });
              }
            });
        }
      });
  }
}
