import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../core/service/alert.service';
import { HttpService } from '../../core/service/http.service';

@Component({
  selector: "app-create-password",
  templateUrl: "./create-password.component.html",
  styles: [],
})
export class CreatePasswordComponent implements OnInit {
  correo;
  chide = true;
  restablecerForm: FormGroup;
  mensajes_validacion = {
    contra: [
      { type: "required", message: "La contraseña es requerida" },
      {
        type: "pattern",
        message:
          "Al menos 8 caracteres de longitud, Letras minusculas, Letras mayúsculas, Números, Caracteres especiales",
      },
    ],
    contrac: [{ type: "required", message: "La contraseña es requerida" }],
  };

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private postSvc: HttpService,
    private alertSvc: AlertService,
    private router: Router
  ) {
    this.restablecerForm = this.formBuilder.group({
      contra: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(
            "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
          ),
        ]),
      ],
      contrac: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(
            "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
          ),
        ]),
      ],
    });
  }

  ngOnInit(): void {
    this.correo = this.route.snapshot.params.correo;
  }

  validarpass() {
    if (
      this.restablecerForm.get("contra").value ===
      this.restablecerForm.get("contrac").value
    ) {
    } else {
      this.alertSvc.alert("", "Las contraseñas no coiciden!", "error");
    }
  }

  onSubmit() {
    const { contra } = this.restablecerForm.value;
    this.postSvc
      .post("auth.php", { caso: 8, mail: this.correo, Password: contra })
      .subscribe((resp: any) => {
        if (resp === "Se actualizo") {
          this.alertSvc.alert("", "Se restableció su contraseña", "success");
          this.restablecerForm.reset();
          this.router.navigate(["/authentication/signin"]);
        } else {
          this.alertSvc.alert(
            "",
            "Ocurrió un error intente de nuevo!",
            "error"
          );
        }
      });
  }
}
