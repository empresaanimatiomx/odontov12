import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/core/service/auth.service';
const headers: any = new HttpHeaders({ "Content-Type": "application/json" });
import Swal from "sweetalert2";
import moment from "moment";
moment.locale("es");
import esLocale from "@fullcalendar/core/locales/es";

@Component({
  selector: 'app-confirmarcita',
  templateUrl: './confirmarcita.component.html',
  styleUrls: []
})
export class ConfirmarcitaComponent implements OnInit {

  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";

  authForm: FormGroup;

  infocita: any = [];

  idcita = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private http: HttpClient,
    private route: ActivatedRoute
  ) {
    this.idcita = this.route.snapshot.params.idc;
  }

  ngOnInit(): void {
    this.authForm = this.formBuilder.group({
      paciente: [""],
      doctor: [""],
      fecha: [""],
      hora: [""],
      motivo: [""],
      servicio: [""],
      observaciones: [""],
    });
    this.obtenerinformaciondecita(this.idcita);
  }

  obtenerinformaciondecita(idc){
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 16, idc: idc };
    const URL: any = this.baseURL + "citas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        if (respuesta.toString() === "Vacio") {
          Swal.fire("Ya asignaste un estatus a esta cita", "", "error");
          this.router.navigate(["/authentication/signin"]);
        } else {
          this.infocita = respuesta;
          const fecha = moment(this.infocita[0].Fecha).format("DD/MM/YYYY");
          this.authForm.setValue({
            paciente: this.infocita[0].NombrePaciente + ' ' + this.infocita[0].ApellidoPaciente,
            doctor: this.infocita[0].NombreUsuario + ' ' + this.infocita[0].AppelidosUsuario,
            fecha: new Date(fecha),
            hora: this.infocita[0].Horario,
            motivo: this.infocita[0].NombreMotivo,
            servicio: this.infocita[0].NombreServicio,
            observaciones: this.infocita[0].Observaciones,
          });
        }
      });
  }

  onSubmit() {
  }

  estatuscita(estatus){
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 17, idc: this.idcita, status: estatus };
    const URL: any = this.baseURL + "citas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        if (respuesta.toString() === "Se modifico") {
          let mensaje = 'Modificación de estatus';
          let detalle = 'El estatus de la cita se ha cambia de Por confirmar' + ' a ' + estatus;
          const options: any = {
            caso: 18,
            id_usuario: this.infocita[0].idPacientes,
            motivo: mensaje,
            detalle: detalle,
            fecha: moment().format("YYYY-MM-DD"),
            hora: moment().format("HH:MM"),
            idc: this.idcita
          };
          const URL: any = this.baseURL + "citas.php";
          this.http
            .post(URL, JSON.stringify(options), headers)
            .subscribe((respuesta: any) => {
              if (respuesta == "Se inserto") {
                Swal.fire("Se actualizo el estatus de la cita correctamente", "", "success");
                this.router.navigate(["/authentication/signin"]);
              }
            });
        } else {
          Swal.fire("Hubo un error, intenta de nuevo por favor", "", "error");
        }
      });
  }

}
