import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HttpService } from "../../core/service/http.service";
import { AlertService } from "../../core/service/alert.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: [],
})
export class ForgotPasswordComponent implements OnInit {
  authForm: FormGroup;
  returnUrl: string;
  isEmail = /\S+@\S+\.\S+/;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpService,
    private sweet: AlertService,
    private router:Router
  ) {}
  ngOnInit() {
    this.authForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.pattern(this.isEmail)]],
    });
  }

  checkEmail(form) {
    this.http.post("usuario.php", { caso: 11, email: form.email }).subscribe({
      next: (res) => {
        if (res !== null) {
          this.router.navigate(["/authentication/confirm-forgot", form.email]);
        } else {
          this.sweet.alert(
            "",
            "El correo electrónico no existe en la base de datos",
            "error"
          );
        }
      },
      error: (err) => {},
    });
  }
}
