import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-confim-forgot',
  templateUrl: './confim-forgot.component.html',
  styles: []
})
export class ConfimForgotComponent {
email;
  constructor(private route: ActivatedRoute, private router: Router) {
    this.email = this.route.snapshot.params.email;
  }


reestablecer(){
  this.router.navigate(["/authentication/forgot-password"]);
}
}
