import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import moment from "moment";
moment.locale("es");
import { Chart } from "chart.js";
const headers: any = new HttpHeaders({ "Content-Type": "application/json" });
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import Swal from "sweetalert2";
import { ActivatedRoute, Router } from "@angular/router";

declare var JitsiMeetExternalAPI: any;

@Component({
  selector: 'app-videollamada-vista',
  templateUrl: './videollamada-vista.component.html',
  styleUrls: []
})
export class VideollamadaVistaComponent implements OnInit {

  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  infopaciente: any = [];
  idpaciente: number;
  idClinica: number;
  yearselect = "";
  citaspacientetablafinalizadas: any = [];
  citaspaciente: any = [];
  chart: any;
  arreglodeyear: any = [];

  api: any;
  domain: string = "meet.jit.si";
  options: any;
  user: any;

  isAudioMuted = false;
  isVideoMuted = false;

  nombreclinica = '';
  nombreadministrador: any;
  referencia = '';
  idcita = '';
  vertexto = false;

  constructor(
    private httpClient: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
  ) {
   }

   ngOnInit(): void {
    this.swalalertmensaje();
  }

  iniciarllamada(): void {
    this.vertexto = true;
    this.options = {
        roomName: this.referencia,
        width: 1000,
        height: 700,
        configOverwrite: { prejoinPageEnabled: false },
        interfaceConfigOverwrite: {
            // overwrite interface properties
        },
        parentNode: document.querySelector('#jitsi-iframe'),
        userInfo: {
            displayName: this.user.name
        }
    }

    this.api = new JitsiMeetExternalAPI(this.domain, this.options);

    this.api.addEventListeners({
        readyToClose: this.handleClose,
        participantLeft: this.handleParticipantLeft,
        participantJoined: this.handleParticipantJoined,
        videoConferenceJoined: this.handleVideoConferenceJoined,
        videoConferenceLeft: this.handleVideoConferenceLeft,
        audioMuteStatusChanged: this.handleMuteStatus,
        videoMuteStatusChanged: this.handleVideoStatus
    });
  }

  //obtener informacion del paciente
  informaciondoctor(idD) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 28, id_usuario: idD };
    const URL: any = this.baseURL + "citas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta: any) => {
        console.log(respuesta);
        this.infopaciente = respuesta;
        this.iniciarllamada();
      }
    );
  }

  datos() {
    if (this.yearselect) {
      this.yearselect = this.yearselect;
    } else {
      this.yearselect = String(new Date().getFullYear());
    }
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = {
      caso: 0,
      id_usuario: this.idpaciente,
      year: this.yearselect,
    };
    const URL: any = this.baseURL + "graficas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        if (respuesta) {
          this.citaspaciente = respuesta;
          this.obtenercitasfinalizadas();
        } else {
          this.citaspaciente = [];
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }

  obtenercitasfinalizadas() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 3, id_usuario: this.idpaciente };
    const URL: any = this.baseURL + "graficas.php";
    this.httpClient.post(URL, JSON.stringify(options), headers).subscribe(
      (respuesta) => {
        if (respuesta.toString() !== "Vacio") {
          this.citaspacientetablafinalizadas = respuesta;
        } else {
          this.citaspacientetablafinalizadas = [];
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.name + " " + error.message);
      }
    );
  }


  executeCommand(command: string) {
    this.api.executeCommand(command);;
    if(command == 'hangup') {
        this.router.navigate(['/admin/dashboard/main']);
        return;
    }

    if(command == 'toggleAudio') {
        this.isAudioMuted = !this.isAudioMuted;
    }

    if(command == 'toggleVideo') {
        this.isVideoMuted = !this.isVideoMuted;
    }
  }

  handleClose = () => {
    console.log("handleClose");
  }

  handleParticipantLeft = async (participant) => {
    console.log("handleParticipantLeft", participant); // { id: "2baa184e" }
    const data = await this.getParticipants();
  }

  handleParticipantJoined = async (participant) => {
    console.log("handleParticipantJoined", participant); // { id: "2baa184e", displayName: "Shanu Verma", formattedDisplayName: "Shanu Verma" }
    const data = await this.getParticipants();
  }

  handleVideoConferenceJoined = async (participant) => {
    console.log("handleVideoConferenceJoined", participant); // { roomName: "bwb-bfqi-vmh", id: "8c35a951", displayName: "Akash Verma", formattedDisplayName: "Akash Verma (me)"}
    const data = await this.getParticipants();
  }

  handleVideoConferenceLeft = () => {
    console.log("handleVideoConferenceLeft");
    this.router.navigate(['/admin/videollamada/historial']);
  }

  handleMuteStatus = (audio) => {
    console.log("handleMuteStatus", audio); // { muted: true }
  }

  handleVideoStatus = (video) => {
    console.log("handleVideoStatus", video); // { muted: true }
  }

  getParticipants() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(this.api.getParticipantsInfo()); // get all participants
        }, 500)
    });
  }

  swalalertmensaje() {
    Swal.fire({
      title: 'Ingresa la contraseña de la reunión',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showDenyButton: true,
      confirmButtonText: 'Verificar',
      showLoaderOnConfirm: true,
      denyButtonText: 'Cancelar',
      preConfirm: (ingreso) => {
        if (ingreso === '') {
          Swal.showValidationMessage(
            'El contraseña no puede estar vacía'
          )
        } else {
          const options: any = { caso: 27, passingreso: ingreso };
          this.httpClient
            .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
            .subscribe((respuesta: any) => {
              if (respuesta.toString() === "Vacio") {
                Swal.fire(
                  `La contraseña no es correcta, intenta de nuevo por favor`,
                  "",
                  "error"
                );
                this.swalalertmensaje();
              } else {
                var fechaf = respuesta[0].Fecha+ " " +respuesta[0].Horario;
                var fechacompuesta = moment(fechaf).format();
                const fechainicial = moment(fechacompuesta).subtract(15, 'minutes').format();
                const fechafinal = moment(fechacompuesta).subtract(-1, 'hours').format();
                const fechaactusl = moment().format();

                if (respuesta[0].EstatusCita === 'Confirmado') {
                  if (fechaactusl <= fechafinal || fechainicial >= fechaactusl) {
                    if (respuesta[0].iniciada_videollamada === '1') {
                      this.idClinica = respuesta[0].idClinica;
                      this.idpaciente = respuesta[0].idPacientes;
                      this.referencia = respuesta[0].videollamada_referencia;
                      this.nombreadministrador = respuesta[0].Nombre+ ' '+ respuesta[0].Apellido;
                      this.user = {
                        name: this.nombreadministrador // set your username
                      }
                      this.informaciondoctor(respuesta[0].idDoctores)
                    } else {
                      Swal.fire({
                        title: 'Sin Iniciar',
                        text: "El anfitrión no ha iniciado la videollamada, intenta en un momento por favor",
                        icon: 'warning',
                        showConfirmButton: true,
                        showDenyButton: false,
                        showCancelButton: false,
                        confirmButtonText: 'Aceptar',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                      }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                          this.router.navigate(["/authentication/signin"]);
                        }
                      })
                    }
                  } else {
                    this.router.navigate(["/authentication/signin"]);
                    Swal.fire(
                      `Esta videollamada ya no esta disponoble`,
                      "",
                      "error"
                    );
                  }

                } else {
                  this.router.navigate(["/authentication/signin"]);
                  Swal.fire(
                    `Esta videollamada ya no esta disponoble`,
                    "",
                    "error"
                  );
                }
              }
            });
        }
      },
      preDeny:  () => {
        this.router.navigate(["/authentication/signin"]);
      },
      allowOutsideClick: () => !Swal.isLoading()
    })
  }


}