import { Component, OnInit, NgZone, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { DatePipe } from "@angular/common";
import { AuthService } from "../../core/service/auth.service";
import { Role } from "../../core/models/role";
import { isNullOrUndefined } from "@swimlane/ngx-datatable";
import Swal from "sweetalert2";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { IPayPalConfig } from "ngx-paypal";
import { StripeService, StripeCardComponent } from "ngx-stripe";
import {
  StripeCardElementOptions,
  StripeElementsOptions,
} from "@stripe/stripe-js";
import { environment } from "../../../environments/environment";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

const headers = new HttpHeaders({ "Content-Type": "application/json" });

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: [],
})
export class SignupComponent implements OnInit {
  public payPalConfig?: IPayPalConfig;
  private baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  @ViewChild(StripeCardComponent) card: StripeCardComponent;
  options = {
    types: [],
    componentRestrictions: { country: "MX" },
  };
  RegistroForm: FormGroup;
  TarjetaForm: FormGroup;
  submitted = false;
  hide = true;
  chide = true;
  paises: any = [];
  pais;
  cardOptions: StripeCardElementOptions = {
    style: {
      base: {
        iconColor: "#666EE8",
        color: "#31325F",
        fontWeight: "300",
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: "18px",
        "::placeholder": {
          color: "#CFD7E0",
        },
      },
    },
  };
  elementsOptions: StripeElementsOptions = {
    locale: "es",
  };
  stripeTest: FormGroup;
  idSuscripcion;
  formapago;
  fecha_pago;
  inicioSubscript;
  statusSubscripcion;
  showFormCard = false;
  verPagos = false;
  matchpass = false;

  constructor(
    private httpClient: HttpClient,
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private stripeService: StripeService,
    private ngZone: NgZone,
    private modalService: NgbModal
  ) {}
  ngOnInit() {
    this.GetPaises();
    this.RegistroForm = this.formBuilder.group({
      nombreUsuario: ["", Validators.required],
      apellido: ["", Validators.required],
      genero: ["", Validators.required],
      correo: ["", [Validators.required, Validators.email]],
      telefono: ["", Validators.required],
      direccion: ["", Validators.required],
      contra: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(
            "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
          ),
        ]),
      ],
      contraC: ["", [Validators.required, Validators.minLength(8)]],
      terminos: ["", Validators.required],
    });

    this.stripeTest = this.formBuilder.group({
      name: ["", Validators.required],
    });
    this.initConfig("P-9HG67747M0301120DMGEZ3MI");
  }

  get f() {
    return this.RegistroForm.controls;
  }

  public handleAddressChange(address: Address) {
    this.RegistroForm.patchValue({
      direccion: address.formatted_address,
    });
  }

  GetPaises() {
    const options: any = { caso: 0.1 };
    this.httpClient
      .post(`${this.baseURL}usuarios.php`, JSON.stringify(options), {headers})
      .subscribe((respuesta) => {
        this.paises = respuesta;
      });
  }

  verFormCard() {
    this.showFormCard = this.showFormCard ? false : true;
  }

  public initConfig(plan_id: string): void {
    this.payPalConfig = {
      currency: "MXN",
      vault: "true",
      clientId: environment.clienId_paypal,
      style: {
        shape: "pill",
        color: "silver",
        layout: "vertical",
      },
      createSubscription: (data, actions) => {
        return actions.subscription.create({
          plan_id,
        });
      },
      onApprove: (data, actions) => {
        actions.subscription.get().then((details: any) => {
          this.idSuscripcion = details.id;
          this.formapago = "paypal";
          this.fecha_pago = details.create_time;
          this.inicioSubscript = details.create_time;
          this.statusSubscripcion = "activa";
          this.ngZone.run(() => this.onSubmit());
        });
      },
      onCancel: (data, actions) => {
        Swal.fire("Error!", "Canceló el proceso de pago", "error");
      },
      onError: (err) => {
        console.log(err);
        Swal.fire("Error!", "Ocurrio un error intentelo de nuevo", "error");
      },
    };
  }

  createToken(): void {
    const name = this.stripeTest.get("name").value;
    this.stripeService
      .createToken(this.card.element, { name })
      .subscribe((result) => {
        if (result.token) {
          console.log(result.token.id);
          this.stripeSubscript(result.token.id);
        } else if (result.error) {
          Swal.fire(
            "Error!",
            "No se aprobo la tarjeta intente con una diferente",
            "error"
          );
        }
      });
  }

  stripeSubscript(token) {
    const email = this.RegistroForm.get("correo").value;
    const options: any = { email, token };
    this.httpClient
      .post(
        `https://animatiomx.com/odonto/stripe/CreateCharge.php`,
        JSON.stringify(options),
        {headers}
      )
      .subscribe((res: any) => {
        console.log(res);
        this.idSuscripcion = res.id;
        this.formapago = "stripe";
        this.fecha_pago = res.created;
        this.inicioSubscript = res.start_date;
        this.statusSubscripcion = "pagado";
        this.onSubmit();
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.RegistroForm.valid) {
      const options: any = {
        caso: 0,
        user: this.RegistroForm.value,
        idsubs: this.idSuscripcion,
        metodoPago: this.formapago,
        fechaPago: this.fecha_pago,
        inicioSuscripcion: this.inicioSubscript,
        statusSubscripcion: this.statusSubscripcion,
        idpais: this.pais,
      };
      const URL: any = this.baseURL + "usuario.php";
      this.httpClient
        .post(URL, JSON.stringify(options), {headers})
        .subscribe((respuesta: any) => {
          if (respuesta == "Se inserto") {
            this.authService
              .login(this.f.correo.value, this.f.contra.value, "Admin")
              .subscribe((res) => {
                if (
                  res !== "La contrasena es incorrecta" &&
                  res !== "El usuario no esta registrado"
                ) {
                  const role = this.authService.currentUserValue.role;
                  const clinica = this.authService.currentUserValue.idclinica;
                  Swal.fire(
                    "Registro exitoso, se ah inciado la suscipción correctamente",
                    "",
                    "success"
                  );
                  if (role === Role.All || role === Role.Admin) {
                    if (isNullOrUndefined(clinica)) {
                      this.router.navigate(["/admin/perfil"]);
                    } else {
                      this.router.navigate(["/admin/perfil"]);
                    }
                  } else if (role === Role.Doctor) {
                    this.router.navigate(["/doctor/dashboard"]);
                  } else if (role === Role.Asistente) {
                    this.router.navigate(["/asistente/agenda"]);
                  } else if (role === Role.AdminSucursal) {
                    this.router.navigate(["/adminSucursal/dashboard"]);
                  } else {
                    this.router.navigate(["/authentication/signin"]);
                  }
                }
              });
          } else {
            console.log(respuesta);
          }
        });
    }
  }

  matchPass() {
    if (
      this.RegistroForm.get("contra").value !==
      this.RegistroForm.get("contraC").value
    ) {
      this.matchpass = true;
    } else {
      this.matchpass = false;
      this.verPagos = true;
    }
  }

  openModal(content: any) {
    this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
      size: "xl",
      centered: true,
    });
  }

  firstLetterUpercase(id) {
    const texto = (<HTMLInputElement>document.getElementById(id)).value;
    if (texto !== "") {
      const textofinal = texto[0].toUpperCase() + texto.slice(1);
      (<HTMLInputElement>document.getElementById(id)).value = textofinal;
    }
  }

  firstWordUpercase(id) {
    let texto = (<HTMLInputElement>document.getElementById(id)).value;
    let cadena = texto.toLowerCase().split(" ");
    for (let i = 0; i < cadena.length; i++) {
      cadena[i] = cadena[i].charAt(0).toUpperCase() + cadena[i].substring(1);
    }
    texto = cadena.join(" ");
    (<HTMLInputElement>document.getElementById(id)).value = texto;
  }
}
