import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { AuthenticationRoutingModule } from "./authentication-routing.module";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxPayPalModule } from "ngx-paypal";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { NgxStripeModule } from "ngx-stripe";
import { MatPasswordStrengthModule } from "@angular-material-extensions/password-strength";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatSelectModule } from "@angular/material/select";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";


import { DatePipe } from '@angular/common';

import { Page500Component } from "./page500/page500.component";
import { Page404Component } from "./page404/page404.component";
import { SigninComponent } from "./signin/signin.component";
import { SignupComponent } from "./signup/signup.component";
import { LockedComponent } from "./locked/locked.component";
import { ConfirmarcitaComponent } from './confirmarcita/confirmarcita.component';
import { CrearCitaComponent } from './crear-cita/crear-cita.component';
import { VideollamadaVistaComponent } from './videollamada-vista/videollamada-vista.component';

@NgModule({
  declarations: [
    Page500Component,
    Page404Component,
    SigninComponent,
    SignupComponent,
    LockedComponent,
    ForgotPasswordComponent,
    ConfirmarcitaComponent,
    CrearCitaComponent,
    VideollamadaVistaComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AuthenticationRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    GooglePlaceModule,
    MatDatepickerModule,
    NgxPayPalModule,
    NgxStripeModule.forRoot(
      "pk_test_51I6L9hKVeCLN5kwXOkG2xza4QcUdgg4xCdvuzCQ1GkA78j6UR1dCB6rYyaclVxrmfmbCaLacPJLk58bCbwDYK5UB00QOilUHkv"
    ),
    MatPasswordStrengthModule,
    MatSlideToggleModule,
    MatSelectModule,
    NgxDatatableModule,
    MatCheckboxModule,
  ],
  providers: [DatePipe],
})
export class AuthenticationModule {}
