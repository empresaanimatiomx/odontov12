import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LockedComponent } from './locked/locked.component';
import { Page404Component } from './page404/page404.component';
import { Page500Component } from './page500/page500.component';
import { ConfirmarcitaComponent } from './confirmarcita/confirmarcita.component';
import { CrearCitaComponent } from './crear-cita/crear-cita.component';
import { VideollamadaVistaComponent } from './videollamada-vista/videollamada-vista.component';
import { ConfimForgotComponent } from './confim-forgot/confim-forgot.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "signin",
    pathMatch: "full",
  },
  {
    path: "signin",
    component: SigninComponent,
  },
  {
    path: "signup",
    component: SignupComponent,
  },
  {
    path: "forgot-password",
    component: ForgotPasswordComponent,
  },
  {
    path: "confirm-forgot/:email",
    component: ConfimForgotComponent,
  },
  {
    path: "locked",
    component: LockedComponent,
  },
  {
    path: "page404",
    component: Page404Component,
  },
  {
    path: "page500",
    component: Page500Component,
  },
  {
    path: "confirmar-cita/:idc",
    component: ConfirmarcitaComponent,
  },
  {
    path: "crear-cita/:idc",
    component: CrearCitaComponent,
  },
  {
    path: "videollamada",
    component: VideollamadaVistaComponent,
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
