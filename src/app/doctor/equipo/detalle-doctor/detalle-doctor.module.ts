import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DetalleDoctorRoutingModule } from "./detalle-doctor-routing.module";
import { DetalleDoctorComponent } from "./detalle-doctor.component";
import { MaterialModule } from "../../../shared/material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [DetalleDoctorComponent],
  imports: [
    CommonModule,
    DetalleDoctorRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class DetalleDoctorModule {}
