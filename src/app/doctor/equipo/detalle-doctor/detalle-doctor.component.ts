/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../../../core/service/http.service';

@Component({
  selector: "app-detalle-doctor",
  templateUrl: "./detalle-doctor.component.html",
  styles: [],
})
export class DetalleDoctorComponent implements OnInit {
  roomForm: FormGroup;
  idDoctor;
  doctorInfo;

  constructor(
    private http: HttpService,
    private fb: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.roomForm = this.fb.group({
      NombreUsuario: [""],
      Apellidos: [""],
      RFC: [""],
      Direccion: [""],
      Celular: [""],
      Correo: [""],
      FechaNac: [""],
      Edad: [""],
      Tipo_usuario: [""],
      Sucursal: [""],
      GSanguineo: [""],
      Genero: [""],
      EstadoCivil: [""],
      Titulo: [""],
      Especialidad: [""],
      Cedula: [""],
    });
    this.route.params.subscribe({
      next: (res) => (this.idDoctor = res.idDoctor),
      error: (err) => console.log(err),
    });
  }

  ngOnInit(): void {
    this.getDocotorInfo();
  }

  getDocotorInfo() {
    this.http
      .post("usuarios.php", { caso: 20, idUsuario: this.idDoctor })
      .subscribe({
        next: ([respuesta]: any) => {
          this.doctorInfo = respuesta;
          this.roomForm.patchValue(this.doctorInfo);
          this.roomForm.disable();
        },
        error: (res) => console.log(res),
      });
  }
}
