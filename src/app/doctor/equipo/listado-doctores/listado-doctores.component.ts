import { Component, OnInit, ViewChild } from "@angular/core";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { AlertService } from "../../../core/service/alert.service";
import { HttpService } from "../../../core/service/http.service";

@Component({
  selector: "app-listado-doctores",
  templateUrl: "./listado-doctores.component.html",
  styles: [],
})
export class ListadoDoctoresComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  doctores = [];
  doctoresFilter = [];

  constructor(private http: HttpService, private sweet: AlertService) {}

  ngOnInit(): void {
    this.getDoctores();
  }

  getDoctores(refresh?): void {
    const { sucursal } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.http.post("usuarios.php", { caso: 19, sucursal }).subscribe({
      next: (respuesta: any) => {
        this.doctores = respuesta;
        this.doctoresFilter = respuesta;
        refresh && this.sweet.toast("los doctores");
      },
      error: (res) => console.log(res),
    });
  }

  filterDatatable(event: any) {
    if (event.target.value === "") {
      this.doctores = this.doctoresFilter;
    }
    this.doctores = this.http.filter(event, this.doctoresFilter);
    this.table.offset = 0;
  }

  exportToExcel() {
    this.http.export("Listado_doctores", this.doctoresFilter);
  }

}
