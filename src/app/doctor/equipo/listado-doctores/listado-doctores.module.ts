import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ListadoDoctoresRoutingModule } from "./listado-doctores-routing.module";
import { ListadoDoctoresComponent } from "./listado-doctores.component";
import { MaterialModule } from "../../../shared/material.module";

@NgModule({
  declarations: [ListadoDoctoresComponent],
  imports: [CommonModule, ListadoDoctoresRoutingModule, MaterialModule],
})
export class ListadoDoctoresModule {}
