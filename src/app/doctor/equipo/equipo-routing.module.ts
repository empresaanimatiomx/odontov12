import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "listado-doctores",
    loadChildren: () =>
      import("./listado-doctores/listado-doctores.module").then(
        (m) => m.ListadoDoctoresModule
      ),
  },
  {
    path: "detalle-doctor/:idDoctor",
    loadChildren: () =>
      import("./detalle-doctor/detalle-doctor.module").then(
        (m) => m.DetalleDoctorModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EquipoRoutingModule { }
