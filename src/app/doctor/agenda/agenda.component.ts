import { Component, ViewChild, OnInit, ElementRef } from "@angular/core";
import { EventClickArg } from "@fullcalendar/angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Calendar } from "./calendar.model";
import esLocale from "@fullcalendar/core/locales/es";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { HttpService } from "../../core/service/http.service";
import { AlertService } from "../../core/service/alert.service";

@Component({
  selector: "app-agenda",
  templateUrl: "./agenda.component.html",
  styles: [],
})
export class AgendaComponent implements OnInit {
  @ViewChild("calendar", { static: false })
  calendar: Calendar | null;
  @ViewChild("nuevacita", { static: false }) nuevacita: ElementRef;
  public addCusForm: FormGroup;
  dialogTitle: string;
  calendarData: any;
  localMx = esLocale;
  modalInfo;
  calendarOptions = {
    headerToolbar: {
      left: "prev,next today",
      center: "title",
      right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek",
    },
    initialView: "dayGridMonth",
    events: [],
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    eventClick: this.handleEventClick.bind(this),
    locale: this.localMx,
  };
  idClinica;
  nuevaCitaForm: FormGroup;
  isEmail = /\S+@\S+\.\S+/;
  pacientes = [];
  motivos = [];
  servicios = [];

  constructor(
    private fb: FormBuilder,
    public http: HttpService,
    private modalService: NgbModal,
    private sweet: AlertService
  ) {
    this.dialogTitle = "Agregar nuevo evento";
    this.calendar = new Calendar({});
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.nuevaCitaForm = this.fb.group({
      idPacientes: ["", [Validators.required]],
      idDoctores: [id, [Validators.required]],
      Fecha: ["", [Validators.required]],
      Horario: ["", [Validators.required]],
      Motivos: ["", [Validators.required]],
      idServicios: ["", [Validators.required]],
      EstatusCita: ["", [Validators.required]],
      Observaciones: [""],
      telefonoMovil: [""],
      email: [""],
    });
  }

  public ngOnInit(): void {
    this.getEvents();
    this.getPacientes();
    this.getServicios();
    this.getMotivos();
  }

  getEvents() {
    const { id } = JSON.parse(localStorage.getItem("datosusuarioclinica"));
    this.http
      .post("citas.php", { caso: 31, idDoctor: id })
      .subscribe((event: any) => {
        if (event !== null) {
          this.calendarOptions.events = event;
        }
      });
  }

  getPacientes(): void {
    const { sucursal } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.http.post("pacientes.php", { caso: 17, sucursal }).subscribe({
      next: (respuesta: any) => {
        this.pacientes = respuesta;
      },
      error: (res) => console.log(res),
    });
  }

  getServicios() {
    const { sucursal } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.http.post("servicios.php", { caso: 6, sucursal }).subscribe({
      next: (respuesta: any) => {
        this.servicios = respuesta;
      },
      error: (err) => console.log(err),
    });
  }

  getMotivos() {
    const { sucursal } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.http.post("citas.php", { caso: 32, sucursal }).subscribe({
      next: (respuesta: any) => {
        this.motivos = respuesta;
      },
      error: (err) => console.log(err),
    });
  }

  guardarNuevaCita(formCita: FormGroup) {
    this.modalService.dismissAll();
    const { idclinica } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    let obj;
    if (this.modalInfo) {
      obj = { caso: 34, idc: this.modalInfo.idCitas };
    } else {
      formCita.value.Fecha = this.formatDate(formCita.value.Fecha);
      obj = { caso: 33, idclinica };
    }
    const options = {
      ...obj,
      nuevacita: formCita.value,
    };
    this.http.post("citas.php", options).subscribe((respuesta: any) => {
      if (respuesta === "Se inserto") {
        formCita.reset();
        this.getEvents();
        this.sweet.alert("", "Se agregó la cita", "success");
      } else if (respuesta === "Se modifico") {
        formCita.reset();
        this.getEvents();
        this.sweet.alert("", "Se modificó la cita", "success");
      } else {
        this.sweet.alert("", "Ocurrió un error intente de nuevo", "error");
      }
    });
  }

  deleteCita(formCita: FormGroup) {
    this.modalService.dismissAll();
    this.http
      .post("citas.php", { caso: 9, idc: this.modalInfo.idCitas })
      .subscribe({
        next: (respuesta: any) => {
          if (respuesta === "Se elimino") {
            formCita.reset();
            this.getEvents();
            this.sweet.alert("", "Se eliminó la cita", "success");
          } else {
            this.sweet.alert("", "Ocurrió un error intente de nuevo", "error");
          }
        },
        error: (err) => console.log(err),
      });
  }

  handleEventClick(Info: EventClickArg) {
    const event = Info?.event?._def?.extendedProps;
    this.abrirModal(this.nuevacita, event);
  }

  abrirModal(modal, info?) {
    if (info) {
      this.nuevaCitaForm.patchValue(info);
      this.modalInfo = info;
    } else {
      this.modalInfo = undefined;
    }
    this.modalService.open(modal, {
      ariaLabelledBy: "modal-basic-title",
      centered: true,
      size: "lg",
    });
  }

  cerrarModal(formCita: FormGroup) {
    formCita.reset();
    this.modalService.dismissAll();
  }

  patchValue(paciente): any {
    this.nuevaCitaForm.patchValue({
      telefonoMovil: paciente.Celular,
      email: paciente.Correo,
    });
  }

  formatDate(fecha: Date): string {
    const day = fecha.getDate();
    const month = fecha.getMonth() + 1;
    const year = fecha.getFullYear();
    if (month < 10) {
      return `${year}-0${month}-${day}`;
    } else {
      return `${year}-${month}-${day}`;
    }
  }
}
