import { Page404Component } from './../authentication/page404/page404.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  {
    path: "agenda",
    loadChildren: () =>
      import("./agenda/agenda.module").then((m) => m.AgendaModule),
  },
  {
    path: "campana",
    loadChildren: () =>
      import("./campana/campana.module").then((m) => m.CampanaModule),
  },
  {
    path: "dashboard",
    loadChildren: () =>
      import("./dashboard/dashboard.module").then((m) => m.DashboardModule),
  },
  {
    path: "equipo",
    loadChildren: () =>
      import("./equipo/equipo.module").then((m) => m.EquipoModule),
  },
  {
    path: "pacientes",
    loadChildren: () =>
      import("./pacientes/pacientes.module").then((m) => m.PacientesModule),
  },
  {
    path: "videollamada",
    loadChildren: () =>
      import("./videollamada/videollamada.module").then((m) => m.VideollamadaModule),
  },
  {
    path: "perfil",
    loadChildren: () =>
      import("./perfil/perfil.module").then((m) => m.PerfilModule),
  },
  { path: "**", component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DoctorRoutingModule {}
