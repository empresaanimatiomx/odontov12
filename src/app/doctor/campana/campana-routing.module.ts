import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CampanaComponent } from './campana.component';

const routes: Routes = [
  {
    path:'',
    component: CampanaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampanaRoutingModule { }
