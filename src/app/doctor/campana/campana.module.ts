import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CampanaRoutingModule } from './campana-routing.module';
import { CampanaComponent } from './campana.component';
import { MaterialModule } from '../../shared/material.module';
import { EmailEditorModule } from "angular-email-editor";

@NgModule({
  declarations: [CampanaComponent],
  imports: [
    CommonModule,
    CampanaRoutingModule,
    MaterialModule,
    EmailEditorModule,
  ],
})
export class CampanaModule {}
