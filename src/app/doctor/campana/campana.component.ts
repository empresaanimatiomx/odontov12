import { Component, ViewChild } from "@angular/core";
import { EmailEditorComponent } from "angular-email-editor";
import sample from "./sample.json";

@Component({
  selector: "app-campana",
  templateUrl: "./campana.component.html",
  styles: [],
})
export class CampanaComponent {
  title = "angular-email-editor";
  @ViewChild(EmailEditorComponent)
  private emailEditor: EmailEditorComponent;

  constructor() {}

  // called when the editor is created
  editorLoaded(e) {
    console.log("editorLoaded");
    this.emailEditor.editor.loadDesign(sample);
  }

  // called when the editor has finished loading
  editorReady(e) {
    console.log("editorReady");
  }

  exportHtml() {
    this.emailEditor.editor.exportHtml((data) =>
      console.log("exportHtml", data.html)
    );
  }
}
