import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IniciarVideollamadaRoutingModule } from './iniciar-videollamada-routing.module';
import { IniciarVideollamadaComponent } from './iniciar-videollamada.component';


@NgModule({
  declarations: [
    IniciarVideollamadaComponent
  ],
  imports: [
    CommonModule,
    IniciarVideollamadaRoutingModule
  ]
})
export class IniciarVideollamadaModule { }
