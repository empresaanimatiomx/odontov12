import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistorialVideollamadasRoutingModule } from './historial-videollamadas-routing.module';
import { HistorialVideollamadasComponent } from './historial-videollamadas.component';
import { MaterialModule } from '../../../shared/material.module';


@NgModule({
  declarations: [
    HistorialVideollamadasComponent
  ],
  imports: [
    CommonModule,
    HistorialVideollamadasRoutingModule,
    MaterialModule
  ]
})
export class HistorialVideollamadasModule { }
