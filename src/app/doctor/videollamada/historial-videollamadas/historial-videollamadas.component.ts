import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { AuthService } from "src/app/core/service/auth.service";
import moment from "moment";
moment.locale("es");
import { Router } from "@angular/router";
import { AlertService } from "src/app/core/service/alert.service";

const headers: any = new HttpHeaders({ "Content-Type": "application/json" });

@Component({
  selector: "app-historial-videollamadas",
  templateUrl: "./historial-videollamadas.component.html",
  styles: [],
})
export class HistorialVideollamadasComponent implements OnInit {
  baseURL = "https://projectsbyanimatiomx.com/phps/odonto/";
  columns = [
    { name: "Nombres" },
    { name: "Apellidos" },
    { name: "Celular" },
    { name: "Genero" },
    { name: "Doctor" },
  ];
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  rows = [];
  selectedOption: string;

  porConfirmar: any = [];

  horaCancelada: any = [];
  finalizada: any = [];
  porConfirmarFilter: any = [];
  horaCanceladaFilter: any = [];
  finalizadaFilter: any = [];

  idClinica;
  idCita;
  infocita;
  NuevaCitaForm: FormGroup;
  pacientes: any = [];
  doctores: any = [];
  servicios: any = [];
  motivosconsulta: any = [];
  historialcita: any = [];
  nombrepaciente = "";
  nombredoctor = "";
  horariocita = "";
  fechahoy = new Date();

  constructor(
    private sweet: AlertService,
    private http: HttpClient,
    private authService: AuthService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private router: Router
  ) {
    this.idClinica = this.authService.currentUserValue.idclinica;
    this.NuevaCitaForm = this.fb.group({
      paciente: [{ value: "", disabled: true }, [Validators.required]],
      doctor: [{ value: "", disabled: true }, [Validators.required]],
      fecha: [{ value: "", disabled: true }, [Validators.required]],
      horario: [{ value: "", disabled: true }, [Validators.required]],
      motivo: [{ value: "", disabled: true }, [Validators.required]],
      estatus: ["", [Validators.required]],
      observaciones: [{ value: "", disabled: true }],
      servicio: [{ value: "0", disabled: true }],
    });
  }

  ngOnInit(): void {
    this.solicitudes();
    this.GetPacientes(this.idClinica);
    this.GetDoctores(this.idClinica);
  }

  solicitudes(refresh?) {
    const options: any = { caso: 24, idC: this.idClinica };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta: any) => {
        console.log(respuesta);
        if (respuesta.toString() === "Vacio") {
          this.porConfirmar = [];
          this.horaCancelada = [];
          this.finalizada = [];
        } else {
          for (let i = 0; i < respuesta.length; i++) {
            const status = respuesta[i].EstatusCita;
            if (status === "Confirmado") {
              this.porConfirmarFilter.push(respuesta[i]);
            }
            if (status === "Hora Cancelanda") {
              this.horaCanceladaFilter.push(respuesta[i]);
            }
            if (status === "Finalizada") {
              this.finalizadaFilter.push(respuesta[i]);
            }
          }
          this.porConfirmar = this.porConfirmarFilter;
          this.horaCancelada = this.horaCanceladaFilter;
          this.finalizada = this.finalizadaFilter;
          refresh && this.sweet.toast("las videollamads");
        }
      });
  }

  GetPacientes(idclinica) {
    const options: any = { caso: 10, idC: idclinica };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.pacientes = respuesta;
      });
  }

  GetDoctores(idclinica) {
    const options: any = { caso: 5, idClinica: idclinica };
    this.http
      .post(`${this.baseURL}pacientes.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.doctores = respuesta;
      });
  }

  filterDatatable(event) {
    const val = event.target.value.toLowerCase();
    const keys = Object.keys(this.porConfirmarFilter[0]);
    this.porConfirmar = this.porConfirmarFilter.filter(function (item) {
      for (let i = 0; i < 50; i++) {
        if (
          item[keys[i]].toString().toLowerCase().indexOf(val) !== -1 ||
          !val
        ) {
          return true;
        }
      }
    });
  }

  modalDetalle(cita: any, content) {
    this.idCita = cita.idCitas;
    const options: any = { caso: 13, idc: this.idCita };
    this.http
      .post(`${this.baseURL}citas.php`, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.infocita = respuesta;
        let ngbModalOptions: NgbModalOptions = {
          backdrop: "static",
          keyboard: false,
          centered: true,
          ariaLabelledBy: "modal-basic-title",
          size: "lg",
        };
        this.nombredoctor =
          this.infocita[0].NombreUsuario + " " + this.infocita[0].Apellidos;
        this.nombrepaciente =
          this.infocita[0].Nombre + " " + this.infocita[0].Apellido;
        this.horariocita = this.infocita[0].Horario;
        this.modalService.open(content, ngbModalOptions);
        this.NuevaCitaForm.setValue({
          paciente: this.infocita[0].idPacientes,
          doctor: this.infocita[0].idDoctores,
          fecha: this.infocita[0].Fecha,
          horario: this.infocita[0].Horario,
          motivo: this.infocita[0].Motivos,
          estatus: this.infocita[0].EstatusCita,
          observaciones: this.infocita[0].ObservacionesCita,
          servicio: 0,
        });
      });
  }

  verificarfecha(fecha, hora, estatus) {
    var fechaf = fecha + " " + hora;
    var fechacompuesta = moment(fechaf).format();
    const fechainicial = moment(fechacompuesta)
      .subtract(15, "minutes")
      .format();
    const fechafinal = moment(fechacompuesta).subtract(-1, "hours").format();
    const fechaactusl = moment().format();
    let texto = "";
    if (estatus === "Confirmado") {
      if (fechaactusl <= fechafinal || fechainicial >= fechaactusl) {
        texto = "Iniciar videollamada";
      } else {
        texto = "No se realizó la videollamada";
      }
    } else {
      texto = estatus;
    }
    return texto;
  }

  iniciarvideollamada(idP, referencia, idC) {
    this.router.navigate([
      "/admin/videollamada/videollamada_usuario",
      idP,
      referencia,
      idC,
    ]);
  }
}
