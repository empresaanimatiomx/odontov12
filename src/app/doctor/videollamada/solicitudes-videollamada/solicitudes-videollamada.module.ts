import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SolicitudesVideollamadaRoutingModule } from './solicitudes-videollamada-routing.module';
import { SolicitudesVideollamadaComponent } from './solicitudes-videollamada.component';
import { MaterialModule } from '../../../shared/material.module';


@NgModule({
  declarations: [
    SolicitudesVideollamadaComponent
  ],
  imports: [
    CommonModule,
    SolicitudesVideollamadaRoutingModule,
    MaterialModule
  ]
})
export class SolicitudesVideollamadaModule { }
