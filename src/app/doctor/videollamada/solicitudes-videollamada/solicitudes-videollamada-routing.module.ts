import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SolicitudesVideollamadaComponent } from './solicitudes-videollamada.component';

const routes: Routes = [
  {
    path:'',
    component:SolicitudesVideollamadaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitudesVideollamadaRoutingModule { }
