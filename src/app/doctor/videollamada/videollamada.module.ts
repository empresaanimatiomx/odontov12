import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideollamadaRoutingModule } from './videollamada-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    VideollamadaRoutingModule
  ]
})
export class VideollamadaModule { }
