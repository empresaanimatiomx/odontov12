import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetallePacienteRoutingModule } from './detalle-paciente-routing.module';
import { DetallePacienteComponent } from './detalle-paciente.component';
import { MaterialModule } from '../../../shared/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DetallePacienteComponent
  ],
  imports: [
    CommonModule,
    DetallePacienteRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class DetallePacienteModule { }
