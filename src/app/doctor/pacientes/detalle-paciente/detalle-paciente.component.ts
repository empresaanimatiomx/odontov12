import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from "@angular/router";
import { HttpService } from "../../../core/service/http.service";

@Component({
  selector: "app-detalle-paciente",
  templateUrl: "./detalle-paciente.component.html",
  styles: [],
})
export class DetallePacienteComponent implements OnInit {
  patientForm: FormGroup;
  idPaciente;
  pacienteInfo;

  constructor(
    private http: HttpService,
    private fb: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.patientForm = this.fb.group({
      Nombre: [""],
      Apellido: [""],
      Rfc: [""],
      Notas_convenio: [""],
      Direccion: [""],
      Tel_Emergencia: [""],
      Correo: [""],
      FechaNac: [""],
      Edad: [""],
      GrupoSanguineo: [""],
      Genero: [""],
      EstadoCivil: [""],
      Ocupacion: [""],
      Encontraste: [""],
      Observaciones: [""],
      Nombre_Emergencia: [""],
      Parentesco_emergencia: [""],
      Responsable_Pago: [""],
    });
    this.route.params.subscribe({
      next: (res) => (this.idPaciente = res.idPaciente),
      error: (err) => console.log(err),
    });
  }

  ngOnInit(): void {
    this.getPaciente();
  }

  getPaciente() {
    this.http
      .post("pacientes.php", { caso: 18, idP: this.idPaciente })
      .subscribe({
        next: ([respuesta]: any) => {
          this.pacienteInfo = respuesta;
          this.patientForm.patchValue(this.pacienteInfo);
          this.patientForm.disable();
        },
        error: (res) => console.log(res),
      });
  }
}
