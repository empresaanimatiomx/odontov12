import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListadoPacientesRoutingModule } from './listado-pacientes-routing.module';
import { ListadoPacientesComponent } from './listado-pacientes.component';
import { MaterialModule } from '../../../shared/material.module';


@NgModule({
  declarations: [
    ListadoPacientesComponent
  ],
  imports: [
    CommonModule,
    ListadoPacientesRoutingModule,
    MaterialModule
  ]
})
export class ListadoPacientesModule { }
