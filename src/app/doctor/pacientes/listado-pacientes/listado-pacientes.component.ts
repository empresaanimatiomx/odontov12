import { Component, OnInit, ViewChild } from "@angular/core";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { AlertService } from "../../../core/service/alert.service";
import { HttpService } from "../../../core/service/http.service";

@Component({
  selector: "app-listado-pacientes",
  templateUrl: "./listado-pacientes.component.html",
  styles: [],
})
export class ListadoPacientesComponent implements OnInit {
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  pacientes = [];
  pacientesFilter = [];

  constructor(private http: HttpService, private sweet: AlertService) {}

  ngOnInit(): void {
    this.getPacientes();
  }

  getPacientes(refresh?): void {
    const { sucursal } = JSON.parse(
      localStorage.getItem("datosusuarioclinica")
    );
    this.http.post("pacientes.php", { caso: 17, sucursal }).subscribe({
      next: (respuesta: any) => {
        this.pacientes = respuesta;
        this.pacientesFilter = respuesta;
        refresh && this.sweet.toast("los pacientes");
      },
      error: (res) => console.log(res),
    });
  }

  filterDatatable(event: any) {
    if (event.target.value === "") {
      this.pacientes = this.pacientesFilter;
    }
    this.pacientes = this.http.filter(event, this.pacientesFilter);
    this.table.offset = 0;
  }

  exportToExcel() {
    this.http.export("Listado_pacientes", this.pacientesFilter);
  }
}
