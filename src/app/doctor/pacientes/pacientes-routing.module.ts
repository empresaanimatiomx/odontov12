import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "listado-pacientes",
    loadChildren: () =>
      import("./listado-pacientes/listado-pacientes.module").then(
        (m) => m.ListadoPacientesModule
      ),
  },
  {
    path: "detalle-paciente/:idPaciente",
    loadChildren: () =>
      import("./detalle-paciente/detalle-paciente.module").then(
        (m) => m.DetallePacienteModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacientesRoutingModule { }
