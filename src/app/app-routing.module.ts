import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Page404Component } from './authentication/page404/page404.component';
import { AuthGuard } from './core/guard/auth.guard';
import { Role } from './core/models/role';
import { AuthLayoutComponent } from './layout/app-layout/auth-layout/auth-layout.component';
import { MainLayoutComponent } from './layout/app-layout/main-layout/main-layout.component';
const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: '/authentication/signin', pathMatch: 'full' },
      {
        path: 'admin',
        canActivate: [AuthGuard],
        data: {
          role: Role.Admin,
        },
        loadChildren: () =>
          import('./admin/admin.module').then((m) => m.AdminModule),
      },
      {
        path: 'doctor',
        canActivate: [AuthGuard],
        data: {
          role: Role.Doctor,
        },
        loadChildren: () =>
          import('./doctor/doctor.module').then((m) => m.DoctorModule),
      },
      {
        path: 'asistente',
        canActivate: [AuthGuard],
        data: {
          role: Role.Asistente,
        },
        loadChildren: () =>
          import('./asistente/asistente.module').then((m) => m.AsistenteModule),
      },
      {
        path: 'paciente',
        canActivate: [AuthGuard],
        data: {
          role: Role.Paciente,
        },
        loadChildren: () =>
          import('./paciente/paciente.module').then((m) => m.PacienteModule),
      },
      {
        path: 'adminSucursal',
        canActivate: [AuthGuard],
        data: {
          role: Role.AdminSucursal,
        },
        loadChildren: () =>
          import('./adminSucursal/admin-sucursal.module').then((m) => m.AdminSucursalModule),
      },
    ],
  },
  {
    path: 'authentication',
    component: AuthLayoutComponent,
    loadChildren: () =>
      import('./authentication/authentication.module').then(
        (m) => m.AuthenticationModule
      ),
  },
  { path: '**', component: Page404Component },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
